package by.epam.webxmlparser.controller;

import java.util.HashMap;
import java.util.Map;

import by.epam.webxmlparser.logic.CommandName;
import by.epam.webxmlparser.logic.ICommand;
import by.epam.webxmlparser.logic.impl.DomCommand;
import by.epam.webxmlparser.logic.impl.NoSuchCommand;
import by.epam.webxmlparser.logic.impl.SaxCommand;
import by.epam.webxmlparser.logic.impl.StaxCommand;


public class CommandHelper {
	private static final CommandHelper instance = new CommandHelper();
	private Map<CommandName, ICommand> commands = new HashMap<>();

	public CommandHelper() {

		commands.put(CommandName.SAX, new SaxCommand());
		commands.put(CommandName.STAX, new StaxCommand());
		commands.put(CommandName.DOM, new DomCommand());
		commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
	}

	public static CommandHelper getInstance() {

		return instance;

	}

	public ICommand getCommand(String commandName) {

		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		ICommand command;

		if (null != name) {

			command = commands.get(name);

		} else {

			command = commands.get(CommandName.NO_SUCH_COMMAND);

		}

		return command;

	}
}
