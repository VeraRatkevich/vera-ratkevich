package by.epam.webxmlparser.controller;

public class JspPageName {
	private JspPageName() {
	}

	public static final String USER_PAGE = "/jsp/UserPage.jsp";
	public static final String ERROR_PAGE = "error.jsp";
}
