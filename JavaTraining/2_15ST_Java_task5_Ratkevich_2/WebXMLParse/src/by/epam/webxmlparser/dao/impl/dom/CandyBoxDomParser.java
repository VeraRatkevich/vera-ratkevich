package by.epam.webxmlparser.dao.impl.dom;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLDaoParser;
import by.epam.webxmlparser.entity.Candy;
import by.epam.webxmlparser.entity.CandyValue;

public class CandyBoxDomParser implements XMLDaoParser {
	private final static CandyBoxDomParser instance = new CandyBoxDomParser();

	public static XMLDaoParser getInstance() {

		return instance;

	}

	private Set<Candy> candyBox;
	private DocumentBuilder docBuilder;

	public CandyBoxDomParser(){

		this.candyBox = new HashSet<Candy>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.out.println("ParserConfigurationException");
		}
	}

	public Set<Candy> getCandyBox() {
		return candyBox;
	}

	public void buildCandyBox(String fileName) throws XMLDaoException {

		Document doc = null;
		try {
			File file = new File(fileName);
			doc = docBuilder.parse(file);
			Element root = doc.getDocumentElement();
			NodeList candyBoxList = root.getElementsByTagName("candy");
			for (int i = 0; i < candyBoxList.getLength(); i++) {
				Element candyElement = (Element) candyBoxList.item(i);
				Candy candy = buildCandy(candyElement);

				candyBox.add(candy);
			}

		} catch (IOException e) {
			throw new XMLDaoException("IOException");

		} catch (SAXException e) {
			throw new XMLDaoException("SAXException");
		}
	}

	private Candy buildCandy(Element candyElement) {
		Candy candy = new Candy();

		candy.setCode(candyElement.getAttribute("code"));

		candy.setProduction(candyElement.getAttribute("production"));

		candy.setName(getElementTextContent(candyElement, "name"));

		Integer energy = Integer.parseInt(getElementTextContent(candyElement,
				"energy"));
		candy.setEnergy(energy);

		Element typeElement = (Element) candyElement.getElementsByTagName(
				"type").item(0);
		candy.setCategory(getElementTextContent(typeElement, "category"));

		Boolean filling = Boolean.parseBoolean(getElementTextContent(
				typeElement, "filling"));
		candy.setFilling(filling);

		Map<String, Integer> ingredients = candy.getIngredients();

		NodeList ingredientsList = candyElement
				.getElementsByTagName("ingredient");

		for (int i = 0; i < ingredientsList.getLength(); i++) {
			Element ingredientElement = (Element) ingredientsList.item(i);
			Integer quantity = Integer.parseInt(ingredientElement
					.getAttribute("quantity"));
			ingredients.put(ingredientElement.getTextContent(), quantity);
		}

		candy.setIngredients(ingredients);

		CandyValue value = new CandyValue();
		Element valueElement = (Element) candyElement.getElementsByTagName(
				"value").item(0);

		Integer proteins = Integer.parseInt(getElementTextContent(valueElement,
				"proteins"));
		value.setProteins(proteins);

		Integer fats = Integer.parseInt(getElementTextContent(valueElement,
				"fats"));
		value.setFats(fats);

		Integer carbohydrates = Integer.parseInt(getElementTextContent(
				valueElement, "carbohydrates"));
		value.setCarbohydrates(carbohydrates);

		candy.setValue(value);

		return candy;
	}

	private static String getElementTextContent(Element element,
			String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}
}
