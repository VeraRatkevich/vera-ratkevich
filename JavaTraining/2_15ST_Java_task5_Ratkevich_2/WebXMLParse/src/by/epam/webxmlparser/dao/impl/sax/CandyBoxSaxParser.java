package by.epam.webxmlparser.dao.impl.sax;

import java.io.IOException;
import java.util.Set;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;




import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLDaoParser;
import by.epam.webxmlparser.entity.Candy;

public class CandyBoxSaxParser implements XMLDaoParser {

	private final static CandyBoxSaxParser instance = new CandyBoxSaxParser();

	public static XMLDaoParser getInstance() {

		return instance;

	}

	private Set<Candy> candyBox;
	private CandyBoxHandler sh;
	private XMLReader reader;

	public CandyBoxSaxParser() {

		sh = new CandyBoxHandler();

		try {

			reader = XMLReaderFactory.createXMLReader();

			reader.setContentHandler(sh);

		} catch (SAXException e) {
			//logger.error("Parser error: ", e);
		}
	}

	public Set<Candy> getCandyBox() {
		return candyBox;
	}

	public void buildCandyBox(String fileName) throws XMLDaoException {

		try {

			reader.parse(fileName);
		} catch (SAXException e) {
			throw new XMLDaoException("SAXException");

		} catch (IOException e) {
			throw new XMLDaoException("IOException");
		}
		candyBox = sh.getCandyBox();
	}
}
