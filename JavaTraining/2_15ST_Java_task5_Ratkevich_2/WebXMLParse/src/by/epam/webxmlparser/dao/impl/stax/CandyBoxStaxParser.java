package by.epam.webxmlparser.dao.impl.stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLDaoParser;
import by.epam.webxmlparser.dao.impl.CandyBoxEnum;
import by.epam.webxmlparser.entity.Candy;
import by.epam.webxmlparser.entity.CandyValue;

public class CandyBoxStaxParser implements XMLDaoParser {

	private final static CandyBoxStaxParser instance = new CandyBoxStaxParser();

	public static XMLDaoParser getInstance() {

		return instance;

	}

	private Set<Candy> candyBox = new HashSet<>();

	private XMLInputFactory inputFactory;

	public CandyBoxStaxParser() {
		this.candyBox = new HashSet<Candy>();
		inputFactory = XMLInputFactory.newInstance();
	}

	public Set<Candy> getCandyBox() {
	
		return candyBox;
	}

	public void buildCandyBox(String fileName) throws XMLDaoException {
		FileInputStream inputStream = null;
		XMLStreamReader reader = null;
		String name;
		try {
			inputStream = new FileInputStream(new File(fileName));
			reader = inputFactory.createXMLStreamReader(inputStream);
	
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					if (name == "candy") {
						Candy candy = buildCandy(reader);
					candyBox.add(candy);
					
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new XMLDaoException("SAXException");

		} catch (FileNotFoundException e) {
			throw new XMLDaoException("FileNotFoundException");
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				throw new XMLDaoException("IOException");
			}
		}
	}

	private Candy buildCandy(XMLStreamReader reader) throws XMLStreamException {

		Candy candy = new Candy();

		candy.setCode(reader.getAttributeValue(null,
				CandyBoxEnum.CODE.getValue()));

		candy.setProduction(reader.getAttributeValue(null,
				CandyBoxEnum.PRODUCTION.getValue()));

		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (CandyBoxEnum.valueOf(name.toUpperCase())) {

				case NAME:
					candy.setName(getXMLText(reader));
					break;
				case ENERGY:
					name = getXMLText(reader);
					candy.setEnergy(Integer.parseInt(name));
					break;
				case CATEGORY:
					candy.setCategory(getXMLText(reader));
					break;
				case FILLING:
					name = getXMLText(reader);
					candy.setFilling(Boolean.parseBoolean(name));
					break;

				case INGREDIENT:

					int quantity = Integer.parseInt(reader.getAttributeValue(
							null, CandyBoxEnum.QUANTITY.getValue()));
					//System.out.println(quantity);
					name = getXMLText(reader);
					//System.out.println(name);
					candy.setIngredient(name, quantity);
					break;
				case VALUE:
					candy.setValue(getXMLCandyValue(reader));
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (CandyBoxEnum.valueOf(name.toUpperCase()) == CandyBoxEnum.CANDY) {
					return candy;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag Student");
	}

	private CandyValue getXMLCandyValue(XMLStreamReader reader)
			throws XMLStreamException {
		CandyValue value = new CandyValue();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (CandyBoxEnum.valueOf(name.toUpperCase())) {
				case PROTEINS:
					name = getXMLText(reader);
					value.setProteins(Integer.parseInt(name));
					break;
				case FATS:
					name = getXMLText(reader);
					value.setFats(Integer.parseInt(name));
					break;
				case CARBOHYDRATES:
					name = getXMLText(reader);
					value.setCarbohydrates(Integer.parseInt(name));
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (CandyBoxEnum.valueOf(name.toUpperCase()) == CandyBoxEnum.VALUE) {
					return value;

				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag Address");
	}

	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}
		return text;
	}
}