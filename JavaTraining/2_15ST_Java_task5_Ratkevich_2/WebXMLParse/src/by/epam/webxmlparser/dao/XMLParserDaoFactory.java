package by.epam.webxmlparser.dao;

import by.epam.webxmlparser.dao.impl.dom.CandyBoxDomParser;
import by.epam.webxmlparser.dao.impl.sax.CandyBoxSaxParser;
import by.epam.webxmlparser.dao.impl.stax.CandyBoxStaxParser;

public class XMLParserDaoFactory {
	private final static XMLParserDaoFactory instance = new XMLParserDaoFactory();

	public static XMLParserDaoFactory getInstance() {

		return instance;

	}

	public XMLDaoParser getDAO(DAOType type) throws XMLDaoException {

		switch (type) {

		case SAX:
			return CandyBoxSaxParser.getInstance();
		case STAX:
			return CandyBoxStaxParser.getInstance();
		case DOM:
			return CandyBoxDomParser.getInstance();

		default:

			throw new XMLDaoException("No such DAO");

		}

	}

	public enum DAOType {

		SAX, STAX, DOM;

	}
}

