package by.epam.webxmlparser.dao.impl;

public enum CandyBoxEnum {
	CANDY_BOX("candy-box"), 
	CANDY("candy"), 
	CODE("code"),
	PRODUCTION("production"),
	NAME("name"), 
	ENERGY("energy"), 
	CATEGORY("category"),
	FILLING("filling"), 
	INGREDIENT("ingredient"), 
	PROTEINS("proteins"), 
	FATS("fats"), 
	CARBOHYDRATES("carbohydrates"),
	INGREDIENTS("ingredients"), 
	TYPE("type"), 
	VALUE("value"),
	QUANTITY("quantity");
	private String value;

	private CandyBoxEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
