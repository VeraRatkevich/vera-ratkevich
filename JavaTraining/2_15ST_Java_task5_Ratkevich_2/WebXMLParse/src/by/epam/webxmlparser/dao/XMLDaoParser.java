package by.epam.webxmlparser.dao;

import java.util.Set;

import by.epam.webxmlparser.entity.Candy;

public interface XMLDaoParser {
	void buildCandyBox(String resourceName) throws XMLDaoException;
	Set<Candy> getCandyBox();
}
