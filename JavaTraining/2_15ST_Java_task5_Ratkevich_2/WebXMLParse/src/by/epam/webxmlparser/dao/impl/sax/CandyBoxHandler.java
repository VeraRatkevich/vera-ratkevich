package by.epam.webxmlparser.dao.impl.sax;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.webxmlparser.dao.impl.CandyBoxEnum;
import by.epam.webxmlparser.entity.Candy;
import by.epam.webxmlparser.entity.CandyValue;

public class CandyBoxHandler extends DefaultHandler {
	private Set<Candy> candyBox;

	private Candy current = null;
	private CandyValue value = null;
	private CandyBoxEnum currentEnum = null;
	private Map<String, Integer> ingredients;
	private EnumSet<CandyBoxEnum> withText;
	private String currentIngredientQuantity;

	public CandyBoxHandler() {
		candyBox = new HashSet<Candy>();
		withText = EnumSet.range(CandyBoxEnum.NAME, CandyBoxEnum.CARBOHYDRATES);
	}

	public Set<Candy> getCandyBox() {
		return candyBox;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attrs) {
		if ("candy".equals(localName)) {
			current = new Candy();
			current.setCode(attrs.getValue(0));
			current.setProduction(attrs.getValue(1));
		} else if ("ingredients".equals(localName)) {
			ingredients = new HashMap<String, Integer>();
			current.setIngredients(ingredients);
		} else if ("ingredient".equals(localName)) {
			currentIngredientQuantity = attrs.getValue(0);
			CandyBoxEnum temp = CandyBoxEnum.valueOf(localName.toUpperCase()
					.replace('-', '_'));
			if (withText.contains(temp)) {
				currentEnum = temp;
			}
		} else if ("value".equals(localName)) {
			value = new CandyValue();
			current.setValue(value);
		} else {

			CandyBoxEnum temp = CandyBoxEnum.valueOf(localName.toUpperCase()
					.replace('-', '_'));
			if (withText.contains(temp)) {
				currentEnum = temp;
			}
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if ("candy".equals(localName)) {
			candyBox.add(current);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		String s = new String(ch, start, length).trim();
		if (currentEnum != null) {
			switch (currentEnum) {
			case NAME:
				current.setName(s);
				break;
			case ENERGY:
				current.setEnergy(Integer.parseInt(s));
				break;
			case CATEGORY:
				current.setCategory(s);
				break;
			case FILLING:
				if (s != null) {
					current.setFilling(Boolean.parseBoolean(s));
				}
				break;
			case PROTEINS:
				current.getValue().setProteins(Integer.parseInt(s));
				break;
			case FATS:
				current.getValue().setFats(Integer.parseInt(s));
				break;
			case CARBOHYDRATES:
				current.getValue().setCarbohydrates(Integer.parseInt(s));
				break;
			case INGREDIENT:
				current.setIngredient(s, new Integer(currentIngredientQuantity));
				break;
			default:
				throw new EnumConstantNotPresentException(
						currentEnum.getDeclaringClass(), currentEnum.name());
			}
		}
		currentEnum = null;
	}
}
