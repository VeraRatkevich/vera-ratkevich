package by.epam.webxmlparser.logic.impl;

import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import by.epam.webxmlparser.controller.JspPageName;
import by.epam.webxmlparser.controller.RequestParameterName;
import by.epam.webxmlparser.dao.XMLDaoException;
import by.epam.webxmlparser.dao.XMLDaoParser;
import by.epam.webxmlparser.dao.XMLParserDaoFactory;
import by.epam.webxmlparser.entity.Candy;
import by.epam.webxmlparser.logic.CommandException;
import by.epam.webxmlparser.logic.ICommand;

public class StaxCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;

		XMLDaoParser dao = null;
		ResourceBundle candyBundle = ResourceBundle
				.getBundle("by.epam.webxmlparser.resourcebundle.AppResources");
		String xmlFile = candyBundle.getString("xmlFile");
		try {

			dao = XMLParserDaoFactory.getInstance().getDAO(
					XMLParserDaoFactory.DAOType.STAX);

			dao.buildCandyBox(xmlFile);

			Set<Candy> candyBox = dao.getCandyBox();

			request.setAttribute(RequestParameterName.SIMPLE_INFO, candyBox);
			page = JspPageName.USER_PAGE;

		} catch (XMLDaoException e) {

			throw new CommandException("cann't get DAO", e);

		}

		return page;

	}

}
