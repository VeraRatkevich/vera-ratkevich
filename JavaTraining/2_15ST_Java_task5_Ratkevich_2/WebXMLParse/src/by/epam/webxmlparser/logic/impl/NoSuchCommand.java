package by.epam.webxmlparser.logic.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.webxmlparser.controller.JspPageName;
import by.epam.webxmlparser.logic.CommandException;
import by.epam.webxmlparser.logic.ICommand;

public class NoSuchCommand  implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		// stub

		return JspPageName.ERROR_PAGE;

	}

}
