package by.epam.webxmlparser.resourcebundle;

import java.util.ListResourceBundle;

public class AppRasources extends ListResourceBundle {

	@Override
	protected Object[][] getContents() {
		return new Object[][]{
			{"xmlFile","candy-box.xml"}
		};
	}

}
