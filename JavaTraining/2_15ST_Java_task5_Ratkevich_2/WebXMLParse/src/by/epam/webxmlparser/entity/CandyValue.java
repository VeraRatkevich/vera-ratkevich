package by.epam.webxmlparser.entity;

public class CandyValue {
	private int proteins;
	private int fats;
	private int carbohydrates;

	public CandyValue() {

	}

	public int getProteins() {
		return proteins;
	}

	public void setProteins(int proteins) {

		if (proteins > 0) {
			this.proteins = proteins;
		}

	}

	public int getFats() {
		return fats;
	}

	public void setFats(int fats) {

		if (fats > 0) {
			this.fats = fats;
		}
	}

	public int getCarbohydrates() {
		return carbohydrates;
	}

	public void setCarbohydrates(int carbohydrates) {
		if (carbohydrates > 0) {
			this.carbohydrates = carbohydrates;
		}
	}

	@Override
	public String toString() {
		return "Proteins: " + proteins + "\n" + "Fats: " + fats + "\n"
				+ "Carbohydrates: " + carbohydrates;
	}
}
