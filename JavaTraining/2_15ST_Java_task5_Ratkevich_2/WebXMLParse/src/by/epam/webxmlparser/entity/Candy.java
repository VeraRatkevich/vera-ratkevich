package by.epam.webxmlparser.entity;

import java.util.HashMap;
import java.util.Map;

public class Candy {
	private String code;
	private String production;
	private String name;
	private int energy;
	private String category;
	private boolean filling;
	private Map<String, Integer> ingredients;

	private CandyValue value;

	public Candy() {
		ingredients = new HashMap<String, Integer>();
	}

	public Candy(String code, String production, String name, int energy,
			String category, boolean filling, int proteins, int fats,
			int carbohydrates) {

		setCode(code);
		setProduction(production);
		setName(name);
		setEnergy(energy);
		setCategory(category);
		setFilling(filling);
		ingredients = new HashMap<String, Integer>();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setProduction(String production) {
		this.production = production;
	}

	public String getProduction() {
		return production;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {

		if (energy > 0) {
			this.energy = energy;
		}
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public boolean getFilling() {
		return filling;
	}

	public void setFilling(boolean filling) {
		this.filling = filling;
	}

	public Map<String, Integer> getIngredients() {
		return ingredients;
	}

	public int getIngredientQuantity(String name) {
		return ingredients.get(name);
	}

	public void setIngredients(Map<String, Integer> ingredients) {
		this.ingredients = ingredients;
	}

	public void setIngredient(String name, int quantity) {
		Integer quantityIngr = quantity;
		ingredients.put(name, quantityIngr);
	}

	public CandyValue getValue() {
		return value;
	}

	public void setValue(CandyValue value) {
		this.value = value;

	}

	@Override
	public String toString() {
		return "Candy: " + "\n" + "code " + code + "\n" + "production "
				+ production + "\n" + "name " + name + "\n" + "energy "
				+ energy + "\n" + "category " + category + "\n" + "filling "
				+ filling + "\n" + "ingredients" + ingredients + "\n"
				+ "Candy value: " + value.toString();
	}
}
