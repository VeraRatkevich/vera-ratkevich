<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Index Page</title>
</head>
<body>
<center>
	<h1>Select parser</h1>
		<form action="controller" method="post">
			<input type="hidden" name="parser" value="SAX" /> <br /> <input
				type="submit" value="SAX" />
		</form>
		<form action="controller" method="post">
			<input type="hidden" name="parser" value="STAX" /> <br /> <input
				type="submit" value="STAX" />
		</form>
		<form action="controller" method="post">
			<input type="hidden" name="parser" value="DOM" /> <br /> <input
				type="submit" value="DOM" />
		</form>
	</center>
</body>
</html>
