package by.epam.busstation.spaceforpassengers;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
 * Класс описывает набор сущностей "Пространство(место) для пассажиров", которые могут находиться на остановке(автобусе).
 * 
 * Поля класса:
 * passengerList - коллекция пассажиров, находящихся на остановке(автобусе)
 * size - размер пространства для пассажиров, максимальное количество пассажиров в автобусе.На остановке может быть неограниченное количество пассажиров.
 * lock 
 *  
 *  Методы класса:
 *  takeInPassenger(Passenger passenger) - добавляет к коллекции пассажиров пассажира, переданного в качестве параметра
 *  takeInPassenger(ArrayList<Passenger> passengers) - добавляет к коллекции пассажиров коллекцию пассажиров, переданных в качестве параметра
 *  letOutPassenger(int amount) - удаляет из коллекции пассажиров некоторое количество пассажиров, переданное в качестве параметра
 *  getSize()
 *  getAmountOfPasengers()
 *  getFreeSeats()
 *  getLock()
 */

public class SpaceForPassengers {

	private ArrayList<Passenger> passengerList; 
	private int size;
	private Lock lock;

	public SpaceForPassengers() {
		passengerList = new ArrayList<Passenger>();
		lock = new ReentrantLock();
	}

	public SpaceForPassengers(int size) {
		passengerList = new ArrayList<Passenger>(size);
		lock = new ReentrantLock();
		this.size = size;
	}

	public boolean takeInPassenger(Passenger passenger) {
		return passengerList.add(passenger);

	}

	public boolean takeInPassenger(ArrayList<Passenger> passengers) {
		return passengerList.addAll(passengers);
	}

	public ArrayList<Passenger> letOutPassenger(int amount) {
		if (passengerList.size() >= amount) {
			ArrayList<Passenger> movedPassengers = new ArrayList<Passenger>(
					passengerList.subList(0, amount));
			passengerList.removeAll(movedPassengers);
			return movedPassengers;
		} else {			
			return null;
		}

	}

	public int getSize() {
		return size;
	}

	public int getAmountOfPasengers() {
		return passengerList.size();
	}

	public int getFreeSeats() {
		return size - passengerList.size();

	}

	public Lock getLock() {
		return lock;
	}
}
