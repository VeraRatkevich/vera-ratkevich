package by.epam.busstation.main;

import java.util.ArrayList;

import by.epam.busstation.bus.Bus;
import by.epam.busstation.rote.Rote;
import by.epam.busstation.spaceforpassengers.Passenger;
import by.epam.busstation.station.Station;


public class Main {
	public static void main(String[] args) throws InterruptedException {
		ArrayList<Passenger> passengerList1 = new ArrayList<Passenger>();
		for (int j = 0; j < 60; j++) {
			passengerList1.add(new Passenger(j));
		}

		Station stop1 = new Station("Зеленая");
		stop1.setPassengersToSpaseInStop(passengerList1);

		ArrayList<Passenger> passengerList2 = new ArrayList<Passenger>();
		for (int j = 0; j < 50; j++) {
			passengerList2.add(new Passenger(j));
		}

		Station stop2 = new Station("Белая");
		stop2.setPassengersToSpaseInStop(passengerList2);

		ArrayList<Passenger> passengerList3 = new ArrayList<Passenger>();
		for (int j = 0; j < 10; j++) {
			passengerList3.add(new Passenger(j));
		}

		Station stop3 = new Station("Красная");
		stop3.setPassengersToSpaseInStop(passengerList3);

		ArrayList<Passenger> passengerList4 = new ArrayList<Passenger>();
		for (int j = 0; j < 40; j++) {
			passengerList4.add(new Passenger(j));
		}

		Station stop4 = new Station("Изумрудная");
		stop4.setPassengersToSpaseInStop(passengerList4);

		Rote rote1 = new Rote(stop1, stop2, stop3);
		Bus bus1 = new Bus("1", 40);
		bus1.setRote(rote1);

		Rote rote2 = new Rote(stop1, stop3, stop4, stop2);
		Bus bus2 = new Bus("2", 40);
		bus2.setRote(rote2);
		
		Rote rote3 = new Rote(stop1, stop3, stop1, stop2);
		Bus bus3 = new Bus("3", 40);
		bus3.setRote(rote3);

		Rote rote4 = new Rote(stop1, stop3, stop4);
		Bus bus4 = new Bus("4", 40);
		bus4.setRote(rote4);

		Rote rote5 = new Rote(stop1, stop4, stop1);
		Bus bus5 = new Bus("5", 40);
		bus5.setRote(rote5);


		bus1.start();
		bus2.start();
		bus3.start();
		bus4.start();
		bus5.start();


		Thread.sleep(3000);

		bus1.stopThread();
		bus2.stopThread();

	}

}
