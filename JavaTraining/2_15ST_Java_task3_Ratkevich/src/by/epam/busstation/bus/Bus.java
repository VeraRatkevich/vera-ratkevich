package by.epam.busstation.bus;

import org.apache.log4j.Logger;

import by.epam.busstation.rote.Rote;
import by.epam.busstation.rote.RoteException;
import by.epam.busstation.spaceforpassengers.SpaceForPassengers;
import by.epam.busstation.station.Station;
import by.epam.busstation.util.Counter;

/*
 * Класс описывает набор сущностей-потоков "Автобус"
 * 
 * Поля класса:
 * stopThread - флаг, который принимает значение true, если поток должен быть остановлен
 * number - номер автобуса
 * spaceForPassengers - пространство(место) для пассажиров в автобусе
 * rote - маршрут, по которому будет двигаться автобус
 * station - остановка, на которой в текущий момент времени находится автобус
 * 
 * Методы класса(кроме get- и set-ов):
 * stopThread() - задает флагу stopThread значение true
 * run() - точка запуска потока
 * onRoad() - автобус находится в дороге (поток спит)
 * atBusStop() - автобус прибывет на остановку (вызывает метод getNextStop() переменной rote, 
 * и присваивает возвращаемую остановку переменной station.). Автобус достиг конца маршрута, то station = null.
 * Далее автобус занимает место на остановке и производится посадка/высадка пассажиров. После этого автобус покидает остановку.
 * Если места нет, то автобус ожидает.
 * letOutPassengers() - высадка пассажиров. Вызывает метод add класса Station и 
 * передает ему объект пространства для пассажиров автобуса и количество пассажиров, желающих выйти на отсановке.
 * takeInPassengers() - посадка пассажиров. Вызывает метод get класса Station и передает ему объект пространства для пассажиров автобуса.
 */

public class Bus extends Thread {
	private final static Logger logger = Logger.getRootLogger();

	private volatile boolean stopThread = false;

	private String number;
	private SpaceForPassengers spaceForPassengers;
	private Rote rote;
	private Station station;

	public Bus(String number, int SpaceForPassengersSize) {
		this.number = number;
		spaceForPassengers = new SpaceForPassengers(SpaceForPassengersSize);
	}

	public void setRote(Rote rote) {
		this.rote = rote;
	}

	public Rote getRote() {
		return rote;
	}

	public Station getStop() {
		return station;
	}

	public String getNumber() {
		return number;
	}

	public SpaceForPassengers getSpaceForPassengers() {
		return spaceForPassengers;
	}

	public void stopThread() {
		stopThread = true;
	}

	public void run() {
		try {
			while (!stopThread) {
				onRoad();
				atBusStop();
			}
		} catch (RoteException e) {
			logger.error("Автобус сбился с маршрута", e);
		} catch (InterruptedException e) {
			logger.error(
					"Неудачная попытка автобуса посадки/высадки пассажиров", e);

		}
	}

	private void onRoad() throws InterruptedException {
		Thread.sleep(1000);
	}

	private void atBusStop() throws RoteException, InterruptedException {

		boolean isLockedStop = false;
		try {
			station = rote.getNextStop();
			if (null != station) {
				logger.debug("Автобус " + number + " прибыл на остановку  "
						+ station.getName());
				isLockedStop = station.lockPlaceForBus(this);
				if (isLockedStop) {

					letOutPassengers();
					takeInPassengers();

				} else {
					logger.debug("Автобусу " + number
							+ " нет места на остановке " + station.getName());
				}
			} else {
				logger.debug("Автобус " + number + " закончил маршрут.");
				stopThread = true;
			}
		} finally {
			if (isLockedStop) {
				station.unlockPlaceForBus(this);
				logger.debug("Автобус " + number + " покинул остановку "
						+ station.getName());
			}
		}

	}

	private void letOutPassengers() throws InterruptedException {
		boolean result = false;
		int numberPassengersInBus = spaceForPassengers.getAmountOfPasengers();
		int numberPassengersToMove = Counter.passengersCount(numberPassengersInBus);

		if (numberPassengersToMove != 0) {
			logger.debug("Из автобуса " + number + " выходят "
					+ numberPassengersToMove + " пассажиров.");

			result = station.add(spaceForPassengers, numberPassengersToMove);

			if (!result) {
				logger.debug("Из автобуса " + number
						+ " не смогли выйти на остановке " + station.getName()
						+ " " + numberPassengersToMove + " пассажиров.");
			} else {
				logger.debug("Из автобуса " + number + " вышло "
						+ numberPassengersToMove + " пассажиров.");
			}

		} else {
			logger.debug("Из автобуса " + number
					+ " не вышло ни одного пассажира.");
		}
	}

	private void takeInPassengers() throws InterruptedException {

		int numberPassengersMovedReally = station.get(spaceForPassengers);
		if (numberPassengersMovedReally > 0) {
			logger.debug("В автобус " + number + " зашло "
					+ numberPassengersMovedReally + " пассажиров.");

		} else {
			logger.debug("В автобус " + number + " не зашел ни один пассажир.");
		}
	}

}
