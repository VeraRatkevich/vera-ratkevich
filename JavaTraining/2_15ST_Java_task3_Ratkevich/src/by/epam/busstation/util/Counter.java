package by.epam.busstation.util;

import java.util.Random;

public class Counter {
	//генерация количества пассажиров для высадки на остановке(посадки в автобус). Параметр задает границы случайной генерации количества пассажиров.
	public static int passengersCount(int bound) {
		if (bound != 0) {
			Random random = new Random();
			return random.nextInt(bound);
		} else
			return 0;
	}
}
