package by.epam.busstation.station;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import org.apache.log4j.Logger;

import by.epam.busstation.bus.Bus;
import by.epam.busstation.spaceforpassengers.Passenger;
import by.epam.busstation.spaceforpassengers.SpaceForPassengers;
import by.epam.busstation.util.Counter;

/*
 * Класс описывает набор сущностей "Остановка"
 * 
 * Переменные класса: 
 * BUS_COUNT - количество автобусов, которые одновременно могут стоять на остановке
 * name - название остановки
 * busList - коллекция автобусов(блокирующая очередь), одновременно стоящих на остановке. Если коллекция заполнена (3 автобуса), 
 * то вновь прибывшие автобусы ждут, когда освободится место на остановке.
 * stopSpace - пространство (место) для пассажиров на остановке. Его размер не ограничен
 * 
 * Методы класса(кроме get- и set-ов):
 * lockPlaceForBus(Bus bus) - помещает автобус в коллекцию автобусов на остановке (блокирующую очередь).
 * Если в коллекции нет места, то автобус ожидает.
 * unlockPlaceForBus(Bus bus) - удаляет автобус из коллекции автобусов на остановке(блокирующей очереди).
 * add(SpaceForPassengers busSpace, int numberOfpassengers) - блокирует пространство(место) для пассажиров на остановке и вызывает метод doMoveFromBus
 * doMoveFromBus(SpaceForPassengers busSpace,int numberOfpassengers) - блокирует пространство(место) для пассажиров а автобусе
 * и производит перемещение пассажиров из автобуса на остановку
 * get(SpaceForPassengers busSpace) - блокирует пространство(место) для пассажиров на остановке, 
 * генерирует число пассажиров для посадки в автобус и вызывает метод doMoveFromStop
 * doMoveFromStop(SpaceForPassengers busSpace,int numberOfpassengers) -блокирует пространство(место) для пассажиров а автобусе
 * и производит перемещение пассажиров из атобуса на остановку
 */

public class Station {
	private final static Logger logger = Logger.getRootLogger();
	public static int BUS_COUNT = 3;

	private String name;

	private BlockingQueue<Bus> busList;
	private SpaceForPassengers stopSpace;

	public Station(String name) {
		this.name = name;
		stopSpace = new SpaceForPassengers();
		busList = new ArrayBlockingQueue<Bus>(BUS_COUNT);

		logger.debug("Остановка " + name + " создана.");

	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SpaceForPassengers getStopSpace() {
		return stopSpace;
	}

	public void setPassengersToSpaseInStop(ArrayList<Passenger> passengerList) {
		stopSpace.takeInPassenger(passengerList);
	}

	public boolean lockPlaceForBus(Bus bus) {
		boolean result = false;
		try {
			busList.put(bus);
			if (busList.contains(bus)) {
				logger.debug("Автобус " + bus.getNumber()
						+ " занял место на остановке " + name);
				result = true;
			}
		} catch (InterruptedException e) {
			logger.debug("Для автобуса " + bus.getNumber()
					+ " нет места на остановке.");
			result = false;
		}
		return result;
	}

	public boolean unlockPlaceForBus(Bus bus) {
		boolean result = false;
		result = busList.remove(bus);
		return result;
	}

	public boolean add(SpaceForPassengers busSpace, int numberOfpassengers)
			throws InterruptedException {
		boolean result = false;
		Lock stopSpaceLock = stopSpace.getLock();
		try{
			stopSpaceLock.lock();
			result = doMoveFromBus(busSpace, numberOfpassengers);
		}
		finally{
			stopSpaceLock.unlock();
		}
		return result;
	}

	private boolean doMoveFromBus(SpaceForPassengers busSpace,
			int numberOfpassengers) throws InterruptedException {
		Lock busSpaceForPassengersLock = busSpace.getLock();
		boolean busLock = false;
		try {
			busLock = busSpaceForPassengersLock.tryLock(30, TimeUnit.SECONDS);
			if (busLock) {

				ArrayList<Passenger> passengers = busSpace
						.letOutPassenger(numberOfpassengers);
				stopSpace.takeInPassenger(passengers);
				return true;

			}
		} finally {
			if (busLock) {
				busSpaceForPassengersLock.unlock();
			}
		}

		return false;
	}


	public int get(SpaceForPassengers busSpace) throws InterruptedException {
		int numberMovedPassengers = 0;

		Lock stopSpaceLock = stopSpace.getLock();
		try{
			stopSpaceLock.lock();
			int numberPassengersOnStop = stopSpace.getAmountOfPasengers();
			int numberPassengersToMove = Counter.passengersCount(numberPassengersOnStop);
			if (numberPassengersToMove != 0) {
				logger.debug("В автобус пытаются зайти "
						+ numberPassengersToMove + " пассажиров.");
				numberMovedPassengers = doMoveFromStop(busSpace,
						numberPassengersToMove);
			}
		}
		finally{
			stopSpaceLock.unlock();
		}

		return numberMovedPassengers;
	}

	private int doMoveFromStop(SpaceForPassengers busSpace,
			int numberOfpassengers) throws InterruptedException {
		Lock busSpaceForPassengerLock = busSpace.getLock();
		boolean busLock = false;
		int numberPassengersNotFitBus = numberOfpassengers;
		int numberOfMovedPassengersReally = 0;
		try {
			busLock = busSpaceForPassengerLock.tryLock(30, TimeUnit.SECONDS);
			if (busLock) {
				int numberFreeSeatsInBus = busSpace.getFreeSeats();
				if (numberOfpassengers <= numberFreeSeatsInBus) {
					numberOfMovedPassengersReally = numberOfpassengers;
					numberPassengersNotFitBus = 0;
				} else {
					numberOfMovedPassengersReally = numberFreeSeatsInBus;
					numberPassengersNotFitBus = numberOfpassengers
							- numberFreeSeatsInBus;
				}

				ArrayList<Passenger> movedPassengers = stopSpace
						.letOutPassenger(numberOfMovedPassengersReally);
				if (null != movedPassengers) {
					busSpace.takeInPassenger(movedPassengers);

				} else {
					logger.debug("Пустая коллекция пассажиров.");
				}

				if (numberPassengersNotFitBus != 0) {
					logger.debug("Недостаточно места в автобусе " + " для "
							+ numberPassengersNotFitBus + " пассажиров.");
				}

			}

		} finally {
			if (busLock) {
				busSpaceForPassengerLock.unlock();
			}
		}
		return numberOfMovedPassengersReally;

	}


}
