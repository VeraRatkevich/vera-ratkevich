package by.epam.busstation.rote;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

import org.apache.log4j.Logger;

import by.epam.busstation.station.Station;

/*Класс описывает набор сущностей "Маршрут", по которому движется автобус.
 * 
 * В классе определены следующие поля:
 * roteNumber - номер маршрута, генерируется в конструкторе
 * busRote - набор остановок(коллекция), из которых состоит маршрут
 * stopIterator - итератор для последовательного перемещения по коллекции и получения доступа к элементам
 * 
 * В классе определено два конструктора: один создает маршрут на основе переданной ему коллекции, 
 * а второй - переменного числа параметров типа Station
 * 
 * В классе имеется метод getNextStop(), который возвращает ссылку на объект класса Station, 
 * следующую в наборе остановок(коллекции) за той, которая была возвращена ранее.
 * Если коллекция остановок пуста, то генерируется исключение RoteException, которое информирует о том, что маршрут не задан.
 */
public class Rote {
	private final static Logger logger = Logger.getRootLogger();

	public final int roteNumber;

	private ArrayList<Station> busRote;
	private ListIterator<Station> stopIterator;

	public Rote(ArrayList<Station> busRote) {
		this.busRote = busRote;
		stopIterator = busRote.listIterator();
		roteNumber = 8 + new Random().nextInt(10) + busRote.hashCode()
				/ 1000000;
		logger.debug("Маршрут № " + roteNumber + " создан");
	}

	public Rote(Station... argsStop) {
		busRote = new ArrayList<Station>(argsStop.length);
		for (Station stop : argsStop) {
			busRote.add(stop);
		}
		stopIterator = busRote.listIterator();
		roteNumber = 8 + new Random().nextInt(10) + busRote.hashCode()
				/ 1000000;
		logger.debug("Маршрут № " + roteNumber + " создан");
	}

	public Station getNextStop() throws RoteException {
		if (!busRote.isEmpty()) {
			if (stopIterator.hasNext()) {
				return stopIterator.next();
			}
			return null;
		} else {
			throw new RoteException("Rote is not set!");
		}

	}

}
