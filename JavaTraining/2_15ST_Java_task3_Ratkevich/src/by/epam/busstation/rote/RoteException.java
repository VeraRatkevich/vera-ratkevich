package by.epam.busstation.rote;

public class RoteException extends Exception {

	private static final long serialVersionUID = 1L;

	public RoteException() {
	}

	public RoteException(String message, Throwable exception) {
		super(message, exception);
	}

	public RoteException(String message) {
		super(message);
	}

	public RoteException(Throwable exception) {
		super(exception);
	}
}
