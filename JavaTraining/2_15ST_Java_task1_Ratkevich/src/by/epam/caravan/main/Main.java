package by.epam.caravan.main;

import by.epam.caravan.entity.coffee.ClassOfCoffeeEnum;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.coffee.GradeOfCoffeeEnum;
import by.epam.caravan.entity.coffee.instantcoffee.InstantCoffee;
import by.epam.caravan.entity.coffee.instantcoffee.TypeOfInstantCoffeeEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.DegreeOfRoastEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.GrindingEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.GroundCoffee;
import by.epam.caravan.entity.coffee.naturalcoffee.RoastedCoffee;
import by.epam.caravan.entity.vancoffee.Van;
import by.epam.caravan.logic.Manager;
import by.epam.caravan.logic.coffeelogic.sort.CriterionEnum;

public class Main {
	public static void main (String[] args){
		
		Manager manager = new Manager();
		
		Coffee coffee1 = manager.createCoffee(ClassOfCoffeeEnum.ARABICA, GradeOfCoffeeEnum.HIGEST, "Ethiopia", 200, 300);
		
		InstantCoffee instantCoffe1 = manager.createInstantCoffee(ClassOfCoffeeEnum.ROBUSTA, GradeOfCoffeeEnum.SECOND, "USA", 500, 100, 10, TypeOfInstantCoffeeEnum.SUBLIMATED);
		InstantCoffee instantCoffe2 = manager.createInstantCoffee(ClassOfCoffeeEnum.LIBERICA, GradeOfCoffeeEnum.FIRST, "Canada", 200, 80, 10, TypeOfInstantCoffeeEnum.GRANULAR);
		RoastedCoffee roastedCoffee1 = manager.createRoastedCoffee(ClassOfCoffeeEnum.ARABICA, GradeOfCoffeeEnum.HIGEST, "Ethiopia", 250, 450, DegreeOfRoastEnum.DARK);
		GroundCoffee groundCoffee1 = manager.createGroundCoffee(ClassOfCoffeeEnum.ARABICA, GradeOfCoffeeEnum.FIRST, "Italy", 400, 100, DegreeOfRoastEnum.MEDIUM, GrindingEnum.AVERAGE);
		
		Van van1 = manager.createVan(5,"��1526");
		
		
		
		manager.loadVanCoffee(van1, coffee1,instantCoffe1,instantCoffe2, roastedCoffee1);
		System.out.println(manager.report(van1));

		manager.sortCoffeInVan(van1, CriterionEnum.RATIO);
		System.out.println(manager.report(van1));
		
		manager.sortCoffeInVan(van1, CriterionEnum.WEIGHT);
		System.out.println(manager.report(van1));
		
		manager.loadVanCoffee(van1, groundCoffee1);
				
		
		manager.unloadVanCoffee(van1, groundCoffee1);;
		
		manager.unloadVanCoffee(van1, groundCoffee1);
		
		System.out.println(manager.report(van1));
		
		manager.unloadVanCoffeeFully(van1);
		

		System.out.println(manager.report(van1));
		
		


	}

}
