package by.epam.caravan.util;

import java.util.ArrayList;

import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.vancoffee.Van;

/**
 * Helper class
 * 
 * Helper class. Class contains methods that make support calculations for the
 * application logic.
 * 
 * @author v.ratkevich
 * 
 */

public class Calculation {

	private Calculation() {

	}

	/**
	 * Calculates the price of the goods in van.
	 * 
	 * Calculates the price of the goods in van. Extracts the collection of
	 * {@link Coffee} objects from the object of class {@link Van}. Summarizes
	 * the variable price of all objects in the collection.
	 * 
	 * @param van
	 *            van, goods price which should be calculated
	 * @return price of all goods in van
	 */
	public final static double goodsPrice(Van van) {
		ArrayList<Coffee> coffeeGoods = van.getCoffeeGoods();
		double price = 0;
		for (Coffee coffee : coffeeGoods) {
			price += (coffee.getPrice() * coffee.getWeightInKilograms());

		}
		return price;
	}

	/**
	 * Calculates the weight of the goods in van.
	 * 
	 * Calculates the weight of the goods in van. Extracts the collection of
	 * {@link Coffee} objects from the object of class {@link Van}. Summarizes
	 * the variable weight of all objects in the collection.
	 * 
	 * @param van
	 *            van, cargo weight which should be calculated
	 * @return weight in tones of goods in van
	 */
	public final static double goodsWeightInTonnes(Van van) {
		ArrayList<Coffee> goodsVan = van.getCoffeeGoods();
		double weight = 0;
		for (Coffee coffee : goodsVan) {
			weight += coffee.getWeightInKilograms();

		}
		return weight / 1000;

	}

	/**
	 * Calculates space in the van.
	 * 
	 * Calculates space in the van. Takes away from the general tonnage of a van
	 * of weights of the loaded goods.
	 * 
	 * @param van
	 *            van, space which should be calculated
	 * @return space in the van in tones
	 */
	public final static double spaceVan(Van van) {
		double space = 0;
		double goodsWeight = goodsWeightInTonnes(van);
		space = van.TONNAGE - goodsWeight;
		return space;
	}

}
