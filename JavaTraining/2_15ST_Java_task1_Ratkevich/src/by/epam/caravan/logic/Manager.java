package by.epam.caravan.logic;

import by.epam.caravan.entity.coffee.ClassOfCoffeeEnum;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.coffee.GradeOfCoffeeEnum;
import by.epam.caravan.entity.coffee.instantcoffee.InstantCoffee;
import by.epam.caravan.entity.coffee.instantcoffee.TypeOfInstantCoffeeEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.DegreeOfRoastEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.GrindingEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.GroundCoffee;
import by.epam.caravan.entity.coffee.naturalcoffee.RoastedCoffee;
import by.epam.caravan.entity.vancoffee.Van;
import by.epam.caravan.logic.coffeelogic.create.CoffeeCreator;
import by.epam.caravan.logic.coffeelogic.sort.CriterionEnum;
import by.epam.caravan.logic.coffeelogic.sort.SortingCoffee;
import by.epam.caravan.logic.vanlogic.VanLoadingUnloading;
import by.epam.caravan.logic.vanlogic.create.VanCreator;

/**
 * Interacting with the logic of the program
 * 
 * Class is an interface for interacting with the logic of the program. The
 * methods of this class call appropriate methods of program logic.
 * 
 * @author v.ratkevich
 */
public class Manager {

	public Manager() {

	}

	public Coffee createCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price) {
		Coffee coffee = CoffeeCreator.createCoffee(classOfCoffee,
				gradeOfCoffee, country, weightInKilograms, price);
		return coffee;
	}

	public InstantCoffee createInstantCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price, int percentageOfCaffeine,
			TypeOfInstantCoffeeEnum typeOfInstantCoffee) {
		InstantCoffee coffee = CoffeeCreator.createCoffee(classOfCoffee,
				gradeOfCoffee, country, weightInKilograms, price,
				percentageOfCaffeine, typeOfInstantCoffee);
		return coffee;
	}

	public RoastedCoffee createRoastedCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price,
			DegreeOfRoastEnum degreeOfRoast) {
		RoastedCoffee coffee = CoffeeCreator
				.createCoffee(classOfCoffee, gradeOfCoffee, country,
						weightInKilograms, price, degreeOfRoast);
		return coffee;
	}

	public GroundCoffee createGroundCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price,
			DegreeOfRoastEnum degreeOfRoast, GrindingEnum grinding) {
		GroundCoffee coffee = CoffeeCreator.createCoffee(classOfCoffee,
				gradeOfCoffee, country, weightInKilograms, price,
				degreeOfRoast, grinding);
		return coffee;
	}

	public Van createVan() {
		return VanCreator.createVan();
	}

	public Van createVan(int tonnage, String licensePlate) {
		return VanCreator.createVan(tonnage, licensePlate);
	}

	public void loadVanCoffee(Van van, Coffee coffee) {
		VanLoadingUnloading.loadVan(van, coffee);
	}

	public void loadVanCoffee(Van van, Coffee... coffeeGoods) {
		VanLoadingUnloading.loadVan(van, coffeeGoods);
	}

	public void unloadVanCoffee(Van van, Coffee... coffeeGoods) {
		VanLoadingUnloading.unloadVan(van, coffeeGoods);
	}

	public void unloadVanCoffeeFully(Van van) {
		VanLoadingUnloading.unloadVanFully(van);
	}

	public void sortCoffeInVan(Van van, CriterionEnum criterion) {
		SortingCoffee.sortCoffeInVanBy(criterion, van.getCoffeeGoods());
	}

	public String report(Van van) {
		return Report.report(van);
	}

}
