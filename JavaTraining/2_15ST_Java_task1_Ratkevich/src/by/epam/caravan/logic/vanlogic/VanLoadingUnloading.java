package by.epam.caravan.logic.vanlogic;

//import java.util.ArrayList;
//import java.util.Arrays;

import java.util.ArrayList;

import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.vancoffee.Van;

/**
 * Class is for loading and unloading the van.
 * 
 * Class is for loading and unloading the van. Class contains static methods of
 * carrying out loading and unloading the van coffee.
 * 
 * @author v.ratkevich
 */
public class VanLoadingUnloading {

	public VanLoadingUnloading() {

	}

	/**
	 * Makes loading van by coffee.
	 * 
	 * Checks on the capacity of the van. If there is free space in the van,
	 * adds object {@link Coffee}. If there isn't free space in the van, doesn't
	 * add object {@link Coffee} and proceeds scanning the next object in the
	 * array of parameters.
	 * 
	 * @param van
	 *            van, which should be loaded coffee
	 * @param coffeeArray
	 *            array of objects {@link Coffee}
	 * @return string with information about the number of objects loaded into
	 *         the van
	 */
	public static void loadVan(Van van, Coffee... coffeeArray) {
		for (Coffee coffee : coffeeArray) {
			boolean spaceInVanForCoffee = VanCapacityValidation
					.checkSpaceInVanForCoffee(van, coffee);
			if (spaceInVanForCoffee) {
				van.addCoffeeGood(coffee);

			}
		}
	}

	/**
	 * Makes unloading the van.
	 * 
	 * Makes unloading the van.Checks if the object passed as a parameter is in
	 * the object van. And if the verification returns true, then removes the
	 * object {@link Coffee} from the object {@link Van}.
	 * 
	 * @param van
	 * @param coffeeArray
	 */
	public static void unloadVan(Van van, Coffee[] coffeeArray) {
		ArrayList<Coffee> goodsCoffee = van.getCoffeeGoods();
		for (Coffee coffee : coffeeArray) {

			if (goodsCoffee.contains(coffee)) {
				van.removeCoffeeGood(coffee);

			}

		}

	}

	public static void unloadVanFully(Van van) {

		van.clearVan();

	}

}
