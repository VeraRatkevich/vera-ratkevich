package by.epam.caravan.logic.vanlogic;

import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.vancoffee.Van;
import by.epam.caravan.util.Calculation;

/**
 * Class class checks for the presence of free tonnage to load a certain weight
 * of goods.
 * 
 * @author v.ratkevich
 */
public class VanCapacityValidation {

	/**
	 * Checks whether there is space in the van for the goods intended to be
	 * loaded into the van.
	 * 
	 * Checks whether there is space in the van for the goods intended to be
	 * loaded into the van. The method calls a static method of class
	 * WeightCalculation for the calculation of free tonnage in the van. And
	 * then compares this figure with the weight of the goods intended to be
	 * loaded into the van.
	 * 
	 * @param van
	 *            van which should be loading the goods
	 * @param coffee
	 *            items that need to be loaded into the van
	 * @return true if there is space in the van, false if no space
	 */

	public static boolean checkSpaceInVanForCoffee(Van van, Coffee coffee) {
		boolean checkSpaceInVanForCoffee = false;
		double spaceInVan = Calculation.spaceVan(van);
		double weightCoffeeInTonnes = coffee.getWeightInKilograms() / 1000;

		if (spaceInVan >= weightCoffeeInTonnes) {
			checkSpaceInVanForCoffee = true;
		}
		return checkSpaceInVanForCoffee;
	}

}
