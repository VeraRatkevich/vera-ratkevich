package by.epam.caravan.logic.vanlogic.create;

import by.epam.caravan.entity.vancoffee.Van;

/**
 * Class creates objects of the class {@link Van}.
 * 
 * Class creates objects of the class {@link Van}. Class contains static
 * overloaded methods that call the constructors of the class {@link Van} return
 * object of the class.
 * 
 * @author v.ratkevich
 */

public class VanCreator {

	private VanCreator() {

	}

	/**
	 * Creation object of class {@link Van}.
	 * 
	 * Creation object of class {@link Van} by calling the constructor of class
	 * {@link Van} without parameters
	 * 
	 * @return object of class {@link Van}
	 */
	public static final Van createVan() {
		return new Van();
	}

	/**
	 * Creation object of class {@link Van}.
	 * 
	 * Creation object of class {@link Van} by calling the constructor of class
	 * {@link Van} with two parameters
	 * 
	 * @param tonnage
	 *            tonnage of the van
	 * @param licensePlate
	 *            license plate of the van
	 * @return object of class {@link Van}
	 */
	public static final Van createVan(int tonnage, String licensePlate) {
		return new Van(tonnage, licensePlate);
	}

}
