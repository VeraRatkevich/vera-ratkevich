package by.epam.caravan.logic.coffeelogic.sort;

public enum CriterionEnum {
	PRICE, WEIGHT, RATIO
}
