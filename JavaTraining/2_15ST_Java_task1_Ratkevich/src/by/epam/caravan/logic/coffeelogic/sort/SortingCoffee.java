package by.epam.caravan.logic.coffeelogic.sort;

import java.util.ArrayList;
import java.util.Collections;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.logic.coffeelogic.comparator.CoffeeComparator;

/**
 * Class is used to sort a collection of objects of the class {@link Coffee}.
 * 
 * @author v.ratkevich
 */
public class SortingCoffee {

	private static CoffeeComparator comp;

	private SortingCoffee() {

	}

	/**
	 * Sorts objects of the class {@link Coffee}.
	 * 
	 * Sorts objects of class {@link Coffee} on the selected criterion.
	 * 
	 * @param criterion
	 *            specifies the type of sorting objects
	 * @param coffeeGoods
	 *            collection for sorting
	 */
	public static void sortCoffeInVanBy(CriterionEnum criterion,
			ArrayList<Coffee> coffeeGoods) {
		comp = new CoffeeComparator(criterion);
		Collections.sort(coffeeGoods, comp);
	}

}
