package by.epam.caravan.logic.coffeelogic.create;

import by.epam.caravan.entity.coffee.ClassOfCoffeeEnum;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.coffee.GradeOfCoffeeEnum;
import by.epam.caravan.entity.coffee.instantcoffee.InstantCoffee;
import by.epam.caravan.entity.coffee.instantcoffee.TypeOfInstantCoffeeEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.DegreeOfRoastEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.GrindingEnum;
import by.epam.caravan.entity.coffee.naturalcoffee.GroundCoffee;
import by.epam.caravan.entity.coffee.naturalcoffee.RoastedCoffee;

/**
 * Class creates objects of the hierarchy of coffee.
 * 
 * Class creates objects of the hierarchy of coffee. Class contains static
 * overloaded methods that return objects of classes that belong to the
 * hierarchy of coffee. Depending on the parameters passed methods create and
 * return objects of corresponding types.
 * 
 * @author v.ratkevich
 */
public class CoffeeCreator {

	private CoffeeCreator() {

	}

	public static final Coffee createCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price) {
		return new Coffee(classOfCoffee, gradeOfCoffee, country,
				weightInKilograms, price);
	}

	public static final InstantCoffee createCoffee(
			ClassOfCoffeeEnum classOfCoffee, GradeOfCoffeeEnum gradeOfCoffee,
			String country, double weightInKilograms, double price,
			int percentageOfCaffeine,
			TypeOfInstantCoffeeEnum typeOfInstantCoffee) {
		return new InstantCoffee(classOfCoffee, gradeOfCoffee, country,
				weightInKilograms, price, percentageOfCaffeine,
				typeOfInstantCoffee);
	}

	public static final RoastedCoffee createCoffee(
			ClassOfCoffeeEnum classOfCoffee, GradeOfCoffeeEnum gradeOfCoffee,
			String country, double weightInKilograms, double price,
			DegreeOfRoastEnum degreeOfRoast) {
		return new RoastedCoffee(classOfCoffee, gradeOfCoffee, country,
				weightInKilograms, price, degreeOfRoast);
	}

	public static final GroundCoffee createCoffee(
			ClassOfCoffeeEnum classOfCoffee, GradeOfCoffeeEnum gradeOfCoffee,
			String country, double weightInKilograms, double price,
			DegreeOfRoastEnum degreeOfRoast, GrindingEnum grinding) {
		return new GroundCoffee(classOfCoffee, gradeOfCoffee, country,
				weightInKilograms, price, degreeOfRoast, grinding);
	}

}
