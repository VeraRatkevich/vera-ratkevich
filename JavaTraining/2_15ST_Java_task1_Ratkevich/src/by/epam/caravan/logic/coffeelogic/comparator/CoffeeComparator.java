package by.epam.caravan.logic.coffeelogic.comparator;

import java.util.Comparator;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.logic.coffeelogic.sort.CriterionEnum;

/**
 * Class for comparing objects of type {@link Coffee}.
 * 
 * Class for comparing objects of type {@link Coffee}. Implements the interface
 * Comparator. Class contains a field of enumeration type to select comparison.
 * In the overridden method compare choice of method comparison is based on a
 * given value of the variable.
 * 
 * @author v.ratkevich
 */
public class CoffeeComparator implements Comparator<Coffee> {

	private CriterionEnum sortingCriterion;

	public CoffeeComparator(CriterionEnum sortingCriterion) {
		
		this.sortingCriterion = sortingCriterion;
	}

	public void setSortingCriterion(CriterionEnum sortingCriterion) {

		this.sortingCriterion = sortingCriterion;
	}

	public CriterionEnum getSortingCriterion() {
		return sortingCriterion;
	}

	@Override
	public int compare(Coffee one, Coffee two) {
		switch (sortingCriterion) {
		case PRICE:
			return Double.compare(two.getPrice(), one.getPrice());
		case WEIGHT:
			return Double.compare(two.getWeightInKilograms(),
					one.getWeightInKilograms());
		case RATIO:
			return Double.compare(two.getPrice() / two.getWeightInKilograms(),
					one.getPrice() / one.getWeightInKilograms());
		default:
			throw new EnumConstantNotPresentException(CriterionEnum.class,
					sortingCriterion.name());
		}
	}
}
