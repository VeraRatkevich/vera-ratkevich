package by.epam.caravan.logic;

import java.util.ArrayList;

import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.vancoffee.Van;
import by.epam.caravan.util.Calculation;

public class Report {
	private Report() {

	}

	/**
	 * Forms a string of report.
	 * 
	 * Forms a string of report , which includes information about the van,
	 * about loads, about weight and price of loads.
	 * 
	 * @param van
	 *            van, which should be formed report
	 * @return the string of report
	 */
	public static String report(Van van) {
		String report = "Van: " + van.toString();
		ArrayList<Coffee> coffeeGoods = van.getCoffeeGoods();
		if (coffeeGoods != null) {
			report += "\nloaded with \n";
			for (Coffee coffee : coffeeGoods) {
				report += coffee + "\n";
			}
			report += "with a total weight of "
					+ Calculation.goodsWeightInTonnes(van) + " tonnes"
					+ "\nfor the sum of " + Calculation.goodsPrice(van)
					+ " dollars";
		} else {
			report += " is empty";
		}

		return report;
	}
}
