package by.epam.caravan.entity.vancoffee;

import java.util.ArrayList;

import by.epam.caravan.entity.coffee.Coffee;

public class Van {

	public final static int MIN_TONNAGE = 5;
	public final static int MAX_TONNAGE = 50;
	public final int TONNAGE;

	private String licensePlate;
	private ArrayList<Coffee> coffeeGoods;

	public Van() {
		TONNAGE = MIN_TONNAGE;
		coffeeGoods = new ArrayList<Coffee>();
	}

	public Van(int tonnage, String licensePlate) {
		if ((MIN_TONNAGE <= tonnage) && (tonnage <= MAX_TONNAGE)) {
			TONNAGE = tonnage;
		} else {
			TONNAGE = MIN_TONNAGE;
		}
		this.licensePlate = licensePlate;
		coffeeGoods = new ArrayList<Coffee>();
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public ArrayList<Coffee> getCoffeeGoods() {
		return coffeeGoods;
	}

	public void setCoffeeGoods(ArrayList<Coffee> coffeeGoods) {
		this.coffeeGoods = coffeeGoods;
	}

	public void addCoffeeGood(Coffee coffee) {
		coffeeGoods.add(coffee);
	}

	public void removeCoffeeGood(Coffee coffee) {
		coffeeGoods.remove(coffee);
	}

	public void clearVan() {
		this.coffeeGoods.clear();
	}

	@Override
	public String toString() {
		return getClass().getName() + "@license plate: " + licensePlate
				+ " tonnage: " + TONNAGE;
	}

}
