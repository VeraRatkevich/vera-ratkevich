package by.epam.caravan.entity.coffee.instantcoffee;

public enum TypeOfInstantCoffeeEnum {
	POWDER, GRANULAR, SUBLIMATED
}
