package by.epam.caravan.entity.coffee;

public enum ClassOfCoffeeEnum {
	ARABICA, ROBUSTA, LIBERICA
}
