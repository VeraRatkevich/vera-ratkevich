package by.epam.caravan.entity.coffee;

public class Coffee {

	private ClassOfCoffeeEnum classOfCoffee;
	private GradeOfCoffeeEnum gradeOfCoffee;
	private String country;
	private double weightInKilograms;
	private double price;

	public Coffee() {
		this.classOfCoffee = null;
		this.gradeOfCoffee = null;
	}

	public Coffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price) {
		this.classOfCoffee = classOfCoffee;
		this.gradeOfCoffee = gradeOfCoffee;
		this.country = country;
		this.setWeightInKilograms(weightInKilograms);
		this.setPrice(price);
	}

	public ClassOfCoffeeEnum getClassOfCoffee() {
		return classOfCoffee;
	}

	public void setClassOfCoffee(ClassOfCoffeeEnum classOfCoffee) {
		this.classOfCoffee = classOfCoffee;
	}

	public GradeOfCoffeeEnum getGradeOfCoffee() {
		return gradeOfCoffee;
	}

	public void setGradeOfCoffee(GradeOfCoffeeEnum gradeOfCoffee) {
		this.gradeOfCoffee = gradeOfCoffee;
	}

	public double getWeightInKilograms() {
		return weightInKilograms;
	}

	public void setWeightInKilograms(double weightInKilograms) {
		this.weightInKilograms = (double) (weightInKilograms >= 0 ? weightInKilograms
				: 0);
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price >= 0 ? price : 0;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coffee other = (Coffee) obj;

		if (classOfCoffee == null) {
			if (other.classOfCoffee != null)
				return false;
		} else if (!classOfCoffee.equals(other.classOfCoffee)) {
			return false;
		}

		if (gradeOfCoffee == null) {
			if (other.gradeOfCoffee != null)
				return false;
		} else if (!gradeOfCoffee.equals(other.gradeOfCoffee)) {
			return false;
		}

		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country)) {
			return false;
		}
		if (other.weightInKilograms != weightInKilograms) {
			return false;
		}
		if (other.price != price) {
			return false;
		}

		return true;
	}

	public int hashCode() {
		return (int) (31 * weightInKilograms + price
				+ ((null == classOfCoffee) ? 0 : classOfCoffee.hashCode())
				+ ((null == gradeOfCoffee) ? 0 : gradeOfCoffee.hashCode()) + ((null == country) ? 0
					: country.hashCode()));
	}

	@Override
	public String toString() {
		return getClass().getName() + "@class of coffee: "
				+ classOfCoffee.name() + " grade of coffee: "
				+ gradeOfCoffee.name() + " country: " + getCountry()
				+ " weight: " + weightInKilograms + " kilograms" + " price: "
				+ price;
	}
}