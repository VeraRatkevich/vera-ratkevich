package by.epam.caravan.entity.coffee.instantcoffee;

import by.epam.caravan.entity.coffee.ClassOfCoffeeEnum;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.coffee.GradeOfCoffeeEnum;

public class InstantCoffee extends Coffee {
	private int percentageOfCaffeine;
	private TypeOfInstantCoffeeEnum typeOfInstantCoffee;

	public InstantCoffee() {
		percentageOfCaffeine = 0;
		typeOfInstantCoffee = null;
	}

	public InstantCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price, int percentageOfCaffeine,
			TypeOfInstantCoffeeEnum typeOfInstantCoffee) {
		super(classOfCoffee, gradeOfCoffee, country, weightInKilograms, price);
		setPercentageOfCaffeine(percentageOfCaffeine);
		this.typeOfInstantCoffee = typeOfInstantCoffee;
	}

	public int getPercentageOfCaffeine() {
		return percentageOfCaffeine;
	}

	public void setPercentageOfCaffeine(int percentageOfCaffeine) {
		if ((percentageOfCaffeine > 0) & (percentageOfCaffeine <= 100)) {
			this.percentageOfCaffeine = percentageOfCaffeine;
		}
	}

	public TypeOfInstantCoffeeEnum getTypeOfInstantCoffee() {
		return typeOfInstantCoffee;
	}

	public void setTypeOfInstantCoffee(
			TypeOfInstantCoffeeEnum typeOfInstantCoffee) {

		this.typeOfInstantCoffee = typeOfInstantCoffee;

	}

	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			InstantCoffee other = (InstantCoffee) obj;
			if (other.percentageOfCaffeine != this.percentageOfCaffeine)
				return false;
			if (typeOfInstantCoffee == null) {
				if (other.typeOfInstantCoffee != null)
					return false;
			} else if (!typeOfInstantCoffee.equals(other.typeOfInstantCoffee)) {
				return false;
			}
			return true;
		}
		return false;
	}

	public int hashCode() {
		return (int) (super.hashCode() + 33 * percentageOfCaffeine + ((null == typeOfInstantCoffee) ? 0
				: typeOfInstantCoffee.hashCode()));
	}

	public String toString() {
		return super.toString() + " percentage of caffeine: "
				+ percentageOfCaffeine + " type of instant coffee: "
				+ typeOfInstantCoffee.name();
	}
}