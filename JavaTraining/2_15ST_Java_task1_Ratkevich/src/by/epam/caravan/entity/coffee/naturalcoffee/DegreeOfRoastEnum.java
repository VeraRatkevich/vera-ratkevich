package by.epam.caravan.entity.coffee.naturalcoffee;

public enum DegreeOfRoastEnum {
	LIGHT, MEDIUM, DARK
}
