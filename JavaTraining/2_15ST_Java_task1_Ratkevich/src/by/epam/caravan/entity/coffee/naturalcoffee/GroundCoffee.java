package by.epam.caravan.entity.coffee.naturalcoffee;

import by.epam.caravan.entity.coffee.ClassOfCoffeeEnum;
import by.epam.caravan.entity.coffee.GradeOfCoffeeEnum;

public class GroundCoffee extends RoastedCoffee {
	
	private GrindingEnum grinding;

	public GroundCoffee() {
		this.grinding = null;
	}

	public GroundCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price,
			DegreeOfRoastEnum degreeOfRoast, GrindingEnum grinding) {
		super(classOfCoffee, gradeOfCoffee, country, weightInKilograms, price,
				degreeOfRoast);
		this.grinding = grinding;
	}

	public GrindingEnum getGrinding() {
		return grinding;
	}

	public void setGrinding(GrindingEnum grinding) {
		this.grinding = grinding;
	}

	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			GroundCoffee other = (GroundCoffee) obj;
			if (grinding == null) {
				if (other.grinding != null) {
					return false;
				}
			} else if (!grinding.equals(other.grinding)) {
				return false;
			}
			return true;
		}
		return false;
	}

	public int hashCode() {
		return (int) (super.hashCode() + ((null == grinding) ? 0 : grinding
				.hashCode()));
	}

	public String toString() {
		return super.toString() + " rate of grinding: " + grinding.name();
	}

}
