package by.epam.caravan.entity.coffee.naturalcoffee;

public enum GrindingEnum {
	ROUGH, AVERAGE, THIN
}
