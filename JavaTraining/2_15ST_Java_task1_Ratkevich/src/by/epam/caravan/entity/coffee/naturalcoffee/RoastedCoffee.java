package by.epam.caravan.entity.coffee.naturalcoffee;

import by.epam.caravan.entity.coffee.ClassOfCoffeeEnum;
import by.epam.caravan.entity.coffee.Coffee;
import by.epam.caravan.entity.coffee.GradeOfCoffeeEnum;

public class RoastedCoffee extends Coffee {

	private DegreeOfRoastEnum degreeOfRoast;

	public RoastedCoffee() {
		this.degreeOfRoast = null;
	}

	public RoastedCoffee(ClassOfCoffeeEnum classOfCoffee,
			GradeOfCoffeeEnum gradeOfCoffee, String country,
			double weightInKilograms, double price,
			DegreeOfRoastEnum degreeOfRoast) {
		super(classOfCoffee, gradeOfCoffee, country, weightInKilograms, price);
		this.degreeOfRoast = degreeOfRoast;
	}

	public DegreeOfRoastEnum getDegreeOfRoast() {
		return degreeOfRoast;
	}

	public void setDegreeOfRoast(DegreeOfRoastEnum degreeOfRoast) {
		this.degreeOfRoast = getDegreeOfRoast();
	}

	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			RoastedCoffee other = (RoastedCoffee) obj;
			if (degreeOfRoast == null) {
				if (other.degreeOfRoast != null) {
					return false;
				}
			} else if (!degreeOfRoast.equals(other.degreeOfRoast)) {
				return false;
			}
			return true;
		}
		return false;
	}

	public int hashCode() {
		return (int) (super.hashCode() + ((null == degreeOfRoast) ? 0
				: degreeOfRoast.hashCode()));
	}

	public String toString() {
		return super.toString() + " degree of roast: " + degreeOfRoast.name();
	}
}
