package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
* Class provides way to the page viewing user information.
* 
* @author RatkevichVera
* @version 1.0
*/

public class ProfileCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		String page;

		int idUser = (int) request.getSession().getAttribute(
				SessionAttributeName.USER_ID);

		User user = null;
		if (idUser != 0) {
			IUserDao dao = DaoFactory.getInstance().getUserDao();
			try {
				user = dao.getById(idUser);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_USER, e);
			}

		}

		if (user != null) {
			request.setAttribute(RequestAttributeName.USER, user);
			page = JspPageName.PROFILE_PAGE;

		} else {

			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.OBJECT_REMOVED));
			page = JspPageName.ERROR_PAGE;
		}

		return page;
	}
}
