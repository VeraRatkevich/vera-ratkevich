package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.EnumSetForJspPage;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Order;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.util.SortingCriterionEnum;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.PageManager;

/*
 * Class is designed to output from a database of information on all orders. 
 * The books are displayed page by page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ShowAllOrdersCommand implements ICommand {
	private static final int COUNT_ORDERS_ONE_PAGE = 10;

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		SortingCriterionEnum criterion = null;
		String strCriterion = request
				.getParameter(RequestParameterName.CRITERION);

		if ((strCriterion != null) && !strCriterion.isEmpty()) {
			try {
				criterion = SortingCriterionEnum.valueOf(strCriterion);
			} catch (IllegalArgumentException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_CONVERTION_SORT_CRITERION,
						e);
			}

		} else {
			criterion = SortingCriterionEnum.DATE_DESC;
		}

		List<Order> orders = null;
		int countOrders = 0;

		IOrderDao dao = DaoFactory.getInstance().getOrderDao();
		try {
			countOrders = dao.getCountOrders();
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_ORDERS, e);

		}

		if ((countOrders == 0)) {

			request.setAttribute(RequestAttributeName.ORDERS_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_ORDERS));
			return JspPageName.ORDERS_PAGE;

		} else if (countOrders <= COUNT_ORDERS_ONE_PAGE) {

			try {

				switch (criterion) {
				case SURENAME_ASC:
					orders = dao.showAllOrderUserAsc();
					break;
				case SURENAME_DESC:
					orders = dao.showAllOrderUserDesc();
					break;
				case DATE_ASC:
					orders = dao.showAllOrderDateAsc();
					break;
				default:
					orders = dao.showAllOrderDateDesc();
					break;
				}

			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_ORDERS, e);
			}

		} else {

			String strPage = request.getParameter(RequestParameterName.PAGE);

			int countPages = (int) Math.ceil((double) countOrders
					/ COUNT_ORDERS_ONE_PAGE);

			PageManager pageManager = new PageManager(countPages);
			pageManager.setCurrentPage(strPage);

			int numberFirstBook = (pageManager.getCurrentPage() - 1)
					* COUNT_ORDERS_ONE_PAGE;

			try {

				switch (criterion) {
				case SURENAME_ASC:
					orders = dao.showAllInLimitUserAsc(numberFirstBook,
							COUNT_ORDERS_ONE_PAGE);
					break;
				case SURENAME_DESC:
					orders = dao.showAllInLimitUserDesc(numberFirstBook,
							COUNT_ORDERS_ONE_PAGE);
					break;
				case DATE_ASC:
					orders = dao.showAllInLimitDateAsc(numberFirstBook,
							COUNT_ORDERS_ONE_PAGE);
					break;
				default:
					orders = dao.showAllInLimitDateDesc(numberFirstBook,
							COUNT_ORDERS_ONE_PAGE);
					break;

				}

			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_ORDERS, e);

			}

			request.setAttribute(RequestAttributeName.PAGE_MANAGER, pageManager);

		}

		if (orders != null) {
			request.setAttribute(RequestAttributeName.ORDERS, orders);
		} else {
			request.setAttribute(RequestAttributeName.ORDERS_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_ORDERS));
		}

		request.setAttribute(RequestParameterName.CRITERION, criterion);
		request.setAttribute(RequestAttributeName.CRITERION_ENUM,
				EnumSetForJspPage.criterionEnum);

		return JspPageName.ORDERS_PAGE;

	}
}
