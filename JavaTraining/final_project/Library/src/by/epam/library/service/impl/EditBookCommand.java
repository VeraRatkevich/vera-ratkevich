package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class is intended to modify the data of the book in the database. 
 * Data are checked before sending to the database.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class EditBookCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String strBookId = request.getParameter(RequestParameterName.BOOK_ID);
		String name = request.getParameter(RequestParameterName.NAME);
		String author = request.getParameter(RequestParameterName.BOOK_AUTHOR);
		String strYear = request.getParameter(RequestParameterName.BOOK_YEAR);
		String strPages = request.getParameter(RequestParameterName.BOOK_PAGES);
		String strGenreId = request
				.getParameter(RequestParameterName.BOOK_GENRE_ID);
		Validator validator = Validator.getInstance();
		if (!validator.validateId(strBookId)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}
	
		if (!validator.validateBookData(name, author, strYear, strGenreId,
				strPages)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_DATA));
			return JspPageName.USER_MESSAGE;
		}

		Book book = new Book();

		book.setId(Integer.parseInt(strBookId));
		book.setName(name);
		book.setAuthor(author);
		book.setYear(Integer.parseInt(strYear));
		book.setPages(Integer.parseInt(strPages));
		book.getGenre().setId(Integer.parseInt(strGenreId));

		IBookDao dao = DaoFactory.getInstance().getBookDao();

		int i = 0;

		try {
			i = dao.update(book);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_EDIT_BOOK,
					e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
			return JspPageName.USER_MESSAGE;

		}

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);
		String currentUrl = RequestManager.getInstance().getCurrentUrlRequest(
				request);

		return currentUrl;
	}

}
