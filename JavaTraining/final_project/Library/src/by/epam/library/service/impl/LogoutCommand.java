package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.service.ICommand;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;

public class LogoutCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page;
		request.getSession().invalidate();
		request.setAttribute(RequestAttributeName.DIRECTION_TYPE, DirectionTypeEnum.SEND_REDIRECT);
		page = UrlForRedirect.ABOUT;
		return page;
	}

}
