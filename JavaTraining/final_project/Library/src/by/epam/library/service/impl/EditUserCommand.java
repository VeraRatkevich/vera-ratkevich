package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.entity.constant.UserTypeEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.util.Validator;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
 * Class is intended to modify the data of the user in the database. 
 * Data are checked before sending to the database.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class EditUserCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String strUserId = request.getParameter(RequestParameterName.USER_ID);
		String surename = request.getParameter(RequestParameterName.SURENAME);
		String name = request.getParameter(RequestParameterName.NAME);
		String patronymic = request
				.getParameter(RequestParameterName.PATRONYMIC);
		String address = request.getParameter(RequestParameterName.ADDRESS);
		String phoneNumber = request
				.getParameter(RequestParameterName.PHONE_NUMBER);
		String strType = request.getParameter(RequestParameterName.USER_TYPE);

		Validator validator = Validator.getInstance();

		if ((strType == null) || (!validator.validateId(strUserId))) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		if (!validator.validateUserData(surename, name, patronymic, address,
				phoneNumber)) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_DATA));
			return JspPageName.USER_MESSAGE;

		}

		UserTypeEnum type = null;
		try {
			type = UserTypeEnum.valueOf(strType);
		} catch (IllegalArgumentException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_CONVERTION_TYPE, e);
		}

		User user = new User();
		user.setId(Integer.parseInt(strUserId));
		user.setSurename(surename);
		user.setName(name);
		user.setPatronymic(patronymic);
		user.setAddress(address);
		user.setPhoneNumber(phoneNumber);
		user.setType(type);

		IUserDao dao = DaoFactory.getInstance().getUserDao();

		int i = 0;

		try {
			i = dao.update(user);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_EDIT_USER,
					e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
			return JspPageName.USER_MESSAGE;

		}

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);
		String currentUrl = RequestManager.getInstance().getCurrentUrlRequest(request);

		 return currentUrl;
	}

}
