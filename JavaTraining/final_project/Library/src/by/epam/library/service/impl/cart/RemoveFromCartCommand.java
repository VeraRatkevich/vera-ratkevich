package by.epam.library.service.impl.cart;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
 * Class is designed for remove books from the cart. 
 * 
 * Method:
 * execute()- takes a collection of session id of books (if there is no collection in the session, it creates a new one), 
 * remove from the collection of book id and puts it back in session.
 *  
 * @author RatkevichVera
 * @version 1.0
 */

public class RemoveFromCartCommand implements ICommand {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		HttpSession session = request.getSession();
		Set<Integer> cart = null;
		try {
			cart = (HashSet<Integer>) session
					.getAttribute(SessionAttributeName.CART);
		} catch (NumberFormatException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_CART);
		}
		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);
		Integer idBook = null;
		if (strIdBook != null) {
			idBook = new Integer(strIdBook);
			cart.remove(idBook);
		}

		session.setAttribute(SessionAttributeName.CART, cart);

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String urlRequest = RequestManager.getInstance().getCurrentUrlRequest(
				request);

		return urlRequest;
	}

}
