package by.epam.library.service;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.service.exception.ServiceException;

public interface ICommand {
	public String execute(HttpServletRequest request) throws ServiceException;
}
