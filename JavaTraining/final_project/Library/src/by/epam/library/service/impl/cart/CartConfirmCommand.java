package by.epam.library.service.impl.cart;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.dao.impl.DbOrderBookDao;
import by.epam.library.dao.impl.DbOrderDao;
import by.epam.library.entity.Order;
import by.epam.library.entity.OrderBook;
import by.epam.library.entity.User;
import by.epam.library.entity.constant.OrderBookStatusEnum;
import by.epam.library.entity.constant.OrderStatusEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
 * Class is designed for the creation of the order of the user and entering order information and order books in the database.
 *  
 * @author RatkevichVera
 * @version 1.0
 */

public class CartConfirmCommand implements ICommand {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = null;
		HttpSession session = request.getSession();

		Set<Integer> cart = null;
		try {
			cart = (HashSet<Integer>) session
					.getAttribute(SessionAttributeName.CART);
		} catch (NumberFormatException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_CART);
		}

		if (cart != null && !cart.isEmpty()) {
			Order order = new Order();
			
			LocalDate date = LocalDate.now();
			int idUser = (int) session
					.getAttribute(SessionAttributeName.USER_ID);

			order.setDate(date);
			User user = new User();
			user.setId(idUser);
			order.setUser(user);
			order.setStatus(OrderStatusEnum.UNEXECUTED);

			IOrderDao orderDao = DbOrderDao.getInstance();

			int i = 0;
			int idOrder = 0;
			try {
				i = orderDao.add(order);
				if (i != 0) {
					idOrder = orderDao.getMaxIdOrderByIdUser(idUser);
				}

			} catch (DaoException e) {
				throw new ServiceException(ServiceExceptionMessage.ERROR_ADD_ORDER);
			}


			List<OrderBook> books = new ArrayList<OrderBook>();
			OrderBook book = null;
			
			for (Integer idBook : cart) {				
				book = new OrderBook();
				book.setId(idBook.intValue());
				book.setDateBegin(date);
				book.setStatus(OrderBookStatusEnum.EXPECTED);
				books.add(book);				
			}

			IOrderBookDao orderBookDao = DbOrderBookDao.getInstance();
			int a = 0;
			try {
				a = orderBookDao.addList(idOrder, books);				
			} catch (DaoException e) {
				throw new ServiceException(ServiceExceptionMessage.ERROR_ADD_ORDER_BOOKS, e);
			}
			if (a!=0){
				request.setAttribute(RequestAttributeName.CART_MESSAGE, MessageManager.getMessage(JspPageMessage.CART_CONFIRM_SUCCESS_MESSAGE));
				cart.clear();
				session.setAttribute(SessionAttributeName.CART, cart);		
				
				request.setAttribute(RequestAttributeName.DIRECTION_TYPE, DirectionTypeEnum.SEND_REDIRECT);
				page = RequestManager.getInstance().getCurrentUrlRequest(request);
			
			}
		} else {
			request.setAttribute(RequestAttributeName.CART_MESSAGE, MessageManager.getMessage(JspPageMessage.CART_EMPTY_MESSAGE));
			page = JspPageName.CART_PAGE;
		}

		
		return page;
	}

}
