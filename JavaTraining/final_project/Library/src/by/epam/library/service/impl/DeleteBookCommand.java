package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IBookDao;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class is designed to remove a book from the database. 
 * Previously method of the class checks whether the book in orders.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class DeleteBookCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String page;

		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);
	
		if (!Validator.getInstance().validateId(strIdBook)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		int idBook = Integer.parseInt(strIdBook);

		IOrderBookDao dao = DaoFactory.getInstance().getOrderBookDao();

		int countOrderBooks = 0;
		try {
			countOrderBooks = dao.getCountById(idBook);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_COUNT_ORDER_BOOKS, e);
		}

		if (countOrderBooks > 0) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.OBJECT_CANNOT_DELETE));
			page =  JspPageName.USER_MESSAGE;

		} else {

			IBookDao bookDdao = DaoFactory.getInstance().getBookDao();

			int i = 0;
			try {
				i = bookDdao.delete(idBook);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_DELETE_BOOK, e);
			}

			if (i == 0) {
				request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
						MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
				page =  JspPageName.USER_MESSAGE;

			} else {
				request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
						DirectionTypeEnum.SEND_REDIRECT);
				page = UrlForRedirect.BOOKS;
			}

		}

		return page;
	}

}
