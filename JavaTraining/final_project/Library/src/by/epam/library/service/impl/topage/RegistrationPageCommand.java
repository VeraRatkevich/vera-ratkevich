package by.epam.library.service.impl.topage;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageName;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.exception.ServiceException;

public class RegistrationPageCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		RequestManager.getInstance().saveUrlRequest(request);
		 return JspPageName.REGISTRATION_PAGE;
	}

}
