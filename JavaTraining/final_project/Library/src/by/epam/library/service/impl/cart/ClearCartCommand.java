package by.epam.library.service.impl.cart;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.RequestManager;

/*
 * Class is designed to remove the books from the cart.
 * 
 * Method:
 * execute()- takes a collection of session id of books, clear it and puts it back in session.
 *  * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ClearCartCommand implements ICommand {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();
		
		Set<Integer> cart = null;
		try {
			cart = (HashSet<Integer>) session
					.getAttribute(SessionAttributeName.CART);
		} catch (NumberFormatException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_CART);
		}
		if (cart != null && !cart.isEmpty()) {
			cart.clear();
			session.setAttribute(SessionAttributeName.CART, cart);
			request.setAttribute(RequestAttributeName.SUCCESS,
					MessageManager.getMessage(JspPageMessage.SUCCESS_MESSAGE));
		}//else{
		//	request.setAttribute(RequestAttributeName.CART_MESSAGE, MessageManager.getMessage(JspPageMessage.CART_EMPTY_MESSAGE));
		//}
request.setAttribute(RequestAttributeName.DIRECTION_TYPE, DirectionTypeEnum.SEND_REDIRECT);
		
		String urlRequest = RequestManager.getInstance().getCurrentUrlRequest(request);
		//page = JspPageName.CART_PAGE;
		
		return urlRequest;
	}
}
