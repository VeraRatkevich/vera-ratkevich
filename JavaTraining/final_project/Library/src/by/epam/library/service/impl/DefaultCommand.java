package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageName;
import by.epam.library.service.ICommand;
import by.epam.library.service.exception.ServiceException;

public class DefaultCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		return JspPageName.ABOUT_PAGE;
	}

}
