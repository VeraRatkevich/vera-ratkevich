package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Order;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.PageManager;

/*
 * Class is designed to output from a database of information on all user orders. 
 * The books are displayed page by page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ShowUserOrdersCommand implements ICommand {
	private static final int COUNT_USER_ORDERS_ONE_PAGE = 8;

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		RequestManager.getInstance().saveUrlRequest(request);

		List<Order> orders = null;

		HttpSession session = request.getSession();

		int idUser = (int) session.getAttribute(SessionAttributeName.USER_ID);

		int countOrders = 0;

		IOrderDao dao = DaoFactory.getInstance().getOrderDao();
		try {
			countOrders = dao.getCountUserOrders(idUser);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_COUNT_ORDERS, e);

		}

		if ((countOrders == 0)) {

			request.setAttribute(RequestAttributeName.ORDERS_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_ORDERS));
			return JspPageName.ORDERS_PAGE;

		} else if (countOrders <= COUNT_USER_ORDERS_ONE_PAGE) {

			try {
				orders = dao.showOrdersByIdUser(idUser);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_ORDERS, e);

			}

		} else {

			int countPages = (int) Math.ceil((double) countOrders
					/ COUNT_USER_ORDERS_ONE_PAGE);
			String strPage = request.getParameter(RequestParameterName.PAGE);

			PageManager pageManager = new PageManager(countPages);

			pageManager.setCurrentPage(strPage);

			int numberFirstBook = (pageManager.getCurrentPage() - 1) * COUNT_USER_ORDERS_ONE_PAGE;

			try {
				orders = dao.showUserOrdersInLimit(idUser, numberFirstBook,
						COUNT_USER_ORDERS_ONE_PAGE);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_ORDERS, e);

			}

			request.setAttribute(RequestAttributeName.PAGE_MANAGER, pageManager);

		}

		if (orders != null) {
			request.setAttribute(RequestAttributeName.ORDERS, orders);
		} else {
			request.setAttribute(RequestAttributeName.ORDERS_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_ORDERS));
		}
		return JspPageName.USER_ORDERS;

	}

}
