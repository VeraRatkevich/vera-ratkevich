package by.epam.library.service.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.controller.constant.UrlForRedirect;

/*
 * Class is designed for the process of saving the URL of the current request in a session.
 * 
 * Methods:
 * rebuildUrl(HttpServletRequest request) - restores the URL of the request.
 * saveUrlRequest(HttpServletRequest request) - saves URL of the request in the session.
 * getCurrentUrlRequest(HttpServletRequest request) - returns the URL of the current request.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class RequestManager {	

	private final static RequestManager instance = new RequestManager();

	private RequestManager() {

	}

	public static RequestManager getInstance() {

		return instance;

	}


	public String getCurrentUrlRequest(HttpServletRequest request) {
		String url = null;
		url = request.getSession()
				.getAttribute(SessionAttributeName.CURRENT_REQUEST).toString();
		if (url == null) {
			url = UrlForRedirect.ABOUT;
		}

		return url;
	}

	public void saveUrlRequest(HttpServletRequest request) {
		String url = rebuildUrl(request);
		request.getSession().setAttribute(SessionAttributeName.CURRENT_REQUEST,
				url);

	}

	private String rebuildUrl(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();
		sb.append(UrlForRedirect.HEADER_PATH);
		Enumeration<String> parameterNames = request.getParameterNames();
		int count = 0;
		while (parameterNames.hasMoreElements()) {

			if (count == 0) {
				sb.append("?");
			} else {
				sb.append("&");
			}

			String parameter = parameterNames.nextElement();

			sb.append(parameter);
			sb.append("=");
			String parameterValue =  request.getParameter(parameter);
			sb.append(parameterValue);

			count++;
		}

		return sb.toString();
	}

}
