package by.epam.library.service.exception;

import by.epam.library.exception.ProjectException;

public class ServiceException extends ProjectException {

	private static final long serialVersionUID = 1L;

	public ServiceException() {

		super();

	}
	
	public ServiceException(String msg) {

		super(msg);

	}

	public ServiceException(String msg, Exception e) {

		super(msg, e);

	}
	public ServiceException(Exception e) {

		super(e);

	}

}

