package by.epam.library.service.impl;

import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;
import by.epam.library.util.DateParser;

/*
 * Class is intended to modify the data expect of the book in the database. 
 * Data are checked before sending to the database.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class EditDateExpectCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		Validator validator = Validator.getInstance();

		String strIdOrder = request.getParameter(RequestParameterName.ORDER_ID);
		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);
		String strDateExpect = request
				.getParameter(RequestParameterName.DATE_EXPECT);

		if ((strDateExpect != null) && (!strDateExpect.isEmpty())) {
			if ((!validator.validateId(strIdOrder))
					|| (!validator.validateId(strIdBook))
					|| (!validator.validateDate(strDateExpect))) {
				request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
						MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
				return JspPageName.USER_MESSAGE;
			}

			LocalDate dateExpect = DateParser.parseToLocalDate(strDateExpect);
			int idOrder = Integer.parseInt(strIdOrder);
			int idBook = Integer.parseInt(strIdBook);

			IOrderBookDao orderBookDao = DaoFactory.getInstance()
					.getOrderBookDao();

			int i = 0;
			try {
				i = orderBookDao.setDateExpect(idOrder, idBook, dateExpect);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_EDIT_ORDER_BOOK, e);
			}

			if (i == 0) {
				request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
						MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
				return JspPageName.USER_MESSAGE;
			}
		}
		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String currentUrl = RequestManager.getInstance().getCurrentUrlRequest(
				request);

		return currentUrl;
	}
}
