package by.epam.library.service.constant;

public class ServiceExceptionMessage {

	
	
	public static final String ERROR_GET_ORDERS = "Can not get orders.";
	public static final String ERROR_GET_USERS = "Can not get users.";
	public static final String ERROR_GET_BOOKS = "Can not get books.";
	public static final String ERROR_GET_ORDER_BOOKS = "Can not get order books.";
	public static final String ERROR_GET_ORDER = "Can not get order.";
	public static final String ERROR_GET_USER = "Can not get user.";
	public static final String ERROR_GET_BOOK = "Can not get book.";
	public static final String ERROR_GET_ORDER_BOOK = "Can not get order book.";
	public static final String ERROR_ADD_BOOK = "Can not add book.";
	public static final String ERROR_ADD_USER = "Can not add user.";
	public static final String ERROR_ADD_ORDER = "Can not add order.";
	public static final String ERROR_ADD_ORDER_BOOKS = "Can not add order books.";
	public static final String ERROR_GET_GENRES = "Can not get genres.";
	public static final String ERROR_GET_ID_USER = "Can not get user id.";
	public static final String ERROR_DELETE_BOOK = "Can not delete book.";
	public static final String ERROR_DELETE_ORDER_BOOK = "Can not delete order book.";
	public static final String ERROR_DELETE_USER = "Can not delete user book.";
	public static final String ERROR_COUNT_ORDERS = "Can not get number of orders.";
	public static final String ERROR_COUNT_BOOKS = "Can not get number of books.";
	public static final String ERROR_COUNT_ORDER_BOOKS = "Can not get number of order books.";
	public static final String ERROR_COUNT_USERS = "Can not get number of users.";
	public static final String ERROR_EDIT_USER = "Can not edit user.";
	public static final String ERROR_EDIT_BOOK = "Can not edit book.";
	public static final String ERROR_EDIT_ORDER_BOOK = "Can not edit order book.";
	public static final String ERROR_GET_BOOK_STATUS = "Can not get order book status.";
	public static final String ERROR_SET_STATUS_ORDER = "Can not set order status.";
	public static final String ERROR_GET_STATUS_ORDER = "Can not get order status.";
	public static final String ERROR_GET_CART = "Can not get cart.";
	public static final String ERROR_CONVERTION_TYPE = "Can not convert user type";
	public static final String ERROR_CONVERTION_SEARCH_CATEGORY = "Can not convert search category";
	public static final String ERROR_CONVERTION_SORT_CRITERION = "Can not convert sort criterion";
	public static final String ERROR_ENCRIPT_PASSWORD = "Can not encrypt password";
}
