package by.epam.library.service.impl.cart;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.dao.impl.DbOrderBookDao;
import by.epam.library.entity.OrderBook;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
 * Class is designed to display information about the books that are in the cart of the reader.
 * 
 * Method:
 * execute()- takes a collection of session id of books (if there is no collection in the session, it creates a new one), 
 * retrieves from the database information about books and their availability by id from collection.
 *  
 * @author RatkevichVera
 * @version 1.0
 */

public class ShowCartCommand implements ICommand {
	String page;

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);
				
		HttpSession session = request.getSession();

		Set<Integer> cart = null;
		try {
			cart = (HashSet<Integer>) session
					.getAttribute(SessionAttributeName.CART);
		} catch (NumberFormatException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_CART);
		}

		if (cart != null && !cart.isEmpty()) {

			List<OrderBook> books = new ArrayList<OrderBook>();
			IOrderBookDao dao = DbOrderBookDao.getInstance();
			OrderBook book = null;
			for (Integer idBook : cart) {
				try {
					book = dao.showActualById(idBook.intValue());
				} catch (DaoException e) {
					throw new ServiceException(ServiceExceptionMessage.ERROR_GET_BOOKS);
				}
				if (book != null) {
					books.add(book);
				}
			}
			request.setAttribute(RequestAttributeName.BOOKS, books);
		} else {
			request.setAttribute(RequestAttributeName.CART_MESSAGE, MessageManager.getMessage(JspPageMessage.CART_EMPTY_MESSAGE));
		}
		page = JspPageName.CART_PAGE;
		return page;

	}

}
