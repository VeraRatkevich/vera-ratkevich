package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.PageManager;

/*
 * Class is designed to output from a database of information on all users. 
 * The books are displayed page by page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ShowAllUsersCommand implements ICommand {
	private static final int COUNT_USERS_ONE_PAGE = 10;

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);
		List<User> users = null;
		int countUsers = 0;

		IUserDao dao = DaoFactory.getInstance().getUserDao();
		try {
			countUsers = dao.getCountUsers();
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_COUNT_USERS, e);

		}

		if ((countUsers == 0)) {

			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_USERS));
			return JspPageName.USERS_PAGE;

		} else if (countUsers <= COUNT_USERS_ONE_PAGE) {

			try {
				users = dao.showAll();
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_USERS, e);

			}

		} else {

			int countPages = (int) Math.ceil((double) countUsers
					/ COUNT_USERS_ONE_PAGE);
			PageManager pageManager = new PageManager(countPages);
			
			String strPage = request.getParameter(RequestParameterName.PAGE);
			pageManager.setCurrentPage(strPage);

			int numberFirstUser = (pageManager.getCurrentPage() - 1)
					* COUNT_USERS_ONE_PAGE;

			try {
				users = dao.showAllInLimit(numberFirstUser,
						COUNT_USERS_ONE_PAGE);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_USERS, e);

			}

			request.setAttribute(RequestAttributeName.PAGE_MANAGER, pageManager);

		}

		if (users != null) {
			request.setAttribute(RequestAttributeName.USERS, users);
		} else {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_USERS));
		}

		return JspPageName.USERS_PAGE;

	}
}
