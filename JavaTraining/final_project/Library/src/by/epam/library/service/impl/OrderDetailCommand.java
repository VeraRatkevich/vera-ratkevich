package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Order;
import by.epam.library.entity.OrderBook;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class provides way to the page view detailed information about the order.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class OrderDetailCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);
		String page = JspPageName.ORDER_DETAIL;
		String strIdOrder = request.getParameter(RequestParameterName.ORDER_ID);

		if ((strIdOrder == null)
				|| (!Validator.getInstance().validateId(strIdOrder))) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return page;
		}

		int idOrder = Integer.parseInt(strIdOrder);

		Order order = null;

		IOrderDao orderDao = DaoFactory.getInstance().getOrderDao();
		try {
			order = orderDao.getByIdWithUser(idOrder);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_ORDER,
					e);
		}

		if (order == null) {
			request.setAttribute(RequestAttributeName.ERROR_ORDER_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.ORDER_NOT_AVAILABLE));
			return page;
		}

		List<OrderBook> books = null;

		IOrderBookDao orderBookDao = DaoFactory.getInstance().getOrderBookDao();
		try {

			books = orderBookDao.showBooksByOrderID(idOrder);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_ORDER_BOOKS, e);
		}

		if (books != null) {
			order.setBooks(books);
		} else {
			request.setAttribute(RequestAttributeName.ERROR_ORDER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_BOOKS));
		}
		request.setAttribute(RequestAttributeName.ORDER, order);

		return page;

	}

}
