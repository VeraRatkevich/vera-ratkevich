package by.epam.library.service.impl.cart;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.constant.OrderBookStatusEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.util.Validator;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
 * Class is designed for adding books to the cart. 
 * 
 * Method:
 * execute()- takes a collection of session id of books (if there is no collection in the session, it creates a new one), 
 * places id of book from a request to the collection and puts it back in session.
 *  * 
 * @author RatkevichVera
 * @version 1.0
 */

public class AddBookToCartCommand implements ICommand {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		HttpSession session = request.getSession();

		Set<Integer> cart = null;
		try {
			cart = (HashSet<Integer>) session
					.getAttribute(SessionAttributeName.CART);
		} catch (NumberFormatException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_CART);
		}
		if (cart == null) {
			cart = new HashSet<Integer>();

		}

		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);

		if (!Validator.getInstance().validateId(strIdBook)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		int idBook = Integer.parseInt(strIdBook);
		int idUser = (int) session.getAttribute(SessionAttributeName.USER_ID);
		IOrderBookDao dao = DaoFactory.getInstance().getOrderBookDao();
		OrderBookStatusEnum status = null;
		
		try {
			status = dao.getByOrderBookUserId(idBook, idUser);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_ORDER_BOOK, e);
		}

		if (status !=null) {
			request.setAttribute(RequestAttributeName.USER_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.BOOK_ALREADY_ORDERED) + " " + MessageManager
							.getMessage(status.toString().toLowerCase()));
			return JspPageName.USER_MESSAGE;

		}

		cart.add(new Integer(strIdBook));
		session.setAttribute(SessionAttributeName.CART, cart);

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String urlRequest = RequestManager.getInstance().getCurrentUrlRequest(
				request);

		return urlRequest;
	}
}
