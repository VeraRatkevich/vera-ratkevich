package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IGenreDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Genre;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;

/*
 * Class takes you to the page entering data about a new book. 
 * Also, a class method selects from the database all genres to display on the page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */
public class AddingBookCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		List<Genre> genres = null;

		IGenreDao dao = DaoFactory.getInstance().getGenreDao();
		try {
			genres = dao.showAll();
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_GENRES, e);
		}		
		request.setAttribute(RequestAttributeName.GENRES, genres);

		return JspPageName.ADDING_BOOK;

	}

}
