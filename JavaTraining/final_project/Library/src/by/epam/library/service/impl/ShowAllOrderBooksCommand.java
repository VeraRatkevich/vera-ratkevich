package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.OrderBookFullData;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.PageManager;

/*
 * Class is designed to output from a database of information on all order books. 
 * The books are displayed page by page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ShowAllOrderBooksCommand implements ICommand {

	private static final int COUNT_ORDER_BOOKS_ONE_PAGE = 6;

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		List<OrderBookFullData> books = null;

		IOrderBookDao dao = DaoFactory.getInstance().getOrderBookDao();

		int countOrderBooks = 0;

		try {
			countOrderBooks = dao.getCountNotClosed();
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_COUNT_ORDER_BOOKS, e);
		}
		
		if ((countOrderBooks == 0)) {

			request.setAttribute(RequestAttributeName.ERROR_ORDER_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_BOOKS));
			return JspPageName.ORDER_BOOKS_PAGE;

		} else if (countOrderBooks <= COUNT_ORDER_BOOKS_ONE_PAGE) {

			try {
				books = dao.showAllNotClosed();
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_ORDER_BOOKS, e);
			}

		} else {
			
			int countPages = (int) Math.ceil((double) countOrderBooks
					/ COUNT_ORDER_BOOKS_ONE_PAGE);
			
			String strPage = request.getParameter(RequestParameterName.PAGE);

			PageManager pageManager = new PageManager(countPages);

			pageManager.setCurrentPage(strPage);

			int numberFirstBook = (pageManager.getCurrentPage() - 1) * COUNT_ORDER_BOOKS_ONE_PAGE;

			try {
				books = dao.showAllNotClosedInLimit(numberFirstBook,
						COUNT_ORDER_BOOKS_ONE_PAGE);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_ORDER_BOOKS, e);

			}

			request.setAttribute(RequestAttributeName.PAGE_MANAGER, pageManager);

		}

		if (books != null) {
			request.setAttribute(RequestAttributeName.BOOKS, books);
		} else {
			request.setAttribute(RequestAttributeName.ERROR_ORDER_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_BOOKS));
		}
		return JspPageName.ORDER_BOOKS_PAGE;

	}
}
