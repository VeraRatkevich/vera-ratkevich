package by.epam.library.service.util;

/*
 * Class is designed for pagination.
 * Storage class information on the number of pages and the current page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class PageManager {

	private int[] pages;
	private int currentPage;

	public PageManager(int countPages) {

		pages = new int[countPages];

		for (int i = 0; i < countPages; i++) {
			pages[i] = (i + 1);

		}
	}

	public int[] getPages() {
		return pages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String strPage) {

		if (Validator.getInstance().validatePage(strPage)) {
			int page = Integer.parseInt(strPage);
			if (page <= pages.length) {
				this.currentPage = page;
			} else {
				this.currentPage = pages.length;
			}

		} else {
			currentPage = 1;
		}

	}


	/*
	 * private final static PageManager instance = new PageManager();
	 * 
	 * public PageManager() {
	 * 
	 * }
	 * 
	 * public static PageManager getInstance() {
	 * 
	 * return instance;
	 * 
	 * }
	 * 
	 * public int[] getPages(int countPages) {
	 * 
	 * 
	 * int[] pages = new int[countPages];
	 * 
	 * for (int i = 0; i < countPages; i++) { pages[i] = (i + 1);
	 * 
	 * } return pages; }
	 * 
	 * public int getCurrentPage(String strPage, int countPages){ int page; if
	 * ((strPage == null) || strPage.isEmpty()) { page = 1; } else { page =
	 * Integer.parseInt(strPage); }
	 * 
	 * if (page>countPages){ page = countPages; }
	 * 
	 * return page; }
	 */

}
