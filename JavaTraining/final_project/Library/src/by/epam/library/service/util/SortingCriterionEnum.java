package by.epam.library.service.util;

public enum SortingCriterionEnum {
	SURENAME_ASC, SURENAME_DESC, DATE_ASC, DATE_DESC
}
