package by.epam.library.service.impl;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Hashing;

/*
 * Class is designed for checking the user logs on to the system.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class LoginCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page;
		HttpSession session = request.getSession();
		String login = request.getParameter(RequestParameterName.LOGIN);

		String password = request.getParameter(RequestParameterName.PASSWORD);

		if (login.isEmpty() || password.isEmpty()) {
			request.setAttribute(RequestAttributeName.ERROR_LOGIN_MESSAGE,
					MessageManager.getMessage(JspPageMessage.EMPTY_FIELDS));
			page = JspPageName.LOGIN_PAGE;
			return page;
		}
		String hashPassword = null;
		try {
			hashPassword = Hashing.getHash(password);
		} catch (NoSuchAlgorithmException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_ENCRIPT_PASSWORD, e);
		}

		IUserDao dao = DaoFactory.getInstance().getUserDao();

		User user = null;
		try {
			user = dao.getByLoginPasword(login, hashPassword);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_USER,
					e);
		}

		if (user != null) {
			session.setAttribute(SessionAttributeName.USER_TYPE, user.getType());

			session.setAttribute(SessionAttributeName.USER_ID, user.getId());
			session.setAttribute(SessionAttributeName.USER_NAME, user.getName());

			request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
					DirectionTypeEnum.SEND_REDIRECT);
			page = UrlForRedirect.ABOUT;

		} else {

			request.setAttribute(
					RequestAttributeName.ERROR_LOGIN_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.LOGIN_PASSWORD_INCORRECT));
			page = JspPageName.LOGIN_PAGE;

		}

		return page;
	}
}
