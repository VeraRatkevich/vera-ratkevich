package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IBookDao;
import by.epam.library.dao.IGenreDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;
import by.epam.library.entity.Genre;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class provides way to the page for changing data of the book.
 * Also, a class method selects from the database all genres to display on the page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class BookEditorCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		String page = JspPageName.BOOK_EDITOR;
		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);

		if (!Validator.getInstance().validateId(strIdBook)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return page;
		}

		int idBook = Integer.parseInt(strIdBook);
		Book book = null;
		IBookDao bookDao = DaoFactory.getInstance().getBookDao();

		try {
			book = bookDao.getById(idBook);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_BOOK,
					e);
		}

		if (book == null) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.BOOK_NOT_AVALIABLE));
			return page;

		}

		List<Genre> genres = null;
		IGenreDao genreDao = DaoFactory.getInstance().getGenreDao();

		try {
			genres = genreDao.showAll();

		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_GENRES, e);
		}

		request.setAttribute(RequestAttributeName.GENRES, genres);
		request.setAttribute(RequestAttributeName.BOOK, book);

		return page;
	}

}
