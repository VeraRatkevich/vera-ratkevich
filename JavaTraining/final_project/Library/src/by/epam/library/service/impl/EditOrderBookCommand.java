package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.OrderBook;
import by.epam.library.entity.constant.OrderBookStatusEnum;
import by.epam.library.entity.constant.OrderStatusEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;
import by.epam.library.util.DateParser;

/*
 * Class is intended to modify the data of the order book in the database. 
 * Data are checked before sending to the database.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class EditOrderBookCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String strIdOrder = request.getParameter(RequestParameterName.ORDER_ID);
		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);
		String dateBegin = (request
				.getParameter(RequestParameterName.DATE_BEGIN));
		String dateEnd = request.getParameter(RequestParameterName.DATE_END);
		String dateExpect = request
				.getParameter(RequestParameterName.DATE_EXPECT);
		String dateReturn = request
				.getParameter(RequestParameterName.DATE_RETURN);
		String strStatus = request
				.getParameter(RequestParameterName.BOOK_STATUS);

		if (!validate(strIdOrder, strIdBook, dateBegin, dateEnd, dateExpect,
				dateReturn, strStatus)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		int idOrder = Integer.parseInt(strIdOrder);
		int idBook = Integer.parseInt(strIdBook);
		OrderBookStatusEnum status = null;

		try {
			status = OrderBookStatusEnum.valueOf(strStatus.toUpperCase());
		} catch (IllegalArgumentException e) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		OrderBook book = new OrderBook();

		book.setId(idBook);
		if (!dateBegin.isEmpty()) {
			book.setDateBegin(DateParser.parseToLocalDate(dateBegin));
		}
		if (!dateEnd.isEmpty()) {
			book.setDateEnd(DateParser.parseToLocalDate(dateEnd));
		}
		if (!dateExpect.isEmpty()) {
			book.setDateExpect(DateParser.parseToLocalDate(dateExpect));
		}
		if (!dateReturn.isEmpty()) {
			book.setDateReturn(DateParser.parseToLocalDate(dateReturn));
		}

		book.setStatus(status);

		IOrderBookDao orderBookDao = DaoFactory.getInstance().getOrderBookDao();

		int i = 0;
		try {
			i = orderBookDao.update(book, idOrder);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_EDIT_ORDER_BOOK, e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
			return JspPageName.USER_MESSAGE;
		}

		List<OrderBookStatusEnum> booksStatus = null;

		try {
			booksStatus = orderBookDao.getStatusByIdOrder(idOrder);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_BOOK_STATUS, e);
		}

		OrderStatusEnum oldOrderStatus = null;
		IOrderDao orderDao = DaoFactory.getInstance().getOrderDao();
		try {
			oldOrderStatus = orderDao.getStatus(idOrder);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_STATUS_ORDER, e);
		}

		OrderStatusEnum newOrderStatus = getOrderStatus(booksStatus);
		i = 0;
		if (oldOrderStatus != newOrderStatus) {
			try {
				i = orderDao.setStatus(idOrder, newOrderStatus);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_SET_STATUS_ORDER, e);
			}

		}

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String urlRequest = RequestManager.getInstance().getCurrentUrlRequest(
				request);

		return urlRequest;

	}

	private OrderStatusEnum getOrderStatus(List<OrderBookStatusEnum> statusList) {

		if (statusList.isEmpty()) {
			return null;
		}

		OrderStatusEnum statusOrder = null;

		int issuedCount = 0;
		int expectedCount = 0;
		int returnedCount = 0;

		for (OrderBookStatusEnum status : statusList) {

			switch (status) {
			case ISSUED:
				issuedCount++;
				break;
			case EXPECTED:
				expectedCount++;
				break;
			case RETURNED:
				returnedCount++;
				break;
			default:
				break;
			}
		}

		if (expectedCount == 0) {
			if (issuedCount != 0) {
				statusOrder = OrderStatusEnum.EXECUTED;
			} else {
				statusOrder = (returnedCount == 0) ? OrderStatusEnum.CANCELED
						: OrderStatusEnum.CLOSED;
			}

		} else if ((issuedCount == 0) && (returnedCount == 0)) {

			statusOrder = OrderStatusEnum.UNEXECUTED;

		} else {
			statusOrder = OrderStatusEnum.PARTIALLY;
		}

		return statusOrder;

	}

	private boolean validate(String strIdOrder, String strIdBook,
			String dateBegin, String dateEnd, String dateExpect,
			String dateReturn, String strStatus) {
		
		if ((dateBegin == null) || (dateEnd == null) || (dateExpect == null)
				|| (dateReturn == null) || (strStatus == null)
				|| (strStatus.isEmpty())) {
			return false;
		}

		Validator validator = Validator.getInstance();

		if ((!validator.validateId(strIdOrder))
				|| (!validator.validateId(strIdBook))
				|| (!validator.validateDate(dateBegin))) {
			return false;
		}
		if ((!dateEnd.isEmpty()) && (!validator.validateDate(dateEnd))) {
			return false;
		}
		if ((!dateExpect.isEmpty()) && (!validator.validateDate(dateExpect))) {
			return false;
		}
		if ((!dateReturn.isEmpty()) && (!validator.validateDate(dateReturn))) {
			return false;
		}
		return true;

	}

}
