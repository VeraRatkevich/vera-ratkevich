package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.EnumSetForJspPage;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Order;
import by.epam.library.entity.OrderBook;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class provides way to the page for changing data of the order book.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class OrderBookEditorCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		String page = JspPageName.ORDER_BOOK_EDITOR;

		String strIdOrder = request.getParameter(RequestParameterName.ORDER_ID);
		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);

		if ((!Validator.getInstance().validateId(strIdOrder))
				|| (!Validator.getInstance().validateId(strIdBook))) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return page;
		}

		int idOrder = Integer.parseInt(strIdOrder);
		int idBook = Integer.parseInt(strIdBook);

		Order order = null;
		IOrderDao orderDao = DaoFactory.getInstance().getOrderDao();
		try {

			order = orderDao.getByIdWithUser(idOrder);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_ORDER,
					e);
		}

		if (order == null) {
			request.setAttribute(RequestAttributeName.ERROR_ORDER_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.ORDER_NOT_AVAILABLE));
			return JspPageName.ORDER_DETAIL;
		}

		OrderBook book = null;
		IOrderBookDao dao = DaoFactory.getInstance().getOrderBookDao();

		try {
			book = dao.getById(idOrder, idBook);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_ORDER_BOOK, e);
		}

		if (book == null) {
			request.setAttribute(
					RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.ORDER_BOOK_NOT_AVAILABLE));
			return page;
		}

		request.setAttribute(RequestAttributeName.BOOK, book);
		request.setAttribute(RequestAttributeName.ORDER, order);
		request.setAttribute(RequestAttributeName.STATUS_ENUM,
				EnumSetForJspPage.statusEnum);

		return page;
	}

}
