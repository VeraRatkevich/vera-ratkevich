package by.epam.library.service.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/*
 * Class is used to encrypt the string using the MD5 encryption algorithm.
 * 
 * @author RatkevichVera
 * @version 1.0
 */
public class Hashing {
	private static final String MD5 = "MD5";

	private Hashing() {

	}

	public static String getHash(String st) throws NoSuchAlgorithmException {
	    MessageDigest messageDigest = null;
	    byte[] digest = new byte[0];
	 
	        messageDigest = MessageDigest.getInstance(MD5);
	        messageDigest.reset();
	        messageDigest.update(st.getBytes());
	        digest = messageDigest.digest();
	  
	 
	    BigInteger bigInt = new BigInteger(1, digest);
	    String md5Hex = bigInt.toString(16);
	 
	    while( md5Hex.length() < 32 ){
	        md5Hex = "0" + md5Hex;
	    }
	 
	    return md5Hex;
	}

}
