package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class is designed to remove a user from the database. 
 * Previously method of the class checks whether contained in a database of user orders.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class DeleteUserCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {


		String strIdUser = request.getParameter(RequestParameterName.USER_ID);

		if (!Validator.getInstance().validateId(strIdUser)) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		int idUser = Integer.parseInt(strIdUser);

		IOrderDao dao = DaoFactory.getInstance().getOrderDao();

		int countOrder = 0;
		try {
			countOrder = dao.getCountIdUser(idUser);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_COUNT_ORDERS, e);
		}

		if (countOrder > 0) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.OBJECT_CANNOT_DELETE));
			return JspPageName.USER_MESSAGE;

		}

		IUserDao userDao = DaoFactory.getInstance().getUserDao();

		int i = 0;
		try {
			i = userDao.delete(idUser);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_DELETE_USER, e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
			return JspPageName.USER_MESSAGE;

		}

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);
		String currentUrl = UrlForRedirect.USERS;

		return currentUrl;
	}
}
