package by.epam.library.service.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
 * Class is designed to validate the data entered by the user, the data of the hidden form fields.
 * Class methods produce test values to null and pattern matching.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class Validator {

	private final static String REGEX_LOGIN = "^[a-zA-Z0-9_-]{3,16}$";
	private final static String REGEX_PASSWORD = "^[a-zA-Z0-9_-]{6,18}$";
	private final static String REGEX_NAME = "^[А-Я]{1}[а-яА-Я-']{2,30}$";
	private final static String REGEX_PHONE = "^[(]{1}[0-9]{3}[)]{1}[0-9]{7}$";
	private final static String REGEX_AUTHOR = "^[а-яА-Я-.,\\s]{2,50}$";
	private final static String REGEX_PAGES = "^[0-9]{1,5}$";
	private final static String REGEX_YEAR = "^[1-9][0-9]{3}$";
	private final static String REGEX_DATE = "(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\\d\\d";
	private final static String REGEX_ID = "^[1-9][0-9]{0,5}$";

	private final Pattern patternLogin;
	private final Pattern patternPassword;
	private final Pattern patternName;
	private final Pattern patternPhone;
	private final Pattern patternAuthor;
	private final Pattern patternPages;
	private final Pattern patternYear;
	private final Pattern patternDate;
	private final Pattern patternNumber;

	private final static Validator instance = new Validator();

	private Validator() {
		patternLogin = Pattern.compile(REGEX_LOGIN);
		patternPassword = Pattern.compile(REGEX_PASSWORD);
		patternName = Pattern.compile(REGEX_NAME);
		patternPhone = Pattern.compile(REGEX_PHONE);
		patternAuthor = Pattern.compile(REGEX_AUTHOR);
		patternPages = Pattern.compile(REGEX_PAGES);
		patternYear = Pattern.compile(REGEX_YEAR);
		patternDate = Pattern.compile(REGEX_DATE);
		patternNumber = Pattern.compile(REGEX_ID);
	}

	public static Validator getInstance() {
		return instance;

	}

	public boolean validateLogin(String login) {
		if (login == null) {
			return false;
		}
		Matcher matcher = patternLogin.matcher(login);
		return matcher.matches();
	}

	public boolean validatePassword(String password) {
		if (password == null) {
			return false;
		}
		Matcher matcher = patternPassword.matcher(password);
		return matcher.matches();
	}

	public boolean validateDate(String date) {
		Matcher matcher = patternDate.matcher(date);
		return matcher.matches();
	}

	public boolean validateId(String id) {
		if (id == null) {
			return false;
		}
		Matcher matcher = patternNumber.matcher(id);
		return matcher.matches();
	}
	public boolean validatePage(String page) {
		if (page == null) {
			return false;
		}
		Matcher matcher = patternNumber.matcher(page);
		return matcher.matches();
	}
	private boolean validateName(String name) {
		Matcher matcher = patternName.matcher(name);
		return matcher.matches();
	}

	private boolean validatePhone(String phone) {
		Matcher matcher = patternPhone.matcher(phone);
		return matcher.matches();
	}

	private boolean validateAuthor(String author) {
		Matcher matcher = patternAuthor.matcher(author);
		return matcher.matches();
	}

	private boolean validatePages(String pages) {
		Matcher matcher = patternPages.matcher(pages);
		return matcher.matches();
	}

	private boolean validateYear(String year) {
		Matcher matcher = patternYear.matcher(year);
		return matcher.matches();
	}

	public boolean validateBookData(String nameBook, String author,
			String year, String idGenre, String pages) {

		if ((nameBook == null) || (author == null) || (year == null)
				|| (idGenre == null) || (pages == null) || nameBook.isEmpty()
				|| author.isEmpty() || year.isEmpty() || idGenre.isEmpty()
				|| pages.isEmpty()) {
			return false;
		}
		
		if ((!this.validateAuthor(author)) || (!this.validateYear(year))
				|| (!this.validatePages(pages)) || (!this.validateId(idGenre))) {

			return false;
		}
		return true;

	}

	public boolean validateUserData(String surename, String name,
			String patronymic, String address, String phoneNumber) {

		if ((surename == null) || (patronymic == null) || (address == null)
				|| (phoneNumber == null) || surename.isEmpty()
				|| name.isEmpty() || patronymic.isEmpty() || address.isEmpty()
				|| phoneNumber.isEmpty()) {
			return false;
		}

		if (!this.validateName(name) || !this.validateName(surename)
				|| !this.validateName(patronymic)
				|| !this.validatePhone(phoneNumber)) {

			return false;
		}
		return true;

	}
}
