package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.constant.OrderBookStatusEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class is designed to remove an order book from the database. 
 * Removal of the order book is possible provided that its status is canceled.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class DeleteOrderBookCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
			
		String strIdOrder = request.getParameter(RequestParameterName.ORDER_ID);
		String strIdBook = request.getParameter(RequestParameterName.BOOK_ID);
		
		if ((!Validator.getInstance().validateId(strIdOrder))
				|| (!Validator.getInstance().validateId(strIdBook))) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		int idOrder = Integer.parseInt(strIdOrder);
		int idBook = Integer.parseInt(strIdBook);
		OrderBookStatusEnum status = null;
		
		IOrderBookDao orderBookDao = DaoFactory.getInstance().getOrderBookDao();	
		
		try{
			status = orderBookDao.getStatus(idOrder, idBook);
		}catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_BOOK_STATUS, e);
		}
		
		if ((status == null)||(status != OrderBookStatusEnum.CANCELED)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.OBJECT_CANNOT_DELETE));
			return JspPageName.USER_MESSAGE;
		}
		

		int i = 0;
		try {
			i = orderBookDao.delete(idOrder, idBook);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_DELETE_ORDER_BOOK, e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
			return JspPageName.USER_MESSAGE;

		}

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String urlRequest = UrlForRedirect.ORDER_DETAIL+idOrder;

		return urlRequest;

	}
}
