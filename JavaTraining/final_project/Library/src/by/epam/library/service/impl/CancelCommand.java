package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.exception.ServiceException;

/*
 *Class provides a URL of the previous request.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class CancelCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String urlRequest = RequestManager.getInstance().getCurrentUrlRequest(
				request);

		return urlRequest;

	}

}
