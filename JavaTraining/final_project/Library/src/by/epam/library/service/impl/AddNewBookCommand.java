package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IBookDao;
import by.epam.library.dao.IGenreDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;
import by.epam.library.entity.Genre;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class is designed to add a new book to the database. 
 * A class method also validates the data entered by the user.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class AddNewBookCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String nameBook = request.getParameter(RequestParameterName.NAME);

		String author = request.getParameter(RequestParameterName.BOOK_AUTHOR);
		String year = request.getParameter(RequestParameterName.BOOK_YEAR);
		String idGenre = request
				.getParameter(RequestParameterName.BOOK_GENRE_ID);
		String pages = request.getParameter(RequestParameterName.BOOK_PAGES);
		
		
		if (!Validator.getInstance().validateBookData(nameBook, author, year, idGenre, pages)) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_DATA));		
			
			List<Genre> genres = null;

			IGenreDao dao = DaoFactory.getInstance().getGenreDao();
			try {
				genres = dao.showAll();
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_GENRES, e);
			}		
			
			request.setAttribute(RequestAttributeName.GENRES, genres);
			
			
			return JspPageName.ADDING_BOOK;
		}

		Book book = new Book();

		book.setName(nameBook);
		book.setAuthor(author);
		book.setYear(Integer.parseInt(year));
		book.getGenre().setId(Integer.parseInt(idGenre));
		book.setPages(Integer.parseInt(pages));

		IBookDao dao = DaoFactory.getInstance().getBookDao();

		int i = 0;
		
		try {
			i = dao.add(book);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_ADD_BOOK,
					e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.REQUEST));
			return JspPageName.USER_MESSAGE;
		}

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE, DirectionTypeEnum.SEND_REDIRECT);
		String currentUrl = UrlForRedirect.BOOKS;

		return currentUrl;
	}

}
