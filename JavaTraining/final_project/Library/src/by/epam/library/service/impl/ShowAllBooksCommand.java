package by.epam.library.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.EnumSetForJspPage;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.PageManager;

/*
 * Class is designed to output from a database of information on all books. 
 * The books are displayed page by page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ShowAllBooksCommand implements ICommand {
	private static final int COUNT_BOOKS_ONE_PAGE = 8;

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		List<Book> books = null;

		int countBooks = 0;

		IBookDao dao = DaoFactory.getInstance().getBookDao();
		try {
			countBooks = dao.getCountBooks();
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_COUNT_BOOKS, e);
		}

		if ((countBooks == 0)) {

			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_BOOKS));
			return JspPageName.BOOKS_PAGE;

		} else if (countBooks <= COUNT_BOOKS_ONE_PAGE) {

			try {
				books = dao.showAll();
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_BOOKS, e);

			}

		} else {

			String strPage = request.getParameter(RequestParameterName.PAGE);

			int countPages = (int) Math.ceil((double) countBooks
					/ COUNT_BOOKS_ONE_PAGE);

			PageManager pageManager = new PageManager(countPages);

			pageManager.setCurrentPage(strPage);

			int numberFirstBook = (pageManager.getCurrentPage() - 1)
					* COUNT_BOOKS_ONE_PAGE;

			try {
				books = dao.showAllInLimit(numberFirstBook,
						COUNT_BOOKS_ONE_PAGE);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_BOOKS, e);

			}

			request.setAttribute(RequestAttributeName.PAGE_MANAGER, pageManager);

		}

		if (books != null) {

			Set<Integer> cart = null;
			try {
				cart = (HashSet<Integer>) request.getSession().getAttribute(
						SessionAttributeName.CART);
			} catch (NumberFormatException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_CART);
			}
			
			if ((cart != null) && (!cart.isEmpty())) {
				setBookInCart(books, cart);
			}
			
			request.setAttribute(RequestAttributeName.BOOKS, books);

		} else {

			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.NO_BOOKS));
		}

		request.setAttribute(RequestAttributeName.CATEGORY_ENUM,
				EnumSetForJspPage.categoryEnum);

		return JspPageName.BOOKS_PAGE;
	}
	
	private void setBookInCart(List<Book> bookList, Set<Integer> cart){
		for(Book book:bookList){
			for(Integer id:cart){
				
				if(id.intValue()==book.getId()){
					book.setInCart(true);
				}				
			}
		}		
		
		
		
	}
}
