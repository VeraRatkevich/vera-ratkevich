package by.epam.library.service.impl;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.entity.constant.UserTypeEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Hashing;
import by.epam.library.service.util.Validator;
/*
 * The class is designed to register a new user in the database. 
 * The data entered by the user is tested. 
 * A class method checks, if not registered in the user database with the same login.
 * 
 * @author RatkevichVera
 * @version 1.0
 */
public class RegistrationCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = JspPageName.REGISTRATION_PAGE;
		

		String login = request.getParameter(RequestParameterName.LOGIN);
		String password = (request.getParameter(RequestParameterName.PASSWORD));
		String repeatedPassword = (request
				.getParameter(RequestParameterName.REPEATED_PASSWORD));
		String surename = request.getParameter(RequestParameterName.SURENAME);
		String name = request.getParameter(RequestParameterName.NAME);
		String patronymic = request
				.getParameter(RequestParameterName.PATRONYMIC);
		String address = request.getParameter(RequestParameterName.ADDRESS);
		String phoneNumber = request
				.getParameter(RequestParameterName.PHONE_NUMBER);

		
		Validator validator = Validator.getInstance();

		if ((!validator.validateLogin(login))
				||(!validator.validatePassword(password))
				||(!validator.validateUserData(surename, name, patronymic, address, phoneNumber))) {

			request.setAttribute(RequestAttributeName.ERROR_REGISTER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_DATA));
			return page;
		}	
		
		if (!password.equals(repeatedPassword)) {
			request.setAttribute(RequestAttributeName.ERROR_REGISTER_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.PASSWORDS_MISMATCH));
			return page;
		}
		

		IUserDao dao = DaoFactory.getInstance().getUserDao();

		int idUserSameLogin = 0;

		try {
			idUserSameLogin = dao.getIdUserByLogin(login);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_USER,
					e);
		}

		if (idUserSameLogin != 0) {
			request.setAttribute(RequestAttributeName.ERROR_LOGIN_MESSAGE,
					MessageManager.getMessage(JspPageMessage.USER_EXISTS));
			return page;

		}

		String hashPassword = null;
		try {
			hashPassword = Hashing.getHash(password);
		} catch (NoSuchAlgorithmException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_ENCRIPT_PASSWORD, e);
		}
		
		User user = new User();

		user.setSurename(surename);
		user.setName(name);
		user.setPatronymic(patronymic);
		user.setAddress(address);
		user.setPhoneNumber(phoneNumber);
		user.setLogin(login);
		user.setPassword(hashPassword);

		user.setType(UserTypeEnum.READER);
	
		int i = 0;
		try {
			i = dao.add(user);
		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_ADD_USER,
					e);
		}

		if (i == 0) {
			request.setAttribute(RequestAttributeName.ERROR_REGISTER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
			return page;

		}

		int idUser = 0;

		try {
			idUser = dao.getIdUserByLogin(user.getLogin());
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_GET_ID_USER, e);
		}

		if (idUser == 0) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_ADD_USER);
		}

		HttpSession session = request.getSession();
		session.setAttribute(SessionAttributeName.USER_TYPE, user.getType());
		session.setAttribute(SessionAttributeName.USER_ID, idUser);
		session.setAttribute(SessionAttributeName.USER_NAME, user.getName());

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);
		String currentUrl = UrlForRedirect.ABOUT;

		return currentUrl;
	}
	
	
	
	
}
