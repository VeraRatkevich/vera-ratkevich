package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class is designed to remove an order from the database. 
 * Removal of the order is possible provided that it does not contain the order books.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class DeleteOrderCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page;

		String strIdOrder = request.getParameter(RequestParameterName.ORDER_ID);
		
		if (!Validator.getInstance().validateId(strIdOrder)) {
			request.setAttribute(RequestAttributeName.ERROR_ORDER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return JspPageName.USER_MESSAGE;
		}

		int idOrder = Integer.parseInt(strIdOrder);

		IOrderBookDao orderBookDao = DaoFactory.getInstance().getOrderBookDao();
		int countBooksByOrder = 0;

		try {
			countBooksByOrder = orderBookDao.getCountBooksByOrder(idOrder);
		} catch (DaoException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_COUNT_ORDER_BOOKS, e);
		}

		if (countBooksByOrder == 0) {
			IOrderDao orderDao = DaoFactory.getInstance().getOrderDao();
			int i = 0;
			try {
				i = orderDao.delete(idOrder);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_COUNT_ORDER_BOOKS, e);
			}

			if (i == 0) {
				request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
						MessageManager.getMessage(JspPageMessage.ACTION_FAILED));
				page = JspPageName.USER_MESSAGE;

			} else {
				request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
						DirectionTypeEnum.SEND_REDIRECT);
				page = UrlForRedirect.ORDERS;
			}

		} else {
			request.setAttribute(RequestAttributeName.ERROR_ORDER_MESSAGE,
					MessageManager
							.getMessage(JspPageMessage.OBJECT_CANNOT_DELETE));
			page = JspPageName.USER_MESSAGE;
		}

		return page;
	}

}
