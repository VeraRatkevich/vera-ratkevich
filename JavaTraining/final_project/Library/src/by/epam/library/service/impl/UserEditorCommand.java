package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.EnumSetForJspPage;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IUserDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.Validator;

/*
 * Class provides way to the page for changing data of the user.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class UserEditorCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);

		String page = JspPageName.USER_EDITOR;
		int idUser = 0;
		String strIdUser = request.getParameter(RequestParameterName.USER_ID);

		if ((strIdUser == null)
				|| (!Validator.getInstance().validateId(strIdUser))) {
			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.INVALID_VALUE));
			return page;
		}

		idUser = Integer.parseInt(strIdUser);

		User user = null;

		IUserDao dao = DaoFactory.getInstance().getUserDao();


		try {
			user = dao.getById(idUser);

		} catch (DaoException e) {
			throw new ServiceException(ServiceExceptionMessage.ERROR_GET_USER,
					e);
		}


		if (user != null) {

			request.setAttribute(RequestAttributeName.USER_TYPE_ENUM,
					EnumSetForJspPage.userTypeEnum);

			request.setAttribute(RequestAttributeName.USER, user);

		} else {

			request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE,
					MessageManager.getMessage(JspPageMessage.USER_NOT_EXIST));
		}
		return page;
	}
}
