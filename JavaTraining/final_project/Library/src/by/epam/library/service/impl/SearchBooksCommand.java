package by.epam.library.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.EnumSetForJspPage;
import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.UrlForRedirect;
import by.epam.library.dao.DaoFactory;
import by.epam.library.dao.IBookDao;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.constant.ServiceExceptionMessage;
import by.epam.library.service.exception.ServiceException;
import by.epam.library.service.util.SearchCategoryEnum;

/*
 * Class is designed to search for books in the database by keyword and category entered by the user.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class SearchBooksCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		RequestManager.getInstance().saveUrlRequest(request);
		String page = null;

		String keyword = request.getParameter(RequestParameterName.KEYWORD);
		String category = request.getParameter(RequestParameterName.CATEGORY);

		if ((keyword == null) || (category == null) || (keyword.isEmpty())
				|| (category.isEmpty())) {

			request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
					DirectionTypeEnum.SEND_REDIRECT);
			return UrlForRedirect.BOOKS;
		}

		SearchCategoryEnum searchCategory = null;

		try {
			searchCategory = SearchCategoryEnum.valueOf(category);
		} catch (IllegalArgumentException e) {
			throw new ServiceException(
					ServiceExceptionMessage.ERROR_CONVERTION_SEARCH_CATEGORY, e);
		}

		List<Book> books = null;
		IBookDao dao = DaoFactory.getInstance().getBookDao();

		switch (searchCategory) {
		case NAME:
			try {
				books = dao.showBooksByName(keyword);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_BOOKS, e);
			}
			break;
		case AUTHOR:
			try {
				books = dao.showBooksByAuthor(keyword);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_BOOKS, e);
			}
			break;
		case GENRE:
			try {
				books = dao.showBooksByGenre(keyword);
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_BOOKS, e);
			}
			break;
		default:
			try {
				books = dao.showAll();
			} catch (DaoException e) {
				throw new ServiceException(
						ServiceExceptionMessage.ERROR_GET_BOOKS, e);
			}

			break;

		}

		if (books != null) {
			request.setAttribute(RequestAttributeName.BOOKS, books);

		} else {
			request.setAttribute(RequestAttributeName.ERROR_BOOK_MESSAGE,
					MessageManager.getMessage(JspPageMessage.BOOKS_NOT_FOUND));
		}

		request.setAttribute(RequestParameterName.KEYWORD, keyword);
		request.setAttribute(RequestParameterName.CATEGORY, category);
		request.setAttribute(RequestAttributeName.CATEGORY_ENUM,
				EnumSetForJspPage.categoryEnum);
		page = JspPageName.BOOKS_PAGE;
		return page;
	}
}
