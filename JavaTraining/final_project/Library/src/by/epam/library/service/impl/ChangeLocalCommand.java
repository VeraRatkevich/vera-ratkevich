package by.epam.library.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.ICommand;
import by.epam.library.service.util.RequestManager;
import by.epam.library.service.exception.ServiceException;

/*
 *The class is designed to change the locale and provide a URL of the previous request.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ChangeLocalCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String local = request.getParameter(RequestParameterName.LOCAL);
		request.getSession(true)
				.setAttribute(SessionAttributeName.LOCAL, local);

		MessageManager.setLanguage(local);

		request.setAttribute(RequestAttributeName.DIRECTION_TYPE,
				DirectionTypeEnum.SEND_REDIRECT);

		String urlRequest = RequestManager.getInstance().getCurrentUrlRequest(
				request);
		return urlRequest;

	}

}
