package by.epam.library.entity;

import java.io.Serializable;

public class Book implements Serializable{

	private static final long serialVersionUID = -1664452437886679385L;
	
	private int id;
	private String name;
	private String author;
	private int year;
	private Genre genre;
	private int pages;

	private boolean inCart;

	public Book() {
		genre = new Genre();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public boolean isInCart() {
		return inCart;
	}

	public void setInCart(boolean inCart) {
		this.inCart = inCart;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (id != other.id)
			return false;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}

		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author)) {
			return false;
		}

		if (year != other.year)
			return false;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;

		if (pages != other.pages)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * id + 5 * year + pages
				+ ((name == null) ? 0 : name.hashCode())
				+ ((author == null) ? 0 : author.hashCode()) + ((genre == null) ? 0
					: genre.hashCode()));

	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Book [").append(" id: ").append(id).append(" name ")
				.append(name).append(" author ").append(author)
				.append(" year ").append(year).append(" genre ")
				.append(genre.getName()).append(" pages ").append(pages)
				.append("]");

		return builder.toString();

	}
}
