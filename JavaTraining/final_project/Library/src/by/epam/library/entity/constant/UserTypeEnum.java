package by.epam.library.entity.constant;

public enum UserTypeEnum {
	GUEST,
	READER,
	LIBRARIAN
}
