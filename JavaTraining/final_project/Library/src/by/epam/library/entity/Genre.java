package by.epam.library.entity;

import java.io.Serializable;

public class Genre implements Serializable{

	private static final long serialVersionUID = 4440094710130112379L;
	
	private int id;
	private String name;

	public Genre() {

	}

	public Genre(int id, String name) {
		setId(id);
		setName(name);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Genre other = (Genre) obj;
		if (id != other.id)
			return false;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * id + ((name == null) ? 0 : name.hashCode()));

	}

	@Override
	public String toString() {
		return "Genre: id " + id + " name " + name;
	}
}
