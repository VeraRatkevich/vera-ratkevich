package by.epam.library.entity;

import java.io.Serializable;

public class OrderBookFullData implements Serializable {

	private static final long serialVersionUID = -666448897923050119L;

	private int orderId;
	private String readerName;
	private String readerSurename;
	private OrderBook orderBook;

	public OrderBookFullData() {

	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getReaderName() {
		return readerName;
	}

	public void setReaderName(String readerName) {
		this.readerName = readerName;
	}

	public String getReaderSurename() {
		return readerSurename;
	}

	public void setReaderSurename(String readerSurename) {
		this.readerSurename = readerSurename;
	}

	public OrderBook getOrderBook() {
		return orderBook;
	}

	public void setOrderBook(OrderBook orderBook) {
		this.orderBook = orderBook;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderBookFullData other = (OrderBookFullData) obj;
		if (orderId != other.orderId)
			return false;

		if (readerName == null) {
			if (other.readerName != null)
				return false;
		} else if (!readerName.equals(other.readerName)) {
			return false;
		}

		if (readerSurename == null) {
			if (other.readerSurename != null)
				return false;
		} else if (!readerSurename.equals(other.readerSurename)) {
			return false;
		}

		if (orderBook == null) {
			if (other.orderBook != null)
				return false;
		} else if (!orderBook.equals(other.orderBook))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * orderId
				+ ((readerName == null) ? 0 : readerName.hashCode())
				+ ((readerSurename == null) ? 0 : readerSurename.hashCode()) + ((orderBook == null) ? 0
					: orderBook.hashCode()));

	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("OrderBookFullData [").append(" orderId: ")
				.append(orderId).append(" reader name ").append(readerName)
				.append(" reader surename ").append(readerSurename)
				.append(" order book ").append(orderBook).append("]");

		return builder.toString();

	}

}
