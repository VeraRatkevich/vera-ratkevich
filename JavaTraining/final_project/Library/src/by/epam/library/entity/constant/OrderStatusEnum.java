package by.epam.library.entity.constant;

public enum OrderStatusEnum {
	
		UNEXECUTED, EXECUTED, PARTIALLY, CANCELED, CLOSED
		
}
