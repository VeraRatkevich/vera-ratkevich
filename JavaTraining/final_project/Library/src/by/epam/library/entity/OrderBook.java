package by.epam.library.entity;

import java.time.LocalDate;

import by.epam.library.entity.constant.OrderBookStatusEnum;

public class OrderBook extends Book {

	private static final long serialVersionUID = 4649970712748422578L;

	private LocalDate dateBegin;
	private LocalDate dateEnd;
	private LocalDate dateExpect;
	private LocalDate dateReturn;
	private OrderBookStatusEnum status;

	public OrderBook() {

	}

	public LocalDate getDateBegin() {
		return dateBegin;
	}

	public void setDateBegin(LocalDate dateBegin) {
		this.dateBegin = dateBegin;
	}

	public LocalDate getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(LocalDate dateEnd) {
		this.dateEnd = dateEnd;
	}

	public LocalDate getDateExpect() {
		return dateExpect;
	}

	public void setDateExpect(LocalDate dateExpect) {
		this.dateExpect = dateExpect;
	}

	public LocalDate getDateReturn() {
		return dateReturn;
	}

	public void setDateReturn(LocalDate dateReturn) {
		this.dateReturn = dateReturn;
	}

	public OrderBookStatusEnum getStatus() {
		return status;
	}

	public void setStatus(OrderBookStatusEnum status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderBook other = (OrderBook) obj;
		if (dateBegin == null) {
			if (other.dateBegin != null)
				return false;
		} else if (!dateBegin.equals(other.dateBegin))
			return false;
		if (dateEnd == null) {
			if (other.dateEnd != null)
				return false;
		} else if (!dateEnd.equals(other.dateEnd))
			return false;
		if (dateExpect == null) {
			if (other.dateExpect != null)
				return false;
		} else if (!dateExpect.equals(other.dateExpect))
			return false;
		if (dateReturn == null) {
			if (other.dateReturn != null)
				return false;
		} else if (!dateReturn.equals(other.dateReturn))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (super.hashCode()
				+ +((dateBegin == null) ? 0 : dateBegin.hashCode())
				+ ((dateEnd == null) ? 0 : dateEnd.hashCode())
				+ ((dateExpect == null) ? 0 : dateExpect.hashCode())
				+ ((dateReturn == null) ? 0 : dateReturn.hashCode()) + ((status == null) ? 0
					: status.hashCode()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrderBook [").append(super.toString())
				.append(" dateBegin ").append(dateBegin).append(" dateEnd ")
				.append(dateEnd).append(" dateExpect ").append(dateExpect)
				.append(" dateReturn ").append(dateReturn).append(" status ")
				.append(status).append("]");
		return builder.toString();
	}
}
