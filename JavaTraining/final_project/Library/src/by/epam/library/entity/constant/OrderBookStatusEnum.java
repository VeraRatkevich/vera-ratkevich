package by.epam.library.entity.constant;

public enum OrderBookStatusEnum {
	ISSUED, EXPECTED, RETURNED, CANCELED
}
