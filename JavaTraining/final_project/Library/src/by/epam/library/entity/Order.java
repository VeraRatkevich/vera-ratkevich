package by.epam.library.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import by.epam.library.entity.constant.OrderStatusEnum;

public class Order implements Serializable{

	private static final long serialVersionUID = -6188470719878620499L;
	
	private int id;
	private User user;
	private LocalDate date;
	private OrderStatusEnum status;
	private List<OrderBook> books;

	public Order() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public OrderStatusEnum getStatus() {
		return status;
	}

	public void setStatus(OrderStatusEnum status) {
		this.status = status;
	}

	public List<OrderBook> getBooks() {

		return books;
	}

	public void setBooks(List<OrderBook> books) {
		this.books = books;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (id != other.id)
			return false;

		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user)) {
			return false;
		}

		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date)) {
			return false;
		}

		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;

		if (books == null) {
			if (other.books != null)
				return false;
		} else if (!books.equals(other.books))
			return false;

		return true;
	}


	@Override
	public int hashCode() {
		return (int) (31 * id + 
				+ ((user == null) ? 0 : user.hashCode())
				+ ((date == null) ? 0 : date.hashCode()) 
				+ ((status == null) ? 0	: status.hashCode())
				+ ((books == null) ? 0	: books.hashCode()));
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Order [").append(" id: ").append(id).append(" user ")
				.append(user).append(" date ").append(date)
				.append(" status ").append(status).append(" books ").append(books)
				.append("]");

		return builder.toString();
	}
}
