package by.epam.library.resources;

import java.util.Locale;

public enum LocalEnum {
	RU {
		{
			this.current = new Locale("ru");
		}
	},
	EN {
		{
			this.current = new Locale("en");
		}
	};

	Locale current;

	public Locale getCurrent() {
		return current;
	}
}
