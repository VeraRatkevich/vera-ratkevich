package by.epam.library.exception;

public class ProjectException extends Exception {

	private static final long serialVersionUID = 1L;
	private Exception hiddenException;

	public ProjectException() {
		super();
	}

	public ProjectException(String msg) {
		super(msg);
	}

	public ProjectException(String msg, Exception e) {
		super(msg);
		hiddenException = e;
	}

	public ProjectException(Exception e) {
		hiddenException = e;
	}

	public Exception getHiddenException() {
		return hiddenException;

	}
}
