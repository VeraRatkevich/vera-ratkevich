package by.epam.library.controller.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.epam.library.entity.constant.UserTypeEnum;
import by.epam.library.service.CommandName;

/*
 * Class is designed for checking the legality of the user's access to the action.
 * 
 * Field:
 * Map commands - stores a collection of types of users who have access rights to command and command name as a key.
 *  
 * Method:
 * chekAccessToCommand(CommandName commandName, UserTypeEnum userType) -method checks whether a user can use the requested command.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class ServletSecurityFilterHelper {
	private static final ServletSecurityFilterHelper instance = new ServletSecurityFilterHelper();
	private final Map<CommandName, List<UserTypeEnum>> commands = new HashMap<>();

	public ServletSecurityFilterHelper() {

		List<UserTypeEnum> allList = new ArrayList<UserTypeEnum>();
		allList.add(UserTypeEnum.GUEST);
		allList.add(UserTypeEnum.READER);
		allList.add(UserTypeEnum.LIBRARIAN);

		List<UserTypeEnum> onlyLibrarianList = new ArrayList<UserTypeEnum>();
		onlyLibrarianList.add(UserTypeEnum.LIBRARIAN);

		List<UserTypeEnum> onlyReaderList = new ArrayList<UserTypeEnum>();
		onlyReaderList.add(UserTypeEnum.READER);

		List<UserTypeEnum> onlyGuestList = new ArrayList<UserTypeEnum>();
		onlyGuestList.add(UserTypeEnum.GUEST);

		List<UserTypeEnum> guestAndReaderList = new ArrayList<UserTypeEnum>();
		guestAndReaderList.add(UserTypeEnum.GUEST);
		guestAndReaderList.add(UserTypeEnum.READER);

		List<UserTypeEnum> readerAndLibrarianList = new ArrayList<UserTypeEnum>();
		readerAndLibrarianList.add(UserTypeEnum.READER);
		readerAndLibrarianList.add(UserTypeEnum.LIBRARIAN);

		commands.put(CommandName.LOGIN, onlyGuestList);
		commands.put(CommandName.LOGOUT, readerAndLibrarianList);
		commands.put(CommandName.REGISTRATION, onlyGuestList);
		commands.put(CommandName.LOCAL, allList);
		commands.put(CommandName.BOOKS, allList);
		commands.put(CommandName.ORDERS, onlyLibrarianList);
		commands.put(CommandName.USERS, onlyLibrarianList);
		commands.put(CommandName.PROFILE, readerAndLibrarianList);
		commands.put(CommandName.ADD_TO_CART, onlyReaderList);
		commands.put(CommandName.CART_CONFIRM, onlyReaderList);
		commands.put(CommandName.CLEAR_CART, onlyReaderList);
		commands.put(CommandName.REMOVE_FROM_CART, onlyReaderList);
		commands.put(CommandName.CART, onlyReaderList);
		commands.put(CommandName.ORDER_DETAIL, readerAndLibrarianList);
		commands.put(CommandName.ORDER_BOOK_EDITOR, onlyLibrarianList);
		commands.put(CommandName.EDIT_ORDER_BOOK, onlyLibrarianList);
		commands.put(CommandName.USER_EDITOR, onlyLibrarianList);
		commands.put(CommandName.ABOUT, allList);
		commands.put(CommandName.LOGIN_PAGE, onlyGuestList);
		commands.put(CommandName.REGISTRATION_PAGE, onlyGuestList);
		commands.put(CommandName.BOOK_EDITOR, onlyLibrarianList);
		commands.put(CommandName.ADDING_BOOK, onlyLibrarianList);
		commands.put(CommandName.ADD_NEW_BOOK, onlyLibrarianList);
		commands.put(CommandName.DELETE_BOOK, onlyLibrarianList);
		commands.put(CommandName.DELETE_ORDER_BOOK, onlyLibrarianList);
		commands.put(CommandName.EDIT_USER, onlyLibrarianList);
		commands.put(CommandName.DELETE_USER, onlyLibrarianList);
		commands.put(CommandName.EDIT_BOOK, onlyLibrarianList);
		commands.put(CommandName.DATE_EXPECT_EDITOR, onlyReaderList);
		commands.put(CommandName.EDIT_DATE_EXPECT, onlyReaderList);
		commands.put(CommandName.SEARCH_BOOKS, allList);
		commands.put(CommandName.USER_ORDERS, onlyReaderList);
		commands.put(CommandName.ORDER_BOOKS, onlyLibrarianList);
		commands.put(CommandName.DELETE_ORDER, onlyLibrarianList);
		commands.put(CommandName.CANCEL, allList);
	}

	public static ServletSecurityFilterHelper getInstance() {

		return instance;

	}

	public boolean chekAccessToCommand(CommandName commandName,
			UserTypeEnum userType) {

		List<UserTypeEnum> commandUserTypesList = commands.get(commandName);

		for (UserTypeEnum commandUserType : commandUserTypesList) {
			if (commandUserType == userType) {
				return true;
			}

		}
		return false;
	}
}
