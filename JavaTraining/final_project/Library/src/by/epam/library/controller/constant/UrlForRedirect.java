package by.epam.library.controller.constant;

public class UrlForRedirect {
	public static final String BOOKS = "http://localhost:8080/Library/Controller?command=books";
	public static final String USERS = "http://localhost:8080/Library/Controller?command=users";
	public static final String ORDERS = "http://localhost:8080/Library/Controller?command=orders";
	public static final String USER_ORDERS = "http://localhost:8080/Library/Controller?command=user-orders";
	public static final String ORDER_DETAIL = "http://localhost:8080/Library/Controller?command=order-detail&idOrder=";
	public static final String HEADER_PATH = "http://localhost:8080/Library/Controller";
	public static final String ABOUT = "http://localhost:8080/Library/Controller?command=about";
	
}
