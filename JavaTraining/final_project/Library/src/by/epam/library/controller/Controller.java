package by.epam.library.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import by.epam.library.controller.constant.DirectionTypeEnum;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.service.ICommand;
import by.epam.library.service.exception.ServiceException;

public final class Controller extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(Controller.class
			.getName());

	private static final long serialVersionUID = 1L;

	public Controller() {

		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);

	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String commandName = request.getParameter(RequestParameterName.COMMAND);

		ICommand command = ControllerCommandHelper.getInstance().getCommand(
				commandName);

		String page = null;
		try {
			page = command.execute(request);

		} catch (ServiceException e) {
			logger.error(e.getMessage(), e.getHiddenException());
			page = JspPageName.ERROR_PAGE;

		} catch (Exception e) {
			logger.error(e);
			page = JspPageName.ERROR_PAGE;

		}
		
		if (page == null) {
			page = JspPageName.INDEX_PAGE;
		}
		
		DirectionTypeEnum directionType = (DirectionTypeEnum) request
				.getAttribute(RequestAttributeName.DIRECTION_TYPE);

		if (directionType == DirectionTypeEnum.SEND_REDIRECT) {
			response.sendRedirect(page);
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher(page);
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			} else {
				errorMessageDireclyFromresponse(response);
			}
		}

	}

	private void errorMessageDireclyFromresponse(HttpServletResponse response)
			throws IOException {
		response.setContentType("text/html");
		response.getWriter().println("E R R O R");
	}
}
