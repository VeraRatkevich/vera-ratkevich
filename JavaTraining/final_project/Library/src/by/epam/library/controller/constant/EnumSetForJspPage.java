package by.epam.library.controller.constant;

import java.util.EnumSet;

import by.epam.library.entity.constant.OrderBookStatusEnum;
import by.epam.library.entity.constant.UserTypeEnum;
import by.epam.library.service.util.SearchCategoryEnum;
import by.epam.library.service.util.SortingCriterionEnum;

public class EnumSetForJspPage {

	public static EnumSet<SearchCategoryEnum> categoryEnum = EnumSet
			.allOf(SearchCategoryEnum.class);

	public static EnumSet<OrderBookStatusEnum> statusEnum = EnumSet
			.allOf(OrderBookStatusEnum.class);
	
	public static EnumSet<UserTypeEnum> userTypeEnum = EnumSet.of(
			UserTypeEnum.READER, UserTypeEnum.LIBRARIAN);
	
	public static EnumSet<SortingCriterionEnum> criterionEnum = EnumSet
			.allOf(SortingCriterionEnum.class);
}
