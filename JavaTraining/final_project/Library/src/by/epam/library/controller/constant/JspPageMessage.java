package by.epam.library.controller.constant;

public class JspPageMessage {
	public static final String ACCESS_DENIED = "message.access.denied";
	public static final String EMPTY_FIELDS = "message.empty.fields";
	public static final String LOGIN_PASSWORD_INCORRECT = "message.login.password.incorrect";
	public static final String PASSWORDS_MISMATCH = "message.password.mismatch";
	public static final String USER_EXISTS = "message.user.exists";
	public static final String USER_ROLE_UNKNOWN = "message.user.role.unknown";
	public static final String WRONG_ACTION = "message.wrong.action";
	public static final String NULL_PAGE = "message.nullpage";
	public static final String INVALID_VALUE = "message.amount.invalid";
	public static final String BOOK_NOT_AVALIABLE = "message.book.empty";
	public static final String REQUEST = "message.exception.request";
	public static final String STATUS_CODE = "message.exception.status.code";
	public static final String EXCEPTION_MESSAGE = "message.exception";
	public static final String SUCCESS_MESSAGE = "message.success";
	public static final String CART_EMPTY_MESSAGE = "message.cart.empty";
	public static final String CART_CONFIRM_SUCCESS_MESSAGE = "message.cart.comfirm.success";
	public static final String OBJECT_CANNOT_DELETE = "message.cannot.delete";
	public static final String USER_NOT_EXIST = "message.user.not.exist";
	public static final String CRITERIA_NOT_DEFINED = "message.criteria.not.defined";
	public static final String BOOKS_NOT_FOUND = "message.books.not.found";
	public static final String NO_BOOKS = "message.books.no";
	public static final String NO_USERS = "message.users.no";
	public static final String NO_ORDERS = "message.orders.no";
	public static final String INVALID_DATA = "message.validation.invalid";
	public static final String ACTION_FAILED = "message.action.failed";
	public static final String OBJECT_REMOVED = "message.object.removed";
	public static final String BOOK_ALREADY_ORDERED = "message.book.ordered";
	public static final String ORDER_BOOK_NOT_AVAILABLE = " message.no.order.book";
	public static final String ORDER_NOT_AVAILABLE = "message.no.order.book";
	public static final String ORDER_STATUS_ERROR = "message.order.status.error";
}
