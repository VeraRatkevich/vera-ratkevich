package by.epam.library.controller.constant;

public class RequestAttributeName {
	public static final String ERROR_LOGIN_MESSAGE = "errorLoginMessage";
	public static final String ERROR_REGISTER_MESSAGE = "errorRegisterMessage";
	public static final String ERROR_USER_MESSAGE = "errorUserMessage";
	public static final String ERROR_SEARCH_MESSAGE = "errorSearchMessage";
	public static final String ERROR_BOOK_MESSAGE = "errorBookMessage";
	public static final String ERROR_ORDER_BOOK_MESSAGE = "errorOrderBookMessage";
	public static final String ERROR_ORDER_MESSAGE = "errorOrderMessage";
	public static final String ORDERS_MESSAGE = "ordersMessage";
	public static final String ERROR_SERVER = "serverError";
	public static final String WRONG_ACTION = "wrongAction";
	public static final String NULL_PAGE = "nullPage";
	public static final String BOOK = "book";
	public static final String BOOKS = "books";
	public static final String USER = "user";
	public static final String USERS = "users";
	public static final String ORDER_BOOK = "orderBook";
	public static final String ORDER_BOOKS = "orderBooks";
	public static final String ORDER = "order";
	public static final String ORDERS = "orders";
	public static final String GENRE = "genre";
	public static final String GENRES = "genres";
	public static final String CURRENT_PAGE = "currentPage";
	public static final String NUMBER_OF_PAGES = "numberOfPages";
	public static final String LANGUAGE = "language";
	public static final String USER_MESSAGE = "userMessage";
	public static final String SUCCESS = "success";	
	public static final String STATUS_ENUM = "statusEnum";
	public static final String USER_TYPE_ENUM = "userTypeEnum";	
	public static final String CART_MESSAGE = "cartMessage";	
	public static final String DIRECTION_TYPE = "directionType";
	public static final String CATEGORY_ENUM = "categoryEnum";	
	public static final String CRITERION_ENUM = "criterionEnum";	
	public static final String SEARCH_HELPER = "searchHelper";
	public static final String PAGES = "pages";
	public static final String PAGE_MANAGER = "pageManager";
	
}
