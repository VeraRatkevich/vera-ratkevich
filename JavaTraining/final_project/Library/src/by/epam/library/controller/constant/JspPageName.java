package by.epam.library.controller.constant;

public class JspPageName {
	private JspPageName() {
	}

	public static final String INDEX_PAGE = "/index.jsp";
	public static final String LOGIN_PAGE = "/jsp/login.jsp";	
	public static final String ERROR_PAGE = "/jsp/error.jsp";
	public static final String BOOKS_PAGE = "/jsp/books.jsp";
	public static final String ABOUT_PAGE = "/jsp/about.jsp";	
	public static final String REGISTRATION_PAGE = "/jsp/registration.jsp";
	public static final String ORDERS_PAGE = "/WEB-INF/jsp/orders.jsp";
	public static final String ORDER_BOOKS_PAGE = "/WEB-INF/jsp/orderBooks.jsp";
	public static final String USERS_PAGE = "/WEB-INF/jsp/users.jsp";	
	public static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";
	public static final String ORDER_DETAIL = "/WEB-INF/jsp/orderDetail.jsp";
	public static final String ORDER_BOOK_EDITOR = "/WEB-INF/jsp/orderBookEditor.jsp";
	public static final String BOOK_EDITOR = "/WEB-INF/jsp/bookEditor.jsp";
	public static final String ADDING_BOOK = "/WEB-INF/jsp/addingBook.jsp";
	public static final String PROFILE_PAGE = "/WEB-INF/jsp/profile.jsp";
	public static final String USER_EDITOR = "/WEB-INF/jsp/userEditor.jsp";
	public static final String DATE_EXPECT_EDITOR = "/WEB-INF/jsp/dateExpectEditor.jsp";
	public static final String USER_ORDERS = "/WEB-INF/jsp/userOrders.jsp";
	public static final String USER_MESSAGE = "/WEB-INF/jsp/userMessage.jsp";
}