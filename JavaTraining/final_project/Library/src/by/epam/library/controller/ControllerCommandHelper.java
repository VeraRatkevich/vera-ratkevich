package by.epam.library.controller;

import java.util.HashMap;
import java.util.Map;

import by.epam.library.service.CommandName;
import by.epam.library.service.ICommand;
import by.epam.library.service.impl.*;
import by.epam.library.service.impl.cart.*;
import by.epam.library.service.impl.topage.*;

/*
 * Class is designed to store objects such as {@code ICommand}
 *  and to provide a desired object by name.
 *  
 * Field:
 *  Map commands - stores the objects such as {@code ICommand} and its name as a key.
 *  
 * Method:
 * getCommand(String strCommandName) - returns object such as {@code ICommand} by its name.
 *  
 * @author RatkevichVera
 * @version 1.0
 */

public class ControllerCommandHelper {

	private static final ControllerCommandHelper instance = new ControllerCommandHelper();

	private final Map<CommandName, ICommand> commands = new HashMap<>();

	private ControllerCommandHelper() {
		commands.put(CommandName.LOGIN, new LoginCommand());
		commands.put(CommandName.LOGOUT, new LogoutCommand());
		commands.put(CommandName.REGISTRATION, new RegistrationCommand());
		commands.put(CommandName.LOCAL, new ChangeLocalCommand());
		commands.put(CommandName.BOOKS, new ShowAllBooksCommand());
		commands.put(CommandName.ORDERS, new ShowAllOrdersCommand());
		commands.put(CommandName.USERS, new ShowAllUsersCommand());
		commands.put(CommandName.PROFILE, new ProfileCommand());
		commands.put(CommandName.ADD_TO_CART, new AddBookToCartCommand());
		commands.put(CommandName.CART_CONFIRM, new CartConfirmCommand());
		commands.put(CommandName.CLEAR_CART, new ClearCartCommand());
		commands.put(CommandName.REMOVE_FROM_CART, new RemoveFromCartCommand());
		commands.put(CommandName.CART, new ShowCartCommand());
		commands.put(CommandName.ORDER_DETAIL, new OrderDetailCommand());
		commands.put(CommandName.ORDER_BOOK_EDITOR,
				new OrderBookEditorCommand());
		commands.put(CommandName.EDIT_ORDER_BOOK, new EditOrderBookCommand());
		commands.put(CommandName.USER_EDITOR, new UserEditorCommand());
		commands.put(CommandName.ABOUT, new AboutPageCommand());
		commands.put(CommandName.LOGIN_PAGE, new LoginPageCommand());
		commands.put(CommandName.REGISTRATION_PAGE,
				new RegistrationPageCommand());
		commands.put(CommandName.BOOK_EDITOR, new BookEditorCommand());
		commands.put(CommandName.ADDING_BOOK, new AddingBookCommand());
		commands.put(CommandName.ADD_NEW_BOOK, new AddNewBookCommand());
		commands.put(CommandName.DELETE_BOOK, new DeleteBookCommand());
		commands.put(CommandName.DELETE_ORDER_BOOK,
				new DeleteOrderBookCommand());
		commands.put(CommandName.EDIT_USER, new EditUserCommand());
		commands.put(CommandName.DELETE_USER, new DeleteUserCommand());
		commands.put(CommandName.EDIT_BOOK, new EditBookCommand());
		commands.put(CommandName.DATE_EXPECT_EDITOR,
				new DateExpectEditorCommand());
		commands.put(CommandName.EDIT_DATE_EXPECT, new EditDateExpectCommand());
		commands.put(CommandName.SEARCH_BOOKS, new SearchBooksCommand());
		commands.put(CommandName.USER_ORDERS, new ShowUserOrdersCommand());
		commands.put(CommandName.ORDER_BOOKS, new ShowAllOrderBooksCommand());
		commands.put(CommandName.DELETE_ORDER, new DeleteOrderCommand());
		commands.put(CommandName.CANCEL, new CancelCommand());
	}

	public static ControllerCommandHelper getInstance() {

		return instance;

	}

	public ICommand getCommand(String strCommandName) {

		ICommand command = null;

		CommandName commandName = CommandName.valueOf(strCommandName
				.toUpperCase().replace("-", "_"));

		if (commandName != null) {
			command = commands.get(commandName);
		} else {
			command = new DefaultCommand();
		}

		return command;

	}

}
