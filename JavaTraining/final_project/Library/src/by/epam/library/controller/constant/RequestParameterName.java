package by.epam.library.controller.constant;

public class RequestParameterName {
	
	private RequestParameterName() {
	}
	public static final String COMMAND = "command";
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";	
	public static final String SURENAME = "surename";
	public static final String NAME = "name";
	public static final String PATRONYMIC = "patronymic";
	public static final String ADDRESS = "address";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String BURTHDAY = "burthday";
	public static final String STATUS = "status";
	public static final String USER_ID = "userId";
	public static final String REPEATED_PASSWORD = "rpeatedPassword";
	public static final String USER_TYPE = "userType";
	
	
	
	public static final String BOOK_LIST = "bookList";
	
	public static final String BOOK_ID = "bookId";
	public static final String BOOK_NAME = "bookName";
	public static final String BOOK_AUTHOR = "author";
	public static final String BOOK_YEAR = "year";
	public static final String BOOK_GENRE_ID = "genreId";
	public static final String BOOK_GENRE_NAME = "genreName";
	public static final String BOOK_PAGES = "pages";
	
	public static final String ORDER_ID_USER = "orderIdUser";
	public static final String ORDER_ID = "idOrder";
	public static final String ORDER_DATE = "orderDate";
	public static final String ORDER_STATUS = "orderStatus";	
	
	public static final String BOOK_STATUS = "bookStatus";
	public static final String DATE_BEGIN = "dateBegin";	
	public static final String DATE_END = "dateEnd";
	public static final String DATE_EXPECT = "dateExpect";
	public static final String DATE_RETURN = "dateReturn";
	
	public static final String LOCAL = "local";
	
	
	public static final String CATEGORIES = "categories";
	public static final String PAGE_TYPE = "pageType";

	public static final String KEYWORD = "keyword";
	public static final String CRITERION = "criterion";
	public static final String CATEGORY = "category";
	public static final String PAGE = "page";
	
	
	
}
