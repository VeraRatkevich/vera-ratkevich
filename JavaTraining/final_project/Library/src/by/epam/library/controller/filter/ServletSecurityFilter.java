package by.epam.library.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.epam.library.controller.constant.JspPageMessage;
import by.epam.library.controller.constant.JspPageName;
import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.controller.constant.RequestParameterName;
import by.epam.library.controller.constant.SessionAttributeName;
import by.epam.library.entity.constant.UserTypeEnum;
import by.epam.library.resources.MessageManager;
import by.epam.library.service.CommandName;

public class ServletSecurityFilter implements Filter {

	static int counter = 0;

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		UserTypeEnum userType = null;
		try {
			userType = (UserTypeEnum) session
					.getAttribute(SessionAttributeName.USER_TYPE);

		} catch (ClassCastException e) {
			forwardUser(req, resp, JspPageMessage.USER_ROLE_UNKNOWN);
			return;
		}

		if (userType == null) {
			userType = UserTypeEnum.GUEST;
			session.setAttribute(SessionAttributeName.USER_TYPE, userType);			
		}
		String strCommandName = null;
		strCommandName = req.getParameter(RequestParameterName.COMMAND);
		if (strCommandName == null) {
			forwardUser(req, resp, JspPageMessage.WRONG_ACTION);
			return;
		}
		CommandName commandName = null;
		try {
			commandName = CommandName.valueOf(strCommandName
					.toUpperCase().replace("-", "_"));
		} catch (IllegalArgumentException e) {
			forwardUser(req, resp, JspPageMessage.WRONG_ACTION);
			return;
		}

		boolean accessToCommand = ServletSecurityFilterHelper.getInstance()
				.chekAccessToCommand(commandName, userType);

		if (!accessToCommand) {
			forwardUser(req, resp, JspPageMessage.ACCESS_DENIED);
			return;
		}

		chain.doFilter(request, response);

	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	private void forwardUser(HttpServletRequest request,
			HttpServletResponse response, String message)
			throws ServletException, IOException {

		request.setAttribute(RequestAttributeName.ERROR_USER_MESSAGE, MessageManager.getMessage(message));
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher(JspPageName.ERROR_PAGE);
		dispatcher.forward(request, response);

	}

}