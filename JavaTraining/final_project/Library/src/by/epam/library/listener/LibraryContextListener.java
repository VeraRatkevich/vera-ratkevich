package by.epam.library.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import by.epam.library.controller.constant.RequestAttributeName;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;

public class LibraryContextListener implements ServletContextListener {
	
	private static final Logger logger = LogManager
			.getLogger(LibraryContextListener.class.getName());	
	
	@Override
	public void contextInitialized(ServletContextEvent ev) {

		try {
			ConnectionPool.getInstance().initPoolData();
		} catch (ConnectionPoolException e) {
			logger.error(e);
			ev.getServletContext().setAttribute(RequestAttributeName.ERROR_SERVER, e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent ev) {
		
		try {
			ConnectionPool.getInstance().dispose();
		} catch (ConnectionPoolException e) {
			logger.error(e);
		}
		
	}

}
