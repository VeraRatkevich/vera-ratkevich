package by.epam.library.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateParser {
	
	private final static String PATTERN = "dd-MM-yyyy";
	private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(PATTERN);
	
	public static LocalDate parseToLocalDate(String strDate){
		return LocalDate.parse(strDate, FORMATTER);
		
	}
	


}
