package by.epam.library.dao.constant;

public class DbAttribute {
public static final String USER_ID = "id_user";
public static final String USER_SURENAME = "surename";
public static final String USER_NAME = "name";
public static final String USER_PATRONYMIC = "patronymic";
public static final String USER_ADDRESS = "address";
public static final String USER_PHONE_NUMBER = "phone_number";
public static final String USER_BURTHDAY = "burthday";
public static final String USER_TYPE = "status";
public static final String USER_LOGIN = "login";
public static final String USER_PASSWORD = "password";
public static final String DELETED = "deleted";
public static final String GENRE_ID = "id_genre";
public static final String GENRE_NAME = "genre_name";
public static final String BOOK_ID = "id_book";
public static final String BOOK_NAME = "b_name";
public static final String BOOK_AUTHOR = "author";
public static final String BOOK_YEAR = "year";
public static final String BOOK_PAGES = "pages";
public static final String ORDER_ID = "id_order";
public static final String ORDER_DATE = "date";
public static final String ORDER_STATUS = "status_order";
public static final String ORDER_BOOK_DATE_BEGIN = "date_begin";
public static final String ORDER_BOOK_DATE_END = "date_end";
public static final String ORDER_BOOK_DATE_EXPECT = "date_expect";
public static final String ORDER_BOOK_DATE_RETURN = "date_return";
public static final String ORDER_BOOK_STATUS = "status";
public static final String COUNT = "count";

}
