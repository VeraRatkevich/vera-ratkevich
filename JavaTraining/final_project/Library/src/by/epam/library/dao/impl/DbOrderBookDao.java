package by.epam.library.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import by.epam.library.dao.IOrderBookDao;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.constant.DbAttribute;
import by.epam.library.dao.constant.DbExceptionMessage;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.OrderBook;
import by.epam.library.entity.OrderBookFullData;
import by.epam.library.entity.constant.OrderBookStatusEnum;

public class DbOrderBookDao implements IOrderBookDao {
	private final static DbOrderBookDao instance = new DbOrderBookDao();

	private static final String INSERT_ORDER_BOOKS = "INSERT INTO orders_books(id_order, id_book, date_begin, status) VALUES (?,?,?,?)";
	private static final String DELETE_ORDER_BOOKS = "DELETE from orders_books where id_order = ? and id_book = ?";
	private static final String SELECT_UNIQUE_BOOKS_WITH_STATUS = "SELECT books.id_book, b_name, author, year, pages, status FROM books LEFT OUTER JOIN (select t1.id_book, status from orders_books as t1 where t1.date_begin=(select max(t2.date_begin) from orders_books as t2 where t2.id_book=t1.id_book)) as ob on books.id_book = ob.id_book";
	private static final String SELECT_BOOK_WITH_STATUS_BY_ID = "SELECT books.id_book, b_name, author, year, pages, status FROM books LEFT OUTER JOIN (select t1.id_book, status from orders_books as t1 where t1.date_begin=(select max(t2.date_begin) from orders_books as t2 where t2.id_book=t1.id_book)) as ob on books.id_book = ob.id_book where books.id_book = ";
	private static final String SELECT_STATUS_BY_ID_ORDER = "SELECT status from orders_books where id_order = ?";
	private static final String SELECT_STATUS_BOOK = "SELECT status FROM orders_books WHERE id_order = ? AND id_book = ?";
	private static final String SELECT_BOOKS_BY_ORDER_ID = "SELECT orders_books.id_book, date_begin, date_end, status, date_expect, date_return, b_name, author, year,pages from orders_books inner join books on orders_books.id_book=books.id_book where id_order = ?";
	private static final String SELECT_BOOK_BY_ORDER_BOOK_ID = "SELECT orders_books.id_book, date_begin, date_end, status, date_expect, date_return, b_name, author, year,pages from orders_books inner join books on orders_books.id_book=books.id_book where id_order = ? and orders_books.id_book = ?";
	private static final String UPDATE_ORDER_BOOK = "UPDATE orders_books SET date_begin=?, date_end=?,date_expect=?,date_return=?, status=? WHERE id_order = ? AND id_book=?";
	private static final String SELECT_COUNT_ID_BOOK = "SELECT count(id_book) as count from orders_books where id_book = ?";
	private static final String SET_DATE_EXPECT = "UPDATE orders_books SET date_expect=? WHERE id_order = ? AND id_book=?";
	private static final String COUNT_NOT_CLOSED = "SELECT COUNT(id_book) as count FROM orders_books where status = 'ISSUED' or status = 'EXPECTED'";
	private static final String SELECT_FULL_DATA_NOT_CLOSED = "SELECT books.id_book, orders.id_order, surename, name, b_name, author, year, pages,date_begin, date_end, date_expect, date_return, orders_books.status FROM books INNER JOIN (orders_books INNER JOIN (orders INNER JOIN users on orders.id_user=users.id_user) on orders_books.id_order = orders.id_order) on books.id_book = orders_books.id_book where orders_books.status = 'ISSUED' or orders_books.status = 'EXPECTED'";
	private static final String SELECT_FULL_DATA_NOT_CLOSED_LIMIT = "SELECT books.id_book, orders.id_order, surename, name, b_name, author, year, pages,date_begin, date_end, date_expect, date_return, orders_books.status FROM books INNER JOIN (orders_books INNER JOIN (orders INNER JOIN users on orders.id_user=users.id_user) on orders_books.id_order = orders.id_order) on books.id_book = orders_books.id_book where orders_books.status = 'ISSUED' or orders_books.status = 'EXPECTED' LIMIT ?, ?";
    private static final String COUNT_BOOKS_BY_ORDER = "SELECT COUNT(id_book) AS count FROM orders_books WHERE id_order = ?";
	private static final String SELECT_BY_BOOK_USER_ID = "select orders_books.status from orders_books inner join orders on orders_books.id_order = orders.id_order where (orders_books.status = 'ISSUED' or orders_books.status = 'EXPECTED') and (orders_books.id_book = ? and orders.id_user = ?)";

	private DbOrderBookDao() {

	}

	public static DbOrderBookDao getInstance() {

		return instance;

	}

	@Override
	public OrderBookStatusEnum getByOrderBookUserId(int idBook, int idUser) throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderBookStatusEnum status = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BY_BOOK_USER_ID);
			pst.setInt(1, idBook);
			pst.setInt(2, idUser);
			rs = pst.executeQuery();
			while (rs.next()) {
				String strStatus = rs.getString(DbAttribute.ORDER_BOOK_STATUS);
				if (strStatus != null) {
					status = OrderBookStatusEnum.valueOf(strStatus);
				}

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		}
		finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return status;

	}

	@Override
	public OrderBookStatusEnum getStatus(int idOrder, int idBook)
			throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String strStatus = null;
		OrderBookStatusEnum status = null;
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_STATUS_BOOK);
			pst.setInt(1, idOrder);
			pst.setInt(2, idBook);
			rs = pst.executeQuery();
			while (rs.next()) {

				strStatus = rs.getString(DbAttribute.ORDER_BOOK_STATUS);
				if (strStatus != null) {
					status = OrderBookStatusEnum.valueOf(strStatus);

				}

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return status;

	}

	@Override
	public List<OrderBookFullData> showAllNotClosedInLimit(int from, int count)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderBook book = null;
		OrderBookFullData bookData = null;
		List<OrderBookFullData> bookDataList = new ArrayList<OrderBookFullData>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_FULL_DATA_NOT_CLOSED_LIMIT);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				bookData = new OrderBookFullData();
				book = getOrderBook(rs);
				bookData.setOrderBook(book);
				bookData.setOrderId(rs.getInt(DbAttribute.ORDER_ID));
				bookData.setReaderName(rs.getString(DbAttribute.USER_NAME));
				bookData.setReaderSurename(rs
						.getString(DbAttribute.USER_SURENAME));

				bookDataList.add(bookData);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookDataList;
	}

	/*
	 * 
	 * @Override public List<OrderBook> showAllInLimit(int from, int count)
	 * throws DaoException { Connection con = null; PreparedStatement pst =
	 * null; ResultSet rs = null; OrderBook book = null;
	 * 
	 * List<OrderBook> bookList = new ArrayList<OrderBook>();
	 * 
	 * try { con = ConnectionPool.getInstance().takeConnection(); pst =
	 * con.prepareStatement(SELECT_LIMIT); pst.setInt(1, from); pst.setInt(2,
	 * count); rs = pst.executeQuery();
	 * 
	 * while (rs.next()) {
	 * 
	 * book = getOrderBook(rs);
	 * 
	 * bookList.add(book); } }
	 * 
	 * catch (SQLException e) { throw new
	 * DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e); } catch
	 * (ConnectionPoolException e) { throw new
	 * DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e); } catch
	 * (IllegalArgumentException e) { throw new
	 * DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS, e);
	 * 
	 * } catch (ParseException e) { throw new
	 * DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e); } finally { try
	 * { ConnectionPool.getInstance().closeConnection(con, pst); } catch
	 * (ConnectionPoolException e) { throw new
	 * DaoException(DbExceptionMessage.ERROR_CLOSE, e);
	 * 
	 * } } return bookList; }
	 */
	@Override
	public int getCountNotClosed() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(COUNT_NOT_CLOSED);

			while (rs.next()) {

				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	@Override
	public int getCountBooksByOrder(int idOrder) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(COUNT_BOOKS_BY_ORDER);
			pst.setInt(1, idOrder);
			rs = pst.executeQuery();

			while (rs.next()) {

				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	@Override
	public int setDateExpect(int idOrder, int idBook, LocalDate dateExpect)
			throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SET_DATE_EXPECT);

			if (dateExpect != null) {
				pst.setDate(1, Date.valueOf(dateExpect));
			} else {
				pst.setNull(1, java.sql.Types.DATE);
			}

			pst.setInt(2, idOrder);
			pst.setInt(3, idBook);

			i = pst.executeUpdate();
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
			// }// catch (ParseException e) {
			// throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE,
			// e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	public List<OrderBookStatusEnum> getStatusByIdOrder(int idOrder)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String strStatus = null;
		List<OrderBookStatusEnum> booksStatus = null;
		OrderBookStatusEnum status = null;
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_STATUS_BY_ID_ORDER);
			pst.setInt(1, idOrder);

			rs = pst.executeQuery();
			booksStatus = new ArrayList<OrderBookStatusEnum>();
			while (rs.next()) {

				strStatus = rs.getString(DbAttribute.ORDER_BOOK_STATUS);
				if (strStatus != null) {
					status = OrderBookStatusEnum.valueOf(strStatus);
					booksStatus.add(status);
				}

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return booksStatus;

	}

	public int getCountById(int idBook) throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();

			pst = con.prepareStatement(SELECT_COUNT_ID_BOOK);
			pst.setInt(1, idBook);
			rs = pst.executeQuery();

			while (rs.next()) {
				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	public List<OrderBook> showAllActual() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		OrderBook book;

		List<OrderBook> bookList = new ArrayList<OrderBook>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();
			rs = st.executeQuery(SELECT_UNIQUE_BOOKS_WITH_STATUS);

			while (rs.next()) {
				book = new OrderBook();
				book.setId(rs.getInt(DbAttribute.BOOK_ID));
				book.setName(rs.getString(DbAttribute.BOOK_NAME));
				book.setAuthor(rs.getString(DbAttribute.BOOK_AUTHOR));
				book.setYear(rs.getInt(DbAttribute.BOOK_YEAR));
				book.setPages(rs.getInt(DbAttribute.BOOK_PAGES));
				OrderBookStatusEnum status = OrderBookStatusEnum.valueOf(rs
						.getString(DbAttribute.ORDER_BOOK_STATUS));

				book.setStatus(status);
				bookList.add(book);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;

	}

	public OrderBook showActualById(int idBook) throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		OrderBook book = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();
			rs = st.executeQuery(SELECT_BOOK_WITH_STATUS_BY_ID + idBook);

			while (rs.next()) {
				book = new OrderBook();
				book.setId(rs.getInt(DbAttribute.BOOK_ID));
				book.setName(rs.getString(DbAttribute.BOOK_NAME));
				book.setAuthor(rs.getString(DbAttribute.BOOK_AUTHOR));
				book.setYear(rs.getInt(DbAttribute.BOOK_YEAR));
				book.setPages(rs.getInt(DbAttribute.BOOK_PAGES));
				String strStatus = rs.getString(DbAttribute.ORDER_BOOK_STATUS);
				if (strStatus != null) {
					OrderBookStatusEnum status = OrderBookStatusEnum
							.valueOf(strStatus);

					book.setStatus(status);

				}

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return book;

	}

	public OrderBook getActualById() {
		return null;

	}

	@Override
	public int addList(int idOrder, List<OrderBook> orderBookList)
			throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;
		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(INSERT_ORDER_BOOKS);
			pst.setInt(1, idOrder);

			for (OrderBook book : orderBookList) {
				pst.setInt(2, book.getId());
				pst.setDate(3, Date.valueOf(book.getDateBegin()));
				pst.setString(4, book.getStatus().toString());
				i = pst.executeUpdate();
				count += i;

			}

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;
	}

	@Override
	public int delete(int idOrder, int idBook) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(DELETE_ORDER_BOOKS);

			pst.setInt(1, idOrder);

			pst.setInt(2, idBook);

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_DELETE_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int deleteList(int idOrder) throws DaoException {

		// Connection con = null;
		// PreparedStatement pst = null;
		// ResultSet rs = null;

		int i = 0;
		/*
		 * try { con = ConnectionPool.getInstance().takeConnection(); pst =
		 * con.prepareStatement(DELETE_ORDER_BOOKS_BY_ORDER_ID);
		 * 
		 * pst.setInt(1, idOrder);
		 * 
		 * i = pst.executeUpdate();
		 * 
		 * }
		 * 
		 * catch (SQLException e) { throw new
		 * DaoException(DbExceptionMessage.ERROR_DELETE_SQL, e); } catch
		 * (ConnectionPoolException e) { throw new
		 * DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e); } finally {
		 * try { ConnectionPool.getInstance().closeConnection(con, pst); } catch
		 * (ConnectionPoolException e) { throw new
		 * DaoException(DbExceptionMessage.ERROR_CLOSE, e);
		 * 
		 * } }
		 */
		return i;
	}

	@Override
	public List<OrderBook> showBooksByOrderID(int orderID) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderBook book;

		List<OrderBook> bookList = new ArrayList<OrderBook>();

		try {
			con = ConnectionPool.getInstance().takeConnection();

			pst = con.prepareStatement(SELECT_BOOKS_BY_ORDER_ID);
			pst.setInt(1, orderID);
			rs = pst.executeQuery();

			while (rs.next()) {
				book = getOrderBook(rs);

				bookList.add(book);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);

		}  finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;

	}

	@Override
	public OrderBook getById(int idOrder, int idBook) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderBook book = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();

			pst = con.prepareStatement(SELECT_BOOK_BY_ORDER_BOOK_ID);
			pst.setInt(1, idOrder);
			pst.setInt(2, idBook);
			rs = pst.executeQuery();

			while (rs.next()) {
				book = getOrderBook(rs);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);

		}  finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return book;

	}

	@Override
	public int update(OrderBook book, int idOrder) throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(UPDATE_ORDER_BOOK);

			LocalDate date = book.getDateBegin();

			if (date != null) {
				pst.setDate(1, Date.valueOf(date));
			} else {
				pst.setNull(1, java.sql.Types.DATE);
			}

			date = book.getDateEnd();

			if (date != null) {
				pst.setDate(2, Date.valueOf(date));
			} else {
				pst.setNull(2, java.sql.Types.DATE);

			}
			date = book.getDateExpect();

			if (date != null) {
				pst.setDate(3, Date.valueOf(date));
			} else {

				pst.setNull(3, java.sql.Types.DATE);
			}

			date = book.getDateReturn();

			if (date != null) {
				pst.setDate(4, Date.valueOf(date));
			} else {
				pst.setNull(4, java.sql.Types.DATE);
			}

			String strStatus = book.getStatus().toString();
			pst.setString(5, strStatus);
			pst.setInt(6, idOrder);
			pst.setInt(7, book.getId());

			i = pst.executeUpdate();
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public List<OrderBookFullData> showAllNotClosed() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		OrderBookFullData bookData = null;
		OrderBook book = null;

		List<OrderBookFullData> bookDataList = new ArrayList<OrderBookFullData>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_FULL_DATA_NOT_CLOSED);

			while (rs.next()) {
				bookData = new OrderBookFullData();
				book = getOrderBook(rs);
				bookData.setOrderBook(book);
				bookData.setOrderId(rs.getInt(DbAttribute.ORDER_ID));
				bookData.setReaderName(rs.getString(DbAttribute.USER_NAME));
				bookData.setReaderSurename(rs
						.getString(DbAttribute.USER_SURENAME));

				bookDataList.add(bookData);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookDataList;
	}

	private OrderBook getOrderBook(ResultSet rs) throws SQLException {

		Date date;

		OrderBook book = new OrderBook();

		book.setId(rs.getInt(DbAttribute.BOOK_ID));
		book.setName(rs.getString(DbAttribute.BOOK_NAME));
		book.setAuthor(rs.getString(DbAttribute.BOOK_AUTHOR));
		book.setYear(rs.getInt(DbAttribute.BOOK_YEAR));
		book.setPages(rs.getInt(DbAttribute.BOOK_PAGES));
		date = rs.getDate(DbAttribute.ORDER_BOOK_DATE_BEGIN);
		book.setDateBegin(date.toLocalDate());
		date = rs.getDate(DbAttribute.ORDER_BOOK_DATE_END);
		if (date != null) {
			book.setDateEnd(date.toLocalDate());
		}
		date = rs.getDate(DbAttribute.ORDER_BOOK_DATE_EXPECT);
		if (date != null) {
			book.setDateExpect(date.toLocalDate());
		}
		date = rs.getDate(DbAttribute.ORDER_BOOK_DATE_RETURN);

		if (date != null) {
			book.setDateReturn(date.toLocalDate());
		}

		OrderBookStatusEnum status = null;
		String strStatus = rs.getString(DbAttribute.ORDER_BOOK_STATUS);
		if (strStatus != null) {
			status = OrderBookStatusEnum.valueOf(strStatus);
		}
		book.setStatus(status);
		return book;
	}

}
