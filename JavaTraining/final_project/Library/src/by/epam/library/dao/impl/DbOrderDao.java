package by.epam.library.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import by.epam.library.dao.IOrderDao;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.constant.DbAttribute;
import by.epam.library.dao.constant.DbExceptionMessage;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Order;
import by.epam.library.entity.User;
import by.epam.library.entity.constant.OrderStatusEnum;

public class DbOrderDao implements IOrderDao {
	private final static DbOrderDao instance = new DbOrderDao();

	private static final String INSERT_ORDER = "INSERT INTO orders(id_user, date, status_order, deleted) VALUES (?,?,?,0)";
	private static final String DELETE_ORDER = "UPDATE orders SET deleted=1 WHERE id_order=?";
	private static final String SELECT_ALL_ORDERS_USERS = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY orders.id_order";
	private static final String SELECT_ALL_ORDERS_USERS_ORDER_USER_ASC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY surename ASC";
	private static final String SELECT_ALL_ORDERS_USERS_ORDER_USER_DESC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY surename DESC";
	private static final String SELECT_ALL_ORDERS_USERS_ORDER_DATE_ASC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY date";
	private static final String SELECT_ALL_ORDERS_USERS_ORDER_DATE_DESC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY date DESC";
	
	private static String SELECT_LIMIT_USER_ASC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY surename ASC LIMIT ?, ?";
	private static String SELECT_LIMIT_USER_DESC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY surename DESC LIMIT ?, ?";
	private static String SELECT_LIMIT_DATE_ASC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY date LIMIT ?, ?";
	private static String SELECT_LIMIT_DATE_DESC = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY date DESC LIMIT ?, ?";
		
	private static final String SELECT_USER_ORDERS = "SELECT id_order, date, status_order FROM orders WHERE id_user= ? and deleted=0 ORDER BY id_order";
	private static final String SELECT_MAX_ID_ORDER_BY_USER = "SELECT max(id_order) as id_order FROM orders WHERE id_user = ?";
	private static final String SELECT_ID_ORDERS_NOT_EXECUTED = "SELECT id_order FROM orders WHERE deleted = 0 and (status_order='UNEXECUTED' or status_order = 'PARTIALLY')";
	private static final String INSERT_STATUS = "UPDATE orders SET status_order=? WHERE id_order=?";
	private static final String SELECT_ORDER_BY_ID = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 AND orders.id_order = ?";
	private static final String SELECT_COUNT_ID_USER = "SELECT count(id_user) as count from orders where id_user = ?";
	private static String COUNT_ALL_ORDERS = "SELECT COUNT(id_order) AS count FROM orders where deleted = 0";
	private static String COUNT_USER_ORDERS = "SELECT COUNT(id_order) AS count FROM orders where id_user = ? and deleted = 0";
	private static String SELECT_LIMIT = "SELECT id_order, date, status_order, surename, name FROM orders INNER JOIN users ON orders.id_user=users.id_user where orders.deleted=0 ORDER BY orders.id_order LIMIT ?, ?";
	private static String SELECT_USER_ORDERS_LIMIT = "SELECT id_order, date, status_order FROM orders WHERE id_user= ? and deleted=0 ORDER BY id_order LIMIT ?, ?";
	private static String SELECT_STATUS = "SELECT status_order FROM orders WHERE id_order = ?";

	private DbOrderDao() {

	}

	public static DbOrderDao getInstance() {

		return instance;

	}
	
	
	@Override
	public List<Order> showAllInLimitUserAsc(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT_USER_ASC);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	
	@Override
	public List<Order> showAllInLimitUserDesc(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT_USER_DESC);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	
	
	@Override
	public List<Order> showAllInLimitDateAsc(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT_DATE_ASC);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	
	@Override
	public List<Order> showAllInLimitDateDesc(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT_DATE_DESC);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public List<Order> showAllOrderUserAsc() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ALL_ORDERS_USERS_ORDER_USER_ASC);

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	@Override
	public List<Order> showAllOrderUserDesc() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ALL_ORDERS_USERS_ORDER_USER_DESC);

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	@Override
	public List<Order> showAllOrderDateAsc() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ALL_ORDERS_USERS_ORDER_DATE_ASC);

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	
	@Override
	public List<Order> showAllOrderDateDesc() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ALL_ORDERS_USERS_ORDER_DATE_DESC);

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}
	
	
	@Override
	public List<Order> showUserOrdersInLimit(int idUser, int from, int count)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_USER_ORDERS_LIMIT);
			pst.setInt(1, idUser);
			pst.setInt(2, from);
			pst.setInt(3, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);
				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}

	@Override
	public int getCountUserOrders(int idUser) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(COUNT_USER_ORDERS);
			pst.setInt(1, idUser);
			rs = pst.executeQuery();

			while (rs.next()) {

				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	@Override
	public List<Order> showAllInLimit(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}

	@Override
	public int getCountOrders() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(COUNT_ALL_ORDERS);

			while (rs.next()) {

				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	@Override
	public int getCountIdUser(int idUser) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_COUNT_ID_USER);

			pst.setInt(1, idUser);

			rs = pst.executeQuery();
			while (rs.next()) {
				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;
	}

	@Override
	public int setStatus(int idOrder, OrderStatusEnum status)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(INSERT_STATUS);
			pst.setString(1, status.toString());
			pst.setInt(2, idOrder);

			i = pst.executeUpdate();
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public List<Integer> showIdNotExecuted() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int idOrder = 0;

		List<Integer> idOrderList = new ArrayList<Integer>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ID_ORDERS_NOT_EXECUTED);

			while (rs.next()) {

				idOrder = rs.getInt(DbAttribute.ORDER_ID);

				idOrderList.add(new Integer(idOrder));
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return idOrderList;
	}

	@Override
	public int getMaxIdOrderByIdUser(int idUser) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int idOrder = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_MAX_ID_ORDER_BY_USER);
			pst.setInt(1, idUser);

			rs = pst.executeQuery();
			if (rs.next()) {
				idOrder = rs.getInt(DbAttribute.ORDER_ID);
			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return idOrder;

	}

	@Override
	public int add(Order order) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(INSERT_ORDER);
			pst.setInt(1, order.getUser().getId());
			pst.setDate(2, Date.valueOf(order.getDate()));
			pst.setString(3, order.getStatus().toString());

			i = pst.executeUpdate();
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int delete(int idOrder) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(DELETE_ORDER);

			pst.setInt(1, idOrder);

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_DELETE_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public Order getById(int idOrder) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_ORDER_BY_ID);
			pst.setInt(1, idOrder);

			rs = pst.executeQuery();
			if (rs.next()) {
				order = getOrder(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return order;
	}

	@Override
	public Order getByIdWithUser(int idOrder) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_ORDER_BY_ID);
			pst.setInt(1, idOrder);

			rs = pst.executeQuery();
			if (rs.next()) {
				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);
			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return order;
	}

	@Override
	public List<Order> showAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Order order = null;
		User user = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ALL_ORDERS_USERS);

			while (rs.next()) {

				order = getOrder(rs);
				user = new User();
				user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
				user.setName(rs.getString(DbAttribute.USER_NAME));
				order.setUser(user);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}

	@Override
	public List<Order> showOrdersByIdUser(int idUser) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Order order = null;
		List<Order> orderList = new ArrayList<Order>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_USER_ORDERS);
			pst.setInt(1, idUser);
			rs = pst.executeQuery();

			while (rs.next()) {

				order = getOrder(rs);

				orderList.add(order);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} catch (ParseException e) {
			throw new DaoException(DbExceptionMessage.ERROR_PARSE_STR_DATE, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return orderList;
	}

	@Override
	public OrderStatusEnum getStatus(int idOrder) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		OrderStatusEnum status = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_STATUS);
			pst.setInt(1, idOrder);
			rs = pst.executeQuery();

			while (rs.next()) {

				status = OrderStatusEnum.valueOf(rs
						.getString(DbAttribute.ORDER_STATUS));
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return status;
	}

	private Order getOrder(ResultSet rs) throws SQLException, ParseException {
		Order order = new Order();

		order.setId(rs.getInt(DbAttribute.ORDER_ID));
		Date date = rs.getDate(DbAttribute.ORDER_DATE);
		order.setDate(date.toLocalDate());
		String strStatus = rs.getString(DbAttribute.ORDER_STATUS);
		OrderStatusEnum status = OrderStatusEnum.valueOf(strStatus);
		order.setStatus(status);

		return order;
	}

}
