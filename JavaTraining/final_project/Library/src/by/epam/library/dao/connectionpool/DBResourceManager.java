package by.epam.library.dao.connectionpool;

import java.util.ResourceBundle;

public class DBResourceManager {
	
private final static DBResourceManager instance = new DBResourceManager();
private final static String PATH_DB_PROPERTIES = "by.epam.library.dao.connectionpool.db";

private ResourceBundle bundle = ResourceBundle.getBundle(PATH_DB_PROPERTIES);

public static DBResourceManager getInstance(){
	return instance;
}

public String getValue (String key){
	return bundle.getString(key);
}


}
