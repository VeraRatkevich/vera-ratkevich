package by.epam.library.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.library.dao.IGenreDao;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.constant.DbAttribute;
import by.epam.library.dao.constant.DbExceptionMessage;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Genre;

public class DbGenreDao implements IGenreDao {
	private final static DbGenreDao instance = new DbGenreDao();

	private static final String INSERT_GENRE = "INSERT INTO genre(genre_name,deleted) VALUES (?,0)";
	private static final String SELECT_GENRE = "SELECT genre_name FROM genre where deleted is null or 0 AND id_genre = ";
	private static final String SELECT_ALL_GENRE = "SELECT id_genre, genre_name FROM genre where deleted = 0";
	private static final String UPDATE_GENRE = "UPDATE genre SET genre_name=? WHERE id_genre=?";

	private DbGenreDao() {
	}

	public static DbGenreDao getInstance() {
		return instance;
	}

	@Override
	public Genre getByID(int genreId) throws DaoException {

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Genre genre = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();
			rs = st.executeQuery(SELECT_GENRE + genreId);

			if (rs.next()) {
				genre = getGenre(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return genre;
	}

	@Override
	public List<Genre> showAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Genre genre = null;

		List<Genre> genreList = new ArrayList<Genre>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_ALL_GENRE);

			while (rs.next()) {

				genre = getGenre(rs);

				genreList.add(genre);
			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return genreList;
	}

	@Override
	public int add(String genre) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(INSERT_GENRE);

			pst.setString(1, genre);

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int update(Genre genre) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(UPDATE_GENRE);

			pst.setString(1, genre.getName());

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_UPDATE_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	private Genre getGenre(ResultSet rs) throws SQLException {
		Genre genre = new Genre();

		genre.setId(rs.getInt(DbAttribute.GENRE_ID));
		genre.setName(rs.getString(DbAttribute.GENRE_NAME));

		return genre;
	}

}
