package by.epam.library.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.epam.library.dao.IBookDao;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.constant.DbAttribute;
import by.epam.library.dao.constant.DbExceptionMessage;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;
import by.epam.library.entity.Genre;

public class DbBookDao implements IBookDao {
	private final static DbBookDao instance = new DbBookDao();

	private static final String INSERT_BOOK = "INSERT INTO books(b_name, author, year, id_genre, pages, deleted) VALUES (?,?,?,?,?,0)";
	private static final String DELETE_BOOK = "UPDATE books SET deleted=1 WHERE id_book=?";
	private static final String SELECT_BOOKS = "SELECT id_book, b_name, author,year,books.id_genre,genre_name, pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE books.deleted = 0";
	private static final String SELECT_BOOK_BY_YEAR = "SELECT id_book, b_name, author,year,books.id_genre,genre_name, pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE year=? AND books.deleted = 0";
	private static final String SELECT_BOOK_BY_PERIOD = "SELECT id_book, b_name, author,year,books.id_genre,pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE year between ? and ? AND books.deleted = 0";
	private static final String SELECT_BOOK_BY_PAGES = "SELECT id_book, b_name, author,year,books.id_genre,pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE pages=? AND books.deleted = 0";
	private static final String SELECT_BOOK_BY_INTERVAL_PAGES = "SELECT id_book, b_name, author,year,books.id_genre,pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE pages between ? and ? AND books.deleted = 0";
	private static final String SELECT_BOOK_BY_NAME = "SELECT id_book, b_name, author,year,books.id_genre,genre_name, pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE b_name like ? and books.deleted = 0";
	private static final String SELECT_BOOK_BY_GENRE = "SELECT id_book, b_name, author,year,books.id_genre, genre_name,pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE genre_name like ? AND books.deleted = 0";
	private static final String SELECT_BOOK_BY_AUTHOR = "SELECT id_book, b_name, author,year,books.id_genre,genre_name, pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE author like ? AND books.deleted = 0";
	private static final String SELECT_BOOK_BY_ID = "SELECT id_book, b_name, author,year,books.id_genre, genre.genre_name, pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE id_book = ? and books.deleted = 0";
	private static final String UPDATE_BOOK = "UPDATE books SET b_name=?, author=?,year=?,books.id_genre=?,pages=? WHERE id_book=?";
	private static String COUNT_ALL_BOOKS = "SELECT COUNT(id_book) AS count FROM books where deleted = 0";
	private static String SELECT_LIMIT = "SELECT id_book, b_name, author,year,books.id_genre,genre_name, pages FROM books inner join genre on books.id_genre = genre.id_genre WHERE books.deleted = 0 LIMIT ?, ?";

	private DbBookDao() {

	}

	public static DbBookDao getInstance() {

		return instance;

	}

	@Override
	public List<Book> showAllInLimit(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Book book = null;

		List<Book> bookList = new ArrayList<Book>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				book = getBook(rs);

				bookList.add(book);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;
	}

	@Override
	public int getCountBooks() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(COUNT_ALL_BOOKS);

			while (rs.next()) {

				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	@Override
	public int add(Book book) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(INSERT_BOOK);

			pst.setString(1, (book.getName()));
			pst.setString(2, (book.getAuthor()));
			pst.setInt(3, (book.getYear()));
			pst.setInt(4, (book.getGenre().getId()));
			pst.setInt(5, (book.getPages()));

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int delete(int idBook) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(DELETE_BOOK);

			pst.setInt(1, idBook);

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_DELETE_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int update(Book book) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(UPDATE_BOOK);

			pst.setString(1, (book.getName()));
			pst.setString(2, (book.getAuthor()));
			pst.setInt(3, (book.getYear()));
			pst.setInt(4, (book.getGenre().getId()));
			pst.setInt(5, (book.getPages()));
			pst.setInt(6, (book.getId()));
			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public List<Book> showAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Book book;

		List<Book> bookList = new ArrayList<Book>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();
			rs = st.executeQuery(SELECT_BOOKS);

			while (rs.next()) {
				book = getBook(rs);
				bookList.add(book);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);

		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);

		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;
	}

	@Override
	public List<Book> showBooksByGenre(String genre) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();
		String bookGenre = "%" + genre + "%";
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_GENRE);
			pst.setString(1, bookGenre);
			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;
	}

	@Override
	public List<Book> showBooksByAuthor(String author) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();
		String bookAuthor = "%" + author + "%";
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_AUTHOR);
			pst.setString(1, bookAuthor);
			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;
	}

	@Override
	public List<Book> showBooksByYear(int year) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_YEAR);

			pst.setInt(1, year);

			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;
	}

	@Override
	public List<Book> showBooksByPeriod(Date dateStart, Date dateEnd)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_PERIOD);

			pst.setDate(1, (java.sql.Date) dateStart);
			pst.setDate(2, (java.sql.Date) dateEnd);

			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;

	}

	@Override
	public List<Book> showBooksByPages(int pages) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_PAGES);
			pst.setInt(1, pages);
			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;
	}

	@Override
	public List<Book> showBooksByIntervalPages(int pagesStart, int pagesEnd)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_INTERVAL_PAGES);

			pst.setInt(1, pagesStart);
			pst.setInt(2, pagesEnd);

			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;

	}

	@Override
	public List<Book> showBooksByName(String name) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book;

		List<Book> bookList = new ArrayList<Book>();
		String bookName = "%" + name + "%";
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_NAME);

			pst.setString(1, bookName);

			rs = pst.executeQuery();

			while (rs.next()) {
				book = getBook(rs);

				bookList.add(book);

			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return bookList;

	}

	@Override
	public Book getById(int idBook) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Book book = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_BOOK_BY_ID);
			pst.setInt(1, idBook);
			rs = pst.executeQuery();

			if (rs.next()) {
				book = getBook(rs);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return book;
	}

	private Book getBook(ResultSet rs) throws SQLException {
		Book book = new Book();

		book.setId(rs.getInt(DbAttribute.BOOK_ID));
		book.setName(rs.getString(DbAttribute.BOOK_NAME));
		book.setAuthor(rs.getString(DbAttribute.BOOK_AUTHOR));
		book.setYear(rs.getInt(DbAttribute.BOOK_YEAR));
		book.setGenre(new Genre(rs.getInt(DbAttribute.GENRE_ID), rs
				.getString(DbAttribute.GENRE_NAME)));
		book.setPages(rs.getInt(DbAttribute.BOOK_PAGES));

		return book;
	}

}
