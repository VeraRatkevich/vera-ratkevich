package by.epam.library.dao;

import java.util.List;

import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;

public interface IUserDao extends IDao {

	int add(User user) throws DaoException;

	int delete(int idUser) throws DaoException;

	User getByLoginPasword(String login, String password) throws DaoException;

	User getById(int idUser) throws DaoException;

	int update(User user) throws DaoException;

	List<User> showAll() throws DaoException;

	int getIdUserByLogin(String login) throws DaoException;

	int getCountUsers() throws DaoException;

	List<User> showAllInLimit(int from, int count) throws DaoException;
}
