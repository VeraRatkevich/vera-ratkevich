package by.epam.library.dao;

import java.util.List;

import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Order;
import by.epam.library.entity.constant.OrderStatusEnum;

public interface IOrderDao extends IDao{
	int add(Order order) throws DaoException;

	int delete(int idOrder) throws DaoException;

	Order getById(int idOrder) throws DaoException;

	List<Order> showAll() throws DaoException;

	List<Order> showOrdersByIdUser(int idUser) throws DaoException;

	int getMaxIdOrderByIdUser(int idUser) throws DaoException;
	
	List<Integer> showIdNotExecuted() throws DaoException;
	
	Order getByIdWithUser(int idOrder) throws DaoException;
	
	int setStatus(int idOrder, OrderStatusEnum status) throws DaoException;

	int getCountIdUser(int idUser) throws DaoException;

	List<Order> showAllInLimit(int from, int count) throws DaoException;

	int getCountOrders() throws DaoException;


	int getCountUserOrders(int userId) throws DaoException;


	List<Order> showUserOrdersInLimit(int idUser, int from, int count)
			throws DaoException;

	OrderStatusEnum getStatus(int idOrder) throws DaoException;

	List<Order> showAllOrderUserAsc() throws DaoException;

	List<Order> showAllOrderUserDesc() throws DaoException;

	List<Order> showAllOrderDateAsc() throws DaoException;

	List<Order> showAllOrderDateDesc() throws DaoException;

	List<Order> showAllInLimitDateDesc(int from, int count) throws DaoException;

	List<Order> showAllInLimitDateAsc(int from, int count) throws DaoException;

	List<Order> showAllInLimitUserDesc(int from, int count) throws DaoException;

	List<Order> showAllInLimitUserAsc(int from, int count) throws DaoException;
}
