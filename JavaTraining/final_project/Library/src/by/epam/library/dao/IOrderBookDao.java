package by.epam.library.dao;

import java.time.LocalDate;
import java.util.List;

import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.OrderBook;
import by.epam.library.entity.OrderBookFullData;
import by.epam.library.entity.constant.OrderBookStatusEnum;

public interface IOrderBookDao extends IDao {

	int addList(int idOrder, List<OrderBook> orderBookList) throws DaoException;

	int delete(int idOrder, int idBook) throws DaoException;

	int deleteList(int idBook) throws DaoException;

	OrderBook getById(int idOrder, int idBook) throws DaoException;

	int update(OrderBook orderBook, int idOrder) throws DaoException;

	List<OrderBook> showBooksByOrderID(int orderID) throws DaoException;

	List<OrderBook> showAllActual() throws DaoException;

	public OrderBook showActualById(int idBook) throws DaoException;

	List<OrderBookStatusEnum> getStatusByIdOrder(int idOrder)
			throws DaoException;

	int getCountById(int idBook) throws DaoException;

	int getCountBooksByOrder(int idOrder) throws DaoException;

	OrderBookStatusEnum getStatus(int idOrder, int idBook) throws DaoException;

	int getCountNotClosed() throws DaoException;

	OrderBookStatusEnum getByOrderBookUserId(int idBook, int idUser) throws DaoException;

	List<OrderBookFullData> showAllNotClosed() throws DaoException;

	List<OrderBookFullData> showAllNotClosedInLimit(int from, int count)
			throws DaoException;

	int setDateExpect(int idOrder, int idBook, LocalDate dateExpect)
			throws DaoException;
	
	
	
	
	
	
}
