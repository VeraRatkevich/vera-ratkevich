package by.epam.library.dao;

import by.epam.library.dao.impl.DbBookDao;
import by.epam.library.dao.impl.DbGenreDao;
import by.epam.library.dao.impl.DbOrderBookDao;
import by.epam.library.dao.impl.DbOrderDao;
import by.epam.library.dao.impl.DbUserDao;

public class DaoFactory {
	private final static DaoFactory instance = new DaoFactory();

	public static DaoFactory getInstance() {

		return instance;

	}

	public IBookDao getBookDao() {

		return DbBookDao.getInstance();
	}

	public IUserDao getUserDao() {

		return DbUserDao.getInstance();
	}

	public IOrderDao getOrderDao() {

		return DbOrderDao.getInstance();
	}
	public IOrderBookDao getOrderBookDao() {

		return DbOrderBookDao.getInstance();
	}
	public IGenreDao getGenreDao() {

		return DbGenreDao.getInstance();
	}

}
