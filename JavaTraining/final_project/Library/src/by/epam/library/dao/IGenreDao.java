package by.epam.library.dao;

import java.util.List;

import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Genre;

public interface IGenreDao extends IDao {
	int add(String genre) throws DaoException;

	Genre getByID(int genreId) throws DaoException;

	List<Genre> showAll() throws DaoException;
	
	int update(Genre genre) throws DaoException;
}
