package by.epam.library.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.library.dao.IUserDao;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.constant.DbAttribute;
import by.epam.library.dao.constant.DbExceptionMessage;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.User;
import by.epam.library.entity.constant.UserTypeEnum;

public class DbUserDao implements IUserDao {

	private final static DbUserDao instance = new DbUserDao();

	private static final String INSERT_USER = "INSERT INTO users(surename, name, patronymic,address,phone_number,status,login,password,deleted) VALUES (?,?,?,?,?,?,?,?,0)";
	private static final String DELETE_USER = "UPDATE users SET deleted=1 WHERE id_user=?";
	private static final String SELECT_USER_BY_PASS_LOGIN = "SELECT id_user, surename, name, patronymic,address,phone_number,status,login,password FROM users WHERE login=? AND password=? AND deleted = 0";
	private static final String SELECT_USER_BY_ID = "SELECT id_user, surename, name, patronymic,address,phone_number,status,login,password FROM users WHERE id_user=? AND deleted = 0";
	private static final String SELECT_USERS = "SELECT id_user, surename, name, patronymic,address,phone_number,status,login,password FROM users WHERE deleted = 0 ORDER BY surename";
	private static final String UPDATE_USER = "UPDATE users SET surename=?, name=?, patronymic=?,address=?,phone_number=?,status=? WHERE id_user=?";
	private static String SELECT_ID_USER_BY_LOGIN = "SELECT id_user FROM users WHERE login=? AND deleted = 0";
	private static String COUNT_ALL_USERS = "SELECT COUNT(id_user) AS count FROM users where deleted = 0";
	private static String SELECT_LIMIT = "SELECT id_user, surename, name, patronymic,address,phone_number,status,login,password FROM users WHERE deleted = 0 ORDER BY surename LIMIT ?, ?";

	private DbUserDao() {

	}

	public static DbUserDao getInstance() {

		return instance;

	}

	public List<User> showAllInLimit(int from, int count) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		List<User> userList = new ArrayList<User>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_LIMIT);
			pst.setInt(1, from);
			pst.setInt(2, count);
			rs = pst.executeQuery();

			while (rs.next()) {

				user = getUser(rs);
				userList.add(user);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return userList;
	}

	public int getCountUsers() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		int count = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(COUNT_ALL_USERS);

			while (rs.next()) {

				count = rs.getInt(DbAttribute.COUNT);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return count;

	}

	public int getIdUserByLogin(String login) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		int idUser = 0;
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_ID_USER_BY_LOGIN);
			pst.setString(1, login);
			rs = pst.executeQuery();
			if (rs.next()) {
				idUser = rs.getInt(DbAttribute.USER_ID);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_DELETE_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return idUser;
	}

	@Override
	public int add(User user) throws DaoException {

		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;
		try {
			con = ConnectionPool.getInstance().takeConnection();

			pst = con.prepareStatement(INSERT_USER);

			pst.setString(1, user.getSurename());
			pst.setString(2, user.getName());
			pst.setString(3, user.getPatronymic());
			pst.setString(4, user.getAddress());
			pst.setString(5, user.getPhoneNumber());
			pst.setString(6, user.getType().toString());
			pst.setString(7, user.getLogin());
			pst.setString(8, user.getPassword());

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int delete(int idUser) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;

		int i = 0;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(DELETE_USER);

			pst.setInt(1, idUser);

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_DELETE_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public int update(User user) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		int i = 0;
		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(UPDATE_USER);
			pst.setString(1, user.getSurename());
			pst.setString(2, user.getName());
			pst.setString(3, user.getPatronymic());
			pst.setString(4, user.getAddress());
			pst.setString(5, user.getPhoneNumber());
			pst.setString(6, user.getType().toString());
			pst.setInt(7, user.getId());

			i = pst.executeUpdate();

		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_INSERT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return i;
	}

	@Override
	public List<User> showAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		User user = null;

		List<User> userList = new ArrayList<User>();

		try {
			con = ConnectionPool.getInstance().takeConnection();
			st = con.createStatement();

			rs = st.executeQuery(SELECT_USERS);

			while (rs.next()) {

				user = getUser(rs);

				userList.add(user);
			}
		}

		catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, st);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return userList;
	}

	@Override
	public User getByLoginPasword(String login, String password)
			throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_USER_BY_PASS_LOGIN);

			pst.setString(1, login);
			pst.setString(2, password);

			rs = pst.executeQuery();
			if (rs.next()) {
				user = getUser(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}

		return user;
	}

	@Override
	public User getById(int idUser) throws DaoException {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		try {
			con = ConnectionPool.getInstance().takeConnection();
			pst = con.prepareStatement(SELECT_USER_BY_ID);

			pst.setInt(1, idUser);

			rs = pst.executeQuery();

			if (rs.next()) {
				user = getUser(rs);

			}
		} catch (SQLException e) {
			throw new DaoException(DbExceptionMessage.ERROR_SELECT_SQL, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(DbExceptionMessage.ERROR_GET_CONNECTION, e);
		} catch (IllegalArgumentException e) {
			throw new DaoException(DbExceptionMessage.ERROR_CONVERTION_STATUS,
					e);
		} finally {
			try {
				ConnectionPool.getInstance().closeConnection(con, pst);
			} catch (ConnectionPoolException e) {
				throw new DaoException(DbExceptionMessage.ERROR_CLOSE, e);

			}
		}
		return user;
	}

	private User getUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt(DbAttribute.USER_ID));
		user.setSurename(rs.getString(DbAttribute.USER_SURENAME));
		user.setName(rs.getString(DbAttribute.USER_NAME));
		user.setPatronymic(rs.getString(DbAttribute.USER_PATRONYMIC));
		user.setAddress(rs.getString(DbAttribute.USER_ADDRESS));
		user.setPhoneNumber(rs.getString(DbAttribute.USER_PHONE_NUMBER));
		String strType = rs.getString(DbAttribute.USER_TYPE);
		UserTypeEnum type = UserTypeEnum.valueOf(strType);
		user.setType(type);
		user.setLogin(rs.getString(DbAttribute.USER_LOGIN));
		user.setPassword(rs.getString(DbAttribute.USER_PASSWORD));

		return user;
	}

}
