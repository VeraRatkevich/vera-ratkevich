package by.epam.library.dao.exception;

import by.epam.library.exception.ProjectException;

public class DaoException extends ProjectException {

	private static final long serialVersionUID = 1L;

	public DaoException() {

		super();

	}
	
	public DaoException(String msg) {

		super(msg);

	}

	public DaoException(String msg, Exception e) {

		super(msg, e);

	}
	public DaoException(Exception e) {

		super(e);

	}
	
}
