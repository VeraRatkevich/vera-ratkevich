package by.epam.library.dao.constant;

public class DbExceptionMessage {
	public static final String ERROR_DRIVER = "Can not register connection driver.";
	public static final String ERROR_CREATE_CONNECTION = "Can not create connection to database.";
	public static final String ERROR_RETURN_CONNECTION = "Can not return connection to connection pool.";
	public static final String ERROR_GET_CONNECTION = "Can not get connection from connection pool.";
	public static final String ERROR_CLOSE_CONNECTION = "Can not close connection.";
	public static final String ERROR_CLOSE_STATEMENT = "Can not close statement.";
	public static final String ERROR_CLOSE_RESULTSET = "Can not close result set.";
	public static final String ERROR_OPEN_TRANSACTION = "Can not open transaction.";
	public static final String ERROR_CLOSE_TRANSACTION = "Can not close transaction.";	
	public static final String ERROR_CLOSE = "Can not close statement/result set/return connection to connection pool";
	public static final String ERROR_SELECT_SQL = "SELECT is failed.";
	public static final String ERROR_INSERT_SQL = "INSERT is failed.";
	public static final String ERROR_UPDATE_SQL = "UPDATE is failed.";
	public static final String ERROR_DELETE_SQL = "DELETE is failed.";
	public static final String ERROR_CONVERTION_STATUS = "Can not convert status";
	public static final String ERROR_TRANSACTION_SQL = "Transaction is failed.";
	public static final String ERROR_PARSE_STR_DATE = "Can not parse to sql/application date format";
}
