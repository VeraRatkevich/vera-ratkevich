package by.epam.library.dao;

import java.util.Date;
import java.util.List;

import by.epam.library.dao.exception.DaoException;
import by.epam.library.entity.Book;

public interface IBookDao extends IDao {
	int add(Book book) throws DaoException;

	int delete(int idBook) throws DaoException;

	Book getById(int idBook) throws DaoException;

	int update(Book book) throws DaoException;

	List<Book> showAll() throws DaoException;


	List<Book> showBooksByAuthor(String author) throws DaoException;

	List<Book> showBooksByYear(int year) throws DaoException;

	List<Book> showBooksByPeriod(Date dateStart, Date dateEnd)
			throws DaoException;

	List<Book> showBooksByPages(int pages) throws DaoException;

	List<Book> showBooksByIntervalPages(int pagesStart, int pagesEnd)
			throws DaoException;

	List<Book> showBooksByName(String name) throws DaoException;

	List<Book> showBooksByGenre(String genre) throws DaoException;

	int getCountBooks() throws DaoException;

	List<Book> showAllInLimit(int from, int count) throws DaoException;
}
