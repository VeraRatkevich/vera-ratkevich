package test.by.epam.library.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.dao.impl.DbUserDao;
import by.epam.library.entity.User;

public class DbUserDaoTest {
	
	public DbUserDao dao = DbUserDao.getInstance();

	@BeforeClass
	public static void initConnectionPool() {
		try {
			ConnectionPool.getInstance().initPoolData();
		} catch (ConnectionPoolException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getCountUsersTest() throws DaoException {
	
		List<User> userList = dao.showAll();

		int expected = userList.size();

		int actual = dao.getCountUsers();		
		
		assertEquals("Count is wrong:", expected, actual, 0.01);
	}
	
	@Test ( expected= DaoException.class )
	public void addEmptyUserTest() throws DaoException {
		int expected = 1;
		User user = new User();
		int actual =  dao.add(user);
	
		assertEquals("For adding empty user wasn't exception:", expected, actual, 0.01);
	}
	
	
	@Test
	public void deleteTest() throws DaoException {
		int expected = 0;
		int actual =  dao.delete(-1);
	
		assertEquals("Remove the object with a negative id:", expected, actual, 0.01);
	}

	
	@Test
	public void getByIdTest() throws DaoException {
		
		int count = dao.getCountUsers();
		User user =  dao.getById(count);
	
		assertNotNull(user);
	}
	
	
	@Test
	public void showAllTest() throws DaoException {
	
		List<User> userList =  dao.showAll();
	
		assertNotNull(userList);
		assertTrue(userList.size() > 0);
	}
	
	@Test
	public void showAllInLimitTest() throws DaoException {
	
		List<User> userList =  dao.showAllInLimit(0, 5);
	
		assertNotNull(userList);
		assertTrue(userList.size() <= 5);
	}
	
	
}
