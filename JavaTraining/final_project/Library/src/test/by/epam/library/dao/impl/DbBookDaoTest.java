package test.by.epam.library.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.exception.DaoException;
import by.epam.library.dao.impl.DbBookDao;
import by.epam.library.entity.Book;

public class DbBookDaoTest {

	public DbBookDao dao = DbBookDao.getInstance();

	@BeforeClass
	public static void initConnectionPool() {
		try {
			ConnectionPool.getInstance().initPoolData();
		} catch (ConnectionPoolException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getCountBooksTest() throws DaoException {

		int actual = 0;
		List<Book> bookList = dao.showAll();

		int expected = bookList.size();

		actual = dao.getCountBooks();

		assertEquals("Count is wrong:", expected, actual, 0.01);
	}

	@Test(expected = DaoException.class)
	public void addEmptyBookTest() throws DaoException {
		int expected = 1;
		Book book = new Book();
		int actual = dao.add(book);

		assertEquals("For adding empty book wasn't exception:", expected,
				actual, 0.01);
	}

	@Test
	public void deleteTest() throws DaoException {
		int expected = 0;
		int actual = dao.delete(-1);

		assertEquals("Remove the object with a negative id:", expected, actual,
				0.01);
	}

	@Test
	public void getByIdTest() throws DaoException {
		int count = dao.getCountBooks();
		Book book = dao.getById(count);

		assertNotNull(book);
	}

	@Test
	public void showAllTest() throws DaoException {

		List<Book> bookList = dao.showAll();

		assertNotNull(bookList);
		assertTrue(bookList.size() > 0);
	}

	@Test
	public void showAllInLimitTest() throws DaoException {

		List<Book> bookList = dao.showAllInLimit(0, 5);

		assertNotNull(bookList);
		assertTrue(bookList.size() <= 5);
	}

}
