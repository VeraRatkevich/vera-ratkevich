<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local"/>
<link rel="stylesheet" type="text/css" href="css/style.css">
<title><fmt:message key="navigation.login" /></title>
</head>
<body>
	<div>
		<div class="header">
			
				<c:import url="/WEB-INF/jsp/header.jsp"></c:import>
			
		</div>
		<div class = "center">
		
				<form name="loginForm" action="Controller" method="post">
					<input type="hidden" name="command" value="login" />		
					
					<fmt:message key="user.login" />
					<br /> <input type="text" name="login" value="" /> <br />
					<br /><fmt:message key="user.password" />
					<br /> <input type="password" name="password" value="" /> <br />
					 <input
						class="greenButton" type="submit"
						value="<fmt:message key="action.signin" />" /><p class="error">${errorLoginMessage}</p> <br />
				</form>
			
		</div>
		<div>
			<c:import url="/WEB-INF/jsp/footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>
