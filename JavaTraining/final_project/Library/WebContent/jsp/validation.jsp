<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local"/>
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Insert title here</title>

</head>
<body>
	<script type="text/javascript">

		function validate(obj) {
			var login = obj.login.value;
			var password = obj.password.value;
			var reppassword = obj.reppassword.value;
			var surename = obj.surename.value;
			var name = obj.name.value;
			var patronymic = obj.patronymic.value;
			var address = obj.address.value;
			var phone = obj.phone.value;

			if (login == "" || password == "" || reppassword == ""
					|| surename == "" || name == "" || patronymic == ""
					|| address == "" || phone == "") {
				document.getElementById("button_lable").innerHTML = "<fmt:message key="message.empty.fields" />";
				return false;
			} else {
				document.getElementById("button_lable").innerHTML = "";
			}

			var regLogin = /^[a-zA-Z0-9_-]{3,16}$/;

			if (!regLogin.test(login)) {
				document.getElementById("login_lable").innerHTML = "<fmt:message key="message.validation.login" />";
				return false;
			} else {
				document.getElementById("login_lable").innerHTML = "";
			}

			var regPassword = /^[a-zA-Z0-9_-]{6,18}$/;

			if (!regPassword.test(password)) {
				document.getElementById("password_lable").innerHTML = "<fmt:message key="message.validation.password" />";
				return false;
			} else {
				document.getElementById("password_lable").innerHTML = "";

			}

			if (password != reppassword) {
				document.getElementById("reppassword_lable").innerHTML = "<fmt:message key="message.password.mismatch" />";
				return false;
			} else {
				document.getElementById("reppassword_lable").innerHTML = "";
			}

			var regName = /^[А-Я]{1}[а-яА-Я-']{2,30}$/;
			var messName = "<fmt:message key="message.validation.name" />";
			if (!regName.test(surename)) {
				document.getElementById("surename_lable").innerHTML = messName;
				return false;
			} else {
				document.getElementById("surename_lable").innerHTML = "";

			}
			if (!regName.test(name)) {
				document.getElementById("name_lable").innerHTML = messName;
				return false;
			} else {
				document.getElementById("name_lable").innerHTML = "";

			}
			if (!regName.test(patronymic)) {
				document.getElementById("patronymic_lable").innerHTML = messName;
				return false;
			} else {
				document.getElementById("patronymic_lable").innerHTML = "";

			}
			
			var regPhone = /^[(]{1}[0-9]{3}[)]{1}[0-9]{7}$/;
			if (!regPhone.test(phone)) {
				document.getElementById("phone_lable").innerHTML = "<fmt:message key="message.validation.phone" />";
				return false;
			} else {
				document.getElementById("phone_lable").innerHTML = "";

			}

		}
	</script>
</body>
</html>