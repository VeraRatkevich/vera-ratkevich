<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="navigation.registration" /></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<c:import url="/jsp/validation.jsp"></c:import>
</head>
<body>
	<div>
		<jsp:useBean id="user" scope="request"
			class="by.epam.library.entity.User"></jsp:useBean>
		<jsp:setProperty name="user" property="*" />

		<div class="header">
			<c:import url="/WEB-INF/jsp/header.jsp"></c:import>
		</div>
		<div>
			<div>
				<div class="navcolumn">
					<h1>
						<fmt:message key="navigation.registration" />
					</h1>
				</div>
			</div>
			<div>

				<form class="regForm" onsubmit="return validate(this)"
					action="Controller" method="POST">
					<input type="hidden" name="command" value="registration" />
					<table>
						<tr>

							<td><fmt:message key="user.login" /></td>
							<td><input type="text" onChange="action=Controller"
								name="login" id="login" value="${user.login}" /></td>
							<td>${errorLoginMessage}<span id="login_lable"></span></td>
						</tr>

						<tr>

							<td><fmt:message key="user.password" /></td>
							<td><input type="password" name="password" id="password"
								value="" /></td>
							<td><span id="password_lable"></span></td>

						</tr>

						<tr>
							<td><fmt:message key="user.repeat.password" /></td>
							<td><input type="password" name="rpeatedPassword"
								id="reppassword" value="" /></td>
							<td><span id="reppassword_lable"></span></td>
						</tr>
						<tr>
							<td><fmt:message key="user.surename" /></td>
							<td><input type="text" name="surename" id="surename"
								value="${user.surename}" /></td>
							<td><span id="surename_lable"></span></td>
						</tr>
						<tr>
							<td><fmt:message key="user.name" /></td>
							<td><input type="text" name="name" id="name"
								value="${user.name}" /></td>
							<td><span id="name_lable"></span></td>
						</tr>
						<tr>
							<td><fmt:message key="user.patronymic" /></td>
							<td><input type="text" name="patronymic" id="patronymic"
								value="${user.patronymic}" /></td>
							<td><span id="patronymic_lable"></span></td>
						</tr>
						<tr>
							<td><fmt:message key="user.address" /></td>
							<td><input type="text" name="address" id="address"
								value="${user.address}" /></td>
							<td><span id="address_lable"></span></td>
						</tr>
						<tr>
							<td><fmt:message key="user.phone" /></td>
							<td><input type="text" name="phoneNumber" id="phone"
								value="${user.phoneNumber}" placeholder="(029)3332222"/></td>
							<td><span id="phone_lable"></span></td>
						</tr>
					</table>
					<p class="error">${errorRegisterMessage}</p>


					<input class="greenButton" type="submit"
						value="<fmt:message key="action.signup" />" /> <span
						id="button_lable"></span>


				</form>

			</div>
		</div>

		<div>
			<c:import url="/WEB-INF/jsp/footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>