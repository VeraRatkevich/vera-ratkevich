<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local"/>
<link rel="stylesheet" type="text/css" href="css/style.css">

<title><fmt:message key="navigation.about" /></title>

</head>

<body>
	<div>
		<div class="header">
				<c:import url="/WEB-INF/jsp/header.jsp"></c:import>
		</div>
		<center>
		<div class = "center">
			
		<h1><fmt:message key="userBar.greeting"/>
		<c:if test="${not empty sessionScope.userName}">
		, <c:out value="${sessionScope.userName}"></c:out>	
		
		</c:if>
		
		!</h1>
		
		</div>
		</center>
		<div>
			<c:import url="/WEB-INF/jsp/footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>