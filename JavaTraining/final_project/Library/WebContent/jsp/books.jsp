<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

<title><fmt:message key="navigation.library" /></title>

</head>
<body>

	<div class="header">
		<center>
			<c:import url="/WEB-INF/jsp/header.jsp"></c:import>
		</center>
	</div>


	<div class="search">
		
		<center>
		<c:if test="${userType=='LIBRARIAN' }">

			<form name="bookEditForm" method="GET" action="Controller">
				<input type="hidden" name="command" value="adding-book" /> <input
					class="greenButton" type="submit"
					value="<fmt:message key="action.book.new" />" />
			</form>

		</c:if>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="search-books" /> <input
					type="text" class="searchfield" name="keyword"
					value="${keyword}" /> <select name=category>

					<c:forEach items="${categoryEnum}" var="categ">
						<option ${category == categ ? 'selected' : ''} value="${categ}"><fmt:message
								key="${categ.toString().toLowerCase()}" /></option>
					</c:forEach>

				</select> <input class="greenButton" type="submit"
					value="<fmt:message key="action.search" />" /> <span
					id="button_lable"></span>


			</form>
		</center>
	</div>

	<div id="container">
		<div id="catalog">
			<c:if test="${not empty books}">
				<c:forEach items="${books}" var="book">
					<div>
						<h3>
							<span><c:out value="${book.name}" /></span>
						</h3>


						<ul>
							<li><fmt:message key="book.author" /> <b><c:out
										value="${book.author}" /></b></li>
							<li><fmt:message key="book.year" /> <b><c:out
										value="${book.year}" /></b></li>
							<li><fmt:message key="book.genre" /> <b><c:out
										value="${book.genre.name}" /></b></li>
							<li><fmt:message key="book.pages" /> <b><c:out
										value="${book.pages}" /></b></li>

							<c:if test="${userType=='READER' }">
								<c:if test="${!book.inCart }">
									<li><form method="post" action="Controller">
											<input type="hidden" name="command" value="add-to-cart" /> <input
												type="hidden" name="bookId"
												value="<c:out value='${book.id }'></c:out>" /> <input
												class="greenButton" type="submit"
												value="<fmt:message key="action.addToCart" />" />
										</form></li>
								</c:if>
								<c:if test="${book.inCart }">
									<li><form method="post" action="Controller">
											<input type="hidden" name="command" value="remove-from-cart" />
											<input type="hidden" name="bookId"
												value="<c:out value='${book.id }'></c:out>" /> <input
												class="greenButton" type="submit"
												value="<fmt:message key="action.removeFromCart" />" />
										</form></li>
								</c:if>
							</c:if>
							<c:if test="${userType=='LIBRARIAN' }">
								<li>
									<form method="GET" action="Controller">
										<input type="hidden" name="command" value="book-editor" /> <input
											type="hidden" name="bookId" value="${book.id }" /> <input
											class="greenButton" type="submit"
											value="<fmt:message key="action.edit" />" />
									</form>
								</li>
							</c:if>

						</ul>


					</div>
				</c:forEach>
			</c:if>
			<p class="error">${ errorBooksMessage}</p>
		</div>
	</div>
	<br>


	<div id="page">
		
			<c:forEach items="${pageManager.pages}" var="page">
				<a
					${page == pageManager.currentPage ? 'style="color: #0db5b5"' : ''}
					href="Controller?command=books&page=${page}">${page}</a>
			</c:forEach>
		
	</div>

	<br>




	<div>
		<c:import url="/WEB-INF/jsp/footer.jsp"></c:import>
	</div>

</body>
</html>