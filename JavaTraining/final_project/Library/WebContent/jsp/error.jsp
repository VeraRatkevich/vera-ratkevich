<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">
<title><fmt:message key="error" /></title>
</head>
<body>
	<div class="header">
		<center>
			<c:import url="/WEB-INF/jsp/header.jsp"></c:import>
		</center>
	</div>

	<strong><fmt:message key="message.error" /></strong>
	<br />
	<p class="error">${ errorUserMessage}</p>
	<div class="header">
		<center>
			<c:import url="/WEB-INF/jsp/footer.jsp"></c:import>
		</center>
	</div>
</body>
</html>


