<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

<title><fmt:message key="title.order" /></title>

</head>
<body>

	<fmt:parseDate value="${order.date}" pattern="yyyy-MM-dd" var="parsedDate" type="date" /> 
	<fmt:formatDate	value="${parsedDate}" var="formatedDate" type="date" pattern="dd-MM-yyyy" /> 
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		<div>
			<c:if test="${userType=='LIBRARIAN' }">
				<a href="Controller?command=orders"><fmt:message
						key="action.to.orders" /></a>
			</c:if>
			<c:if test="${userType=='READER' }">
				<a href="Controller?command=user-orders"><fmt:message
						key="action.to.orders" /></a>
			</c:if>
		</div>
		<br>

		<div>


			<c:if test="${not empty order}">
				<label><fmt:message key="order" /> № <b><c:out
							value="${order.id}" /> </b> <fmt:message key="order.date" />: <b><c:out
							value="${formatedDate}" /> </b> <fmt:message key="order.status" />: <b><fmt:message
							key="${order.status.toString().toLowerCase()}" /></b></label>
				<label><fmt:message key="user.reader" />: <b><c:out
							value="${order.user.surename}" /> <c:out
							value="${order.user.name}" /></b></label>
				
				<hr>
			</c:if>

			<c:if test="${not empty order.books}">
				<table class="list">
					<tr>
						<th><fmt:message key="book" /></th>
						<th><fmt:message key="order.book.date.begin" /></th>
						<th><fmt:message key="order.book.date.expect" /></th>
						<th><fmt:message key="order.book.date.end" /></th>
						<th><fmt:message key="order.book.date.return" /></th>
						<th><fmt:message key="order.book.status" /></th>
						<th><fmt:message key="action" /></th>
					</tr>


					<c:forEach items="${order.books}" var="book">
						<tr>
							<td class="bookData">
								<div>
									<c:out value="${book.name}" />
								</div>
								<div>
									<c:out value="${book.author}" />
								</div>
								<div>
									<c:out value="${book.year}" />
									,

									<c:out value="${book.pages}" />
								</div>
							</td>

							<td><fmt:parseDate value="${book.dateBegin}"
									pattern="yyyy-MM-dd" var="parsedDateBegin" type="date" /> <fmt:formatDate
									value="${parsedDateBegin}" var="formatedDateBegin" type="date"
									pattern="dd-MM-yyyy" /> <c:out value="${formatedDateBegin}" /></td>

							<td><fmt:parseDate value="${book.dateExpect}"
									pattern="yyyy-MM-dd" var="parsedDateExpect" type="date" /> <fmt:formatDate
									value="${parsedDateExpect}" var="formatedDateExpect"
									type="date" pattern="dd-MM-yyyy" /> <c:out
									value="${formatedDateExpect}" /></td>

							<td><fmt:parseDate value="${book.dateEnd}"
									pattern="yyyy-MM-dd" var="parsedDateEnd" type="date" /> <fmt:formatDate
									value="${parsedDateEnd}" var="formatedDateEnd" type="date"
									pattern="dd-MM-yyyy" /> <c:out value="${formatedDateEnd}" /></td>

							<td><fmt:parseDate value="${book.dateReturn}"
									pattern="yyyy-MM-dd" var="parsedDateReturn" type="date" /> <fmt:formatDate
									value="${parsedDateReturn}" var="formatedDateReturn"
									type="date" pattern="dd-MM-yyyy" /> <c:out
									value="${formatedDateReturn}" /></td>


							<td><fmt:message
									key="${book.status.toString().toLowerCase()}" /></td>


							<td>
								<ul>

									<li><c:if test="${userType=='LIBRARIAN' }">


											<a
												href="Controller?command=order-book-editor&idOrder=${order.id}&bookId=${book.id}"><fmt:message
													key="action.edit" /></a>



										</c:if> <c:if test="${userType=='READER' }">

											<a
												href="Controller?command=date-expect-editor&idOrder=${order.id}&bookId=${book.id}"><fmt:message
													key="action.edit" /></a>




										</c:if></li>
								</ul>
							</td>
						</tr>
					</c:forEach>

				</table>
			</c:if>




			<p class="error">${errorOrderMessage}</p>

			<c:if test="${(not empty order)and(empty order.books)}">
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="delete-order" /> <input
						type="hidden" name="idOrder" value="${order.id}" /> <input
						class="greenButton" type="submit"
						value="<fmt:message key="action.remove" />" />
				</form>
			</c:if>
		</div>

		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>