<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">
<title><fmt:message key="navigation.cart" /></title>

</head>
<body>
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>

		<div>
			<c:if test="${not empty books}">
				<form method="POST" action="Controller">
					<input type="hidden" name="command" value="cart-confirm" />
					
					<table class="list">
						<tr>
							<th><fmt:message key="book" /></th>
							<th><fmt:message key="order.book.status" /></th>
							<th><fmt:message key="action.remove" /></th>
						</tr>



						<c:forEach items="${books}" var="book">
							<tr>
								<td class="bookDataInCart">
									<div>
										<b><c:out value="${book.name}" /></b>
									</div>
									<div>
										<c:out value="${book.author}" />, 
									
										<c:out value="${book.year}" />
										,

										<c:out value="${book.pages}" />
									</div>
								</td>


								<td><c:if test="${not empty book.status}">
										<fmt:message key="${book.status.toString().toLowerCase()}" />
									</c:if> <c:if test="${empty book.status}">
										<fmt:message key="book.instok" />
									</c:if></td>
								<!-- 
								<td><big>Кликните на поле ввода даты для вызова
										календаря:</big><br> 
									<input type="hidden" name="idBookExpect" value="${book.name}" />		
										
										<input type="text" value="yyyy-mm-dd"
									id="date" onfocus="this.select();lcs(this)"
									onclick="event.cancelBubble=true;this.select();lcs(this)"
									readonly></td>
									 -->

								<td class="actionButton"><a
									href="Controller?command=remove-from-cart&bookId=${book.id}"><fmt:message
											key="action.remove" /></a></td>
							</tr>
						</c:forEach>



					</table>
					<p class="error">${ errorBooksMessage}</p>


					<input class="greenButton" type="submit"
						value="<fmt:message key="action.cart.confirm" />" />

				</form>
			</c:if>
			<c:if test="${not empty books}">
				<form method="GET" action="Controller">
					<input type="hidden" name="command" value="clear-cart" /> <input
						class="greenButton" type="submit"
						value="<fmt:message key="action.cart.clear" />" />
				</form>
			</c:if>
			<br>${cartMessage} <br />
		</div>




















		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>