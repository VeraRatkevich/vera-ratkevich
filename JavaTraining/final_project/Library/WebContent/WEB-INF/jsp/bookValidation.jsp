<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Insert title here</title>

</head>
<body>
	<script type="text/javascript">
		function validate(obj) {
			var name = obj.name.value;
			var author = obj.author.value;
			var year = obj.year.value;
			var pages = obj.pages.value;

			if (name == "" || author == "" || year == "" || pages == "") {
				document.getElementById("button_lable").innerHTML = "<fmt:message key="message.empty.fields" />";
				return false;
			} else {
				document.getElementById("button_lable").innerHTML = "";
			}

			var regAuthor = /^[а-яА-Я-.,\s]{2,50}$/;
			if (!regAuthor.test(author)) {
				document.getElementById("author_lable").innerHTML = "<fmt:message key="message.validation.author" />";
				return false;
			} else {
				document.getElementById("author_lable").innerHTML = "";

			}
			
			
			var regPages = /^[0-9]{1,5}$/;
			if (!regPages.test(pages)) {
				document.getElementById("pages_lable").innerHTML = "<fmt:message key="message.validation.pages" />";
				return false;
			} else {
				document.getElementById("pages_lable").innerHTML = "";

			}
			

			var regYear = /^[1-9][0-9]{3}$/;
			if (!regYear.test(year)) {
				document.getElementById("year_lable").innerHTML = "<fmt:message key="message.validation.year" />";
				return false;
			} else {
				document.getElementById("year_lable").innerHTML = "";

			}
			
		}
	</script>
</body>
</html>