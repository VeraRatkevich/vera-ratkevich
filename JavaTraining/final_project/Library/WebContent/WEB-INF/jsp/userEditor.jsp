<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="title.user.editor" /></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<c:import url="userEditValidation.jsp"></c:import>
</head>
<body>
<jsp:useBean id="user" scope="request"
			class="by.epam.library.entity.User"></jsp:useBean>
		<jsp:setProperty name="user" property="*" />
	<div>


		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>

		<div>
			<a href="Controller?command=users"><fmt:message
					key="action.return" /></a>

		</div>
		<br>
		<div>
			<div class="navcolumn">
				<h1>
					<fmt:message key="navigation.profile" />
				</h1>
			</div>

			<c:if test="${not empty user }">

					<form class="regForm" onsubmit="return validate(this)"
						action="Controller" method="post">
						<input type="hidden" name="command" value="edit-user" /> <input
							type="hidden" name="userId" value="${user.id}" />
							
							<span id="button_lable"></span>
							
						<table>
							<tr>
								<td><fmt:message key="user.surename" /></td>
								<td><input type="text" name="surename" id="surename"
									value="${user.surename}" /></td>
								<td><span id="surename_lable"></span></td>
							</tr>
							<tr>
								<td><fmt:message key="user.name" /></td>
								<td><input type="text" name="name" id="name"
									value="${user.name}" /></td>
								<td><span id="name_lable"></span></td>
							</tr>
							<tr>
								<td><fmt:message key="user.patronymic" /></td>
								<td><input type="text" name="patronymic" id="patronymic"
									value="${user.patronymic}" /></td>
								<td><span id="patronymic_lable"></span></td>
							</tr>
							<tr>
								<td><fmt:message key="user.address" /></td>
								<td><input type="text" name="address" id="address"
									value="${user.address}" /></td>
								<td><span id="address_lable"></span></td>
							</tr>

							<tr>

								<td><fmt:message key="user.type" /></td>
								<td><select class="category" name="userType">

										<c:forEach items="${userTypeEnum}" var="type">
											<option ${user.type == type ? 'selected' : ''}
												value="${type}"><fmt:message
													key="${type.toString().toLowerCase()}" /></option>
										</c:forEach>
								</select></td>
								<td></td>
							</tr>

							<tr>
								<td><fmt:message key="user.phone" /></td>
								<td><input type="text" name="phoneNumber" id="phone"
									value="${user.phoneNumber}" /></td>
								<td><span id="phone_lable"></span></td>
							
								
							
						</table>


						<input class="greenButton" type="submit"
							value="<fmt:message key="action.edit" />" />


					</form>

					<form action="Controller" method="POST">
						<input type="hidden" name="command" value="delete-user" /> <input
							type="hidden" name="userId" value="${user.id}" /> <input
							class="greenButton" type="submit"
							value="<fmt:message key="action.remove" />" />
					</form>




					<form method="get" action="Controller">

						<input type="hidden" name="command" value="cancel" /> <input
							class="greenButton" type="submit"
							value="<fmt:message key="action.cancel" />" />
					</form>
			</c:if>
			<p class="error">${errorUserMessage }</p>








			<div>
				<c:import url="footer.jsp"></c:import>
			</div>
		</div>
		</div>
</body>
</html>