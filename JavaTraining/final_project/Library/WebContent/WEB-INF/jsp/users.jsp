<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

<title><fmt:message key="navigation.readers" /></title>

</head>
<body>
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		<div class="orders">
			<c:if test="${not empty users}">
				<table class="list">
					<tr>
						<th><fmt:message key="user.surename" /></th>
						<th><fmt:message key="user.name" /></th>
						<th><fmt:message key="user.patronymic" /></th>
						<th><fmt:message key="user.address" /></th>
						<th><fmt:message key="user.phone" /></th>
						<th><fmt:message key="user.type" /></th>
						<th><fmt:message key="action" /></th>
					</tr>



					<c:forEach items="${users}" var="user">
						<tr>
							<td><c:out value="${user.surename}" /></td>
							<td><c:out value="${user.name}" /></td>
							<td><c:out value="${user.patronymic}" /></td>
							<td><c:out value="${user.address}" /></td>
							<td><c:out value="${user.phoneNumber}" /></td>
							<td><fmt:message key="${user.type.toString().toLowerCase()}" />
							</td>

							<td class="actionButton">
							 <a href="Controller?command=user-editor&userId=${user.id }"><fmt:message
										key="action.detail" /></a>
							</td>
						</tr>
					</c:forEach>



				</table>
				<center>
					<div>
						<c:forEach items="${pageManager.pages}" var="page">
							<a
								${page == pageManager.currentPage ? 'style="color: #0db5b5"' : ''}
								href="Controller?command=users&page=${page}">${page}</a>
						</c:forEach>
					</div>
				</center>
			</c:if>

			<p class="error">${errorUserMessage}</p>
		</div>


		<c:import url="footer.jsp"></c:import>
	</div>

</body>
</html>