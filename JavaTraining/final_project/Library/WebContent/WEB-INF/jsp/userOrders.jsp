<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="navigation.orders" /></title>

<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		<div class="orders">
			<c:if test="${not empty orders}">
				<table class="list" id="orders" border="1">
					<tr>
						<th><fmt:message key="order.id" /></th>
						<th><fmt:message key="order.date" /></th>
						<th><fmt:message key="order.status" /></th>
						<th><fmt:message key="action" /></th>
					</tr>

					<c:forEach items="${orders}" var="order">
						<tr>
							<td><c:out value="${order.id}" /></td>
							
							<fmt:parseDate value="${order.date}" pattern="yyyy-MM-dd" var="parsedDate" type="date" /> 
									<fmt:formatDate	value="${parsedDate}" var="formatedDate" type="date" pattern="dd-MM-yyyy" /> 
							
							
							<td><c:out value="${formatedDate}" /></td>
							<td><fmt:message
									key="${order.status.toString().toLowerCase()}" /></td>

							<td class="actionButton"><a
								href="Controller?command=order-detail&idOrder=${order.id }"><fmt:message
										key="action.detail" /></a></td>
						</tr>
					</c:forEach>
				</table>
				<center>
					<div >
						<c:forEach items="${pageManager.pages}" var="page">
							<a
								${page == pageManager.currentPage ? 'style="color: #0db5b5"' : ''}
								href="Controller?command=user-orders&page=${page}">${page}</a>
						</c:forEach>
					</div>
				</center>
			</c:if>


			<br>${ordersMessage}<br />

		</div>
		<c:import url="footer.jsp" />

	</div>
</body>
</html>