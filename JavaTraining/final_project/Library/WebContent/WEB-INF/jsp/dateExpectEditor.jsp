<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="title.order.book" /></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<c:import url="calendar.jsp"></c:import>
</head>
<body>
<fmt:parseDate value="${order.date}" pattern="yyyy-MM-dd" var="parsedDate" type="date" /> 
<fmt:formatDate	value="${parsedDate}" var="formatedDate" type="date" pattern="dd-MM-yyyy" /> 
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		<div>
			<a href="Controller?command=order-detail&idOrder=${order.id }"><fmt:message
					key="action.return" /></a>

		</div>
		<br>
		<div>
<c:if test="${not empty order}">
			<label><fmt:message key="order" /> №  <b><c:out
					value="${order.id}" /></b> <fmt:message key="order.date" />:  <b><c:out
					value="${formatedDate}" /></b> <fmt:message key="order.status" />: <b><fmt:message
					key="${order.status.toString().toLowerCase()}" /></b></label>
					</c:if>
<hr>
			<c:if test="${not empty book}">
				<form method="POST" action="Controller">
					<input type="hidden" name="command" value="edit-date-expect" /> <input
						type="hidden" name="idOrder" value="${order.id}" /> <input
						type="hidden" name="bookId" value="${book.id}" />

					<table class="list">
						<tr>
							<th><fmt:message key="book" /></th>
							<th><fmt:message key="order.book.date.begin" /></th>
							<th><fmt:message key="order.book.date.expect" /></th>
							<th><fmt:message key="order.book.status" /></th>
						</tr>

						<tr>
							<td class="bookData">
								<div>
									<c:out value="${book.name}" />
								</div>
								<div>
									<c:out value="${book.author}" />
								</div>
								<div>
									<c:out value="${book.year}" />, 
								
									<c:out value="${book.pages}" />
								</div>
							</td>

							<td>
							<fmt:parseDate value="${book.dateBegin}" pattern="yyyy-MM-dd" var="parsedDateBegin" type="date" />
                            <fmt:formatDate value="${parsedDateBegin}" var="formatedDateBegin" type="date" pattern="dd-MM-yyyy" />
							<c:out value="${formatedDateBegin}" /></td>
							
							<td>
							
							<fmt:parseDate value="${book.dateExpect}" pattern="yyyy-MM-dd" var="parsedDateExpect" type="date" />
                            <fmt:formatDate value="${parsedDateExpect}" var="formatedDateExpect" type="date" pattern="dd-MM-yyyy" />
							
							<input type="text" name="dateExpect"
								onfocus="this.select();lcs(this)"
								onclick="event.cancelBubble=true;this.select();lcs(this)"
								readonly value="${formatedDateExpect}" /></td>
							
							
							<td><fmt:message
									key="${book.status.toString().toLowerCase()}" /></td>
						</tr>

					</table>

					<input class="greenButton" type="submit"
						value="<fmt:message key="action.edit" />" />
				</form>
				<form method="GET" action="Controller">
					<input type="hidden" name="command" value="cancel" /> <input
						class="greenButton" type="submit"
						value="<fmt:message key="action.cancel" />" />
				</form>
			</c:if>



		</div>
		<p class="error">${errorBooksMessage}</p>
		<p>${success}</p>
		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>


</body>
</html>