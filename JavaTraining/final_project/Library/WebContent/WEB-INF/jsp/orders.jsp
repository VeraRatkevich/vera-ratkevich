<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="navigation.orders" /></title>

<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		<div>
			<c:if test="${not empty orders}">
				<center>
					<table>
						<tr>
							<td><fmt:message key="action.sorting" /></td>

							<td><form action="Controller" method="post">
									<input type="hidden" name="command" value="orders" /> <input
										type="hidden" name="page" value="${pageManager.currentPage}" />
									<select class="category" name="criterion"
										onchange="this.form.submit()">
										<c:forEach items="${criterionEnum}" var="crit">
											<option ${criterion == crit ? 'selected' : ''}
												value="${crit}"><fmt:message
													key="${crit.toString().toLowerCase()}" /></option>
										</c:forEach>

									</select>
								</form>
								</td>
						</tr>
					</table>
				</center>
				<br />

				<table class="list">
					<tr>
						<th><fmt:message key="order.id" /></th>
						<th><fmt:message key="user.reader" /></th>
						<th><fmt:message key="order.date" /></th>
						<th><fmt:message key="order.status" /></th>
						<th><fmt:message key="action" /></th>
					</tr>

					<c:forEach items="${orders}" var="order">
						<tr>
							<td><c:out value="${order.id}" /></td>

							<td><c:out value="${order.user.surename}" /> <c:out
									value="${order.user.name}" /></td>

							<fmt:parseDate value="${order.date}" pattern="yyyy-MM-dd"
								var="parsedDate" type="date" />
							<fmt:formatDate value="${parsedDate}" var="formatedDate"
								type="date" pattern="dd-MM-yyyy" />

							<td><c:out value="${formatedDate}" /></td>
							<td><fmt:message
									key="${order.status.toString().toLowerCase()}" /></td>

							<td class="actionButton"><a
								href="Controller?command=order-detail&idOrder=${order.id }"><fmt:message
										key="action.detail" /></a></td>
						</tr>
					</c:forEach>
				</table>
				<center>
					<div>
						<c:forEach items="${pageManager.pages}" var="page">
							<a
								${page == pageManager.currentPage ? 'style="color: #0db5b5"' : ''}
								href="Controller?command=orders&page=${page}&criterion=${criterion}">${page}</a>
						</c:forEach>
					</div>
				</center>
			</c:if>


			<br>${ordersMessage}<br />

		</div>
		<c:import url="footer.jsp" />

	</div>
</body>
</html>