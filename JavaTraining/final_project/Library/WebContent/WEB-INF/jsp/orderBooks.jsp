<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

<title><fmt:message key="navigation.order.books" /></title>

</head>
<body>
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>

		<div>

			<c:if test="${not empty books}">
				<table class="list">
					<tr>
						<th><fmt:message key="book" /></th>
						<th><fmt:message key="reader" /></th>
						<th><fmt:message key="order.book.date.begin" /></th>
						<th><fmt:message key="order.book.date.expect" /></th>
						<th><fmt:message key="order.book.date.end" /></th>
						<th><fmt:message key="order.book.date.return" /></th>
						<th><fmt:message key="order.book.status" /></th>
						<th><fmt:message key="action" /></th>
					</tr>


					<c:forEach items="${books}" var="book">
						<tr>
							<td class="bookData">
								<div>
									<c:out value="${book.orderBook.name}" />
								</div>
								<div>
									<c:out value="${book.orderBook.author}" />
								</div>
								<div>
									<c:out value="${book.orderBook.year}" />
									,

									<c:out value="${book.orderBook.pages}" />
								</div>
							</td>
							<td>
								
									<c:out value="${book.readerName}" /> 
						
								
									<c:out value="${book.readerSurename}" />
								
								
							</td>
			


<td><fmt:parseDate value="${book.orderBook.dateBegin}"
									pattern="yyyy-MM-dd" var="parsedDateBegin" type="date" /> <fmt:formatDate
									value="${parsedDateBegin}" var="formatedDateBegin" type="date"
									pattern="dd-MM-yyyy" /> <c:out value="${formatedDateBegin}" /></td>

							<td><fmt:parseDate value="${book.orderBook.dateExpect}"
									pattern="yyyy-MM-dd" var="parsedDateExpect" type="date" /> <fmt:formatDate
									value="${parsedDateExpect}" var="formatedDateExpect"
									type="date" pattern="dd-MM-yyyy" /> <c:out
									value="${formatedDateExpect}" /></td>

							<td><fmt:parseDate value="${book.orderBook.dateEnd}"
									pattern="yyyy-MM-dd" var="parsedDateEnd" type="date" /> <fmt:formatDate
									value="${parsedDateEnd}" var="formatedDateEnd" type="date"
									pattern="dd-MM-yyyy" /> <c:out value="${formatedDateEnd}" /></td>

							<td><fmt:parseDate value="${book.orderBook.dateReturn}"
									pattern="yyyy-MM-dd" var="parsedDateReturn" type="date" /> <fmt:formatDate
									value="${parsedDateReturn}" var="formatedDateReturn"
									type="date" pattern="dd-MM-yyyy" /> <c:out
									value="${formatedDateReturn}" /></td>

							<td><fmt:message
									key="${book.orderBook.status.toString().toLowerCase()}" /></td>

							<td><a
								href="Controller?command=order-book-editor&bookId=${book.orderBook.id}&idOrder=${book.orderId}"><fmt:message
										key="action.edit" /></a></td>
						</tr>
					</c:forEach>

				</table>
			</c:if>
			<p class="error">${errorOrderBookMessage}</p>


		</div>
		<center>
			<div>
				<c:forEach items="${pageManager.pages}" var="page">
					<a
						${page == pageManager.currentPage ? 'style="color: #0db5b5"' : ''}
						href="Controller?command=order-books&page=${page}">${page}</a>
				</c:forEach>
			</div>
		</center>
		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>