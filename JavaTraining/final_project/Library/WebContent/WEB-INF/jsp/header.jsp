<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />


<div id="headerMain">
	<div id="header" class="navtable">
		<div class="navcolumn">

			<h1>
				<fmt:message key="label.library" />
			</h1>
			<p>
				<fmt:message key="lable.library.slogan" />
			</p>

		</div>

		<div class="navcolumn">
			<form name="coffeeshopForm" method="get" action="Controller">
				<input type="hidden" name="command" value="books" /> <input
					type="submit" value="<fmt:message key="navigation.library" />" />
			</form>
		</div>
		<c:if test="${userType=='READER' or userType=='LIBRARIAN'}">
			<div class="navcolumn">
				<form name="accountForm" method="get" action="Controller">
					<input type="hidden" name="command" value="profile" /> <input
						type="submit" value="<fmt:message key="navigation.profile" />" />
				</form>
			</div>

			<c:if test="${userType=='LIBRARIAN' }">
				<div class="navcolumn">
					<form name="ordersForm" method="get" action="Controller">
						<input type="hidden" name="command" value="orders" /> <input
							type="submit" value="<fmt:message key="navigation.orders" />" />
					</form>
				</div>
			</c:if>

			<c:if test="${userType=='READER' }">
				<div class="navcolumn">
					<form name="ordersForm" method="get" action="Controller">
						<input type="hidden" name="command" value="user-orders" /> <input
							type="submit" value="<fmt:message key="navigation.orders" />" />
					</form>
				</div>
			</c:if>

			<c:if test="${userType=='READER'}">
				<div class="navcolumn">
					<form name="ordersForm" method="get" action="Controller">
						<input type="hidden" name="command" value="cart" /><input
							type="submit"
							value="<fmt:message key="navigation.cart" />(<c:out value="${sessionScope.cart.size()}"></c:out>)" />

					</form>


				</div>

			</c:if>


			<c:if test="${userType=='LIBRARIAN' }">
				<div class="navcolumn">
					<form name="ordersForm" method="get" action="Controller">
						<input type="hidden" name="command" value="users" /> <input
							type="submit" value="<fmt:message key="navigation.readers" />" />
					</form>
				</div>

				<div class="navcolumn">
					<form name="ordersForm" method="get" action="Controller">
						<input type="hidden" name="command" value="order-books" /> <input
							type="submit"
							value="<fmt:message key="navigation.order.books" />" />
					</form>
				</div>
			</c:if>
		</c:if>

		<div class="navcolumn">
			<form name="aboutForm" method="get" action="Controller">
				<input type="hidden" name="command" value="about" /><input
					type="submit" value="<fmt:message key="navigation.about" />" />
			</form>
		</div>

		<c:if test="${not empty userType and userType!='GUEST'}">
			<div class="navcolumn">
				<form name="aboutForm" method="get" action="Controller">
					<input type="hidden" name="command" value="logout" /> <input
						type="submit" value="<fmt:message key="navigation.logout" />" />
				</form>
			</div>
		</c:if>



		<c:if test="${empty userType or userType=='GUEST' }">
			<div class="navcolumn">
				<form name="registerForm" method="get" action="Controller">
					<input type="hidden" name="command" value="registration-page" /> <input
						type="submit"
						value="<fmt:message key="navigation.registration" />" />
				</form>
			</div>

			<div class="navcolumn">
				<form name="loginForm" method="get" action="Controller">
					<input type="hidden" name="command" value="login-page" /> <input
						type="submit" value="<fmt:message key="navigation.login" />" />
				</form>

			</div>
		</c:if>



	</div>

</div>
