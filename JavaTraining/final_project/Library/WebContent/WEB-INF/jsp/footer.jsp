<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<div class="footer">
	<div style="float: left; width: 70%;">
		<p>
			<fmt:message key="footer.copyright" />
		</p>
	</div>
	<div style="float: right; width: 20%;">
		<table class="local">
			<tr>
				<td><fmt:message key="local.language" />:&nbsp;&nbsp;</td>
				<td><a href="Controller?command=local&local=en"><fmt:message
							key="local.locbutton.name.en" /></a></td>


				<td><a href="Controller?command=local&local=ru"><fmt:message
							key="local.locbutton.name.ru" /></a></td>
			</tr>
		</table>
	</div>
</div>

