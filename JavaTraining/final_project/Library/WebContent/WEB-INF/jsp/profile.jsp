<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="navigation.profile" /></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div>
		<jsp:useBean id="user" scope="request"
			class="by.epam.library.entity.User"></jsp:useBean>
		<jsp:setProperty name="user" property="*" />

		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		<center>
		<div>
		
			<div>
				<div >
					<h1>
						<fmt:message key="navigation.profile" />
					</h1>
				</div>


			</div>


			<table>
				<tr>
					<td><fmt:message key="user.login" /></td>
					<td><input type="text" readonly="readonly"
						value="${user.login}" /></td>
				</tr>
				<tr>
					<td><fmt:message key="user.surename" /></td>
					<td><input type="text" readonly="readonly"
						value="${user.surename}" /></td>
				</tr>
				<tr>

					<td><fmt:message key="user.name" /></td>

					<td><input type="text" readonly="readonly"
						value="${user.name}" /></td>
				</tr>
				<tr>
					<td><fmt:message key="user.patronymic" /></td>
					<td><input type="text" readonly="readonly"
						value="${user.patronymic}" /></td>

				</tr>
				<tr>
					<td><fmt:message key="user.address" /></td>
					<td><input type="text" readonly="readonly"
						value="${user.address}" /></td>

				</tr>


				<tr>
					<td><fmt:message key="user.phone" /></td>
					<td><input type="text" readonly="readonly"
						value="${user.phoneNumber}" /></td>

				</tr>

			</table>

		</div>
</center>
		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>