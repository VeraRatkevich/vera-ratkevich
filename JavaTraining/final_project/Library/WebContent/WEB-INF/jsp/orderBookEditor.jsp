<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="title.order.book" /></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<c:import url="calendar.jsp"></c:import>
</head>
<body>
	<fmt:parseDate value="${order.date}" pattern="yyyy-MM-dd" var="parsedDate" type="date" /> 
									<fmt:formatDate	value="${parsedDate}" var="formatedDate" type="date" pattern="dd-MM-yyyy" /> 
 <fmt:parseDate value="${book.dateBegin}" pattern="yyyy-MM-dd" var="parsedDateBegin" type="date" /> 
									<fmt:formatDate	value="${parsedDateBegin}" var="formatedDateBegin" type="date" pattern="dd-MM-yyyy" /> 
									
								    
								   <fmt:parseDate value="${book.dateExpect}" pattern="yyyy-MM-dd" var="parsedDateExpect" type="date" /> 
									<fmt:formatDate	value="${parsedDateExpect}" var="formatedDateExpect" type="date" pattern="dd-MM-yyyy" /> 
									
								    
								    <fmt:parseDate value="${book.dateEnd}" pattern="yyyy-MM-dd" var="parsedDateEnd" type="date" /> 
									<fmt:formatDate	value="${parsedDateEnd}" var="formatedDateEnd" type="date" pattern="dd-MM-yyyy" /> 
									
								    
								    <fmt:parseDate value="${book.dateReturn}" pattern="yyyy-MM-dd" var="parsedDateReturn" type="date" /> 
									<fmt:formatDate	value="${parsedDateReturn}" var="formatedDateReturn" type="date" pattern="dd-MM-yyyy" />
	<div>
		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>




		<c:if test="${empty order}">
			<div>
				<a href="Controller?command=orders"><fmt:message
						key="action.return" /></a>

			</div>
		</c:if>
		<c:if test="${not empty order}">
			<div>
				<a href="Controller?command=order-detail&idOrder=${order.id }"><fmt:message
						key="action.to.order" /></a>

			</div>
		</c:if>
		<br>
		<div>
			<c:if test="${not empty order}">
				<label><fmt:message key="order" /> № <b><c:out
							value="${order.id}" /></b> <fmt:message key="order.date" />: <b><c:out
							value="${formatedDate}" /></b> <fmt:message key="order.status" />: <b><fmt:message
							key="${order.status.toString().toLowerCase()}" /></b></label>
				<label><fmt:message key="user.reader" />: <b><c:out
							value="${order.user.surename}" /> <c:out
							value="${order.user.name}" /></b></label>
			</c:if>
			<hr>
			<c:if test="${not empty book}">
				<form method="POST" action="Controller">
					<input type="hidden" name="command" value="edit-order-book" /> <input
						type="hidden" name="idOrder" value="${order.id}" /> <input
						type="hidden" name="bookId" value="${book.id}" /> <input
						type="hidden" name="orderStatus" value="${order.status}" />



					<table class="list">
						<tr>
							<th><fmt:message key="book" /></th>
							<th><fmt:message key="order.book.date.begin" /></th>
							<th><fmt:message key="order.book.date.expect" /></th>
							<th><fmt:message key="order.book.date.end" /></th>
							<th><fmt:message key="order.book.date.return" /></th>
							<th><fmt:message key="order.book.status" /></th>
						</tr>

						<tr>
							<td class="bookData">
								<div>
									<c:out value="${book.name}" />
								</div>
								<div>
									<c:out value="${book.author}" />
								</div>
								<div>
									<c:out value="${book.year}" />
									,

									<c:out value="${book.pages}" />
								</div>
							</td>

							  	<td><input type="text" name="dateBegin"
								onfocus="this.select();lcs(this)"
								onclick="event.cancelBubble=true;this.select();lcs(this)"
								readonly value="${formatedDateBegin}" /></td>
							<td><input type="text" name="dateExpect"
								onfocus="this.select();lcs(this)"
								onclick="event.cancelBubble=true;this.select();lcs(this)"
								readonly value="${formatedDateExpect}" /></td>
							<td><input type="text" name="dateEnd"
								onfocus="this.select();lcs(this)"
								onclick="event.cancelBubble=true;this.select();lcs(this)"
								readonly value="${formatedDateEnd}" /></td>
							<td><input type="text" name="dateReturn"
								onfocus="this.select();lcs(this)"
								onclick="event.cancelBubble=true;this.select();lcs(this)"
								readonly value="${formatedDateReturn}" /></td>


                         
							    
									
								    
							
							
							
							
							
							
							

							<td><select class="category" name="bookStatus">

									<c:forEach items="${statusEnum}" var="bStatus">
										<option ${book.status == bStatus ? 'selected' : ''}
											value="${bStatus}"><fmt:message
												key="${bStatus.toString().toLowerCase()}" /></option>
									</c:forEach>



							</select></td>

						</tr>

					</table>

					<input class="greenButton" type="submit"
						value="<fmt:message key="action.edit" />" />
				</form>
				<form method="POST" action="Controller">

					<input type="hidden" name="command" value="delete-order-book" /> <input
						type="hidden" name="idOrder" value="${order.id }" /> <input
						type="hidden" name="bookId" value="${book.id}" /><input
						class="greenButton" type="submit"
						value="<fmt:message key="action.remove" />" />
				</form>
				<form method="get" action="Controller">

					<input type="hidden" name="command" value="cancel" /> <input
						class="greenButton" type="submit"
						value="<fmt:message key="action.cancel" />" />
				</form>
			</c:if>
		</div>
		<p class="error">${errorBookMessage}</p>
		<p>${success}</p>
		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>


</body>
</html>