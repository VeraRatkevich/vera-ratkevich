<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local"/>
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Insert title here</title>

</head>
<body>
<script type="text/javascript">

		function validate(obj) {
			var surename = obj.surename.value;
			var name = obj.name.value;
			var patronymic = obj.patronymic.value;
			var address = obj.address.value;
			var phone = obj.phone.value;

			if (surename == "" || name == "" || patronymic == ""
					|| address == "" || phone == "") {
				document.getElementById("button_lable").innerHTML = "<fmt:message key="message.empty.fields" />";
				return false;
			} else {
				document.getElementById("button_lable").innerHTML = "";
			}

			var regName = /^[А-Я]{1}[а-яА-Я-']{2,30}$/;
			var messName = "В поле допускаются русские буквы, дефисы и апострафы, от 3 до 30 символов, первая буква заглавная";
			if (!regName.test(surename)) {
				document.getElementById("surename_lable").innerHTML = messName;
				return false;
			} else {
				document.getElementById("surename_lable").innerHTML = "";

			}
			if (!regName.test(name)) {
				document.getElementById("name_lable").innerHTML = messName;
				return false;
			} else {
				document.getElementById("name_lable").innerHTML = "";

			}
			if (!regName.test(patronymic)) {
				document.getElementById("patronymic_lable").innerHTML = messName;
				return false;
			} else {
				document.getElementById("patronymic_lable").innerHTML = "";

			}
			
			var regPhone = /^[(]{1}[0-9]{3}[)]{1}[0-9]{7}$/;
			if (!regPhone.test(phone)) {
				document.getElementById("phone_lable").innerHTML = "Номер следует вводить в формате (029)3332222";
				return false;
			} else {
				document.getElementById("phone_lable").innerHTML = "";

			}

		}
	</script>
</body>
</html>