<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" />
<title><fmt:message key="adding.book" /></title>


<link rel="stylesheet" type="text/css" href="css/style.css">
<c:import url="bookValidation.jsp"></c:import>
</head>
<body>

	<div>
		<jsp:useBean id="book" scope="request"
			class="by.epam.library.entity.Book"></jsp:useBean>
		<jsp:setProperty name="book" property="*" />

		<div class="header">
			<c:import url="header.jsp"></c:import>
		</div>
		
		<div>
		<a href = "Controller?command=books"><fmt:message key="action.return" /></a>
		
		</div>
		<br>
		<div>

			<div class="navcolumn">
				<h1>
					<fmt:message key="adding.book" />
				</h1>

			</div>

			<div>
				<form class="bookForm" onsubmit="return validate(this)"
					action="Controller" method="POST">
					<input type="hidden" name="command" value="add-new-book" />
					<span id="button_lable"></span> 
					
<table>
					<tr>

						<td><fmt:message key="book.name" /></td>
						<td><input type="text" name="name" id="name" value="${book.name}" /></td>
						<td><span id="name_lable"></span></td>
					</tr>

					<tr>

						<td><fmt:message key="book.author" /></td>
						<td><input type="text" name="author" id="author"
							value="${book.author}" /></td>
							<td><span id="author_lable"></span></td>

					</tr>

					<tr>
						<td><fmt:message key="book.year" /></td>
						<td><input type="text" name="year" id="year" value="${book.year}" /></td>
						<td><span id="year_lable"></span></td>
					</tr>
					<tr>
						<td><fmt:message key="book.pages" /></td>
						<td><input type="text" name="pages" id="pages" value="${book.pages}" /></td>
						<td><span
							id="pages_lable"></span></td>
					</tr>
					<tr>
						<td><fmt:message key="book.genre" /></td>
					
						<td><select class="category" name="genreId">

							<c:forEach items="${genres}" var="genre">
								<option ${book.genre.name == genre.name ? 'selected' : ''}
									value="${genre.id}">${genre.name}</option>
							</c:forEach>

						</select></td>
<td></td>
					</tr>
					
					
					
</table>
					
							<input class="greenButton" type="submit"
								value="<fmt:message key="action.add" />" />
					
				</form>
			

					<form action="Controller" method="GET">
						<input type="hidden" name="command" value="cancel" /> <input
							class="greenButton" type="submit"
							value="<fmt:message key="action.cancel" />" />

					</form>

			</div>
		</div>
		<p class="error">${ errorBookMessage}</p>
	

		<div>
			<c:import url="footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>