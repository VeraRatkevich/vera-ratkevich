<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Login Page</title>


<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.loginform.resources.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>

	<div>
		<center>
			<form name="loginForm" action="Controller" method="post">
				<input type="hidden" name="command" value="login" />
				<fmt:message key="user.login" />
				<br /> <input type="text" name="login" value="" /> <br />
				<fmt:message key="user.password" />
				<br /> <input type="password" name="password" value="" /> <br />
				${errorLoginMessage}  <br /> <input
					class="greenButton" type="submit"
					value="<fmt:message key="label.button.signin" />" />
			</form>
		</center>
	</div>
<div>
			<c:import url="/jsp/footer.jsp"></c:import>
		</div>
</body>
</html>
