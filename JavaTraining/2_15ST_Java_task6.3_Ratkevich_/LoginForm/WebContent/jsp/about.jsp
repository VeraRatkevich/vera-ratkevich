<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.loginform.resources.local" />
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Insert title here</title>

</head>



<body>
	<div>

		<div>
			<fmt:message key="userBar.greeting" />
			!

		</div>

		<div>
			<form method="POST" action="Controller">
				<input type="hidden" name="command" value="logout" /> <a href="#"
					onClick="submit()"><fmt:message key="userBar.logout" /></a>
			</form>

		</div>

		<div>
			<c:import url="/jsp/footer.jsp"></c:import>
		</div>
	</div>
</body>
</html>