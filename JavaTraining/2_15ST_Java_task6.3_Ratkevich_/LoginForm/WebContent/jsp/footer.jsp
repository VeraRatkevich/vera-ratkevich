<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.loginform.resources.local" />
<div class="footer">
	<div style="float: left; width: 70%;">
		<p>
			<fmt:message key="footer.copyright" />
		</p>
	</div>
	<div style="float: left; width: 20%;">
		<table class="local">
			<tr>
				<td><fmt:message key="langBar.language" />:&nbsp;&nbsp;</td>
				<td>

					<form method="POST" action="Controller">
						<input type="hidden" name="local" value="ru" /> <input
							type="hidden" name="command" value="local" /><a href="#"
							onClick="submit()"><fmt:message key="local.locbutton.name.ru" /></a>
					</form>
					</td>
					
					<td>
					<form method="POST" action="Controller">
						<input type="hidden" name="local" value="en" /> <input
							type="hidden" name="command" value="local" /><a href="#"
							onClick="submit()"><fmt:message key="local.locbutton.name.en" /></a>
					</form>

				</td>
			</tr>
		</table>
	</div>
</div>

