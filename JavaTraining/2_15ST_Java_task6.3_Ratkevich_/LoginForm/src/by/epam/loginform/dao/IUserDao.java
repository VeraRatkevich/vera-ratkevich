package by.epam.loginform.dao;

import java.util.List;

import by.epam.loginform.dao.exception.DaoException;
import by.epam.loginform.entity.User;

public interface IUserDao extends IDao {
	
	int add(User user) throws DaoException;

	int delete(int idUser) throws DaoException;

	User getByLoginPasword(String login, String password) throws DaoException;

	User getById(int idUser) throws DaoException;

	int update(User user) throws DaoException;

	List<User> showAll() throws DaoException;

	int getIdUserByLogin(String login) throws DaoException;
}
