package by.epam.loginform.dao.exception;

import by.epam.loginform.exception.ProjectException;

public class DaoException extends ProjectException {

	private static final long serialVersionUID = 1L;

	public DaoException(String msg) {

		super(msg);

	}

	public DaoException(String msg, Exception e) {

		super(msg, e);

	}
	
	
}
