package by.epam.loginform.dao;

import by.epam.loginform.dao.exception.DaoException;
import by.epam.loginform.dao.impl.DbUserDao;

public class DaoFactory {
	private final static DaoFactory instance = new DaoFactory();

	public static DaoFactory getInstance() {

		return instance;

	}

	public IDao getDAO(DAOType type) throws DaoException {

		switch (type) {

		case DB:
			return DbUserDao.getInstance();
		default:

			throw new DaoException("No such DAO");

		}

	}

	public enum DAOType {

		DB,XML;

	}
	
}
