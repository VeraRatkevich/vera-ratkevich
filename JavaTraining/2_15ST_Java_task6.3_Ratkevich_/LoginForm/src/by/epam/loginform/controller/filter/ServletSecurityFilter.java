package by.epam.loginform.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.epam.loginform.controller.constant.SessionAttributeName;
import by.epam.loginform.entity.constant.UserTypeEnum;

public class ServletSecurityFilter implements Filter {

	static int counter = 0;

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		System.out.println("Выполнение фильтра");

		UserTypeEnum type = (UserTypeEnum) session
				.getAttribute(SessionAttributeName.USER_TYPE);
		System.out.println("юзертайп " + type);
		if (type == null) {
			type = UserTypeEnum.GUEST;
			session.setAttribute(SessionAttributeName.USER_TYPE, type);
		
		}
		// pass the request along the filter chain
		chain.doFilter(request, response);

	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}