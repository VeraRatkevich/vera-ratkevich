package by.epam.loginform.controller.constant;

public class JspPageMessage {
	public static final String ACCESS_DENIED = "message.access.denied";
	public static final String EMPTY_FIELDS = "message.empty.fields";
	public static final String LOGIN_PASSWORD_INCORRECT = "message.login.password.incorrect";
	public static final String PASSWORDS_MISMATCH = "message.password.mismatch";
	public static final String USER_EXISTS = "message.user.exists";
	public static final String USER_ROLE_UNKNOWN = "message.user.role.unknown";
	public static final String WRONG_ACTION = "message.wrong.action";
	public static final String NULL_PAGE = "message.nullpage";
	public static final String INVALID_VALUE = "message.amount.invalid";
	public static final String BOOK_NOT_AVALIABLE = "message.book.empty";	
	public static final String REQUEST = "message.exception.request";	
	public static final String STATUS_CODE = "message.exception.status.code";
	public static final String EXCEPTION_MESSAGE = "message.exception";
	
	public static final String SUCCESS_MESSAGE = "message.success";
}
