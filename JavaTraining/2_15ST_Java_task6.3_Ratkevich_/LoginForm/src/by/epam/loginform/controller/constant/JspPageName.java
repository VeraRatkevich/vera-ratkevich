package by.epam.loginform.controller.constant;

public class JspPageName {
	private JspPageName() {
	}

	public static final String USER_PAGE = "/jsp/UserPage.jsp";
	public static final String LOGIN_PAGE = "/jsp/login.jsp";
	public static final String MAIN_PAGE = "/jsp/main.jsp";
	public static final String INDEX_PAGE = "/index.jsp";
	public static final String ERROR_PAGE = "/jsp/error.jsp";
	public static final String REGISTRATION_PAGE = "/jsp/registration.jsp";
	public static final String ADD_BOOK_PAGE = "/jsp/addBook.jsp";
	public static final String ORDERS_PAGE = "/jsp/orders.jsp";
	public static final String ORDER_BOOKS_PAGE = "/jsp/orderBooks.jsp";

	public static final String BOOKS_PAGE = "/jsp/books.jsp";
	public static final String USERS_PAGE = "/jsp/users.jsp";
	
	public static final String ABOUT_PAGE = "/jsp/about.jsp";
	public static final String CART_PAGE = "/jsp/cart.jsp";
	
	public static final String ORDER_DETAIL = "/jsp/orderDetail.jsp";
	public static final String ORDER_BOOK_EDITOR = "/jsp/orderBookEditor.jsp";
}