package by.epam.loginform.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.loginform.controller.constant.JspPageName;
import by.epam.loginform.service.ICommand;
import by.epam.loginform.service.exception.ServiceException;

public final class Controller extends HttpServlet {


	private static final long serialVersionUID = 1L;

	public Controller() {

		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		processRequest(request, response);

	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("processRequest(HttpServletRequest request,HttpServletResponse response)");
		
		ICommand command = ControllerHelper.getInstance().getCommand(
				request);

		System.out.println(command);

		String page = null;;
		// System.out.println();
		try {
			page = command.execute(request);

			// page = JspPageName.USER_PAGE;
		} catch (ServiceException e) {
			System.out.println("CommandException");
			page = JspPageName.ERROR_PAGE;

		} catch (Exception e) {
		 System.out.println("CommandException");
			page = JspPageName.ERROR_PAGE;

		}

	
		RequestDispatcher dispatcher = request.getRequestDispatcher(page);
		if (dispatcher != null) {

			dispatcher.forward(request, response);

		} else {

			errorMessageDireclyFromresponse(response);

		}
		
	
	}

	// ////////////////////
	private void errorMessageDireclyFromresponse(HttpServletResponse response)
			throws IOException {

		response.setContentType("text/html");

		response.getWriter().println("E R R O R");

	}

}
