package by.epam.loginform.controller.constant;

public class SessionAttributeName {
	
public static final String USER_TYPE = "userType";
public static final String USER_ID = "userId";
public static final String USER_NAME = "userName";
public static final String PAGE_TYPE = "pageType";
public static final String LOCAL = "local";
public static final String CART = "cart";
public static final String CURRENT_REQUEST = "currentRequest";
}
