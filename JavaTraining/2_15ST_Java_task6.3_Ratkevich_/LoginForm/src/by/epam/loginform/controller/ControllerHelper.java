package by.epam.loginform.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.loginform.controller.constant.JspPageMessage;
import by.epam.loginform.controller.constant.RequestAttributeName;
import by.epam.loginform.controller.constant.RequestParameterName;
import by.epam.loginform.controller.constant.SessionAttributeName;
import by.epam.loginform.entity.constant.UserTypeEnum;
import by.epam.loginform.resources.MessageManager;
import by.epam.loginform.service.CommandName;
import by.epam.loginform.service.ICommand;
import by.epam.loginform.service.impl.ChangeLocalCommand;
import by.epam.loginform.service.impl.DefaultCommand;
import by.epam.loginform.service.impl.LoginCommand;
import by.epam.loginform.service.impl.LogoutCommand;


public class ControllerHelper {
	private static final ControllerHelper instance = new ControllerHelper();
	private final Map<CommandName, ICommand> commands = new HashMap<>();

	public ControllerHelper() {
		System.out.println("Конструктор ControllerHelper");
		commands.put(CommandName.LOGIN, new LoginCommand());
		commands.put(CommandName.LOGOUT, new LogoutCommand());
		commands.put(CommandName.LOCAL, new ChangeLocalCommand());
	}

	public static ControllerHelper getInstance() {

		return instance;

	}

	public ICommand getCommand(HttpServletRequest request) {

		System.out.println("Сработал метод getCommand");

		ICommand command = null;

		String strCommandName = request
				.getParameter(RequestParameterName.COMMAND);
		System.out.println(strCommandName);
		if (strCommandName == null) {
			command = new DefaultCommand();
			return command;
		}
		try {
			CommandName commandName = CommandName.valueOf(strCommandName
					.toUpperCase().replace("-", "_"));
			System.out.println(commandName);
			if (commandName == null) {
				command = new DefaultCommand();
				return command;
				// /и сообщение
			}

			UserTypeEnum commandUserType = commandName.getUserType();

			HttpSession session = request.getSession();

			UserTypeEnum currentUserType = (UserTypeEnum) session
					.getAttribute(SessionAttributeName.USER_TYPE);

			if (checkUserType(currentUserType, commandUserType)) {
				command = commands.get(commandName);
			} else {
				command = new DefaultCommand();
				// сообщение в запрос
			}
		} catch (IllegalArgumentException e) {
			request.setAttribute(RequestAttributeName.WRONG_ACTION,
					MessageManager.getMessage(JspPageMessage.WRONG_ACTION));
		}
		return command;

	}

	private boolean checkUserType(UserTypeEnum currentUserType,
			UserTypeEnum commandUserType) {

		if (currentUserType.ordinal() >= commandUserType.ordinal()) {
			return true;
		} else {
			return false;
		}

	}

}
