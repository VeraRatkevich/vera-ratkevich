package by.epam.loginform.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import by.epam.loginform.dao.connectionpool.ConnectionPool;
import by.epam.loginform.dao.connectionpool.ConnectionPoolException;

public class LibraryContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent ev) {

		try {
			ConnectionPool.getInstance().initPoolData();
		} catch (ConnectionPoolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent ev) {
	}

}
