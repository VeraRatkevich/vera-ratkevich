package by.epam.loginform.entity.constant;

public enum UserTypeEnum {
	GUEST,
	UNCONFIRMED_READER,
	READER,
	LIBRARIAN
}
