package by.epam.loginform.entity;

import by.epam.loginform.entity.constant.UserTypeEnum;

public class User {
	
	
	private int id;
	private String surename;
	private String name;
	private String patronymic;
	private String address;
	private String phoneNumber;
	private UserTypeEnum type;
	private String login;
	private String password;

	public User() {
		type = UserTypeEnum.GUEST;
	}

	public User(int id, String surename, String name, String patronymic,
			String address, String phoneNumber, UserTypeEnum type,
			String email, String login, String password) {
		this.setId(id);
		this.setSurename(surename);
		this.setName(name);
		this.setPatronymic(patronymic);
		this.setAddress(address);
		this.setPhoneNumber(phoneNumber);
		this.setType(type);
		this.setLogin(login);
		this.setPassword(password);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSurename() {
		return surename;
	}

	public void setSurename(String surename) {
		this.surename = surename;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public UserTypeEnum getType() {
		return type;
	}

	public void setType(UserTypeEnum type) {
		this.type = type;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;

		if (surename == null) {
			if (other.surename != null)
				return false;
		} else if (!surename.equals(other.surename)) {
			return false;
		}

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}

		if (patronymic == null) {
			if (other.patronymic != null)
				return false;
		} else if (!patronymic.equals(other.patronymic)) {
			return false;
		}

		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address)) {
			return false;
		}

		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber)) {
			return false;
		}


		if (type!=other.type){
			return false;
		}

		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * id 
				+ ((password == null) ? 0 : password.hashCode())
				+ ((surename == null) ? 0 : surename.hashCode())
				+ ((name == null) ? 0 : name.hashCode())
				+ ((patronymic == null) ? 0 : patronymic.hashCode())
				+ ((address == null) ? 0 : address.hashCode())
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode())
				+ ((type == null) ? 0 : type.hashCode()) 
				+((login == null) ? 0 : login.hashCode()));

	}

	@Override
	public String toString() {
		return "User: id " + id + " surename " + surename + " name " + name
				+ " patronymic " + patronymic + " address " + address
				+ " phone number " + phoneNumber 
				+ " type " + type + " login " + login + " password "
				+ password;
	}
}
