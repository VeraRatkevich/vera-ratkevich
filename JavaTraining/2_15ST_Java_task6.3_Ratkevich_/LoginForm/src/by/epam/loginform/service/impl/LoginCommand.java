package by.epam.loginform.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.loginform.controller.constant.JspPageMessage;
import by.epam.loginform.controller.constant.JspPageName;
import by.epam.loginform.controller.constant.RequestAttributeName;
import by.epam.loginform.controller.constant.RequestParameterName;
import by.epam.loginform.controller.constant.SessionAttributeName;
import by.epam.loginform.dao.exception.DaoException;
import by.epam.loginform.dao.impl.DbUserDao;
import by.epam.loginform.entity.User;
import by.epam.loginform.resources.MessageManager;
import by.epam.loginform.service.ICommand;
import by.epam.loginform.service.exception.ServiceException;

public class LoginCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page;
		HttpSession session = request.getSession();
		String login = request.getParameter(RequestParameterName.LOGIN);

		String password = request.getParameter(RequestParameterName.PASSWORD);

		System.out.println("login " + login + "   password " + password);

		DbUserDao dao = DbUserDao.getInstance();

		System.out.println("DAO: " + dao);

		User user = null;
		try {
			user = dao.getByLoginPasword(login, password);

		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		System.out.println(user);

		if (user != null) {
			session.setAttribute(SessionAttributeName.USER_TYPE, user.getType());
			session.setAttribute(SessionAttributeName.USER_ID, user.getId());
			session.setAttribute(SessionAttributeName.USER_NAME, user.getName());

			request.setAttribute(RequestParameterName.NAME, user.getName());

			page = JspPageName.ABOUT_PAGE;

		} else {

			request.setAttribute(RequestAttributeName.ERROR_LOGIN_MESSAGE,
					MessageManager.getMessage(JspPageMessage.LOGIN_PASSWORD_INCORRECT));
			page = JspPageName.LOGIN_PAGE;
		}

		return page;
	}
}
