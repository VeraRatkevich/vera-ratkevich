package by.epam.loginform.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.loginform.controller.constant.JspPageName;
import by.epam.loginform.controller.constant.RequestParameterName;
import by.epam.loginform.controller.constant.SessionAttributeName;
import by.epam.loginform.resources.MessageManager;
import by.epam.loginform.service.ICommand;
import by.epam.loginform.service.exception.ServiceException;

public class ChangeLocalCommand implements ICommand {
	private static final String LOCALE_DEFAULT = "en";

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {

		String local = request.getParameter(RequestParameterName.LOCAL);
		request.getSession(true)
				.setAttribute(SessionAttributeName.LOCAL, local);		
		MessageManager.setLanguage(local);

		return JspPageName.INDEX_PAGE;

	}

}
