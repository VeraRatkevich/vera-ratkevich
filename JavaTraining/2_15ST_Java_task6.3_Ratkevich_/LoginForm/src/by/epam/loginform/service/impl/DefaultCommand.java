package by.epam.loginform.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.loginform.controller.constant.JspPageName;
import by.epam.loginform.service.ICommand;
import by.epam.loginform.service.exception.ServiceException;

public class DefaultCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
	
		return JspPageName.INDEX_PAGE;
	}

}
