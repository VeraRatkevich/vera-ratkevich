package by.epam.loginform.service.exception;

import by.epam.loginform.exception.ProjectException;

public class ServiceException extends ProjectException {

	private static final long serialVersionUID = 1L;

	public ServiceException(String msg) {

		super(msg);

	}
	public ServiceException(Exception e) {

		super(e);

	}
	public ServiceException(String msg, Exception e) {

		super(msg, e);

	}

}

