package by.epam.loginform.service;

import javax.servlet.http.HttpServletRequest;

import by.epam.loginform.service.exception.ServiceException;

public interface ICommand {
	public String execute(HttpServletRequest request) throws ServiceException;
}
