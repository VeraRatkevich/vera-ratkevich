package by.epam.loginform.service.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.loginform.controller.constant.JspPageName;
import by.epam.loginform.service.ICommand;
import by.epam.loginform.service.exception.ServiceException;

public class LogoutCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = JspPageName.INDEX_PAGE;
		System.out.println("уничтожение сессии " + request.getSession().getAttribute("userType"));
		request.getSession().invalidate();
	
		return page;
	}

}
