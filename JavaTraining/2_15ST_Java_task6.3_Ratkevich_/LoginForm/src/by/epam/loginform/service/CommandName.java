package by.epam.loginform.service;

import by.epam.loginform.entity.constant.UserTypeEnum;

public enum CommandName {
	LOGIN {
		{
			this.type = UserTypeEnum.GUEST;
		}

	},
	
	LOCAL {
		{
			this.type = UserTypeEnum.GUEST;
		}

	},
	DEFAULT_COMMAND {
		{
			this.type = UserTypeEnum.GUEST;
		}

	},
	LOGOUT {
		{
			this.type = UserTypeEnum.READER;
		}

	};

	UserTypeEnum type = null;

	public UserTypeEnum getUserType() {
		return type;
	}

}
