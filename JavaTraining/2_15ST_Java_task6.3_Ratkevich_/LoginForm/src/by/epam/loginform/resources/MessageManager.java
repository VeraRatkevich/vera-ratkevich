package by.epam.loginform.resources;

import java.util.ResourceBundle;

public class MessageManager {
	private static final String MESSAGES_PATH = "by.epam.loginform.resources.local";

	private static ResourceBundle bundle = ResourceBundle
			.getBundle(MESSAGES_PATH, LocalEnum.EN.getCurrent());

	private MessageManager() {
	}

	public static String getMessage(String key) {
		return bundle.getString(key);
	}
	
	public static void setLanguage(String str) {
		LocalEnum lang = LocalEnum.valueOf(str.toUpperCase());
		bundle = ResourceBundle.getBundle(MESSAGES_PATH, lang.getCurrent());
	}
}
