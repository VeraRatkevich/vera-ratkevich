package by.epam.textbook.object.leaf;

import java.util.Comparator;

import by.epam.textbook.object.LeafPart;

public class Word extends LeafPart {

	public Word(String part) {
		super(part);
	}
	public static class AlphabeticalComparator implements Comparator <Word> {
		
		@Override
		public int compare(Word wordOne, Word wordTwo) {
			return wordOne.getPart().compareTo(wordTwo.getPart());
		}
		}

}
