package by.epam.textbook.object;

import java.util.ArrayList;

public class CompositePart extends ArrayList<ITextComponent> implements
		ITextComponent {

	public CompositePart() {
		super();
	}

	@Override
	public void addComponent(ITextComponent component) {
		super.add(component);
	}

	@Override
	public void removeComponent(ITextComponent component) {
		super.remove(component);

	}

	@Override
	public ITextComponent getComponent(int index) {
		return super.get(index);
	}

	@Override
	public void setComponent(int index, ITextComponent element) {
		super.set(index, element);

	}
	/*
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (ITextComponent textComponent: this){
			sb.append(textComponent.toString());			
		}
		return sb.toString();
	}
	
	*/
	public int size() {
		return super.size();
	}


}
