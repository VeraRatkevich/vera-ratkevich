package by.epam.textbook.object;

public class LeafPart implements ITextComponent {
	
	private String part;
	
	public LeafPart() {
		
	}
	
	public LeafPart(String part) {
		this.part = part;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	@Override
	public final void addComponent(ITextComponent component) {
		throw new UnsupportedOperationException();

	}

	@Override
	public final ITextComponent getComponent(int index) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void removeComponent(ITextComponent component) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setComponent(int index, ITextComponent element) {
		throw new UnsupportedOperationException();

	}

	@Override
	public String toString() {
		return part;
	}

	public int lenth() {
		return part.length();
	}

}
