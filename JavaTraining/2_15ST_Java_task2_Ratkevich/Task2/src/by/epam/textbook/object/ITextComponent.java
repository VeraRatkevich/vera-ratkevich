package by.epam.textbook.object;

public interface ITextComponent {
	
	void addComponent(ITextComponent component);
	
    void removeComponent(ITextComponent component);
    
    ITextComponent getComponent(int index);
    
    void setComponent(int index, ITextComponent element);
    
}
