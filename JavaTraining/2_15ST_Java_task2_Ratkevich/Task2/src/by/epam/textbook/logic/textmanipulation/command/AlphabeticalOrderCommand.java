package by.epam.textbook.logic.textmanipulation.command;

import java.util.ArrayList;
import java.util.Collections;

import by.epam.textbook.logic.textmanipulation.ICommand;
import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.ITextComponent;
import by.epam.textbook.object.leaf.Word;

public class AlphabeticalOrderCommand implements ICommand {
	ArrayList wordList = new CompositePart();

	@Override
	public ITextComponent execute(CompositePart composite) {
		for (ITextComponent paragraphPart : composite) {
			if (paragraphPart instanceof CompositePart) {
				for (ITextComponent sentencePart : (CompositePart) paragraphPart) {
					for (ITextComponent wordSighLeaf : (CompositePart) sentencePart) {
						if (wordSighLeaf instanceof Word) {
							wordList.add((Word) wordSighLeaf);
						}

					}
				}
			}
		}
		Collections.sort(wordList, new Word.AlphabeticalComparator());
		return (CompositePart) wordList;
	}

}
