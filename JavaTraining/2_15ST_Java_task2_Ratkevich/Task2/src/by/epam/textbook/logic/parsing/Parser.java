package by.epam.textbook.logic.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.LeafPart;
import by.epam.textbook.object.leaf.Sign;
import by.epam.textbook.object.leaf.Word;

public class Parser {

	public static final String REGEX_PARAGRAPH_WITHOUT_CODE = "((\\d\\.)*)?(\\s?[A-Z]{1}[a-z]+[ ,][\\d\\sA-Za-z \\.,:;%\\-'\"\\)\\(=><]+([\\.:\\n]))+";
	public static final String REGEX_CODE = "(.+\\{(.*\\n)+?\\}\\n*)";
	public static final String REGEX_SENTENCE = "((\\d\\.)*)?(\\s?[A-Z]{1}[a-z]+[ ,][\\dA-Za-z ,:;%\\-'\\)\\(=><]+([\\.: $]?))";
	public static final String REGEX_WORD = "[A-Za-z\\-]+";
	public static final String REGEX_SIGN = "([\\.,!\\?:;@1])";
	public static final String REGEX_WORD_AND_SIGN = "([\\.,!\\?:;@1])|([A-Za-z\\-]+)";

	public Parser() {

	}

	public CompositePart parse(String text) {
		CompositePart textComposite = new CompositePart();
		textComposite = parseToParagraph(text);
		return textComposite;
	}

	private CompositePart parseToParagraph(String text) {
		CompositePart textComposite = new CompositePart();
		CompositePart paragraphComposite = new CompositePart();
		LeafPart paragraphLeaf = null;
		String paragraph = "";
		Pattern patternParagraph = Pattern
				.compile(REGEX_PARAGRAPH_WITHOUT_CODE);
		Matcher matcher = patternParagraph.matcher(text);

		while (matcher.find()) {
			paragraph = matcher.group();

			if (Pattern.matches(REGEX_CODE, paragraph)) {
				paragraphLeaf = new LeafPart(paragraph);
				textComposite.addComponent(paragraphLeaf);
			} else {
				paragraphComposite = parseToSentense(paragraph);
				textComposite.addComponent(paragraphComposite);
			}
		}
		return textComposite;
	}

	private CompositePart parseToSentense(String paragraph) {
		CompositePart paragraphComposite = new CompositePart();
		CompositePart sentenceComposite = new CompositePart();
		Pattern patternSentence = Pattern.compile(REGEX_SENTENCE);
		Matcher m2 = patternSentence.matcher(paragraph);
		String sentence = "";

		while (m2.find()) {
			sentence = m2.group();
			sentenceComposite = parseToWordSign(sentence);
			paragraphComposite.addComponent(sentenceComposite);
		}
		return paragraphComposite;
	}

	private CompositePart parseToWordSign(String sentence) {
		CompositePart sentenceComposite = new CompositePart();
		Pattern patternWordSign = Pattern.compile(REGEX_WORD_AND_SIGN);
		LeafPart wordLeaf = new LeafPart();
		String wordSign = "";
		Matcher matcher = patternWordSign.matcher(sentence);

		while (matcher.find()) {
			wordSign = matcher.group();
			if (Pattern.matches(REGEX_WORD, wordSign)) {
				wordLeaf = new Word(wordSign);
			} else if (Pattern.matches(REGEX_SIGN, wordSign)) {
				wordLeaf = new Sign(wordSign);
			}
			sentenceComposite.addComponent(wordLeaf);
		}
		return sentenceComposite;
	}

}
