package by.epam.textbook.logic.textmanipulation;

import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.ITextComponent;

public interface ICommand {
ITextComponent execute(CompositePart composite);
}
