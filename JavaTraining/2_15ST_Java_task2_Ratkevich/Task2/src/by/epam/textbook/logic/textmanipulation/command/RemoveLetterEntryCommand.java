package by.epam.textbook.logic.textmanipulation.command;

import by.epam.textbook.logic.textmanipulation.ICommand;
import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.ITextComponent;
import by.epam.textbook.object.LeafPart;
import by.epam.textbook.object.leaf.Word;

public class RemoveLetterEntryCommand implements ICommand {

	@Override
	public ITextComponent execute(CompositePart composite) {
		StringBuilder sb;
		for (ITextComponent paragraphPart : composite) {
			if (paragraphPart instanceof CompositePart) {
				for (ITextComponent sentencePart : (CompositePart) paragraphPart) {
					for (ITextComponent partOfSentencePart : (CompositePart) sentencePart) {
						sb = new StringBuilder(
								((LeafPart) partOfSentencePart).getPart());
						if (partOfSentencePart instanceof Word) {
							sb = new StringBuilder(
									((Word) partOfSentencePart).getPart());
							for (int i = 0; i < sb.length(); i++) {
								if (sb.charAt(i) == sb.charAt(0)) {
									sb.deleteCharAt(i);
								}
							}
							((Word) partOfSentencePart).setPart(sb.toString());
						}
					}
				}
			}
		}
		return composite;
	}

}
