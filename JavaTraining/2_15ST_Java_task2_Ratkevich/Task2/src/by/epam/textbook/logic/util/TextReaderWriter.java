package by.epam.textbook.logic.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class TextReaderWriter {
	private String text;
	private FileInputStream inFileByte;
	private File file;
	private FileOutputStream outFileByte;
	private FileWriter fw;
	
	public TextReaderWriter() {
		text = "";
	}

	public String readByte(String path) {
		file = new File(path);

		try {
			inFileByte = new FileInputStream(file);
			byte[] str = new byte[inFileByte.available()];
			inFileByte.read(str);
			text = new String(str);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (inFileByte != null) {
					inFileByte.close(); 
				}
			} catch (IOException e) {
				System.err.println("������ ��������: " + e);
			}
		}
		return text;
	}

	public void writeByte(String path, String text) {
		file = new File(path);
		
		try {
			fw = new FileWriter(file);
			fw.write(text);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {

				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
