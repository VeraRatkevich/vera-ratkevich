package by.epam.textbook.logic.textmanipulation;

import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.ITextComponent;

public class Controller {

	private TextCommandHelper tch;

	public Controller() {
		tch = new TextCommandHelper();
	}

	public ITextComponent execute(TypeCommandEnum typeCommand,
			CompositePart text) {
		ICommand command = tch.getCommand(typeCommand);
		ITextComponent changedText = command.execute(text);
		return changedText;
	}
}
