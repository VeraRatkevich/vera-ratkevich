package by.epam.textbook.logic.textmanipulation;

import java.util.HashMap;

import by.epam.textbook.logic.textmanipulation.command.AlphabeticalOrderCommand;
import by.epam.textbook.logic.textmanipulation.command.BuildTextCommand;
import by.epam.textbook.logic.textmanipulation.command.RemoveLetterEntryCommand;

public class TextCommandHelper {

	private HashMap<TypeCommandEnum, ICommand> textCommands = new HashMap<>();

	public TextCommandHelper() {
		textCommands.put(TypeCommandEnum.BUILD_TEXT, new BuildTextCommand());
		textCommands.put(TypeCommandEnum.ALPHABETICAL_ORDER,
				new AlphabeticalOrderCommand());
		textCommands.put(TypeCommandEnum.REMOVE_FIRST_LETTER_ENTRY,
				new RemoveLetterEntryCommand());
	}

	public ICommand getCommand(TypeCommandEnum typeCommand) {
		return textCommands.get(typeCommand);
	}

}
