package by.epam.textbook.logic;

import by.epam.textbook.logic.parsing.Parser;
import by.epam.textbook.logic.textmanipulation.Controller;
import by.epam.textbook.logic.textmanipulation.TypeCommandEnum;
import by.epam.textbook.logic.util.TextReaderWriter;
import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.ITextComponent;

public class Manager {

	private TextReaderWriter readerWriter;
	private Controller controller;
	private Parser parser;

	public Manager() {
		readerWriter = new TextReaderWriter();
		controller = new Controller();
		parser = new Parser();
	}

	public String readText(String path) {
		String text = readerWriter.readByte(path);
		return text;
	}

	public void writeText(String path, String text) {
		readerWriter.writeByte(path, text);
	}

	public ITextComponent parseText(String text) {
		return parser.parse(text);
	}

	public ITextComponent manipulateText(TypeCommandEnum typeCommand,
			CompositePart textComposite) {
		return controller.execute(typeCommand, textComposite);
	}

}
