package by.epam.textbook.logic.textmanipulation.command;

import by.epam.textbook.logic.textmanipulation.ICommand;
import by.epam.textbook.object.CompositePart;
import by.epam.textbook.object.ITextComponent;
import by.epam.textbook.object.LeafPart;
import by.epam.textbook.object.leaf.Word;

public class BuildTextCommand implements ICommand {

	@Override
	public ITextComponent execute(CompositePart composite) {
		StringBuilder sb = new StringBuilder();

		for (ITextComponent paragraphPart : composite) {
			sb.append("\n");
			if (paragraphPart instanceof LeafPart) {
				sb.append(((LeafPart) paragraphPart).getPart());
			} else {
				for (ITextComponent sentencePart : (CompositePart) paragraphPart) {
					if (sentencePart instanceof LeafPart) {
						sb.append(((LeafPart) sentencePart).getPart());
					} else {						
						for (ITextComponent partOfSentencePart : (CompositePart) sentencePart) {
							if (partOfSentencePart instanceof LeafPart) {
								if (partOfSentencePart instanceof Word){
									sb.append(" ");
								}
								sb.append(((LeafPart) partOfSentencePart)
										.getPart());
							} 
						}
					}

				}
			}
		}
		return new LeafPart(sb.toString());
	}

}
