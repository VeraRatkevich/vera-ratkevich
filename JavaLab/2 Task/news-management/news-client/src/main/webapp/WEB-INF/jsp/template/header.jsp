<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header">
	<div class="portal-name">
		<h2>
			<spring:message code="label.portalname" />
		</h2>
	</div>
	<div align="right">
		<a href="?lang=en"><spring:message code="label.en" /></a> <a class="language" href="?lang=ru"><spring:message code="label.ru" /></a>
	</div>
</div>