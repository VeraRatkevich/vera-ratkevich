<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="<c:url value="/resources/js/checkbox.js" />"></script>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="top-filter">
				<table>
					<tr>
						<td>
							<form name="filter" method="GET" action="newslist/filter">
								<table>
									<tr>
										<td><select name="author">
												<option value=""><spring:message code="label.select.author" /></option>
												<c:forEach items="${authorList}" var="author">
													<option ${filterAuthor == author.authorId ? 'selected' : ''} value="${author.authorId}">${author.name}</option>
												</c:forEach>
										</select></td>
										<td>
											<div class="multiselect">
												<div class="selectBox" onclick="showCheckboxes()">
													<select>
														<option><spring:message code="label.select.tag" /></option>
													</select>
													<div class="overSelect"></div>
												</div>
												<div id="checkboxes">
													<c:forEach var="tagL" items="${tagList}">
														<label for="one"><input type="checkbox" name="tagL" ${filterTag.contains(tagL.tagId) ? 'checked' : ''} value="${tagL.tagId}" />${tagL.name}</label>
													</c:forEach>
												</div>
											</div>
										</td>
										<td>
											<button type="submit" value="">
												<spring:message code="label.button.filter" />
											</button>
										<td>
									</tr>
								</table>
							</form>
						</td>
						<td>
							<form name="reset" method="GET" action="newslist/reset">
								<button type="submit" value="">
									<spring:message code="label.button.reset" />
								</button>
							</form>
						</td>
					</tr>
				</table>
			</div>
			<div>
				<c:forEach items="${newslist}" var="news">
					<div class="list-news">
						<div class="tags-comments-edit">
							<div class="left">
								<strong><c:out value="${news.title}" /> (<spring:message code="label.by" /> ${news.author.name})</strong>
							</div>
							<fmt:message key="date.format" var="dateFormat" />
							<fmt:formatDate value="${news.modificationDate}" var="formatedDate" type="date" pattern="${dateFormat}" />
							<c:out value="${formatedDate}" />
						</div>
						<div class="shorttext">
							<c:out value="${news.shortText}" />
						</div>
						<div class="tags-comments-edit">
							<c:forEach items="${news.tags}" var="tag">
								<a href="newslist/filter/${tag.tagId}">${tag.name}</a>
								<c:if test="${tag.name != null}">,</c:if>
							</c:forEach>
							<label id="comment-count"><spring:message code="label.comments" />(${news.comments.size()}) </label> <a href="viewnews/${news.newsId}"><spring:message
									code="label.view" /></a>
						</div>
					</div>
					<br />
					<br />
				</c:forEach>
				<br /> <br />
			</div>
			<form name="newslist" method="GET" action="newslist">
				<c:if test="${pageManager.getNumberOfPages() > 1}">
					<div class="page-counter">
						<c:forEach items="${pageManager.pages}" var="page">
							<button type="submit" ${page == pageManager.currentPage ? 'class="button-current"' : ''} name="page" value="${page}">${page}</button>
						</c:forEach>
					</div>
				</c:if>
			</form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>