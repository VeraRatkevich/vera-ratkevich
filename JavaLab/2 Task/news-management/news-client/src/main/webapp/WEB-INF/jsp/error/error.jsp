<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title>Error page</title>
</head>
<body>
    Oops! ${message}
 
    <p>
        error
    </p>
 
    <br /><br /><br />
    <a href="newslist"><spring:message code="label.back.newslist" /></a>
</body>
</html>