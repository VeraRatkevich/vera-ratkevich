<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">${news.title}"</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="back-botton">
			<a href="back"><spring:message code="label.back" /></a>
		</div>
		<fmt:message key="date.format" var="dateFormat" />
		<div class="view-news">
			<div class="tags-comments-edit">
				<div class="left">
					<strong><c:out value="${news.title}" /> &nbsp;&nbsp; (<spring:message code="label.by" /> ${news.author.name})</strong>
				</div>
				<div>
					<fmt:formatDate value="${news.modificationDate}" var="formatedDate" type="date" pattern="${dateFormat}" />
					<c:out value="${formatedDate}" />
				</div>
			</div>
			<div class="fulltext">
				<c:out value="${news.fullText}" />
			</div>
			<br /> <br />
			<div class="comments-heap">
				<c:forEach items="${news.comments}" var="comment">
					<fmt:formatDate value="${comment.creationDate}" var="formatedCommentDate" type="date" pattern="${dateFormat}" />
					<c:out value="${formatedCommentDate}" />
					<br />
					<div class="comments">
						<c:out value="${comment.text}" />
					</div>
					<br />
				</c:forEach>
				<br /> <br />
				<fmt:message key="error.comment.text.size" var="errCommentMess" />
				<sf:form method="POST" modelAttribute="comment" action="${news.newsId}">
					<sf:textarea path="text" class="comment-text" required="required" minlength="1" maxlength="100" title="${errCommentMess}" />
					<br />
					<div class="error-message">
						<sf:errors path="text" />
					</div>
					<br />
					<sf:button>
						<spring:message code="label.postcomment" />
					</sf:button>
				</sf:form>
			</div>
		</div>
		<div class="back-previous">
			<c:if test="${prevNewsId != null}">
				<a href="${prevNewsId}"><spring:message code="label.previous" /></a>
			</c:if>
		</div>
		<div class="back-next">
			<c:if test="${nextNewsId != null}">
				<a href="${nextNewsId}"><spring:message code="label.next" /></a>
			</c:if>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>