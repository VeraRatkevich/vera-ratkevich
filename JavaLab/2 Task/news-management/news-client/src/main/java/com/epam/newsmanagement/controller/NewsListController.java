package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IManagerService;
import com.epam.newsmanagement.util.PageBean;

@Controller
@RequestMapping({ "/newslist" })
public class NewsListController {

	@Value("${news.on.page}")
	private int NEWS_ON_PAGE;

	@Autowired
	private IManagerService managerService;

	@RequestMapping(method = RequestMethod.GET)
	public String getNewsList(@RequestParam(value = "page", defaultValue = "1") int page, Model model, HttpSession session) throws ServiceException {

		SearchCriteria sc = getSearchCriteria(session);

		List<News> newsList = managerService.getAllNews(sc, getStartIndex(page), NEWS_ON_PAGE);
		model.addAttribute("newslist", newsList);

		List<Tag> tags = managerService.getAllTags();
		model.addAttribute("tagList", tags);

		List<Author> authors = managerService.getAllAuthors();
		model.addAttribute("authorList", authors);

		PageBean pm = createPageManager(sc, page);
		model.addAttribute("pageManager", pm);

		return "newslist";
	}

	@RequestMapping(value = { "/filter" }, method = RequestMethod.GET)
	public String filterNews(@RequestParam(value = "author", required = false) Long authorId, @RequestParam(value = "tagL", required = false) List<Long> tagIdList, HttpSession session)
			throws ServiceException {
		session.setAttribute("filterTag", tagIdList);
		session.setAttribute("filterAuthor", authorId);
		return "redirect:/newslist";
	}

	@RequestMapping(value = { "/reset" }, method = RequestMethod.GET)
	public String resetFilter(HttpSession session) throws ServiceException {
		session.removeAttribute("filterTag");
		session.removeAttribute("filterAuthor");
		return "redirect:/newslist";
	}

	@RequestMapping(value = { "/filter/{tagId}" }, method = RequestMethod.GET)
	public String filterNewsByTag(@PathVariable("tagId") long tagId, HttpSession session) throws ServiceException {
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(tagId);
		session.setAttribute("filterTag", tagIdList);
		session.removeAttribute("filterAuthor");
		return "redirect:/newslist";
	}

	private SearchCriteria getSearchCriteria(HttpSession session) {

		Long authorId = (Long) session.getAttribute("filterAuthor");
		@SuppressWarnings("unchecked")
		List<Long> tagIdList = (List<Long>) session.getAttribute("filterTag");

		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(authorId);
		sc.setTagIdList(tagIdList);

		return sc;
	}

	private int getStartIndex(int page) throws ServiceException {
		int startIndex = (page - 1) * NEWS_ON_PAGE + 1;
		return startIndex;
	}

	private PageBean createPageManager(SearchCriteria sc, int page) throws ServiceException {
		int numberOfPages = calculateNumberOfPages(sc);
		PageBean pm = new PageBean(numberOfPages);
		pm.setCurrentPage(page);
		return pm;
	}

	private int calculateNumberOfPages(SearchCriteria sc) throws ServiceException {
		int countNews = managerService.countNews(sc);
		int numberPages = (int) Math.ceil((double) countNews / NEWS_ON_PAGE);
		return numberPages;
	}

}
