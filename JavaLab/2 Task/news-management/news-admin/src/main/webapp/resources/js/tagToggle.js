var currentTag;
var currentEditElementId;
var currentInputElementId;
var currentValue;

function showStuff(elementId, editElementId, inputElementId) {

	hideStuff(currentEditElementId, currentInputElementId);
	
    if (false == $(elementId).is(':visible')) {	
        $(elementId).show();
    }else {
        $(elementId).hide();
    }
    
   $(editElementId).hide();
   $(inputElementId).prop("disabled", false );
   
   currentTag = elementId;
   currentEditElementId = editElementId;
   currentInputElementId = inputElementId;   
   currentValue = $(inputElementId).val(); 
}

function hideStuff(editElementId, inputElementId) {
    $(currentTag).hide();
    $(editElementId).show();
    $(inputElementId).prop( "disabled", true );
    $(inputElementId).val(currentValue);
 
}