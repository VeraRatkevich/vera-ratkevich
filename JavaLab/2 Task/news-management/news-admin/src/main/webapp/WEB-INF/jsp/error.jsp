<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="error-message">${errorMessage}</div>
	</tiles:putAttribute>
</tiles:insertDefinition>