<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="LogindefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.login" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="body-login">
			<spring:url var="authUrl" value="/static/j_spring_security_check" />
			<fmt:message key="error.login.size" var="errLoginMess" />
			<fmt:message key="error.password.size" var="errPasswordMess" />
			<form method="post" action="${authUrl}">
				<div class="form-group">
					<table class=login-element>
						<tr>
							<td><spring:message code="label.login" /></td>
							<td><input type="text" name="username" required="required" maxlength="30" title="${errLoginMess}"> <br /> <br /></td>
						</tr>
						<tr>
							<td><spring:message code="label.password" /></td>
							<td><input type="password" name="password" required="required" maxlength="50" title="${errPasswordMess}"><br /></td>
						</tr>
						<tr>
							<td></td>
							<td align="right">
								<button type="submit" class="btn btn-default" value="">
									<spring:message code="label.logIn" />
								</button>
							</td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>