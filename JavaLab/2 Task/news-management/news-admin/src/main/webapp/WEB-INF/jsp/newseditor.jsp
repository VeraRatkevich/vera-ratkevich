<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<fmt:message key="error.news.title.size" var="errTitleMess" />
		<fmt:message key="error.news.shorttext.size" var="errShortTextMess" />
		<fmt:message key="error.news.fulltext.size" var="errFullTextMess" />
		<div class="save-page">
			<c:if test="${errorVersionMessage != null}">
				<p class="error-message">
				<spring:message code="error.version.news" />
				</p>
			</c:if>
			<sf:form method="POST" action="${news.newsId}" modelAttribute="news" id="addForm">
				<sf:hidden path="newsId" />
				<sf:hidden path="version" />
				<table class="save-form">
					<tr>
						<td><spring:message code="label.title" />:</td>
						<td><sf:textarea path="title" rows="1" cols="100" required="required" minlength="1" maxlength="30" title="${errTitleMess}" onclick="wow()" /></td>
					</tr>
					<tr>
						<td><br /></td>
						<td><sf:errors path="title" class="error-message" /></td>
					</tr>
					<tr>
						<fmt:message key="date.format" var="dateFormat" />
						<fmt:message key="error.date.format" var="errDateFormat" />
						<td><spring:message code="label.date" />:</td>
						<fmt:formatDate var="fmtCreatDate" value="${news.creationDate}" pattern="${dateFormat}" />
						<fmt:formatDate var="fmtModifDate" value="${news.modificationDate}" pattern="${dateFormat}" />
						<td><c:choose>
								<c:when test="${news.newsId == null}">
									<input type="text" id="datepicker" value="${fmtCreatDate}" title="${errDateFormat}" readonly="readonly">
									<sf:input id="alt-datepicker" type="hidden" path="creationDate" />
									<sf:hidden path="modificationDate" />
								</c:when>
								<c:otherwise>
									<input type="text" id="datepicker" value="${fmtModifDate}" title="${errDateFormat}" readonly="readonly">
									<sf:input id="alt-datepicker" type="hidden" path="modificationDate" />
									<sf:hidden path="creationDate" />
								</c:otherwise>
							</c:choose></td>
					</tr>
					<tr>
						<td><br /></td>
						<td><c:choose>
								<c:when test="${news.newsId == null}">
									<sf:errors path="creationDate" class="error-message" />
								</c:when>
								<c:otherwise>
									<sf:errors path="modificationDate" class="error-message" />
								</c:otherwise>
							</c:choose></td>
					</tr>
					<tr>
						<td><br /></td>
					</tr>
					<tr></tr>
					<tr>
						<td><spring:message code="label.brief" />:</td>
						<td><sf:textarea path="shortText" rows="3" cols="100" required="required" minlength="2" maxlength="100" title="${errShortTextMess}" /></td>
					</tr>
					<tr>
						<td><br /></td>
						<td><sf:errors path="shortText" class="error-message" /></td>
					</tr>
					<tr>
						<td><spring:message code="label.content" />:</td>
						<td><sf:textarea path="fullText" rows="6" cols="100" required="required" minlength="2" maxlength="2000" title="${errFullTextMess}" /></td>
					</tr>
					<tr>
						<td></td>
						<td><sf:errors path="fullText" class="error-message" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="error-message">${message}</div>
						</td>
					</tr>
				</table>
				<div class="filter-form">
					<table>
						<tr>
							<fmt:message key="label.author.empty" var="errAuthorMess" />
							<td><sf:select name="author" path="author.authorId" required="required" title="${errAuthorMess}">
									<option value=""><spring:message code="label.select.author" /></option>
									<c:forEach items="${authors}" var="author">
										<option ${news.author.authorId == author.authorId ? 'selected' : ''} value="${author.authorId}">${author.name}</option>
									</c:forEach>
								</sf:select> <%-- 							<sf:input type="text" path="author.authorId" /> --%> <!-- 								<div class="selects"> --> <%-- 									<sf:select path="author.authorId"> --%>
								<%-- 										<c:forEach items="${authors}" var="authors"> --%> <%-- 											<option ${author.authorName == authors.authorName ? 'selected' : '' } value="${authors.authorId}"><c:out --%>
								<%-- 													value="${authors.authorName}" /></option> --%> <%-- 										</c:forEach> --%> <%-- 									</sf:select> --%> <br>
								<div class="error-message">
									<c:if test="${authorError}">
										<spring:message code="label.author.empty" />
									</c:if>
								</div></td>
							<td>
								<div class="multiselect">
									<div class="selectBox" onclick="showCheckboxes()">
										<select>
											<option><spring:message code="label.select.tag" /></option>
										</select>
										<div class="overSelect"></div>
									</div>
									<div id="checkboxes">
										<c:forEach var="tag" items="${tags}">
											<label for="one"><input id="one" type="checkbox" name="tag" ${news.tags.contains(tag) ? 'checked' : ''} value="${tag.tagId}" />${tag.name}</label>
										</c:forEach>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="button-save">
					<button type="submit" value="">
						<spring:message code="label.buttonsave" />
					</button>
				</div>
			</sf:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
