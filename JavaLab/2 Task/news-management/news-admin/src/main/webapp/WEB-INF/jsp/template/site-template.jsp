<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="<c:url value="/resources/js/checkbox.js" />"></script>
<script src="<c:url value="/resources/js/tagToggle.js" />"></script>
<c:import url="datepicker.jsp"></c:import>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" /></title>
</head>
<body>
	<div class="page">
		<tiles:insertAttribute name="header" />
		<div class="content">
			<div class="frame">
				<tiles:insertAttribute name="menu" />
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>