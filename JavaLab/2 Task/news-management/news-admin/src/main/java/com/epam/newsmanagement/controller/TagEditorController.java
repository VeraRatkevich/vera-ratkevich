package com.epam.newsmanagement.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IManagerService;

@Controller
@RequestMapping("/tageditor")
public class TagEditorController {

	private IManagerService managerService;

	@Inject
	public void setManagerService(IManagerService managerService) {
		this.managerService = managerService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String tagEditor(Model model, HttpSession session) throws ServiceException {
		setTagListToModel(model);
		model.addAttribute("tagForAdd", new Tag());
		return "tageditor";
	}

	@RequestMapping(value = "/update/{tagId}", method = RequestMethod.POST)
	public String updateTag(@RequestParam(value = "name") String name, @PathVariable("tagId") Long tagId, Model model) throws ServiceException {

		if ((null != name) && (!name.isEmpty())) {
			Tag tag = new Tag();
			tag.setTagId(tagId);
			tag.setName(name);

			managerService.editTag(tag);
			return "redirect:/tageditor";
		} else {
			setTagListToModel(model);
			model.addAttribute("tagForAdd", new Tag());
			return "tageditor";
		}

	}

	@RequestMapping(value = "delete/{tagId}", method = RequestMethod.GET)
	public String deleteTag(@PathVariable("tagId") Long tagId, Model model) throws ServiceException {
		managerService.deleteTag(tagId);
		return "redirect:/tageditor";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String saveTag(@Valid @ModelAttribute("tagForAdd") Tag tagForAdd, BindingResult bindingResult, Model model) throws ServiceException {
		if (bindingResult.hasErrors()) {
			setTagListToModel(model);
			return "tageditor";
		} else {
			managerService.addTag(tagForAdd);
			return "redirect:/tageditor";
		}
	}

	private void setTagListToModel(Model model) throws ServiceException {
		List<Tag> tags = managerService.getAllTags();
		model.addAttribute("tags", tags);
	}

}
