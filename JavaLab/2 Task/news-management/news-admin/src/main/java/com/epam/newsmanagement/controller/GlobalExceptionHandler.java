package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.exception.ServiceException;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = Logger.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleServiceException(ServiceException e) {
		logger.error("ServiceException ", e);
		ModelAndView model = new ModelAndView();
		model.addObject("errorMessage", "ServiceException: " + e.getMessage());
		model.setViewName("error");
		return model;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception e) {
		logger.error("Exception ", e);
		ModelAndView model = new ModelAndView();
		model.addObject("errorMessage", "Exception: " + e.getMessage());
		model.setViewName("error");
		return model;
	}
}
