package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IManagerService;

@Controller
@RequestMapping("/newseditor")
public class NewsEditorController {

	private IManagerService managerService;

	@Inject
	public void setManagerService(IManagerService managerService) {
		this.managerService = managerService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String newsEditor(Model model, HttpSession session) throws ServiceException {
		session.removeAttribute("filterTag");
		session.removeAttribute("filterAuthor");
		News news = new News();
		Date currentDate = new Date();
		news.setCreationDate(currentDate);
		news.setModificationDate(currentDate);
		model.addAttribute("news", news);
		setAllTagsAuthors(model);
		return "newseditor";
	}

	@RequestMapping(value = { "/{newsId}" }, method = RequestMethod.GET)
	public String newsEditorWithNews(@PathVariable("newsId") Long newsId, Model model, HttpSession session) throws ServiceException {
		session.removeAttribute("filterTag");
		session.removeAttribute("filterAuthor");
		News news = managerService.getNews(newsId);
		model.addAttribute("news", news);
		setAllTagsAuthors(model);
		return "newseditor";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String editNews(@Valid @ModelAttribute("news") News news, @RequestParam(value = "tag", required = false) List<Long> tagIdList, BindingResult bindingResult, Model model)
			throws ServiceException {

		if ((tagIdList != null) && (!tagIdList.isEmpty())) {
			news.setTags(createTagSet(tagIdList));
		}
		if (bindingResult.hasErrors()) {
			setAllTagsAuthors(model);
			return "newseditor";
		}
		try {
			managerService.editNews(news);
		} catch (OptimisticLockException e) {
			model.addAttribute("errorVersionMessage", "${error.version.news}");
			setAllTagsAuthors(model);
			return "newseditor";
		}
		Long newsId = news.getNewsId();
		return "redirect:/newseditor/" + newsId;
	}

	private Set<Tag> createTagSet(List<Long> tagIdList) {
		Set<Tag> tags = new HashSet<Tag>();
		Tag tag = null;
		for (Long tagId : tagIdList) {
			tag = new Tag();
			tag.setTagId(tagId);
			tags.add(tag);
		}
		return tags;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String addNews(@Valid @ModelAttribute("news") News news, BindingResult bindingResult, @RequestParam(value = "tag", required = false) List<Long> tagIdList, HttpSession session, Model model)
			throws ServiceException {
		if ((tagIdList != null) && (!tagIdList.isEmpty())) {
			news.setTags(createTagSet(tagIdList));
		}
		if (bindingResult.hasErrors()) {
			setAllTagsAuthors(model);
			return "newseditor";
		} else {
			news.setModificationDate(news.getCreationDate());
			Long newsId = managerService.addNews(news);
			return "redirect:/viewnews/" + newsId;
		}
	}

	private void setAllTagsAuthors(Model model) throws ServiceException {
		List<Tag> tags = managerService.getAllTags();
		model.addAttribute("tags", tags);

		List<Author> authors = managerService.getAllAuthors();
		model.addAttribute("authors", authors);
	}

}
