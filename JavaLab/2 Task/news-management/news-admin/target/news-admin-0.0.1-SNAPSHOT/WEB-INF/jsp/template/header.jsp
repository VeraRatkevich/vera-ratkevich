<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header">
	<div class="portal-name">
		<h2>
			<s:message code="label.portalname" />
		</h2>
	</div>
	<div class="logout" align="right">
	
		<c:if test="${userLogin != null}">
		<div>	<strong><s:message code = "label.greeting"/>, ${userLogin}! </strong></div>
			<div><form action="j_spring_security_logout">
				<button type="submit" value="">
					<s:message code="label.logout" />
				</button>

			</form></div>

		</c:if>
	</div>
	<div align="right">
		<a href="?lang=en"><s:message code="label.en" /></a> 
		<a class="language" href="?lang=ru"><s:message code="label.ru" /></a>
	</div>
<%-- <c:set var = "rootUrl" value = "${pageContext.request.contextPath}"/> --%>
<%-- <c:out value="${rootUrl}"/> --%>
</div>