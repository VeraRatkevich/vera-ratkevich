<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<c:forEach var="author" items="${authors}">
			<div>
				<div class="author-field">
					<spring:message code="label.author" />
					:
				</div>
				<c:choose>
					<c:when test="${author.authorId == authorForUpdate.authorId}">
						<div class="author-field-name">
							<form:form method="POST" modelAttribute="authorForUpdate" action="${author.authorId}">
								<form:hidden path="authorId" />
								<form:input path="name" placeholder="${author.name}" type="text" />
								<input class="btn-update" type="submit" value="<spring:message code="label.tag.update"/>">
								<br />
								<form:errors path="name" class="error-message" />
							</form:form>
						</div>

						<div class="author-field-delete">
							<a href="expire/${author.authorId}"><spring:message code="label.expire" /></a>
						</div>

						<div class="author-field-delete">
							<a href="/news-admin/authoreditor"><spring:message code="label.tag.cancel" /></a>
						</div>
					</c:when>
					<c:otherwise>
						<div class="author-field-name">
							<input name="id" type="text" disabled="disabled" required maxlength="30" value="${author.name}"> <a
								href="/news-admin/authoreditor/${author.authorId}"><spring:message code="label.tag.edit" /></a>
						</div>
						<div class="author-field-delete">
							<c:if test="${author.expired != null}">
								<spring:message code="label.expired" />
							</c:if>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</c:forEach>
		<br>
		<spring:message code="label.author.add" />:
			<div class="author-field-name">
			<form:form method="POST" modelAttribute="authorForAdd">
				<form:input path="name"/>
				<input class="btn-update" type="submit" value="<spring:message code="label.save"/>" onclick="return confirm('<spring:message code="label.save.author.comfirm"/>')">
				<br />
				<form:errors path="name" class="error-message" />
			</form:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>

