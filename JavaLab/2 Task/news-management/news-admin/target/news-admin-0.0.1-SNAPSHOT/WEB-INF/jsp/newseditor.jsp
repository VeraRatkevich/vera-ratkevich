<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="<c:url value="/resources/js/checkbox.js" />"></script>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="save-page">
			<sf:form method="POST" action="/news-admin/newseditor/${news.newsId}" modelAttribute="news" id="addForm">
				<sf:hidden path="newsId" />
				<table class="save-form">
					<tr>
						<td><spring:message code="label.title" />:</td>
						<td><sf:textarea path="title" rows="1" cols="120" /></td>
					</tr>
					<tr>
						<td><br /></td>
						<td><sf:errors path="title" class="error-message" /></td>
					</tr>
					<tr>
						<td><spring:message code="label.date" />:</td>
						<td><c:choose>
								<c:when test="${news.newsId == null}">
									<sf:input path="creationDate" />
									<sf:hidden path="modificationDate" />
								</c:when>
								<c:otherwise>
									<sf:input path="modificationDate" />
									<sf:hidden path="creationDate" />
								</c:otherwise>
							</c:choose></td>

					</tr>

					<tr>
						<td><br /></td>
						<td><c:choose>
								<c:when test="${news.newsId == null}">
									<sf:errors path="creationDate" class="error-message"/>
								</c:when>
								<c:otherwise>
									<sf:errors path="modificationDate" class="error-message"/>
								</c:otherwise>
							</c:choose></td>
					</tr>
					<tr>
						<td><br /></td>

					</tr>
					<tr></tr>
					<tr>
						<td><spring:message code="label.brief" />:</td>
						<td><sf:textarea path="shortText" rows="3" cols="120" /></td>
					</tr>
					<tr>
						<td><br /></td>
						<td><sf:errors path="shortText" class="error-message" /></td>
					</tr>
					<tr>
						<td><spring:message code="label.content" />:</td>
						<td><sf:textarea path="fullText" rows="6" cols="120" /></td>
					</tr>
					<tr>
						<td></td>
						<td><sf:errors path="fullText" class="error-message" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="error-message">${message}</div>
						</td>
					</tr>
				</table>
				<div class="filter-form">
					<table>
						<tr>
							<td><select name="author">
									<option value=""><spring:message code="label.select.author" /></option>

									<c:forEach items="${authors}" var="author">
										<option ${newsAuthorId == author.authorId ? 'selected' : ''} value="${author.authorId}">${author.name}</option>
									</c:forEach>

							</select> <br>
								<div class="error-message">
									<c:if test="${authorError}">
										<spring:message code="label.author.empty" />
									</c:if>
								</div></td>
							<td>
								<div class="multiselect">
									<div class="selectBox" onclick="showCheckboxes()">
										<select>
											<option><spring:message code="label.select.tag" /></option>
										</select>
										<div class="overSelect"></div>
									</div>
									<div id="checkboxes">
										<c:forEach var="tag" items="${tags}">
											<label for="one"><input type="checkbox" name="tag" ${newsTagIds.contains(tag.tagId) ? 'checked' : ''} value="${tag.tagId}" />${tag.name}</label>
										</c:forEach>
									</div>
								</div>

							</td>
						</tr>
					</table>
				</div>
				<div class="button-save">
					<button type="submit" value="" onclick="return confirm('<spring:message code="label.save.news.comfirm"/>')">
						<spring:message code="label.buttonsave" />
					</button>
				</div>
			</sf:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>

