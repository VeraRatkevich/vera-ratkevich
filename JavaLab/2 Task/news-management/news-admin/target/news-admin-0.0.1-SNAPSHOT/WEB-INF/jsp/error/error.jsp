<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title>Error page</title>
</head>
<body>
 <div class = "error-message">${errorMessage}</div>

    <br /><br /><br />
    <a href="/news-admin/newslist"><spring:message code="label.back.newslist" /></a>
</body>
</html>