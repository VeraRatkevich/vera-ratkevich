package com.epam.newsmanagement.dao.impl.hb;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class TagDaoImpl implements ITagDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long add(Tag tag) throws DaoException {
		Long tagId = (Long) sessionFactory.getCurrentSession().save(tag);
		return tagId;
	}

	@Override
	public Tag get(Long tagId) throws DaoException {
		Tag tag = (Tag) sessionFactory.getCurrentSession().get(Tag.class, tagId);
		return tag;
	}

	@Override
	public void edit(Tag tag) throws DaoException {
		sessionFactory.getCurrentSession().saveOrUpdate(tag);
	}

	@Override
	public void delete(Long tagId) throws DaoException {
		Tag tag = new Tag();
		tag.setTagId(tagId);
		sessionFactory.getCurrentSession().delete(tag);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> getAllTags() throws DaoException {
		Criteria cr = sessionFactory.getCurrentSession().createCriteria(Tag.class);
		return cr.list();
	}

}
