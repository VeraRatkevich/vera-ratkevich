package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.springext.Raplace;

@Raplace(otherClass = CopyOfAuthorServiceImpl.class)
public class AuthorServiceImpl implements IAuthorService {

	private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);

	private IAuthorDao authorDao;

	public void setDao(IAuthorDao dao) {
		this.authorDao = dao;
	}

	@Override
	public Long addAuthor(Author author) throws ServiceException {
		Long idAuthor = null;
		try {
			idAuthor = authorDao.add(author);
		} catch (DaoException e) {
			logger.error("Can not add author.", e);
			throw new ServiceException("Can not add author.", e);
		}
		return idAuthor;
	}

	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		List<Author> authorList = null;
		try {
			authorList = authorDao.getAllAuthors();
		} catch (DaoException e) {
			logger.error("Can not get all authors", e);
			throw new ServiceException("Can not get all authors", e);
		}
		return authorList;
	}

	@Override
	public Author getAuthor(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.get(authorId);
		} catch (DaoException e) {
			logger.error("Can not get author by id = " + authorId, e);
			throw new ServiceException("Can not get author by id = " + authorId, e);
		}
		return author;
	}

	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		try {
			authorDao.delete(authorId);
		} catch (DaoException e) {
			logger.error("Can not delete author with id = " + authorId, e);
			throw new ServiceException("Can not delete author with id = " + authorId, e);
		}
	}

	@Override
	public void editAuthor(Author author) throws ServiceException {
		try {
			authorDao.edit(author);
		} catch (DaoException e) {
			logger.error("Can not edit tag.", e);
			throw new ServiceException("Can not edit tag.", e);
		}
	}
}
