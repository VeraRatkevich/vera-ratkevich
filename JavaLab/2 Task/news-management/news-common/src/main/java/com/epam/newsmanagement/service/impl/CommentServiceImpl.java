package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

public class CommentServiceImpl implements ICommentService {

	private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);

	private ICommentDao commentDao;

	public void setDao(ICommentDao dao) {
		this.commentDao = dao;
	}

	@Override
	public void deleteComment(Long idComment) throws ServiceException {
		try {
			commentDao.delete(idComment);
		} catch (DaoException e) {
			logger.error("Can not delete comment.", e);
			throw new ServiceException("Can not delete comment.", e);
		}
	}

	@Override
	public Long addComment(Comment comment) throws ServiceException {
		Long id = null;
		try {
			commentDao.add(comment);
		} catch (DaoException e) {
			logger.error("Can not add comment.", e);
			throw new ServiceException("Can not add comment.", e);
		}
		return id;
	}

}
