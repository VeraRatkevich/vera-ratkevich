package com.epam.newsmanagement.springext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class ReplaceClassesBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		String[] names = beanFactory.getBeanDefinitionNames();
		for (String name : names) {
			BeanDefinition bd = beanFactory.getBeanDefinition(name);
			String className = bd.getBeanClassName();
			try {
				Class<?> beanClass = Class.forName(className);
				Raplace replaseAnnotation = beanClass.getAnnotation(Raplace.class);
				if(replaseAnnotation!=null){
					bd.setBeanClassName(replaseAnnotation.otherClass().getName());
				}
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
