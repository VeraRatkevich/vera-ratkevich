package com.epam.newsmanagement.dao.impl.el;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class AuthorDaoImpl implements IAuthorDao {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public Long add(Author author) throws DaoException {
		em.persist(author);
		return author.getAuthorId();
	}

	@Override
	public Author get(Long authorId) throws DaoException {
		Author author = em.find(Author.class, authorId);
		return author;
	}

	@Override
	public void edit(Author author) throws DaoException {
		em.merge(author);
	}

	@Override
	public void delete(Long authorId) throws DaoException {
		Author author = em.find(Author.class, authorId);
		author.setExpired(new Date());
		em.merge(author);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Author> getAllAuthors() throws DaoException {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
		Root<Author> root = criteriaQuery.from(Author.class);
		criteriaQuery.where(criteriaBuilder.isNull(root.get("expired")));
		Query query = em.createQuery(criteriaQuery);
		List<Author> authorList = (List<Author>) query.getResultList();
		return authorList;
	}

}
