package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides tag-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface ITagService {

    /**
     * Adds tag
     *
     * @param tag
     *            tag object
     * @return tag identifier
     * @throws ServiceException
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * Deletes tag
     *
     * @param tag
     *            tag object
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;
    
    /**
     * Edits tag
     *
     * @param tag
     *            tag object
     * @throws ServiceException
     */
    void editTag(Tag tag) throws ServiceException;
    
    /**
     * Gets tag by tag id
     *
     * @param tagId
     *            tag identifier
     * @return tag object
     * @throws ServiceException
     */
    Tag getTag(Long tagId) throws ServiceException;

    /**
     * Gets all tags
     *
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;
    
}
