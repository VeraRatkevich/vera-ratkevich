package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;

public class TagServiceImpl implements ITagService {

	private static final Logger logger = Logger.getLogger(TagServiceImpl.class);

	private ITagDao tagDao;

	public void setDao(ITagDao dao) {
		this.tagDao = dao;
	}

	@Override
	public Long addTag(Tag tag) throws ServiceException {
		Long id = null;
		try {
			id = tagDao.add(tag);
		} catch (DaoException e) {
			logger.error("Can not add tag.", e);
			throw new ServiceException("Can not add tag.", e);
		}
		return id;
	}

	@Override
	public void editTag(Tag tag) throws ServiceException {
		try {
			tagDao.edit(tag);
		} catch (DaoException e) {
			logger.error("Can not edit tag.", e);
			throw new ServiceException("Can not edit tag.", e);
		}
	}

	@Override
	public Tag getTag(Long tagId) throws ServiceException {
		Tag tag = null;
		try {
			tag = tagDao.get(tagId);
		} catch (DaoException e) {
			logger.error("Can not get tag by tagId = " + tagId, e);
			throw new ServiceException("Can not get tag by tagId = " + tagId, e);
		}
		return tag;
	}

	@Override
	public List<Tag> getAllTags() throws ServiceException {
		List<Tag> tagList = null;
		try {
			tagList = tagDao.getAllTags();
		} catch (DaoException e) {
			logger.error("Can not get all tags.", e);
			throw new ServiceException("Can not get all tags.", e);
		}
		return tagList;
	}

	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		try {
			tagDao.delete(tagId);
		} catch (DaoException e) {
			logger.error("Can not delete tag with id = " + tagId, e);
			throw new ServiceException("Can not delete tag with id = " + tagId, e);
		}
	}
}
