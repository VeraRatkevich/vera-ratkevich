package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Provides methods to work with a table of news
 * 
 * @author Vera_Ratkevich
 */
public interface INewsDao extends IDao<News> {
	/**
	 * Searches news by author and tags
	 *
	 * @param sc
	 *            search criteria
	 * @return list of news objects
	 * @throws DaoException
	 */
	List<News> getAllNews(SearchCriteria sc, int startIndex, int endIndex) throws DaoException;

	/**
	 * Counts number of all news
	 * 
	 * @param sc
	 *            search criteria
	 *
	 * @return number of news
	 * @throws DaoException
	 */
	int countNews(SearchCriteria sc) throws DaoException;

	/**
	 * Gets next and previous news identifiers
	 * 
	 * @param sc
	 *            search criteria
	 * @param currentNewsId
	 *            current news identifier
	 * 
	 * @return list of next and previous news identifiers
	 * @throws DaoException
	 */
	List<Long> getPrevNextNewsId(SearchCriteria sc, Long currentNewsId) throws DaoException;

}
