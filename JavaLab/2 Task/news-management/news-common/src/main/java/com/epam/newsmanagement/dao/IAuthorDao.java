package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
/**
 * Provides methods to work with a table of authors
 * 
 * @author Vera_Ratkevich
 */
public interface IAuthorDao extends IDao<Author> {
	
	/**
	 * Gets all authors from database
	 *
	 * @return list of objects
	 * @throws DaoException
	 */
	List<Author> getAllAuthors() throws DaoException;

}
