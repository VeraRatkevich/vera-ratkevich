package com.epam.newsmanagement.entity;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="TAGS")
public class Tag implements Serializable {

    private static final long serialVersionUID = -138557737808646067L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="TAGS_SEQUENCE")
	@SequenceGenerator(name="TAGS_SEQUENCE", sequenceName="TAGS_SEQUENCE",allocationSize=1)
	@Column(name = "TAG_ID")
    private Long tagId;
    
	@Column(name = "TAG_NAME")
    @Size(min=1, max=30, message="{error.tag.name.size}")
    private String name;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_TAGS",joinColumns={@JoinColumn(name = "TAG_ID")}, 
	inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
	private Set<News> newsSet  = new HashSet<News>();
	
    public Tag() {
    }
    

    public Long getTagId() {
	return tagId;
    }

    public void setTagId(Long id) {
	this.tagId = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Tag other = (Tag) obj;

	if (tagId == null) {
	    if (other.tagId != null)
		return false;
	} else if (!tagId.equals(other.tagId)) {
	    return false;
	}

	if (name == null) {
	    if (other.name != null)
		return false;
	} else if (!name.equals(other.name)) {
	    return false;
	}

	return true;
    }

    @Override
    public int hashCode() {
	return (int) (((tagId == null) ? 0 : tagId.hashCode()*31) + ((name == null) ? 0 : name.hashCode()));

    }

    @Override
    public String toString() {

	StringBuilder builder = new StringBuilder();
	builder.append("Tag [").append(" id: ").append(tagId).append(" name ").append(name).append("]");

	return builder.toString();

    }
}
