package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;

/**
 * Provides methods to work with a table of comments
 * 
 * @author Vera_Ratkevich
 */
public interface ICommentDao extends IDao<Comment> {

}
