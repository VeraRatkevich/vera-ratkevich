package com.epam.newsmanagement.dao.util;

import java.util.Comparator;

import com.epam.newsmanagement.entity.News;

public enum NewsComparator implements Comparator<News> {
	DATE_SORT {
		public int compare(News o1, News o2) {
			return (o1.getModificationDate()).compareTo(o2.getModificationDate());
		}
	},
	COUNT_SORT {
		public int compare(News o1, News o2) {
			return o1.getCommentSize() - o2.getCommentSize();
		}
	};

	public static Comparator<News> decending(final Comparator<News> other) {
		return new Comparator<News>() {
			public int compare(News o1, News o2) {
				return -1 * other.compare(o1, o2);
			}
		};
	}

	public static Comparator<News> getComparator(NewsComparator... multipleOptions) {
		return new Comparator<News>() {
			public int compare(News o1, News o2) {
				for (NewsComparator option : multipleOptions) {
					int result = option.compare(o1, o2);
					if (result != 0) {
						return result;
					}
				}
				return 0;
			}
		};
	}
}