package com.epam.newsmanagement.dao.impl.el;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class CommentDaoImpl implements ICommentDao {

	@PersistenceContext
	EntityManager em;

	@Override
	public Long add(Comment comment) throws DaoException {
		em.persist(comment);
		em.getEntityManagerFactory().getCache().evictAll();
		return comment.getCommentId();
	}

	@Override
	public Comment get(Long commentId) throws DaoException {
		Comment comment = em.find(Comment.class, commentId);
		return comment;
	}

	@Override
	public void edit(Comment comment) throws DaoException {
		em.merge(comment);
		em.getEntityManagerFactory().getCache().evictAll();
	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Comment comment = em.find(Comment.class, commentId);
		em.remove(comment);
		em.getEntityManagerFactory().getCache().evictAll();
	}
}
