package com.epam.newsmanagement.dao.util;

import java.util.List;

import com.epam.newsmanagement.entity.SearchCriteria;

public class SearchQueryBuilder {

    private static final String SQL_SELECT_NEWS_DATA = "SELECT * FROM (SELECT inner.NEWS_ID, inner.TITLE, inner.SHORT_TEXT, inner.FULL_TEXT, inner.CREATION_DATE, inner.MODIFICATION_DATE, ROWNUM rn FROM (SELECT n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE, COUNT(c.COMMENT_ID) AS COUNT_COMMENTS FROM NEWS n LEFT JOIN COMMENTS c ON n.NEWS_ID = c.NEWS_ID ";
    private static final String SQL_COUNT_NEWS = "SELECT COUNT(n.NEWS_ID) AS COUNT_NEWS FROM NEWS n ";
    private static final String SQL_WITH_AUTHOR = "INNER JOIN NEWS_AUTHORS na ON na.NEWS_ID = n.NEWS_ID WHERE na.AUTHOR_ID = ";
    private static final String SQL_WITH_TAGS = " n.NEWS_ID IN(SELECT nt.NEWS_ID FROM NEWS_TAGS nt WHERE nt.TAG_ID IN ";
    private static final String SQL_GROUP_ORDER_BY = " GROUP BY n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE ORDER BY n.MODIFICATION_DATE DESC, COUNT_COMMENTS DESC, n.NEWS_ID DESC)inner ) WHERE rn >= ";
    private static final String SQL_GROUP_WHERE_NEWS_ID = " GROUP BY n.NEWS_ID, n.MODIFICATION_DATE) inner) outer WHERE outer.NEWS_ID = ";
    private static final String SQL_NEXT_PREV_NEWS_ID = "SELECT outer.NEXT_NEWS_ID, outer.PREV_NEWS_ID FROM (SELECT inner.NEWS_ID, LEAD(inner.NEWS_ID, 1, 0) OVER (ORDER BY inner.MODIFICATION_DATE ASC, inner.COUNT_COMMENTS ASC, inner.NEWS_ID ASC) AS NEXT_NEWS_ID, LAG(inner.NEWS_ID, 1, 0) OVER (ORDER BY inner.MODIFICATION_DATE ASC, inner.COUNT_COMMENTS ASC, inner.NEWS_ID ASC) AS PREV_NEWS_ID FROM (SELECT n.NEWS_ID, n.MODIFICATION_DATE, COUNT(c.COMMENT_ID) AS COUNT_COMMENTS FROM NEWS n LEFT JOIN COMMENTS c ON n.NEWS_ID = c.NEWS_ID ";

    private SearchQueryBuilder() {

    }    
    
    public static String createNextPrevNewsIdsQuery(SearchCriteria sc,  Long currentNewsId) {
	StringBuilder sb = new StringBuilder(SQL_NEXT_PREV_NEWS_ID);

	if (null != sc) {
	    sb.append(searchCriteriaSubquery(sc));
	}
	sb.append(SQL_GROUP_WHERE_NEWS_ID);
	sb.append(currentNewsId);
	return sb.toString();
    }
  

    public static String createSearhQuery(SearchCriteria sc, int startIndex, int endIndex) {
	StringBuilder sb = new StringBuilder(SQL_SELECT_NEWS_DATA);
	if (null != sc) {
	    sb.append(searchCriteriaSubquery(sc));
	}
	sb.append(SQL_GROUP_ORDER_BY);
	sb.append(startIndex);
	sb.append(" AND rn <= ");
	sb.append(endIndex);	
	return sb.toString();
    }

    public static String createCountQuery(SearchCriteria sc) {
	StringBuilder sb = new StringBuilder(SQL_COUNT_NEWS);
	if (null != sc) {
	    sb.append(searchCriteriaSubquery(sc));
	}
	return sb.toString();
    }


    private static String searchCriteriaSubquery(SearchCriteria sc) {
	StringBuilder sb = new StringBuilder();
	Long authorId = sc.getAuthorId();
	List<Long> tagIdList = sc.getTagIdList();
	boolean isWithAuthor = false;

	if (null != authorId) {
	    sb.append(SQL_WITH_AUTHOR);
	    sb.append(authorId);
	    isWithAuthor = true;
	}

	if ((null != tagIdList) && (tagIdList.size() > 0)) {
	    if (isWithAuthor) {
		sb.append(" OR");
	    } else {
		sb.append(" WHERE");
	    }
	    sb.append(SQL_WITH_TAGS);
	    sb.append(numberToQueryString(tagIdList));
	    sb.append(")");
	}
	return sb.toString();
    }

    private static String numberToQueryString(List<Long> numberList) {
	StringBuilder sb = new StringBuilder("(");

	for (Long number : numberList) {
	    sb.append(number);
	    sb.append(",");
	}
	int sbLegth = sb.length();
	sb.replace(sbLegth - 1, sbLegth, ")");
	return sb.toString();

    }
}
