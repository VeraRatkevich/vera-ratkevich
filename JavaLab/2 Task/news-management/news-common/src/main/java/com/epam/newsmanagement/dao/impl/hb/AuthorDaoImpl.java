package com.epam.newsmanagement.dao.impl.hb;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class AuthorDaoImpl implements IAuthorDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long add(Author author) throws DaoException {
		Long authorId = (Long) sessionFactory.getCurrentSession().save(author);
		return authorId;
	}

	@Override
	public Author get(Long authorId) throws DaoException {
		Author author = (Author) sessionFactory.getCurrentSession().get(Author.class, authorId);
		return author;
	}

	@Override
	public void edit(Author author) throws DaoException {
		sessionFactory.getCurrentSession().saveOrUpdate(author);
	}

	@Override
	public void delete(Long authorId) throws DaoException {
		sessionFactory.getCurrentSession().createSQLQuery("UPDATE AUTHORS a SET a.EXPIRED = CURRENT_TIMESTAMP WHERE AUTHOR_ID = :authorId").setParameter("authorId", authorId).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> getAllAuthors() throws DaoException {
		Criteria cr = sessionFactory.getCurrentSession().createCriteria(Author.class);
		cr.add(Restrictions.isNull("expired"));
		return cr.list();
	}

}
