package com.epam.newsmanagement.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;
import com.epam.newsmanagement.util.EntityCreator;

public class AuthorServiceTest {
	@Mock
	private IAuthorDao authorDao;

	private AuthorServiceImpl authorService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		authorService = new AuthorServiceImpl();
		authorService.setDao(authorDao);
	}

	@Test
	public void addAuthorSuccess() throws DaoException, ServiceException {
		Author author = EntityCreator.createAuthor(3L, "author 3");
		when(authorDao.add(author)).thenReturn(3L);
		Long actualAuthorId = authorService.addAuthor(author);
		Long expectedAuthorId = author.getAuthorId();
		assertEquals(expectedAuthorId, actualAuthorId);
	}

}
