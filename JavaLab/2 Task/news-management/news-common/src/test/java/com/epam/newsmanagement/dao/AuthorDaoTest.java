package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.EntityCreator;

@DataSet
@SpringApplicationContext("eclipseLink.cfg.test.xml")
//@SpringApplicationContext("hibernate.cfg.test.xml")
public class AuthorDaoTest extends UnitilsJUnit4 {

	@SpringBean("authorDao")
	private IAuthorDao authorDao;

	@Test
	public void testAddAuthor() throws DaoException {
		Author expectedAuthor = new Author();
		String expectedAuthorName = "Iosif";
		expectedAuthor.setName(expectedAuthorName);
		expectedAuthor.setAuthorId(256L);
		Long idAuthor = authorDao.add(expectedAuthor);
		Author actualAuthor = authorDao.get(idAuthor);
		String actualAuthorName = actualAuthor.getName();
		assertEquals(expectedAuthorName, actualAuthorName);
	}

	@Test
	public void testGetAuthor() throws DaoException {
		Author expectedAuthor = new Author();
		Long authorId = 1L;
		expectedAuthor.setAuthorId(authorId);
		expectedAuthor.setName("Sara");
		Author actualAuthor = authorDao.get(authorId);
		compareAuthorsData(expectedAuthor, actualAuthor);
	}

	@Test
	public void testDelete() throws DaoException {
		int expectedCountAuthors = 4;
		Long authorId = 4L;
		authorDao.delete(authorId);
		List<Author> authorsList = authorDao.getAllAuthors();
		int actualCountAuthors = authorsList.size();
		assertEquals(expectedCountAuthors, actualCountAuthors);
	}

	@Test
	public void testEditAuthor() throws DaoException {
		Long authorId = 4L;
		Author expectedAuthor = EntityCreator.createAuthor(authorId, "Mamonov");
		authorDao.edit(expectedAuthor);
		Author actualAuthor = authorDao.get(authorId);
		compareAuthorsData(expectedAuthor, actualAuthor);
	}

	private void compareAuthorsData(Author expectedAuthor, Author actualAuthor) {
		assertEquals(expectedAuthor.getAuthorId(), actualAuthor.getAuthorId());
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	}

}