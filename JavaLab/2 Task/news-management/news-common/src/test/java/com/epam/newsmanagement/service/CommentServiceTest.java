package com.epam.newsmanagement.service;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

public class CommentServiceTest {

	@Mock
	private ICommentDao commentDAO;

	private CommentServiceImpl commentService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		commentService = new CommentServiceImpl();
		commentService.setDao(commentDAO);
	}

	@Test
	public void deleteCommentSuccess() throws DaoException, ServiceException {
		commentService.deleteComment(1L);
		verify(commentDAO, times(1)).delete(1L);
	}

	@Test(expected = ServiceException.class)
	public void deleteCommentFail() throws DaoException, ServiceException {
		doThrow(DaoException.class).when(commentDAO).delete(-1L);
		commentService.deleteComment(-1L);
	}

	@Test
	public void addCommentSuccess() throws DaoException, ServiceException {
		Comment comment = new Comment();
		commentService.addComment(comment);
		verify(commentDAO, times(1)).add(comment);
	}

}
