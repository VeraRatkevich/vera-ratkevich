package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.EntityCreator;

@DataSet
@SpringApplicationContext("eclipseLink.cfg.test.xml")
//@SpringApplicationContext("hibernate.cfg.test.xml")
public class TagDaoTest extends UnitilsJUnit4 {

	@SpringBean("tagDao")
	private ITagDao tagDao;

	@Test
	public void testReadAll() throws DaoException {
		List<Tag> tags = tagDao.getAllTags();
		assertEquals(5, tags.size());
	}

	@Test
	public void testCreate() throws DaoException {
		Tag expectedTag = new Tag();
		String expectedTagName = "tag6";
		expectedTag.setName(expectedTagName);
		Long idTag = tagDao.add(expectedTag);
		expectedTag.setTagId(idTag);
		Tag actualTag = tagDao.get(idTag);
		compareTagData(expectedTag, actualTag);
	}

	@Test
	public void testRead() throws DaoException {

		String expectedTagName = "tag5";
		Tag actualTag = tagDao.get(5L);
		String actualTagName = actualTag.getName();
		assertEquals(expectedTagName, actualTagName);
	}

	@Test
	public void testDelete() throws DaoException {
		Long tagId = 5L;
		tagDao.delete(tagId);
		Tag actualTag = tagDao.get(tagId);
		assertEquals(null, actualTag);
	}

	@Test
	public void testUdate() throws DaoException {
		Long tagId = 4L;
		Tag tagExpected = EntityCreator.createTag(tagId, "tag10");
		tagDao.edit(tagExpected);
		Tag tagActual = tagDao.get(tagId);
		assertEquals(tagExpected, tagActual);
	}

	private void compareTagData(Tag expectedTag, Tag actualTag) {
		assertEquals(expectedTag.getTagId(), actualTag.getTagId());
		assertEquals(expectedTag.getName(), actualTag.getName());
	}

}
