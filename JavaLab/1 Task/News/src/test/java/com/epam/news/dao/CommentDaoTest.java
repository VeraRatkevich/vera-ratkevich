package com.epam.news.dao;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.news.dao.impl.CommentDaoImpl;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DaoException;
import com.epam.news.util.EntityCreator;

@DataSet
public class CommentDaoTest extends UnitilsJUnit4 {

    private CommentDaoImpl commentDao;

    @TestDataSource
    public DataSource dataSource;

    @Before
    public void initDataSource() {
	commentDao = new CommentDaoImpl();
	commentDao.setDataSource(dataSource);
    }

    @Test
    public void testAddComment() throws DaoException {
	Comment expectedComment = EntityCreator.createComment(0L, "wow", new Date(), 21L);
	Long idComment = commentDao.add(expectedComment);
	expectedComment.setId(idComment);
	Comment actualComment = commentDao.get(idComment);
	compareCommentData(expectedComment, actualComment);
    }

    @Test
    public void testGetComment() throws DaoException {
	String expectedCommentText = "comment text 1";
	Comment actualComment = commentDao.get(1L);
	String actualCommentText = actualComment.getText();
	assertEquals(expectedCommentText, actualCommentText);
    }

    @Test
    public void testDeleteComment() throws DaoException {
	Long commentId = 4L;
	commentDao.delete(commentId);
	Comment actualComment = commentDao.get(commentId);
	assertEquals(null, actualComment);
    }

    @Test
    public void testDeleteByNewsId() throws DaoException {
	Long newsId = 11L;
	commentDao.deleteCommentsByNewsId(newsId);
	List<Comment> commentList = commentDao.getCommentsByNewsId(newsId);
	int countComments = commentList.size();
	assertEquals(0, countComments);
    }

    @Test
    public void testEditComment() throws DaoException {
	Long commentId = 4L;
	Comment commentExpected = EntityCreator.createComment(commentId, "wow", new Date(), 21L);
	commentDao.edit(commentExpected);
	Comment commentActual = commentDao.get(commentId);
	compareCommentData(commentExpected, commentActual);
    }

    @Test
    public void testGetAllByNewsId() throws DaoException {
	int commentCountExpected = 2;
	Long newsId = 21L;
	List<Comment> commentList = commentDao.getCommentsByNewsId(newsId);
	int commentCountActual = commentList.size();
	assertEquals(commentCountExpected, commentCountActual);

    }

    private void compareCommentData(Comment expectedComment, Comment actualComment) {
	assertEquals(expectedComment.getId(), actualComment.getId());
	assertEquals(expectedComment.getIdNews(), actualComment.getIdNews());
	assertEquals(expectedComment.getText(), actualComment.getText());
	assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
    }

}
