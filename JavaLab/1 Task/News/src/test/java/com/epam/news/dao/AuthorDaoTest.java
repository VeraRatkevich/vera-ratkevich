package com.epam.news.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.news.dao.impl.AuthorDaoImpl;
import com.epam.news.entity.Author;
import com.epam.news.exception.DaoException;
import com.epam.news.util.EntityCreator;

@DataSet
public class AuthorDaoTest extends UnitilsJUnit4 {

    private AuthorDaoImpl authorDao;

    @TestDataSource
    private DataSource dataSource;

    @Before
    public void initDataSource() {
	authorDao = new AuthorDaoImpl();
	authorDao.setDataSource(dataSource);
    }

    @Test
    public void testGetAllAuthors() throws DaoException {
	List<Author> authors = authorDao.getAllAuthors();
	assertEquals(5, authors.size());
    }

    @Test
    public void testAddAuthor() throws DaoException {
	Author expectedAuthor = new Author();
	String expectedAuthorName = "Iosif";
	expectedAuthor.setName(expectedAuthorName);
	Long idAuthor = authorDao.add(expectedAuthor);
	Author actualAuthor = authorDao.get(idAuthor);
	String actualAuthorName = actualAuthor.getName();

	assertEquals(expectedAuthorName, actualAuthorName);
    }

    @Test
    public void testGetAuthor() throws DaoException {
	Author expectedAuthor = new Author();
	Long authorId = 1L;
	expectedAuthor.setId(authorId);
	expectedAuthor.setName("Sara");
	Author actualAuthor = authorDao.get(authorId);
	compareAuthorsData(expectedAuthor, actualAuthor);
    }

    @Test
    public void testDelete() throws DaoException {
	int expectedCountAuthors = 4;
	Long authorId = 4L;
	authorDao.delete(authorId);		
	List<Author> authorsList = authorDao.getAllAuthors();
	int actualCountAuthors = authorsList.size();
	assertEquals(expectedCountAuthors, actualCountAuthors);
    }

    @Test
    public void testDeleteBindingNewsAuthor() throws DaoException {
	Long newsId = 5L;
	authorDao.deleteBindingNewsAuthor(newsId);
	Author author = authorDao.getAuthorByNewsId(newsId);
	assertEquals(null, author);
    }

    @Test
    public void testEditAuthor() throws DaoException {
	Long authorId = 4L;
	Author expectedAuthor = EntityCreator.createAuthor(authorId, "Mamonov");
	authorDao.edit(expectedAuthor);
	Author actualAuthor = authorDao.get(authorId);
	compareAuthorsData(expectedAuthor, actualAuthor);
    }

    @Test
    public void testAttachNewsAuthor() throws DaoException {
	Long authorId = 5L;
	Long newsId = 8L;
	Author expectedAuthor = EntityCreator.createAuthor(authorId, "Eremey");	
	authorDao.attachNewsAuthor(newsId, authorId);
	Author actualAuthor = authorDao.getAuthorByNewsId(newsId);	
	compareAuthorsData(expectedAuthor, actualAuthor);
    }

    @Test
    public void testReadByNewsId() throws DaoException {
	Long authorId = 3L;
	Long newsId = 7L;
	Author expectedAuthor = EntityCreator.createAuthor(authorId, "Mariya");	
	Author actualAuthor = authorDao.getAuthorByNewsId(newsId);	
	compareAuthorsData(expectedAuthor, actualAuthor);
    }

    private void compareAuthorsData(Author expectedAuthor, Author actualAuthor) {
	assertEquals(expectedAuthor.getId(), actualAuthor.getId());
	assertEquals(expectedAuthor.getName(), actualAuthor.getName());
    }

}