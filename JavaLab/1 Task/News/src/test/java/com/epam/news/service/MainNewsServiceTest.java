package com.epam.news.service;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.epam.news.entity.News;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.ManagerServiceImpl;

public class MainNewsServiceTest {

    @Mock
    private INewsService newsService;
    @Mock
    private ITagService tagService;
    @Mock
    private IAuthorService authorService;
    @Mock
    private ICommentService commentService;

    private ManagerServiceImpl mainNewsService;

    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
	mainNewsService = new ManagerServiceImpl();
	mainNewsService.setNewsService(newsService);
	mainNewsService.setTagService(tagService);
	mainNewsService.setAuthorService(authorService);
	mainNewsService.setCommentService(commentService);
    }

    @Test
    public void deleteNewsSuccess() throws ServiceException {
	Long idNews = 1L;
	mainNewsService.deleteNews(idNews);
	verify(newsService, times(1)).deleteNews(idNews);
	verify(tagService, times(1)).deleteBindingNewsTags(idNews);
	verify(authorService, times(1)).deleteBindingNewsAuthor(idNews);
	verify(commentService, times(1)).deleteCommentsByNewsId(idNews);
    }

    @Test(expected = ServiceException.class)
    public void deleteNewsFail() throws ServiceException {
	Long idNews = -1L;
	doThrow(new ServiceException()).when(authorService).deleteBindingNewsAuthor(idNews);
	mainNewsService.deleteNews(idNews);
    }

    @Test
    public void addNewsFullDataSuccess() throws ServiceException {
	News news = new News();
	List<Long> tagIdList = new ArrayList<Long>();
	Long authorId = 1L;
	Long newsId = 1L;
	doReturn(newsId).when(newsService).addNews(news);
	mainNewsService.addNews(news, tagIdList, authorId);
	verify(newsService, times(1)).addNews(news);
	verify(authorService, times(1)).attachNewsAuthor(newsId, authorId);
	verify(tagService, times(1)).attachNewsTags(newsId, tagIdList);
    }
}
