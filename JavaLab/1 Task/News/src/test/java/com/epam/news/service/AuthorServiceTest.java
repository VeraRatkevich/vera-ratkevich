package com.epam.news.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import com.epam.news.dao.IAuthorDao;
import com.epam.news.entity.Author;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.AuthorServiceImpl;
import com.epam.news.util.EntityCreator;

public class AuthorServiceTest {
    @Mock
    private IAuthorDao authorDao;

    private AuthorServiceImpl authorService;

    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
	authorService = new AuthorServiceImpl();
	authorService.setDao(authorDao);
    }

    @Test
    public void addAuthorSuccess() throws DaoException, ServiceException {
	Author author = EntityCreator.createAuthor(3L, "author 3");
	when(authorDao.add(author)).thenReturn(3L);
	Long actualAuthorId = authorService.addAuthor(author);
	Long expectedAuthorId = author.getId();
	assertEquals(expectedAuthorId, actualAuthorId);
    }

    @Test
    public void getAuthorByNewsIdSuccess() throws DaoException, ServiceException {
	Author author = EntityCreator.createAuthor(2L, "author 2");
	when(authorDao.getAuthorByNewsId(3L)).thenReturn(author);
	Author expectedAuthor = authorService.getAuthorByNewsId(3L);
	assertEquals(author.getId(), expectedAuthor.getId());
	assertEquals(author.getName(), expectedAuthor.getName());
    }

    @Test(expected = ServiceException.class)
    public void getAuthorByNewsIdFail() throws DaoException, ServiceException {
	doThrow(DaoException.class).when(authorDao).getAuthorByNewsId(-1L);
	authorService.getAuthorByNewsId(-1L);
    }

    @Test
    public void attachNewsAuthorSuccess() throws ServiceException, DaoException {
	authorService.attachNewsAuthor(1L, 1L);
	verify(authorDao, times(1)).attachNewsAuthor(1L, 1L);
    }

}
