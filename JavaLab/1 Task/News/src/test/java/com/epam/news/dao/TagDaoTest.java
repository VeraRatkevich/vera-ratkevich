package com.epam.news.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.news.dao.impl.TagDaoImpl;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DaoException;
import com.epam.news.util.EntityCreator;

@DataSet
public class TagDaoTest extends UnitilsJUnit4 {

    private TagDaoImpl tagDao;

    @TestDataSource
    private DataSource dataSource;

    @Before
    public void initDataSource() {
	tagDao = new TagDaoImpl();
	tagDao.setDataSource(dataSource);
    }

    @Test
    public void testReadAll() throws DaoException {
	List<Tag> tags = tagDao.getAllTags();
	assertEquals(5, tags.size());
    }

    @Test
    public void testCreate() throws DaoException {
	Tag expectedTag = new Tag();
	String expectedTagName = "tag6";
	expectedTag.setName(expectedTagName);
	Long idTag = tagDao.add(expectedTag);
	expectedTag.setId(idTag);
	Tag actualTag = tagDao.get(idTag);
	compareTagData(expectedTag, actualTag);
    }

    @Test
    public void testRead() throws DaoException {

	String expectedTagName = "tag5";
	Tag actualTag = tagDao.get(5L);
	String actualTagName = actualTag.getName();
	assertEquals(expectedTagName, actualTagName);
    }

    @Test
    public void testDelete() throws DaoException {
	Long tagId = 5L;
	tagDao.delete(tagId);
	Tag actualTag = tagDao.get(tagId);
	assertEquals(null, actualTag);
    }

    @Test
    public void testDeleteNewsTags() throws DaoException {
	Long newsId = 2L;
	tagDao.deleteBindingNewsTags(newsId);
	List<Tag> tagList = tagDao.getTagsByNewsId(newsId);
	assertEquals(0, tagList.size());
    }

    @Test
    public void testUdate() throws DaoException {
	Long tagId = 4L;
	Tag tagExpected = EntityCreator.createTag(tagId, "tag10");
	tagDao.edit(tagExpected);
	Tag tagActual = tagDao.get(tagId);
	assertEquals(tagExpected, tagActual);
    }

    @Test
    public void testReadAllByNewsId() throws DaoException {
	int tagCountExpected = 3;
	Long newsId = 2L;
	List<Tag> tagList = tagDao.getTagsByNewsId(newsId);
	int tagCountActual = tagList.size();
	assertEquals(tagCountExpected, tagCountActual);

    }

    @Test
    public void testCreateNewsTags() throws DaoException {
	int tagCountExpected = 2;
	Long newsId = 4L;
	Tag tag1 = new Tag();
	tag1.setId(1L);
	Tag tag2 = new Tag();
	tag2.setId(2L);
	List<Long> tagIdList = new ArrayList<Long>();
	tagIdList.add(1L);
	tagIdList.add(2L);
	tagDao.attachNewsTags(newsId, tagIdList);
	int tagCountActual = tagDao.getTagsByNewsId(newsId).size();
	assertEquals(tagCountExpected, tagCountActual);

    }

    private void compareTagData(Tag expectedTag, Tag actualTag) {
	assertEquals(expectedTag.getId(), actualTag.getId());
	assertEquals(expectedTag.getName(), actualTag.getName());
    }

}
