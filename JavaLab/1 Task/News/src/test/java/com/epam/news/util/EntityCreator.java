package com.epam.news.util;

import java.time.LocalDate;
import java.util.Date;

import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;

public class EntityCreator {
	
	public static Author createAuthor(Long authorId, String name) {
		Author author = new Author();
		author.setId(authorId);
		author.setName(name);
		return author;
	}
	public static Tag createTag(Long tagId, String name) {
		Tag tag = new Tag();
		tag.setId(tagId);
		tag.setName(name);
		return tag;
	}
	public static Comment createComment(Long commentId, String commentText,
			Date creationDate, Long newsId) {
		Comment comment = new Comment();
		comment.setId(commentId);
		comment.setText(commentText);
		comment.setCreationDate(creationDate);
		comment.setIdNews(newsId);
		return comment;
	}
	public static News createNews(Long id, String title, String shortText, String fullText, Date creationDate, LocalDate modificationDate) {
        News news = new News();
        news.setId(id);
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);
        return news;
    }
}
