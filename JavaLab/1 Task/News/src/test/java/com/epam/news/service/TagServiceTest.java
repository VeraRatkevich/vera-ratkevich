package com.epam.news.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.news.dao.ITagDao;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.TagServiceImpl;
import com.epam.news.util.EntityCreator;

public class TagServiceTest {
    @Mock
    private ITagDao tagDao;
    private TagServiceImpl tagService;

    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
	tagService = new TagServiceImpl();
	tagService.setDao(tagDao);
    }

    @Test
    public void addTagSuccess() throws DaoException, ServiceException {
	Tag tag = EntityCreator.createTag(1L, "tag 1");
	when(tagDao.add(tag)).thenReturn(1L);
	assertEquals(tag.getId(), tagService.addTag(tag));
    }

    @Test
    public void attachNewsTagsSuccess() throws ServiceException, DaoException {
	List<Long> tagIdList = new ArrayList<>();
	tagService.attachNewsTags(1L, tagIdList);
	verify(tagDao, times(1)).attachNewsTags(1L, tagIdList);
    }

    @Test
    public void deleteBindingNewsTagsSuccess() throws ServiceException, DaoException {
	Long idNews = 1L;
	tagService.deleteBindingNewsTags(idNews);
	verify(tagDao, times(1)).deleteBindingNewsTags(idNews);
    }

    @Test
    public void getSuccess() throws DaoException, ServiceException {
	Tag tag1 = EntityCreator.createTag(1L, "tag 1");
	Tag tag2 = EntityCreator.createTag(2L, "tag 2");
	List<Tag> tagList = new ArrayList<>();
	tagList.add(tag1);
	tagList.add(tag2);
	Long newsId = 1L;
	doReturn(tagList).when(tagDao).getTagsByNewsId(newsId);
	assertEquals(tagList.size(), tagService.getTagsByNewsId(newsId).size());
    }

    @Test
    public void getAllTagsSuccess() throws DaoException, ServiceException {
	tagService.getAllTags();
	verify(tagDao, times(1)).getAllTags();
    }

}
