package com.epam.news.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import com.epam.news.dao.ICommentDao;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.impl.CommentServiceImpl;
import com.epam.news.util.EntityCreator;

public class CommentServiceTest {

    @Mock
    private ICommentDao commentDAO;

    private CommentServiceImpl commentService;

    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
	commentService = new CommentServiceImpl();
	commentService.setDao(commentDAO);
    }

    @Test
    public void fetchSuccess() throws DaoException, ServiceException {

	Comment comment1 = EntityCreator.createComment(1L, "comment text 1", new Date(), 1L);
	Comment comment2 = EntityCreator.createComment(2L, "comment text 2", new Date(), 1L);
	List<Comment> commentList = new ArrayList<>();
	commentList.add(comment1);
	commentList.add(comment2);
	doReturn(commentList).when(commentDAO).getCommentsByNewsId(1L);
	assertEquals(commentList.size(), commentService.getCommentsByNewsId(1L).size());
	verify(commentDAO, times(1)).getCommentsByNewsId(1L);
    }

    @Test(expected = ServiceException.class)
    public void getNewsCommentsFail() throws DaoException, ServiceException {
	doThrow(DaoException.class).when(commentDAO).getCommentsByNewsId(-1L);
	commentService.getCommentsByNewsId(-1L);
    }

    @Test
    public void deleteCommentSuccess() throws DaoException, ServiceException {
	commentService.deleteComment(1L);
	verify(commentDAO, times(1)).delete(1L);
    }

    @Test(expected = ServiceException.class)
    public void deleteCommentFail() throws DaoException, ServiceException {
	doThrow(DaoException.class).when(commentDAO).delete(-1L);
	commentService.deleteComment(-1L);
    }

    @Test
    public void deleteAllNewsCommentsSuccess() throws DaoException, ServiceException {
	commentService.deleteCommentsByNewsId(1L);
	verify(commentDAO, times(1)).deleteCommentsByNewsId(1L);
    }

    @Test(expected = ServiceException.class)
    public void deleteAllNewsCommentsCommentFail() throws DaoException, ServiceException {
	doThrow(DaoException.class).when(commentDAO).deleteCommentsByNewsId(-1L);
	commentService.deleteCommentsByNewsId(-1L);
    }

    @Test
    public void addCommentSuccess() throws DaoException, ServiceException {
	Comment comment = new Comment();
	commentService.addComment(comment);
	verify(commentDAO, times(1)).add(comment);
    }

}
