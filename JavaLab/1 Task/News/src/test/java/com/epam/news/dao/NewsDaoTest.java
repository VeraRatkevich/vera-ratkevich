package com.epam.news.dao;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;

import com.epam.news.dao.impl.NewsDaoImpl;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.DaoException;
import com.epam.news.util.EntityCreator;

@DataSet
public class NewsDaoTest extends UnitilsJUnit4 {

    private NewsDaoImpl newsDao;

    @TestDataSource
    public DataSource dataSource;

    @Before
    public void initDataSource() {
	newsDao = new NewsDaoImpl();
	newsDao.setDataSource(dataSource);
    }

    @Test
    public void testGetAll() throws DaoException {
	int expectedCountNews = 5;
	SearchCriteria sc = null;
	List<News> news = newsDao.getNewsBySearchCriteria(sc);
	int actualCountNews = news.size();
	assertEquals(expectedCountNews, actualCountNews);
    }

    @Test
    public void testShowNewsBySearchCriteria() throws DaoException {
	List<Long> tagList = new ArrayList<Long>();
	tagList.add(11L);
	tagList.add(12L);

	SearchCriteria sc = new SearchCriteria();
	sc.setAuthorId(11L);
	sc.setTagIdList(tagList);

	List<News> news = newsDao.getNewsBySearchCriteria(sc);
	assertEquals(2, news.size());
    }

    @Test
    public void testCountAllNews() throws DaoException {
	int expectedCountNews = 5;
	int actualCountNews = newsDao.countNewsBySearchCriteria(null);
	assertEquals(expectedCountNews, actualCountNews);
    }

    @Test
    public void testDelete() throws DaoException {
	newsDao.delete(15L);
	News news = newsDao.get(15L);
	assertEquals(null, news);
    }

    @Test
    public void testRead() throws DaoException {
	News actualNews = newsDao.get(13L);
	String title = actualNews.getTitle();
	assertEquals("title13", title);
    }

    @Test
    public void testCreate() throws DaoException {
	News expectedNews = EntityCreator.createNews(0L, "title", "short text", "full text", new Date(),
	        LocalDate.now());
	Long newsId = newsDao.add(expectedNews);
	expectedNews.setId(newsId);
	News actualNews = newsDao.get(newsId);
	compareNewsData(expectedNews, actualNews);
    }

    @Test
    public void testUpdate() throws DaoException {
	Long newsId = 14L;
	News expectedNews = EntityCreator.createNews(newsId, "title 14141414", "short text 14", "full text 14",
	        new Date(), LocalDate.now());
	newsDao.edit(expectedNews);
	News actualNews = newsDao.get(newsId);
	compareNewsData(expectedNews, actualNews);
    }

    private void compareNewsData(News expectedNews, News actualNews) {
	assertEquals(expectedNews.getId(), actualNews.getId());
	assertEquals(expectedNews.getTitle(), actualNews.getTitle());
	assertEquals(expectedNews.getShortText(), actualNews.getShortText());
	assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
	assertEquals(expectedNews.getModificationDate(), actualNews.getModificationDate());
    }

}
