package com.epam.news.entity;

import java.io.Serializable;
import java.util.List;

public class NewsVO implements Serializable {

    private static final long serialVersionUID = -4501295961324342689L;

    private News news;
    private List<Tag> tagList;
    private List<Comment> commentList;
    private Author author;

    public NewsVO() {

    }

    public News getNews() {
	return news;
    }

    public void setNews(News news) {
	this.news = news;
    }

    public List<Tag> getTagList() {
	return tagList;
    }

    public void setTagList(List<Tag> tagList) {
	this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
	return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
	this.commentList = commentList;
    }

    public Author getAuthor() {
	return author;
    }

    public void setAuthor(Author author) {
	this.author = author;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NewsVO other = (NewsVO) obj;
	if (tagList == null) {
	    if (other.tagList != null)
		return false;
	} else if (!tagList.equals(other.tagList)) {
	    return false;
	}

	if (commentList == null) {
	    if (other.commentList != null)
		return false;
	} else if (!commentList.equals(other.commentList)) {
	    return false;
	}

	if (author == null) {
	    if (other.author != null)
		return false;
	} else if (!author.equals(other.author))
	    return false;

	return true;
    }

    @Override
    public int hashCode() {
	return (int) ((tagList == null) ? 0 : tagList.hashCode())
	        + ((commentList == null) ? 0 : commentList.hashCode()) + ((author == null) ? 0 : author.hashCode());

    }

    @Override
    public String toString() {

	StringBuilder builder = new StringBuilder();
	builder.append("NewsVO [").append(" author: ").append(author).append(" tag list ").append(tagList)
	        .append(" comment list ").append(commentList).append("]");

	return builder.toString();

    }
}
