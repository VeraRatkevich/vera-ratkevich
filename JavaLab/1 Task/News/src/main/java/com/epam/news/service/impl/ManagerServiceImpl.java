package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.ICommentService;
import com.epam.news.service.IManagerService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;

@Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED, value = "transactionManager")
public class ManagerServiceImpl implements IManagerService {

    private INewsService newsService;
    private ICommentService commentService;
    private IAuthorService authorService;
    private ITagService tagService;

    public void setNewsService(INewsService newsService) {
	this.newsService = newsService;
    }

    public void setCommentService(ICommentService commentService) {
	this.commentService = commentService;
    }

    public void setAuthorService(IAuthorService authorService) {
	this.authorService = authorService;
    }

    public void setTagService(ITagService tagService) {
	this.tagService = tagService;
    }

    @Override
    public Long addNews(News news, List<Long> tagIdList, Long authorId) throws ServiceException {
	Long idNews = newsService.addNews(news);
	authorService.attachNewsAuthor(idNews, authorId);
	tagService.attachNewsTags(idNews, tagIdList);
	return idNews;
    }

    @Override
    public List<NewsVO> getAllNews(SearchCriteria sc) throws ServiceException {
	List<News> newsList = null;
	List<NewsVO> newsVOList = new ArrayList<NewsVO>();
	NewsVO newsVO = null;
	newsList = newsService.getNewsBySearchCriteria(sc);
	for (News news : newsList) {
	    newsVO = getNewsVO(news);
	    newsVOList.add(newsVO);
	}
	return newsVOList;
    }

    @Override
    public NewsVO getNews(Long idNews) throws ServiceException {
	NewsVO newsVO = null;
	News news = null;
	news = newsService.getNews(idNews);
	newsVO = getNewsVO(news);
	return newsVO;
    }

    public void editNews(News news) throws ServiceException {
	newsService.editNews(news);
    }

    @Override
    public void deleteNews(Long idNews) throws ServiceException {
	authorService.deleteBindingNewsAuthor(idNews);
	commentService.deleteCommentsByNewsId(idNews);
	tagService.deleteBindingNewsTags(idNews);
	newsService.deleteNews(idNews);
    }

    @Override
    public void attachNewsAuthor(Long idNews, Long idAuthor) throws ServiceException {
	authorService.attachNewsAuthor(idNews, idAuthor);
    }

    @Override
    public void attachNewsTags(Long idNews, List<Long> tagIdList) throws ServiceException {
	tagService.attachNewsTags(idNews, tagIdList);
    }

    @Override
    public Long addTag(Tag tag) throws ServiceException {
	return tagService.addTag(tag);
    }

    @Override
    public Long addAuthor(Author author) throws ServiceException {
	return authorService.addAuthor(author);
    }

    @Override
    public void addComment(Comment comment) throws ServiceException {
	commentService.addComment(comment);
    }

    @Override
    public void deleteComment(Long idNews, Long idComment) throws ServiceException {
	commentService.deleteComment(idComment);
    }

    @Override
    public void deleteCommentsByNewsId(Long idNews) throws ServiceException {
	commentService.deleteCommentsByNewsId(idNews);
    }

    @Override
    public int countNewsBySearchCriteria(SearchCriteria sc) throws ServiceException {
	return newsService.countNewsBySearchCriteria(sc);
    }

    private NewsVO getNewsVO(News news) throws ServiceException {
	Long idNews = news.getId();
	List<Tag> tagList = tagService.getTagsByNewsId(idNews);
	List<Comment> commentList = commentService.getCommentsByNewsId(idNews);
	Author author = authorService.getAuthorByNewsId(idNews);

	NewsVO newsVO = new NewsVO();
	newsVO.setNews(news);
	newsVO.setTagList(tagList);
	newsVO.setCommentList(commentList);
	newsVO.setAuthor(author);

	return newsVO;
    }

}
