package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.INewsDao;
import com.epam.news.dao.util.DbResourceCloser;
import com.epam.news.dao.util.SearchQueryBuilder;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.DaoException;

public class NewsDaoImpl implements INewsDao {

    private static final String SQL_INSERT_NEWS = "INSERT INTO NEWS n (n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE) VALUES (NEWS_SEQUENCE.nextval,?,?,?,?,?)";
    private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS n WHERE n.NEWS_ID = ?";
    private static final String SQL_UPDATE_NEWS = "UPDATE NEWS n SET n.TITLE=?, n.SHORT_TEXT=?, n.FULL_TEXT=?, n.CREATION_DATE=?, n.MODIFICATION_DATE=? WHERE n.NEWS_ID=?";
    private static final String SQL_GET_BY_ID = "SELECT n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE FROM NEWS n WHERE n.NEWS_ID=?";
    
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    public Long add(News news) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet generatedKeys = null;
	Long i = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_INSERT_NEWS, new String[] { "NEWS_ID" });
	    pst.setString(1, (news.getTitle()));
	    pst.setString(2, (news.getShortText()));
	    pst.setString(3, (news.getFullText()));
	    pst.setTimestamp(4, (new Timestamp(news.getCreationDate().getTime())));
	    pst.setDate(5, (Date.valueOf(news.getModificationDate())));
	    pst.executeUpdate();
	    generatedKeys = pst.getGeneratedKeys();
	    if (null != generatedKeys && generatedKeys.next()) {
		i = generatedKeys.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to add news.", e);
	} finally {
	    DbResourceCloser.close(generatedKeys, pst, con, dataSource);
	}
	return i;
    }

    public News get(Long newsId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	News news = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_BY_ID);
	    pst.setLong(1, newsId);
	    rs = pst.executeQuery();
	    if (rs.next()) {
		news = parseResultSet(rs);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get news by newsId = " + newsId, e);
	} finally {
	    DbResourceCloser.close(rs, pst, con, dataSource);
	}
	return news;
    }

    public void edit(News news) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_UPDATE_NEWS);
	    pst.setString(1, (news.getTitle()));
	    pst.setString(2, (news.getShortText()));
	    pst.setString(3, (news.getFullText()));
	    pst.setTimestamp(4, (new Timestamp(news.getCreationDate().getTime())));
	    pst.setDate(5, (Date.valueOf(news.getModificationDate())));
	    pst.setLong(6, (news.getId()));
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to edit news with newsId = " + news.getId(), e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public void delete(Long newsId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_NEWS);
	    pst.setLong(1, newsId);
	    pst.execute();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete news by newsId = " + newsId, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public List<News> getNewsBySearchCriteria(SearchCriteria sc) throws DaoException {
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	News news = null;
	List<News> newsList = new ArrayList<News>();
	String searchQuery = SearchQueryBuilder.createSearhQuery(sc);
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    st = con.createStatement();
	    rs = st.executeQuery(searchQuery);
	    while (rs.next()) {
		news = parseResultSet(rs);
		newsList.add(news);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get news by search criteria.", e);
	} finally {
	    DbResourceCloser.close(rs, st, con, dataSource);
	}
	return newsList;
    }

    public int countNewsBySearchCriteria(SearchCriteria sc) throws DaoException {
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	int count = 0;
	String countQuery = SearchQueryBuilder.createCountQuery(sc);
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    st = con.createStatement();
	    rs = st.executeQuery(countQuery);
	    while (rs.next()) {
		count = rs.getInt("COUNT_NEWS");
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get count of news by search criteria.", e);
	} finally {
	    DbResourceCloser.close(rs, st, con, dataSource);
	}
	return count;
    }

    private News parseResultSet(ResultSet rs) throws SQLException { 
	News news = new News();
	news.setId(rs.getLong("NEWS_ID"));
	news.setTitle(rs.getString("TITLE"));
	news.setShortText(rs.getString("SHORT_TEXT"));
	news.setFullText(rs.getString("FULL_TEXT"));

	Timestamp crDate = rs.getTimestamp("CREATION_DATE");
	Date creationDate = new Date(crDate.getTime());
	news.setCreationDate(creationDate);

	Date modDate = rs.getDate("MODIFICATION_DATE");
	news.setModificationDate(modDate.toLocalDate());

	return news;
    }

}
