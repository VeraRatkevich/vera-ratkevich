package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.news.dao.ICommentDao;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;

public class CommentServiceImpl implements ICommentService {

    private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);

    private ICommentDao commentDao;

    public void setDao(ICommentDao dao) {
	this.commentDao = dao;
    }

    @Override
    public void deleteComment(Long idComment) throws ServiceException {
	try {
	    commentDao.delete(idComment);
	} catch (DaoException e) {
	    logger.error("Can not delete comment.", e);
	    throw new ServiceException("Can not delete comment.", e);
	}
    }

    @Override
    public void deleteCommentsByNewsId(Long idNews) throws ServiceException {
	try {
	    commentDao.deleteCommentsByNewsId(idNews);
	} catch (DaoException e) {
	    logger.error("Can not delete news comments.", e);
	    throw new ServiceException("Can not delete news comments.", e);
	}
    }

    @Override
    public Long addComment(Comment comment) throws ServiceException {
	Long id = null;
	try {
	    commentDao.add(comment);
	} catch (DaoException e) {
	    logger.error("Can not add comment.", e);
	    throw new ServiceException("Can not add comment.", e);
	}
	return id;
    }

    @Override
    public List<Comment> getCommentsByNewsId(Long idNews) throws ServiceException {
	List<Comment> commentList = null;
	try {
	    commentList = commentDao.getCommentsByNewsId(idNews);
	} catch (DaoException e) {
	    logger.error("Can not get news comments.", e);
	    throw new ServiceException("Can not get news comments.", e);
	}
	return commentList;
    }

}
