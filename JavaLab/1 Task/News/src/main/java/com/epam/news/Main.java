package com.epam.news;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IManagerService;

public class Main {

    public static void main(String[] args) throws ServiceException, DaoException {

	@SuppressWarnings("resource")
	ClassPathXmlApplicationContext cn = new ClassPathXmlApplicationContext("applicationContextBeans.xml");

	IManagerService service = (IManagerService) cn.getBean("managerService");

	SearchCriteria sc = null;

	System.out.println(service.countNewsBySearchCriteria(sc));

	// service.deleteNews(13L);

    }

}
