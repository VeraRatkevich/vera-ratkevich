package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DaoException;
/**
 * Provides methods to work with a table of authors
 * 
 * @author Vera_Ratkevich
 */
public interface IAuthorDao extends IDao<Author> {
	/**
	 * Creates binding author to news
	 *
	 * @param idNews
	 *            news identifier
	 * @param idAuthor
	 *            author identifier
	 * @throws DaoException
	 */
	void attachNewsAuthor(Long idNews, Long idAuthor) throws DaoException;

	/**
	 * Gets author of news
	 *
	 * @param idNews
	 *            news identifier
	 * @return author object
	 * @throws DaoException
	 */
	Author getAuthorByNewsId(Long idNews) throws DaoException;

	/**
	 * Deletes binding author to news
	 *
	 * @param idNews
	 *            news identifier
	 * @throws DaoException
	 */
	void deleteBindingNewsAuthor(Long idNews) throws DaoException;
	
	/**
	 * Gets all authors from database
	 *
	 * @return list of objects
	 * @throws DaoException
	 */
	List<Author> getAllAuthors() throws DaoException;
}
