package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;

public class Author implements Serializable {

    private static final long serialVersionUID = 1702851194012015874L;

    private Long authorId;
    private String name;
    private Date expired;

    public Author() {
    }

    public Long getId() {
	return authorId;
    }

    public void setId(Long id) {
	this.authorId = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Date getExpired() {
	return expired;
    }

    public void setExpired(Date expired) {
	this.expired = expired;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Author other = (Author) obj;
	if (authorId != other.authorId)
	    return false;
	if (name == null) {
	    if (other.name != null)
		return false;
	} else if (!name.equals(other.name)) {
	    return false;
	}
	if (expired == null) {
	    if (other.expired != null)
		return false;
	} else if (!expired.equals(other.expired)) {
	    return false;
	}

	return true;
    }

    @Override
    public int hashCode() {
	return (int) (31 * authorId + ((name == null) ? 0 : name.hashCode()) + ((expired == null) ? 0 : expired
	        .hashCode()));

    }

    @Override
    public String toString() {

	StringBuilder builder = new StringBuilder();
	builder.append("Author [").append(" id: ").append(authorId).append(" name ").append(name).append(" expired ")
	        .append(expired).append("]");

	return builder.toString();

    }
}
