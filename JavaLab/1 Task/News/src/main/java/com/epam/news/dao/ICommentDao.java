package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DaoException;

/**
 * Provides methods to work with a table of comments
 * 
 * @author Vera_Ratkevich
 */
public interface ICommentDao extends IDao<Comment> {
	/**
	 * Reads all comments of news
	 *
	 * @param idNews
	 *            news identifier
	 * @return list of comments objects
	 * @throws DaoException
	 */
	List<Comment> getCommentsByNewsId(Long idNews) throws DaoException;

	/**
	 * Deletes comments of news
	 *
	 * @param idNews
	 *            news identifier
	 * @throws DaoException
	 */
	void deleteCommentsByNewsId(Long idNews) throws DaoException;
}
