package com.epam.news.service.impl;

import org.apache.log4j.Logger;

import com.epam.news.dao.IAuthorDao;
import com.epam.news.entity.Author;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;

public class AuthorServiceImpl implements IAuthorService {

    private static final Logger logger = Logger.getLogger(AuthorServiceImpl.class);

    private IAuthorDao authorDao;

    public void setDao(IAuthorDao dao) {
	this.authorDao = dao;
    }

    @Override
    public void attachNewsAuthor(Long idNews, Long idAuthor) throws ServiceException {
	try {
	    authorDao.attachNewsAuthor(idNews, idAuthor);
	} catch (DaoException e) {
	    logger.error("Can not attache author to news.", e);
	    throw new ServiceException("Can not attache author to news.", e);
	}
    }

    @Override
    public Author getAuthorByNewsId(Long idNews) throws ServiceException {
	Author author = null;
	try {
	    author = authorDao.getAuthorByNewsId(idNews);
	} catch (DaoException e) {
	    logger.error("Can not get author by news id = " + idNews, e);
	    throw new ServiceException("Can not get author by newsId = " + idNews, e);
	}
	return author;
    }

    @Override
    public Long addAuthor(Author author) throws ServiceException {
	Long idAuthor = null;
	try {
	    idAuthor = authorDao.add(author);
	} catch (DaoException e) {
	    logger.error("Can not add author.", e);
	    throw new ServiceException("Can not add author.", e);
	}
	return idAuthor;
    }

    @Override
    public void deleteBindingNewsAuthor(Long idNews) throws ServiceException {
	try {
	    authorDao.deleteBindingNewsAuthor(idNews);
	} catch (DaoException e) {
	    logger.error("Can not delete news author by newsId = " + idNews, e);
	    throw new ServiceException("Can not delete news author by newsId = " + idNews, e);
	}

    }

}
