package com.epam.news.dao.impl;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.IAuthorDao;
import com.epam.news.dao.util.DbResourceCloser;
import com.epam.news.entity.Author;
import com.epam.news.exception.DaoException;

public class AuthorDaoImpl implements IAuthorDao {

    private static final String SQL_INSERT_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHORS na (na.NEWS_ID, na.AUTHOR_ID) VALUES (?,?)";
    private static final String SQL_INSERT_AUTHOR = "INSERT INTO AUTHORS a (a.AUTHOR_ID, a.AUTHOR_NAME) VALUES (AUTHORS_SEQUENCE.nextval,?)";
    private static final String SQL_GET_BY_ID = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME, a.EXPIRED FROM AUTHORS a WHERE AUTHOR_ID=?";
    private static final String SQL_GET_ALL = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME FROM AUTHORS a WHERE EXPIRED IS NULL";
    private static final String SQL_DELETE_AUTHOR = "UPDATE AUTHORS a SET a.EXPIRED = CURRENT_TIMESTAMP WHERE AUTHOR_ID = ?";
    private static final String SQL_GET_BY_NEWS_ID = "SELECT a.AUTHOR_ID, a.AUTHOR_NAME, a.EXPIRED FROM AUTHORS a INNER JOIN NEWS_AUTHORS na ON a.AUTHOR_ID = na.AUTHOR_ID WHERE NEWS_ID = ?";
    private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHORS a SET a.AUTHOR_NAME=? WHERE a.AUTHOR_ID=?";
    private static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHORS na WHERE na.NEWS_ID = ?";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    @Override
    public Long add(Author author) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet generatedKeys = null;
	Long id = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_INSERT_AUTHOR, new String[] { "AUTHOR_ID" });
	    pst.setString(1, author.getName());
	    pst.executeUpdate();
	    generatedKeys = pst.getGeneratedKeys();
	    if (null != generatedKeys && generatedKeys.next()) {
		id = generatedKeys.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to add author.", e);
	} finally {
	    DbResourceCloser.close(generatedKeys, pst, con, dataSource);
	}
	return id;
    }

    @Override
    public Author get(Long authorId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Author author = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_BY_ID);
	    pst.setLong(1, authorId);
	    rs = pst.executeQuery();
	    if (rs.next()) {
		author = parseResultSet(rs);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get author by authorId = " + authorId, e);
	} finally {
	    DbResourceCloser.close(rs, pst, con, dataSource);
	}
	return author;
    }

    @Override
    public List<Author> getAllAuthors() throws DaoException {
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	Author author;
	List<Author> authorList = new ArrayList<Author>();
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    st = con.createStatement();
	    rs = st.executeQuery(SQL_GET_ALL);
	    while (rs.next()) {
		author = parseResultSet(rs);
		authorList.add(author);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get all authors.", e);
	} finally {
	    DbResourceCloser.close(rs, st, con, dataSource);
	}
	return authorList;
    }

    @Override
    public void edit(Author author) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_UPDATE_AUTHOR);
	    pst.setString(1, (author.getName()));

	    Date date = null;
	    date = author.getExpired();
	    if (date != null) {
		pst.setTimestamp(2, new Timestamp(date.getTime()));
	    } else {
		pst.setNull(2, java.sql.Types.TIMESTAMP);
	    }
	    pst.setLong(2, (author.getId()));
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to edit author with id = " + author.getId(), e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    @Override
    public void delete(Long authorId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_AUTHOR);
	    pst.setLong(1, authorId);
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete author by authorId = " + authorId, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    @Override
    public void deleteBindingNewsAuthor(Long idNews) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_NEWS_AUTHOR);
	    pst.setLong(1, idNews);
	    pst.execute();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete binding news to author by newsId = " + idNews, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    @Override
    public Author getAuthorByNewsId(Long idNews) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Author author = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_BY_NEWS_ID);
	    pst.setLong(1, idNews);
	    rs = pst.executeQuery();
	    while (rs.next()) {
		author = parseResultSet(rs);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get author by newsId = " + idNews, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
	return author;
    }

    @Override
    public void attachNewsAuthor(Long idNews, Long idAuthor) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_INSERT_NEWS_AUTHORS);
	    pst.setLong(1, idNews);
	    pst.setLong(2, idAuthor);
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to attach news to author by newsId = " + idNews + " and authorId = " + idAuthor, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    private Author parseResultSet(ResultSet rs) throws SQLException {
	Author author = new Author();
	author.setId(rs.getLong("AUTHOR_ID"));
	author.setName(rs.getString("AUTHOR_NAME"));
	return author;
    }
}
