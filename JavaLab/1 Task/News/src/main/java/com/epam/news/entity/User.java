package com.epam.news.entity;

import java.io.Serializable;
import java.util.List;

import com.epam.news.entity.constant.UserRoleEnum;

public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 848606398007331840L;

	private Long userId;
	private String name;
	private String login;
	private String password;
	private List<UserRoleEnum> userRoleList;

	public User() {
	}

	public Long getId() {
		return userId;
	}

	public void setId(Long id) {
		this.userId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<UserRoleEnum> getUserRoleEnum() {
		return userRoleList;
	}

	public void setPassword(List<UserRoleEnum> userRoleList) {
		this.userRoleList = userRoleList;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userId != other.userId)
			return false;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}

		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login)) {
			return false;
		}

		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (userRoleList == null) {
			if (other.userRoleList != null)
				return false;
		} else if (!userRoleList.equals(other.userRoleList)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * userId + ((name == null) ? 0 : name.hashCode())
				+ ((login == null) ? 0 : login.hashCode()) + ((password == null) ? 0
					: password.hashCode())
				+ ((userRoleList == null) ? 0 : userRoleList.hashCode())	
				
				
				);

	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Tag [").append(" id: ").append(userId).append(" name ")
				.append(name).append(" login ").append(login)
				.append(" password ").append(password)
				.append(" user roles ").append(userRoleList).append("]");

		return builder.toString();

	}
}
