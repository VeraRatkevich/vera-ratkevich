package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.ICommentDao;
import com.epam.news.dao.util.DbResourceCloser;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DaoException;

public class CommentDaoImpl implements ICommentDao {

    private static final String SQL_INSERT_COMMENT = "INSERT INTO COMMENTS c (c.COMMENT_ID, c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE) VALUES (COMMENTS_SEQUENCE.nextval,?,?,?)";
    private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS c WHERE c.COMMENT_ID = ?";
    private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID = "DELETE FROM COMMENTS c WHERE c.NEWS_ID = ?";
    private static final String SQL_GET_BY_ID = "SELECT c.COMMENT_ID, c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c WHERE COMMENT_ID=?";
    private static final String SQL_GET_ALL_BY_NEWS_ID = "SELECT c.COMMENT_ID, c.NEWS_ID, c.COMMENT_TEXT, c.CREATION_DATE FROM COMMENTS c WHERE NEWS_ID = ?";
    private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS c SET c.NEWS_ID = ?, c.COMMENT_TEXT = ?, c.CREATION_DATE = ? WHERE c.COMMENT_ID = ?";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    public Long add(Comment comment) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet generatedKeys = null;
	Long i = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_INSERT_COMMENT, new String[] { "COMMENT_ID" });
	    pst.setLong(1, (comment.getIdNews()));
	    pst.setString(2, (comment.getText()));
	    Timestamp creationDate = new Timestamp(comment.getCreationDate().getTime());
	    pst.setTimestamp(3, creationDate);
	    pst.executeUpdate();
	    generatedKeys = pst.getGeneratedKeys();
	    if (null != generatedKeys && generatedKeys.next()) {
		i = generatedKeys.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to add comment.", e);
	} finally {
	    DbResourceCloser.close(generatedKeys, pst, con, dataSource);
	}
	return i;
    }

    public Comment get(Long commentId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Comment comment = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_BY_ID);
	    pst.setLong(1, commentId);
	    rs = pst.executeQuery();
	    if (rs.next()) {
		comment = parseResultSet(rs);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get comment by commentId = " + commentId, e);
	} finally {
	    DbResourceCloser.close(rs, pst, con, dataSource);
	}
	return comment;
    }

    public void edit(Comment comment) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_UPDATE_COMMENT);
	    pst.setLong(1, (comment.getIdNews()));
	    pst.setString(2, (comment.getText()));
	    pst.setTimestamp(3, (new Timestamp(comment.getCreationDate().getTime())));
	    pst.setLong(4, comment.getId());
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to edit comment with commentId = " + comment.getId(), e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public void delete(Long commentId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_COMMENT);
	    pst.setLong(1, commentId);
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete comment by commentId = " + commentId, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public List<Comment> getCommentsByNewsId(Long newsId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Comment comment;
	List<Comment> commentList = new ArrayList<Comment>();
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_ALL_BY_NEWS_ID);
	    pst.setLong(1, newsId);
	    rs = pst.executeQuery();
	    while (rs.next()) {
		comment = parseResultSet(rs);
		commentList.add(comment);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get comment by newsId = " + newsId, e);
	} finally {
	    DbResourceCloser.close(rs, pst, con, dataSource);
	}
	return commentList;
    }

    public void deleteCommentsByNewsId(Long idNews) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_COMMENTS_BY_NEWS_ID);
	    pst.setLong(1, idNews);
	    pst.execute();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete comments by newsId = " + idNews, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    private Comment parseResultSet(ResultSet rs) throws SQLException {
	Comment comment = new Comment();
	comment.setId(rs.getLong("COMMENT_ID"));
	comment.setIdNews(rs.getLong("NEWS_ID"));
	comment.setText(rs.getString("COMMENT_TEXT"));
	Timestamp crDate = rs.getTimestamp("CREATION_DATE");
	comment.setCreationDate(new Date(crDate.getTime()));
	return comment;
    }
}
