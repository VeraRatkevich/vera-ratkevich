package com.epam.news.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class News implements Serializable {

    private static final long serialVersionUID = -8974116866376103791L;

    private Long newsId;
    private String title;
    private String shortText;
    private String fullText;
    private Date creationDate;
    private LocalDate modificationDate;

    public News() {
    }

    public Long getId() {
	return newsId;
    }

    public void setId(Long id) {
	this.newsId = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getShortText() {
	return shortText;
    }

    public void setShortText(String shortText) {
	this.shortText = shortText;
    }

    public String getFullText() {
	return fullText;
    }

    public void setFullText(String fullText) {
	this.fullText = fullText;
    }

    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDate getModificationDate() {
	return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
	this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	News other = (News) obj;
	if (newsId != other.newsId)
	    return false;

	if (title == null) {
	    if (other.title != null)
		return false;
	} else if (!title.equals(other.title)) {
	    return false;
	}

	if (shortText == null) {
	    if (other.shortText != null)
		return false;
	} else if (!shortText.equals(other.shortText)) {
	    return false;
	}

	if (fullText == null) {
	    if (other.fullText != null)
		return false;
	} else if (!fullText.equals(other.fullText))
	    return false;

	if (creationDate == null) {
	    if (other.creationDate != null)
		return false;
	} else if (!creationDate.equals(other.creationDate))
	    return false;

	if (modificationDate == null) {
	    if (other.modificationDate != null)
		return false;
	} else if (!modificationDate.equals(other.modificationDate))
	    return false;

	return true;
    }

    @Override
    public int hashCode() {
	return (int) (31 * newsId + ((title == null) ? 0 : title.hashCode())
	        + ((shortText == null) ? 0 : shortText.hashCode()) + ((fullText == null) ? 0 : fullText.hashCode())
	        + ((creationDate == null) ? 0 : creationDate.hashCode()) + ((modificationDate == null) ? 0
	            : modificationDate.hashCode()));

    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("News [").append(" id: ").append(newsId).append(" title ").append(title).append(" short text ")
	        .append(shortText).append(" full text ").append(fullText).append(" creation date ")
	        .append(creationDate).append(" modification date ").append(modificationDate).append("]");
	return builder.toString();
    }
}
