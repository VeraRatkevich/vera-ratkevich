package com.epam.news.service;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;

/**
 * Provides author-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface IAuthorService {
	/**
	 * Creates binding author to news
	 * 
	 * @param idNews
	 *            news identifier
	 * @param idAuthor
	 *            author identifier
	 * @throws ServiceException
	 */
	void attachNewsAuthor(Long idNews, Long idAuthor) throws ServiceException;

	/**
	 * Adds author
	 *
	 * @param author
	 *            author object
	 * @return author identifier
	 * @throws ServiceException
	 */
	Long addAuthor(Author author) throws ServiceException;

	/**
	 * Gets author of news
	 *
	 * @param idNews
	 *            news identifier
	 * @return author object
	 * @throws ServiceException
	 */
	Author getAuthorByNewsId(Long idNews) throws ServiceException;

	/**
	 * Deletes binding author to news
	 *
	 * @param idNews
	 *            news identifier
	 * @throws ServiceException
	 */
	void deleteBindingNewsAuthor(Long idNews) throws ServiceException;
}
