package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DaoException;

/**
 * Provides methods to work with a table of tags
 * 
 * @author Vera_Ratkevich
 */
public interface ITagDao extends IDao<Tag> {

    /**
     * Creates binding tags to news
     *
     * @param idNews
     *            news identifier
     * @param tagIdList
     *            list of tag identifiers
     * @throws DaoException
     */
    void attachNewsTags(Long idNews, List<Long> tagIdList) throws DaoException;

    /**
     * Gets all tags of news
     *
     * @param idNews
     *            news identifiers
     * @return list of tags
     * @throws DaoException
     */
    List<Tag> getTagsByNewsId(Long idNews) throws DaoException;

    /**
     * Deletes binding tags to news
     *
     * @param idNews
     *            news identifier
     * @throws DaoException
     */
    void deleteBindingNewsTags(Long idNews) throws DaoException;

    /**
     * Gets all tags from database
     *
     * @return list of objects
     * @throws DaoException
     */
    List<Tag> getAllTags() throws DaoException;

}
