package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.ServiceException;

/**
 * Provides news-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface INewsService {
    /**
     * Adds news
     *
     * @param news
     *            news object
     * @return news identifier
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * Edits news
     *
     * @param news
     *            object
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * Deletes news
     *
     * @param idNews
     *            news identifier
     * @throws ServiceException
     */
    void deleteNews(Long idNews) throws ServiceException;

    /**
     * Gets single news
     *
     * @param idNews
     *            news identifier
     * @return news object
     * @throws ServiceException
     */
    News getNews(Long idNews) throws ServiceException;

    /**
     * Searches news by author and tags
     *
     * @param sc
     *            search criteria
     * @return list of news objects
     * @throws ServiceException
     */
    List<News> getNewsBySearchCriteria(SearchCriteria sc) throws ServiceException;

    /**
     * Counts number of all news
     * 
     * @param sc
     *            search criteria
     * @return number of news
     * @throws ServiceException 
     */
    int countNewsBySearchCriteria(SearchCriteria sc) throws ServiceException;
}
