package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.DaoException;

/**
 * Provides methods to work with a table of news
 * 
 * @author Vera_Ratkevich
 */
public interface INewsDao extends IDao<News> {
    /**
     * Searches news by author and tags
     *
     * @param sc
     *            search criteria
     * @return list of news objects
     * @throws DaoException
     */
    List<News> getNewsBySearchCriteria(SearchCriteria sc) throws DaoException;

    /**
     * Counts number of all news
     * @param sc TODO
     *
     * @return number of news
     * @throws DaoException
     */
    int countNewsBySearchCriteria(SearchCriteria sc) throws DaoException;
}
