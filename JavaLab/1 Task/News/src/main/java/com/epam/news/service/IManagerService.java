package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * Provides all operations with all objects
 * 
 * @author Vera_Ratkevich
 */
public interface IManagerService {
    /**
     * Adds full data about the news
     *
     * @param news
     *            news value object
     * @param tagIdList
     *            list of tag identifiers
     * @param authorId
     *            author identifier
     * @throws ServiceException
     */
    Long addNews(News news, List<Long> tagIdList, Long authorId) throws ServiceException;

    /**
     * Gets news and full data about it by author and tags
     *
     * @param sc
     *            search criteria
     * @return list of news objects
     * @throws ServiceException
     */
    List<NewsVO> getAllNews(SearchCriteria sc) throws ServiceException;

    /**
     * Gets single news with full data(author, tags, comments)
     *
     * @param idNews
     *            news identifier
     * @return newsVO news value object
     * @throws ServiceException
     */
    NewsVO getNews(Long idNews) throws ServiceException;

    /**
     * Edits news and full data about it
     *
     * @param news
     *            object
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * Deletes news and full data about it (author, tags, comments)
     *
     * @param newsId
     *            news identifier
     * @throws ServiceException
     */
    void deleteNews(Long idNews) throws ServiceException;

    /**
     * Add binding author to news
     * 
     * @param idNews
     *            news identifier
     * @param idAuthor
     *            author identifier
     * @throws ServiceException
     */
    void attachNewsAuthor(Long idNews, Long idAuthor) throws ServiceException;

    /**
     * Add binding tags to news
     * 
     * @param idNews
     *            news identifier
     * @param tagList
     *            list of tags
     * @throws ServiceException
     */
    void attachNewsTags(Long idNews, List<Long> tagIdList) throws ServiceException;

    /**
     * Adds tag
     *
     * @param tag
     *            tag object
     * @return tag identifier
     * @throws ServiceException
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * Adds author
     *
     * @param author
     *            author object
     * @return author identifier
     * @throws ServiceException
     */
    Long addAuthor(Author author) throws ServiceException;

    /**
     * Adds comment
     *
     * @param comment
     *            comment object
     * @throws ServiceException
     */
    void addComment(Comment comment) throws ServiceException;

    /**
     * Deletes comment
     *
     * @param idComment
     *            comment identifier
     * @throws ServiceException
     */
    void deleteComment(Long idNews, Long idComment) throws ServiceException;

    /**
     * Deletes comments of news
     *
     * @param idNews
     *            news identifier
     * @throws ServiceException
     */
    void deleteCommentsByNewsId(Long idNews) throws ServiceException;

    /**
     * Counts number of all news
     * 
     * @param sc
     *            search criteria
     * @return number of news
     * @throws ServiceException
     */
    int countNewsBySearchCriteria(SearchCriteria sc) throws ServiceException;
}
