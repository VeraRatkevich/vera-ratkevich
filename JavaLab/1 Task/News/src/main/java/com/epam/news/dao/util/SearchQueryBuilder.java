package com.epam.news.dao.util;

import java.util.List;

import com.epam.news.entity.SearchCriteria;

public class SearchQueryBuilder {

    private static final String SQL_SELECT_NEWS_DATA = "SELECT n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE, COUNT(c.COMMENT_ID) AS COUNT_COMMENTS FROM NEWS n LEFT JOIN COMMENTS c ON n.NEWS_ID = c.NEWS_ID ";
    private static final String SQL_COUNT_NEWS = "SELECT COUNT(n.NEWS_ID) AS COUNT_NEWS FROM NEWS n ";
    private static final String SQL_WITH_AUTHOR = "INNER JOIN NEWS_AUTHORS na ON na.NEWS_ID = n.NEWS_ID WHERE na.AUTHOR_ID = ";
    private static final String SQL_WITH_TAGS = " n.NEWS_ID IN(SELECT nt.NEWS_ID FROM NEWS_TAGS nt WHERE nt.TAG_ID IN ";
    private static final String SQL_GROUP_ORDER_BY = " GROUP BY n.NEWS_ID, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE ORDER BY n.MODIFICATION_DATE DESC, COUNT_COMMENTS DESC";

    private SearchQueryBuilder() {

    }

    public static String createSearhQuery(SearchCriteria sc) {
	StringBuilder sb = new StringBuilder(SQL_SELECT_NEWS_DATA);
	if (null != sc) {
	    sb.append(searchCriteriaSubquery(sc));
	}
	sb.append(SQL_GROUP_ORDER_BY);
	return sb.toString();
    }

    public static String createCountQuery(SearchCriteria sc) {
	StringBuilder sb = new StringBuilder(SQL_COUNT_NEWS);
	if (null != sc) {
	    sb.append(searchCriteriaSubquery(sc));
	}
	return sb.toString();
    }

    private static String searchCriteriaSubquery(SearchCriteria sc) {
	StringBuilder sb = new StringBuilder();
	Long authorId = sc.getAuthorId();
	List<Long> tagIdList = sc.getTagIdList();
	boolean isWithAuthor = false;

	if (null != authorId) { 
	    sb.append(SQL_WITH_AUTHOR);
	    sb.append(authorId);
	    isWithAuthor = true;
	}

	if ((null != tagIdList) && (tagIdList.size() > 0)) {
	    if (isWithAuthor) {
		sb.append(" OR");
	    }
	    sb.append(SQL_WITH_TAGS);
	    sb.append(numberToQueryString(tagIdList));
	    sb.append(")");
	}
	return sb.toString();
    }

    private static String numberToQueryString(List<Long> numberList) { 
	StringBuilder sb = new StringBuilder("(");

	for (Long number : numberList) {
	    sb.append(number); 
	    sb.append(",");
	}
	int sbLegth = sb.length();
	sb.replace(sbLegth - 1, sbLegth, ")");
	return sb.toString();

    }
}
