package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * Provides tag-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface ITagService {

	/**
	 * Adds tag
	 *
	 * @param tag
	 *            tag object
	 * @return tag identifier
	 * @throws ServiceException
	 */
	Long addTag(Tag tag) throws ServiceException;

	/**
	 * Adds binding tags to news
	 * 
	 * @param idNews
	 *            news identifier
	 * @param tagIdList
	 *            list of tag identifiers
	 * @throws ServiceException
	 */
	void attachNewsTags(Long idNews, List<Long> tagIdList) throws ServiceException;

	/**
	 * Gets all tags of news
	 *
	 * @param idNews
	 *            news identifier
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> getTagsByNewsId(Long idNews) throws ServiceException;

	/**
	 * Gets all tags
	 *
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> getAllTags() throws ServiceException;

	/**
	 * Deletes binding tags to news
	 *
	 * @param idNews
	 *            news identifier
	 */
	void deleteBindingNewsTags(Long idNews) throws ServiceException;
}
