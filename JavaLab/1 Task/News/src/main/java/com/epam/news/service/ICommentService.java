package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;

/**
 * Provides comment-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface ICommentService {
    /**
     * Deletes comment
     *
     * @param idComment
     *            comment identifier
     * @throws ServiceException
     */
    void deleteComment(Long idComment) throws ServiceException;

    /**
     * Deletes comments of news
     *
     * @param idNews
     *            news identifier
     * @throws ServiceException
     */
    void deleteCommentsByNewsId(Long idNews) throws ServiceException;

    /**
     * Adds comment
     *
     * @param comment
     *            comment object
     * @throws ServiceException
     */
    Long addComment(Comment comment) throws ServiceException;

    /**
     * Gets all comments of news
     *
     * @param idNews
     *            news identifier
     * @return list of comments objects
     * @throws ServiceException
     */
    List<Comment> getCommentsByNewsId(Long idNews) throws ServiceException;
}
