package com.epam.news.dao;

import com.epam.news.exception.DaoException;
/**
 * Common interface for all DAO
 * 
 * @author Vera_Ratkevich
 */
public interface IDao<T> {
    
	/**
	 * Creates object entry in database
	 *
	 * @param entity
	 *            object
	 * @return object identifier
	 * @throws DaoException
	 */
	Long add(T entity) throws DaoException;

	/**
	 * Gets object
	 *
	 * @param id
	 *            object identifier
	 * @return object
	 * @throws DaoException
	 */
	T get(Long id) throws DaoException;

	/**
	 * Updates object entry in database
	 *
	 * @param entity
	 *            object
	 * @throws DaoException
	 */
	void edit(T entity) throws DaoException;

	/**
	 * Deletes object entry from database
	 *
	 * @param id
	 *            object identifier
	 * @throws DaoException
	 */
	void delete(Long id) throws DaoException;

}
