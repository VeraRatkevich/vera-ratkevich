package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.news.dao.ITagDao;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ITagService;

public class TagServiceImpl implements ITagService {

    private static final Logger logger = Logger.getLogger(TagServiceImpl.class);

    private ITagDao tagDao;

    public void setDao(ITagDao dao) {
	this.tagDao = dao;
    }

    @Override
    public Long addTag(Tag tag) throws ServiceException {
	Long id = null;
	try {
	    id = tagDao.add(tag);
	} catch (DaoException e) {
	    logger.error("Can not add tag.", e);
	    throw new ServiceException("Can not add tag.", e);
	}
	return id;
    }

    @Override
    public void attachNewsTags(Long idNews, List<Long> tagIdList) throws ServiceException {
	try {
	    tagDao.attachNewsTags(idNews, tagIdList);
	} catch (DaoException e) {
	    logger.error("Can not attache tag to news.", e);
	    throw new ServiceException("Can not attache tag to news.", e);
	}
    }

    @Override
    public List<Tag> getTagsByNewsId(Long idNews) throws ServiceException {
	List<Tag> tagList = null;
	try {
	    tagList = tagDao.getTagsByNewsId(idNews);
	} catch (DaoException e) {
	    logger.error("Can not get tags by news id.", e);
	    throw new ServiceException("Can not get tags by news id.", e);
	}
	return tagList;
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
	List<Tag> tagList = null;
	try {
	    tagList = tagDao.getAllTags();
	} catch (DaoException e) {
	    logger.error("Can not get all tags.", e);
	    throw new ServiceException("Can not get all tags.", e);
	}
	return tagList;
    }

    @Override
    public void deleteBindingNewsTags(Long idNews) throws ServiceException {
	try {
	    tagDao.deleteBindingNewsTags(idNews);
	} catch (DaoException e) {
	    logger.error("Can not delete news tags.", e);
	    throw new ServiceException("Can not delete news tags.", e);
	}
    }
}
