package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

    private static final long serialVersionUID = -2978753253716007539L;

    private Long commentId;
    private Long idNews;
    private String text;
    private Date creationDate;

    public Comment() {
    }

    public Long getId() {
	return commentId;
    }

    public void setIdNews(Long idNews) {
	this.idNews = idNews;
    }

    public Long getIdNews() {
	return idNews;
    }

    public void setId(Long id) {
	this.commentId = id;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Comment other = (Comment) obj;
	if (commentId != other.commentId)
	    return false;

	if (text == null) {
	    if (other.text != null)
		return false;
	} else if (!text.equals(other.text)) {
	    return false;
	}

	if (creationDate == null) {
	    if (other.creationDate != null)
		return false;
	} else if (!creationDate.equals(other.creationDate)) {
	    return false;
	}

	return true;
    }

    @Override
    public int hashCode() {
	return (int) (31 * commentId + 5 * idNews + ((text == null) ? 0 : text.hashCode()) + ((creationDate == null) ? 0
	        : creationDate.hashCode()));

    }

    @Override
    public String toString() {

	StringBuilder builder = new StringBuilder();
	builder.append("Comment [").append(" id: ").append(commentId).append(" id news: ").append(idNews)
	        .append(" text ").append(text).append(" creation date ").append(creationDate).append("]");

	return builder.toString();

    }
}
