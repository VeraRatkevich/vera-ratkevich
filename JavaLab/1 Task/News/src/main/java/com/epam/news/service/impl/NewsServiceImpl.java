package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.news.dao.INewsDao;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.INewsService;

public class NewsServiceImpl implements INewsService {

    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);

    private INewsDao newsDao;

    public void setDao(INewsDao dao) {
	this.newsDao = dao;
    }
    
    @Override
    public Long addNews(News news) throws ServiceException {
	Long id = null;
	try {
	    id = newsDao.add(news);
	} catch (DaoException e) {
	    logger.error("Can not add news.", e);
	    throw new ServiceException("Can not add news.", e);
	}
	return id;
    }

    @Override
    public void editNews(News news) throws ServiceException {
	try {
	    newsDao.edit(news);
	} catch (DaoException e) {
	    logger.error("Can not edit news.", e);
	    throw new ServiceException("Can not edit news.", e);
	}
    }

    @Override
    public void deleteNews(Long idNews) throws ServiceException {
	try {
	    newsDao.delete(idNews);
	} catch (DaoException e) {
	    logger.error("Can not delete news.", e);
	    throw new ServiceException("Can not delete news.", e);
	}

    }

    @Override
    public News getNews(Long idNews) throws ServiceException {
	News news = null;
	try {
	    news = newsDao.get(idNews);
	} catch (DaoException e) {
	    logger.error("Can not get news.", e);
	    throw new ServiceException("Can not get news.", e);
	}
	return news;
    }

    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria sc) throws ServiceException {
	List<News> newsList = null;
	try {
	    newsList = newsDao.getNewsBySearchCriteria(sc);
	} catch (DaoException e) {
	    logger.error("Can not get news by search criteria.", e);
	    throw new ServiceException("Can not get news by search criteria.", e);
	}
	return newsList;
    }

    @Override
    public int countNewsBySearchCriteria(SearchCriteria sc) throws ServiceException {
	int countNews = 0;
	try {
	    countNews = newsDao.countNewsBySearchCriteria(sc);
	} catch (DaoException e) {
	    logger.error("Can not count the number of news.", e);
	    throw new ServiceException("Can not count the number of news.", e);
	}
	return countNews;
    }

}
