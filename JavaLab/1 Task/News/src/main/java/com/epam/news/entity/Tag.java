package com.epam.news.entity;

import java.io.Serializable;

public class Tag implements Serializable {

    private static final long serialVersionUID = -138557737808646067L;

    private Long tagId;
    private String name;

    public Tag() {
    }

    public Long getId() {
	return tagId;
    }

    public void setId(Long id) {
	this.tagId = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Tag other = (Tag) obj;
	if (tagId != other.tagId)
	    return false;

	if (name == null) {
	    if (other.name != null)
		return false;
	} else if (!name.equals(other.name)) {
	    return false;
	}

	return true;
    }

    @Override
    public int hashCode() {
	return (int) (31 * tagId + ((name == null) ? 0 : name.hashCode()));

    }

    @Override
    public String toString() {

	StringBuilder builder = new StringBuilder();
	builder.append("Tag [").append(" id: ").append(tagId).append(" name ").append(name).append("]");

	return builder.toString();

    }
}
