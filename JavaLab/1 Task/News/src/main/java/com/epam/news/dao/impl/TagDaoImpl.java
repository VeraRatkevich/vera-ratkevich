package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.ITagDao;
import com.epam.news.dao.util.DbResourceCloser;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DaoException;

public class TagDaoImpl implements ITagDao {

    private static final String SQL_INSERT_NEWS_TAGS = "INSERT INTO NEWS_TAGS nt (nt.NEWS_ID, nt.TAG_ID) VALUES (?,?)";
    private static final String SQL_INSERT_TAG = "INSERT INTO TAGS t (t.TAG_ID, t.TAG_NAME) VALUES (TAGS_SEQUENCE.nextval, ?)";
    private static final String SQL_GET_BY_ID = "SELECT t.TAG_ID, t.TAG_NAME FROM TAGS t WHERE t.TAG_ID=?";
    private static final String SQL_GET_ALL_BY_NEWS_ID = "SELECT t.TAG_ID, t.TAG_NAME FROM TAGS t INNER JOIN NEWS_TAGS nt ON t.TAG_ID= nt.TAG_ID WHERE nt.NEWS_ID=?";
    private static final String SQL_GET_ALL = "SELECT t.TAG_ID, t.TAG_NAME FROM TAGS t";
    private static final String SQL_DELETE_TAG = "DELETE FROM TAGS t WHERE t.TAG_ID = ?";
    private static final String SQL_UPDATE_TAG = "UPDATE TAGS t SET t.TAG_NAME=? WHERE t.TAG_ID=?";
    private static final String SQL_DELETE_NEWS_TAGS = "DELETE FROM NEWS_TAGS nt WHERE nt.NEWS_ID = ?";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    public Long add(Tag tag) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet generatedKeys = null;
	Long id = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_INSERT_TAG, new String[] { "TAG_ID" });
	    pst.setString(1, tag.getName());
	    pst.executeUpdate();

	    generatedKeys = pst.getGeneratedKeys();
	    if (null != generatedKeys && generatedKeys.next()) {
		id = generatedKeys.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to add tag.", e);
	} finally {
	    DbResourceCloser.close(generatedKeys, pst, con, dataSource);
	}
	return id;
    }

    public Tag get(Long tagId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Tag tag = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_BY_ID);
	    pst.setLong(1, tagId);
	    rs = pst.executeQuery();
	    if (rs.next()) {
		tag = parseResultSet(rs);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get tag by tagId = " + tagId, e);
	} finally {
	    DbResourceCloser.close(rs, pst, con, dataSource);
	}
	return tag;
    }

    public List<Tag> getAllTags() throws DaoException {
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	Tag tag;
	List<Tag> newsList = new ArrayList<Tag>();
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    st = con.createStatement();
	    rs = st.executeQuery(SQL_GET_ALL);
	    while (rs.next()) {
		tag = parseResultSet(rs);
		newsList.add(tag);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get all tags.", e);

	} finally {
	    DbResourceCloser.close(rs, st, con, dataSource);
	}
	return newsList;
    }

    public void edit(Tag tag) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_UPDATE_TAG);
	    pst.setString(1, (tag.getName()));
	    pst.setLong(2, (tag.getId()));
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to edit tag with tagId = " + tag.getId(), e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public void delete(Long tagId) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_TAG);
	    pst.setLong(1, tagId);
	    pst.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete tag by tagId = " + tagId, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public void deleteBindingNewsTags(Long idNews) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_DELETE_NEWS_TAGS);
	    pst.setLong(1, idNews);
	    pst.execute();
	} catch (SQLException e) {
	    throw new DaoException("Failed to delete binding news to tags by newsId = " + idNews, e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    public List<Tag> getTagsByNewsId(Long idNews) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	Tag tag;
	List<Tag> tagList = new ArrayList<Tag>();
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_GET_ALL_BY_NEWS_ID);
	    pst.setLong(1, idNews);
	    rs = pst.executeQuery();
	    while (rs.next()) {
		tag = parseResultSet(rs);
		tagList.add(tag);
	    }
	} catch (SQLException e) {
	    throw new DaoException("Failed to get tags by newsId = " + idNews, e);
	} finally {
	    DbResourceCloser.close(rs, pst, con, dataSource);
	}
	return tagList;
    }

    public void attachNewsTags(Long idNews, List<Long> tagIdList) throws DaoException {
	Connection con = null;
	PreparedStatement pst = null;
	try {
	    con = DataSourceUtils.getConnection(dataSource);
	    pst = con.prepareStatement(SQL_INSERT_NEWS_TAGS);
	    for (Long tagId : tagIdList) {
		pst.setLong(1, idNews);
		pst.setLong(2, tagId);
		pst.addBatch();
	    }
	    pst.executeBatch();
	} catch (SQLException e) {
	    throw new DaoException("Failed to attache news to tags.", e);
	} finally {
	    DbResourceCloser.close(pst, con, dataSource);
	}
    }

    private Tag parseResultSet(ResultSet rs) throws SQLException {
	Tag tag = new Tag();
	tag.setId(rs.getLong("TAG_ID"));
	tag.setName(rs.getString("TAG_NAME"));
	return tag;
    }
}
