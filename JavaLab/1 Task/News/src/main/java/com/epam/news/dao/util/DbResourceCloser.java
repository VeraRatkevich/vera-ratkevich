package com.epam.news.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class DbResourceCloser {

    private static final Logger logger = Logger.getLogger(DbResourceCloser.class);

    public static void close(ResultSet rs, Statement st, Connection con, DataSource ds) {
	if (rs != null) {
	    try {
		rs.close();
	    } catch (SQLException e) {
		logger.error("Can not close result set.", e);
	    }
	}
	if (st != null) {
	    try {
		st.close();
	    } catch (SQLException e) {
		logger.error("Can not close statement.", e);
	    }
	}
	DataSourceUtils.releaseConnection(con, ds);
    }

    public static void close(Statement st, Connection con, DataSource ds) {
	if (st != null) {
	    try {
		st.close();
	    } catch (SQLException e) {
		logger.error("Can not close statement.", e);
	    }
	}
	DataSourceUtils.releaseConnection(con, ds);
    }

}
