package com.epam.newsmanagement.util;

import java.util.LinkedList;
import java.util.List;

/*
 * Class is designed for pagination.
 * Storage class information on the number of pages and the current page.
 * 
 * @author RatkevichVera
 * @version 1.0
 */

public class PageBean {

    private List<Integer> pages;
    private Integer currentPage;

    public PageBean(int countPages) {
	pages = new LinkedList<Integer>();
	int counter = 1;
	while (counter <= countPages) {
	    pages.add(counter);
	    counter++;
	}
    }

    public List<Integer> getPages() {
	return pages;
    }

    public Integer getCurrentPage() {
	return currentPage;
    }

    public int getNumberOfPages() {
 	return pages.size();
     }
    
    public void setCurrentPage(Integer page) {
	int countPages = pages.size();
	if (page <= countPages) {
	    this.currentPage = page;
	} else {
	    this.currentPage = countPages;
	}
    }

}
