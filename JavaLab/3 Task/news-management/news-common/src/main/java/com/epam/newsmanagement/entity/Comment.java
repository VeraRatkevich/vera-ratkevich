package com.epam.newsmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

	private static final long serialVersionUID = -2978753253716007539L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "COMMENTS_SEQUENCE")
	@SequenceGenerator(name = "COMMENTS_SEQUENCE", sequenceName = "COMMENTS_SEQUENCE",allocationSize=1)
	@Column(name = "COMMENT_ID")
	private Long commentId;

	@Column(name = "NEWS_ID")
	private Long newsId;

	@Column(name = "COMMENT_TEXT")
	@Size(min = 1, max = 100, message = "{error.comment.text.size}")
	private String text;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	public Comment() {
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long id) {
		this.commentId = id;
	}

	public void setNewsId(Long idNews) {
		this.newsId = idNews;
	}

	public Long getNewsId() {
		return newsId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;

		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId)) {
			return false;
		}
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId)) {
			return false;
		}
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text)) {
			return false;
		}

		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (((commentId == null) ? 0 : commentId.hashCode() * 31) + ((newsId == null) ? 0 : newsId.hashCode() * 5) + ((text == null) ? 0 : text.hashCode()) + ((creationDate == null) ? 0
				: creationDate.hashCode()));

	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Comment [").append(" id: ").append(commentId).append(" id news: ").append(newsId).append(" text ").append(text).append(" creation date ").append(creationDate).append("]");

		return builder.toString();

	}
}
