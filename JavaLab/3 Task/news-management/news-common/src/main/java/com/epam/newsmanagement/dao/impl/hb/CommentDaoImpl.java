package com.epam.newsmanagement.dao.impl.hb;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class CommentDaoImpl implements ICommentDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long add(Comment comment) throws DaoException {
		Long commentId = (Long) sessionFactory.getCurrentSession().save(comment);
		return commentId;
	}

	@Override
	public Comment get(Long commentId) throws DaoException {
		Comment comment = (Comment) sessionFactory.getCurrentSession().get(Comment.class, commentId);
		return comment;
	}

	@Override
	public void edit(Comment comment) throws DaoException {
		sessionFactory.getCurrentSession().saveOrUpdate(comment);
	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Comment comment = new Comment();
		comment.setCommentId(commentId);
		sessionFactory.getCurrentSession().delete(comment);
	}

}
