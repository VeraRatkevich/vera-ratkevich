package com.epam.newsmanagement.entity.constant;

public enum UserRoleEnum {
	USER, ADMIN, AUTHOR
}