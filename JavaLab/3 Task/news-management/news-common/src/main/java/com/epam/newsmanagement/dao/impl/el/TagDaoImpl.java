package com.epam.newsmanagement.dao.impl.el;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class TagDaoImpl implements ITagDao {

	@PersistenceContext
	EntityManager em;

	@Override
	public Long add(Tag tag) throws DaoException {
		em.persist(tag);
		return tag.getTagId();
	}

	@Override
	public Tag get(Long tagId) throws DaoException {
		Tag tag = em.find(Tag.class, tagId);
		return tag;
	}

	@Override
	public void edit(Tag tag) throws DaoException {
		em.merge(tag);
	}

	@Override
	public void delete(Long tagId) throws DaoException {
		Tag tag = em.find(Tag.class, tagId);			
		em.remove(tag);
	}

	@Override
	public List<Tag> getAllTags() throws DaoException {
		CriteriaQuery<Tag> criteria = em.getCriteriaBuilder().createQuery(Tag.class);
		criteria.select(criteria.from(Tag.class));
		List<Tag> tagList = em.createQuery(criteria).getResultList();
		return tagList;
	}

}
