package com.epam.newsmanagement.entity;

import java.util.ArrayList;
import java.util.List;

public class SearchCriteria {

    private Long authorId;
    private List<Long> tagIdList;

    public SearchCriteria() {
	tagIdList = new ArrayList<Long>();
    }

    public Long getAuthorId() {
	return authorId;
    }

    public void setAuthorId(Long authorId) {
	this.authorId = authorId;
    }

    public List<Long> getTagIdList() {
	return tagIdList;
    }

    public void setTagIdList(List<Long> tagIdList) {
	this.tagIdList = tagIdList;
    }

    public void addTagId(long tagId) {
	tagIdList.add(tagId);
    }

    public void deleteTagId(long tagId) {
	tagIdList.remove(tagId);
    }

}
