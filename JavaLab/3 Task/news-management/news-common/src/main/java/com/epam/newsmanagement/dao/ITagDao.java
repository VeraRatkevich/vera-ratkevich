package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Provides methods to work with a table of tags
 * 
 * @author Vera_Ratkevich
 */
public interface ITagDao extends IDao<Tag> {

    /**
     * Gets all tags from database
     *
     * @return list of objects
     * @throws DaoException
     */
    List<Tag> getAllTags() throws DaoException;

}
