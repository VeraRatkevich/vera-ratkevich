package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides all operations with all objects
 * 
 * @author Vera_Ratkevich
 */
public interface IManagerService {
    /**
     * Adds full data about the news
     *
     * @param news
     *            news value object
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * Deletes tag
     *
     * @param tag
     *            tag object
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Gets news and full data about it by author and tags
     *
     * @param sc
     *            search criteria
     * @return list of news objects
     * @throws ServiceException
     */
    List<News> getAllNews(SearchCriteria sc, int startIndex, int endIndex) throws ServiceException;

    /**
     * Gets single news with full data(author, tags, comments)
     *
     * @param idNews
     *            news identifier
     * @return newsVO news value object
     * @throws ServiceException
     */
    News getNews(Long idNews) throws ServiceException;

    /**
     * Deletes news and full data about it (author, tags, comments)
     *
     * @param newsId
     *            news identifier
     * @throws ServiceException
     */
    void deleteNews(Long idNews) throws ServiceException;

    /**
     * Adds tag
     *
     * @param tag
     *            tag object
     * @return tag identifier
     * @throws ServiceException
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * Adds author
     *
     * @param author
     *            author object
     * @return author identifier
     * @throws ServiceException
     */
    Long addAuthor(Author author) throws ServiceException;

    /**
     * Adds comment
     *
     * @param comment
     *            comment object
     * @throws ServiceException
     */
    void addComment(Comment comment) throws ServiceException;

    /**
     * Deletes comment
     * 
     * @param idComment
     *            comment identifier
     *
     * @throws ServiceException
     */
    void deleteComment(Long idComment) throws ServiceException;

    /**
     * Counts number of all news
     * 
     * @param sc
     *            search criteria
     * @return number of news
     * @throws ServiceException
     */
    int countNews(SearchCriteria sc) throws ServiceException;

    /**
     * Gets all tags
     *
     * @return list of tag objects
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Gets all authors
     *
     * @return list of author objects
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Gets next and previous news identifiers
     * 
     * @param sc
     *            search criteria
     * @param currentNewsId
     *            current news identifier
     * 
     * @return list of next and previous news identifiers
     * @throws ServiceException
     */
    List<Long> getPrevNextNewsId(SearchCriteria sc, Long currentNewsId) throws ServiceException;

    /**
     * Deletes news by its identifiers
     *
     * @param newsIdList
     *            list of news identifiers
     * @throws ServiceException
     */
    void deleteListNews(List<Long> newsIdList) throws ServiceException;

    /**
     * Gets author
     *
     * @param authorId
     *            author identifier
     * @return author object
     * @throws ServiceException
     */
    Author getAuthor(Long authorId) throws ServiceException;


    /**
     * Edits news and full data about it
     *
     * @param news
     *            object
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * Gets tag by tag id
     *
     * @param tagId
     *            tag identifier
     * @return tag object
     * @throws ServiceException
     */
    Tag getTag(Long tagId) throws ServiceException;

    /**
     * Edits tag
     *
     * @param tag
     *            tag object
     * @throws ServiceException
     */
    void editTag(Tag tag) throws ServiceException;

    /**
     * Deletes tag
     *
     * @param author
     *            author object
     * @throws ServiceException
     */
    void deleteAuthor(Long authorId) throws ServiceException;

    /**
     * Edits tag
     *
     * @param author
     *            author object
     * @throws ServiceException
     */
    void editAuthor(Author author) throws ServiceException;
}
