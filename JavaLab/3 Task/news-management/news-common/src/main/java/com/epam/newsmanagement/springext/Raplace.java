package com.epam.newsmanagement.springext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Raplace {

	Class<?> otherClass();
	
}
