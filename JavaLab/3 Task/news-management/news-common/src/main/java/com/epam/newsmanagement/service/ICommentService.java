package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides comment-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface ICommentService {
    /**
     * Deletes comment
     *
     * @param idComment
     *            comment identifier
     * @throws ServiceException
     */
    void deleteComment(Long idComment) throws ServiceException;

    /**
     * Adds comment
     *
     * @param comment
     *            comment object
     * @throws ServiceException
     */
    Long addComment(Comment comment) throws ServiceException;
    
}
