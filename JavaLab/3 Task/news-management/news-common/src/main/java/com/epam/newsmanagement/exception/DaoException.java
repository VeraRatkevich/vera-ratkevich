package com.epam.newsmanagement.exception;



public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DaoException() {

		super();

	}
	
	public DaoException(String msg) {

		super(msg);

	}

	public DaoException(String msg, Exception e) {

		super(msg, e);

	}
	public DaoException(Exception e) {

		super(e);

	}
	
}
