package com.epam.newsmanagement.dao.impl.hb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.dao.util.NewsComparator;
import com.epam.newsmanagement.dao.util.SearchQueryBuilder;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class NewsDaoImpl implements INewsDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Long add(News news) throws DaoException {
		Long newsId = (Long) sessionFactory.getCurrentSession().save(news);
		return newsId;
	}

	@Override
	public News get(Long newsId) throws DaoException {
		News news = (News) sessionFactory.getCurrentSession().get(News.class, newsId);
		return news;
	}

	@Override
	public void edit(News news) throws DaoException {
		sessionFactory.getCurrentSession().saveOrUpdate(news);
	}

	@Override
	public void delete(Long newsId) throws DaoException {
		Session session = sessionFactory.getCurrentSession();	
		News news = (News) session.get(News.class, newsId);
		session.delete(news);	
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Override
	public List<News> getAllNews(SearchCriteria sc, int startIndex, int numberOnPage) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		Criteria idCriteria = session.createCriteria(News.class);
		Criteria resultCriteria = session.createCriteria(News.class);
		Disjunction disjunction = Restrictions.disjunction();
		
		if (sc != null) {
			Long authorId = sc.getAuthorId();
			List<Long> tagIdList = sc.getTagIdList();
			if (authorId != null) {
				idCriteria.createAlias("author", "author");
				disjunction.add(Restrictions.eq("author.authorId", authorId));
			}
			if (tagIdList != null && tagIdList.size() != 0) {
				idCriteria.createAlias("tags", "tag", JoinType.LEFT_OUTER_JOIN);
				disjunction.add(Restrictions.in("tag.tagId", tagIdList));
			}
		}
		idCriteria.add(disjunction);
		
		idCriteria.createAlias("comments", "comments", JoinType.LEFT_OUTER_JOIN);

		idCriteria.setProjection((Projections.distinct(Projections.projectionList().add(Projections.property("newsId"), "newsId").add(Projections.property("modificationDate"), "modificationDate")
				.add(Projections.groupProperty("modificationDate")).add(Projections.groupProperty("newsId")).add(Projections.count("comments").as("numberOfComments")))));
		idCriteria.addOrder(Property.forName("modificationDate").desc());
		idCriteria.addOrder(Order.desc("numberOfComments"));
		idCriteria.setFirstResult(startIndex - 1);
		idCriteria.setMaxResults(numberOnPage);

		List<Long> newsIdList = getNewsIdList(idCriteria.list());
		resultCriteria.add(Restrictions.in("newsId", newsIdList));
		resultCriteria.setResultTransformer(resultCriteria.DISTINCT_ROOT_ENTITY);

		List<News> newsList = resultCriteria.list();
		Collections.sort(newsList, NewsComparator.decending(NewsComparator.getComparator(NewsComparator.DATE_SORT, NewsComparator.COUNT_SORT)));
		return newsList;
	}

	private List<Long> getNewsIdList(List<Object[]> objectList) {
		List<Long> newsIdList = new ArrayList<Long>();
		Long newsId = null;
		for (Object[] detail : objectList) {
			newsId = (Long) detail[0];
			newsIdList.add(newsId);
		}
		return newsIdList;
	}

	@Override
	public int countNews(SearchCriteria sc) throws DaoException {
		Criteria resultCriteria = sessionFactory.getCurrentSession().createCriteria(News.class);
		Disjunction disjunction = Restrictions.disjunction();
		if (sc != null) {
			Long authorId = sc.getAuthorId();
			List<Long> tagIdList = sc.getTagIdList();

			if (authorId != null) {
				resultCriteria.createAlias("author", "author");
				disjunction.add(Restrictions.eq("author.authorId", authorId));
			}
			if (tagIdList != null && tagIdList.size() != 0) {
				resultCriteria.createAlias("tags", "tags", JoinType.LEFT_OUTER_JOIN);
				disjunction.add(Restrictions.in("tags.tagId", sc.getTagIdList()));
			}
		}
		resultCriteria.add(disjunction);
		Projection idCountProjection = Projections.countDistinct("newsId");
		resultCriteria.setProjection(idCountProjection);
		int totalResultCount = ((Long) resultCriteria.uniqueResult()).intValue();
		return totalResultCount;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public List<Long> getPrevNextNewsId(SearchCriteria sc, Long currentNewsId) throws DaoException {
		String nextPrevNewsIdsQuery = SearchQueryBuilder.createNextPrevNewsIdsQuery(sc, currentNewsId);
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(nextPrevNewsIdsQuery);
		List<Long> nextPrev = new ArrayList<Long>();
		List result = query.list();
		Iterator iterator = result.iterator();
		while (iterator.hasNext()) {
			Object[] row = (Object[]) iterator.next();
			for (int col = 0; col < row.length; col++) {
				Object newsIdObj = row[col];
				nextPrev.add(Long.valueOf(newsIdObj.toString()));
			}
		}		
		Collections.reverse(nextPrev);		
		return nextPrev;
	}
}
