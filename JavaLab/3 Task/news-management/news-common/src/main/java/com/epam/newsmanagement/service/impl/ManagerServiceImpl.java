package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.IManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;

public class ManagerServiceImpl implements IManagerService {

	private INewsService newsService;
	private ICommentService commentService;
	private IAuthorService authorService;
	private ITagService tagService;

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	@Override
	public Long addNews(News news) throws ServiceException {
		Long idNews = newsService.addNews(news);
		return idNews;
	}

	@Override
	public List<News> getAllNews(SearchCriteria sc, int startIndex, int endIndex) throws ServiceException {
		List<News> newsList = newsService.getAllNews(sc, startIndex, endIndex);
		return newsList;
	}

	@Override
	public News getNews(Long idNews) throws ServiceException {
		News news = null;
		news = newsService.getNews(idNews);
		return news;
	}

	@Override
	public void deleteNews(Long idNews) throws ServiceException {
		newsService.deleteNews(idNews);
	}

	@Override
	public Long addTag(Tag tag) throws ServiceException {
		return tagService.addTag(tag);
	}

	@Override
	public Long addAuthor(Author author) throws ServiceException {
		return authorService.addAuthor(author);
	}

	@Override
	public void addComment(Comment comment) throws ServiceException {
		commentService.addComment(comment);
	}

	@Override
	public void deleteComment(Long idComment) throws ServiceException {
		commentService.deleteComment(idComment);
	}

	@Override
	public int countNews(SearchCriteria sc) throws ServiceException {
		return newsService.countNews(sc);
	}

	@Override
	public List<Tag> getAllTags() throws ServiceException {
		return tagService.getAllTags();
	}

	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		return authorService.getAllAuthors();
	}

	@Override
	public Tag getTag(Long tagId) throws ServiceException {
		return tagService.getTag(tagId);
	}

	@Override
	public void deleteListNews(List<Long> newsIdList) throws ServiceException {
		for (Long newsId : newsIdList) {
			newsService.deleteNews(newsId);
		}
	}

	@Override
	public void editNews(News news) throws ServiceException {
		newsService.editNews(news);
	}

	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.deleteTag(tagId);
	}

	@Override
	public List<Long> getPrevNextNewsId(SearchCriteria sc, Long currentNewsId) throws ServiceException {
		List<Long> newsIdList = null;
		newsIdList = newsService.getPrevNextNewsId(sc, currentNewsId);
		return newsIdList;
	}

	@Override
	public void editTag(Tag tag) throws ServiceException {
		tagService.editTag(tag);
	}

	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		authorService.deleteAuthor(authorId);
	}

	@Override
	public void editAuthor(Author author) throws ServiceException {
		authorService.editAuthor(author);
	}

	@Override
	public Author getAuthor(Long authorId) throws ServiceException {
		return authorService.getAuthor(authorId);
	}

}
