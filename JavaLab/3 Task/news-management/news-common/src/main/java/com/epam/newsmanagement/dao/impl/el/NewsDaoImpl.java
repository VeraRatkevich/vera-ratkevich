package com.epam.newsmanagement.dao.impl.el;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;

@Transactional
public class NewsDaoImpl implements INewsDao {

	@PersistenceContext
	EntityManager em;

	@Override
	public Long add(News news) throws DaoException {
		em.persist(news);
		return news.getNewsId();
	}

	@Override
	public News get(Long newsId) throws DaoException {
		News news = em.find(News.class, newsId);
		return news;
	}

	@Override
	public void edit(News news) throws DaoException {
		em.merge(news);
	}

	@Override
	public void delete(Long newsId) throws DaoException {
		News news = em.find(News.class, newsId);
		em.remove(news);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<News> getAllNews(SearchCriteria sc, int startIndex, int numberOnPage) throws DaoException {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
		Root root = criteriaQuery.from(News.class);

		setSearchCriteria(sc, criteriaQuery, criteriaBuilder, root);

		Join commentJoin = root.join("comments", JoinType.LEFT);
		Expression<Long> expCount = criteriaBuilder.countDistinct(commentJoin);
		criteriaQuery.multiselect(root, expCount);
		criteriaQuery.groupBy(root);
		criteriaQuery.orderBy(criteriaBuilder.desc(root.get("modificationDate")), criteriaBuilder.desc(expCount));

		Query query = em.createQuery(criteriaQuery);
		query.setFirstResult(startIndex - 1);
		query.setMaxResults(numberOnPage);
		List<News> newsList = createNewsList(query);
		return newsList;
	}

	@Override
	public int countNews(SearchCriteria sc) throws DaoException {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<News> root = criteriaQuery.from(News.class);

		setSearchCriteria(sc, criteriaQuery, criteriaBuilder, root);

		criteriaQuery.select(criteriaBuilder.countDistinct(root.get("newsId")));
		Query query = em.createQuery(criteriaQuery);
		Long countNews = (Long) query.getSingleResult();
		return countNews.intValue();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Long> getPrevNextNewsId(SearchCriteria sc, Long currentNewsId) throws DaoException {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
		Root root = criteriaQuery.from(News.class);

		setSearchCriteria(sc, criteriaQuery, criteriaBuilder, root);

		Join commentJoin = root.join("comments", JoinType.LEFT);
		Expression<Long> expCount = criteriaBuilder.countDistinct(commentJoin);
		criteriaQuery.multiselect(root, expCount);
		criteriaQuery.groupBy(root);
		criteriaQuery.orderBy(criteriaBuilder.desc(root.get("modificationDate")), criteriaBuilder.desc(expCount));
		criteriaQuery.select(root.get("newsId"));
		Query query = em.createQuery(criteriaQuery);

		List<Long> newsIdList = query.getResultList();
		int currentIndex = newsIdList.indexOf(currentNewsId);
		Long nextNewsId = null;
		Long prevNewsId = null;
		int newsIdListSize = newsIdList.size();

		if (currentIndex + 1 < newsIdListSize) {
			prevNewsId = newsIdList.get(currentIndex + 1);
		}
		if (currentIndex > 0) {
			nextNewsId = newsIdList.get(currentIndex - 1);
		}

		List<Long> nextPrev = new ArrayList<Long>();
		nextPrev.add(prevNewsId);
		nextPrev.add(nextNewsId);
		return nextPrev;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setSearchCriteria(SearchCriteria sc, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Root root) {
		if (sc != null) {
			Long authorId = sc.getAuthorId();
			List<Long> tagIdList = sc.getTagIdList();

			if ((null != authorId) && (null != tagIdList) && (!tagIdList.isEmpty())) {
				Join authorJoin = root.join("author");
				Join tagJoin = root.join("tags", JoinType.LEFT);
				Expression<String> exp = tagJoin.get("tagId");
				Predicate orClause = criteriaBuilder.or(criteriaBuilder.equal(authorJoin.get("authorId"), sc.getAuthorId()), exp.in(tagIdList));
				criteriaQuery.where(orClause);
			} else if (authorId != null) {
				Join authorJoin = root.join("author", JoinType.LEFT);
				criteriaQuery.where(criteriaBuilder.equal(authorJoin.get("authorId"), authorId));
			} else if ((null != tagIdList) && (!tagIdList.isEmpty())) {
				Join tagJoin = root.join("tags", JoinType.LEFT);
				Expression<String> exp = tagJoin.get("tagId");
				Predicate predicate = exp.in(tagIdList);
				criteriaQuery.where(predicate);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private List<News> createNewsList(Query query) {
		List<News> newsList = new ArrayList<News>();
		List<Object[]> newsParameters = query.getResultList();
		Iterator<Object[]> iterator = newsParameters.iterator();
		while (iterator.hasNext()) {
			newsList.add((News) iterator.next()[0]);
		}
		return newsList;
	}
}