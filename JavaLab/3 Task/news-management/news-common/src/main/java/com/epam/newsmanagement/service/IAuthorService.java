package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides author-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface IAuthorService {

    /**
     * Deletes tag
     *
     * @param author
     *            author object
     * @throws ServiceException
     */
    void deleteAuthor(Long authorId) throws ServiceException;

    /**
     * Edits tag
     *
     * @param author
     *            author object
     * @throws ServiceException
     */
    void editAuthor(Author author) throws ServiceException;

    /**
     * Adds author
     *
     * @param author
     *            author object
     * @return author identifier
     * @throws ServiceException
     */
    Long addAuthor(Author author) throws ServiceException;

    /**
     * Gets author
     *
     * @param authorId
     *            author identifier
     * @return author object
     * @throws ServiceException
     */
    Author getAuthor(Long authorId) throws ServiceException;

    /**
     * Gets all authors
     *
     * @return list of author object
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

}
