package com.epam.newsmanagement.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "NEWS")
public class News implements Serializable {

	private static final long serialVersionUID = -8974116866376103791L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "NEWS_SEQUENCE")
	@SequenceGenerator(name = "NEWS_SEQUENCE", sequenceName = "NEWS_SEQUENCE",allocationSize=1)
	@Column(name = "NEWS_ID")
	private Long newsId;

	@Column(name = "TITLE")
	@Size(min = 1, max = 30, message = "{error.news.title.size}")
	private String title;

	@Column(name = "SHORT_TEXT")
	@Size(min = 2, max = 100, message = "{error.news.shorttext.size}")
	private String shortText;

	@Column(name = "FULL_TEXT")
	@Size(min = 2, max = 2000, message = "{error.news.fulltext.size}")
	private String fullText;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE")
	@NotNull(message = "{error.date.empty}")
	@DateTimeFormat(pattern = "${date.format}")
	private Date creationDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFICATION_DATE")
	@NotNull(message = "{error.date.empty}")
	@DateTimeFormat(pattern = "${date.format}")
	private Date modificationDate;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "TAG_ID") })
	private Set<Tag> tags = new HashSet<Tag>();

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "NEWS_ID", nullable = true, insertable = false, updatable = false)
	private Set<Comment> comments = new HashSet<Comment>();

	@NotNull(message = "{label.author.empty}")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = { @JoinColumn(name = "news_ID") }, inverseJoinColumns = { @JoinColumn(name = "author_ID") })
	private Author author;

	@Version
	private Long version;

	public News() {
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long id) {
		this.newsId = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public int getCommentSize() {
		return comments.size();
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;

		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId)) {
			return false;
		}

		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title)) {
			return false;
		}

		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText)) {
			return false;
		}

		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;

		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;

		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (((newsId == null) ? 0 : newsId.hashCode() * 31) + ((title == null) ? 0 : title.hashCode()) + ((shortText == null) ? 0 : shortText.hashCode())
				+ ((fullText == null) ? 0 : fullText.hashCode()) + ((creationDate == null) ? 0 : creationDate.hashCode()) + ((modificationDate == null) ? 0 : modificationDate.hashCode()));

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("News [").append(" id: ").append(newsId).append(" title ").append(title).append(" short text ").append(shortText).append(" full text ").append(fullText)
				.append(" creation date ").append(creationDate).append(" modification date ").append(modificationDate).append("]");
		return builder.toString();
	}
}
