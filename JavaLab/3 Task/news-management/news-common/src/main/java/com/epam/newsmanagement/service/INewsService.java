package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides news-related operations
 * 
 * @author Vera_Ratkevich
 */
public interface INewsService {
	/**
	 * Adds news
	 *
	 * @param news
	 *            news object
	 * @return news identifier
	 * @throws ServiceException
	 */
	Long addNews(News news) throws ServiceException;

	/**
	 * Edits news
	 *
	 * @param news
	 *            object
	 * @throws ServiceException
	 */
	void editNews(News news) throws ServiceException;

	/**
	 * Deletes news
	 *
	 * @param idNews
	 *            news identifier
	 * @throws ServiceException
	 */
	void deleteNews(Long idNews) throws ServiceException;

	/**
	 * Gets single news
	 *
	 * @param idNews
	 *            news identifier
	 * @return news object
	 * @throws ServiceException
	 */
	
	News getNews(Long idNews) throws ServiceException;

	/**
	 * Searches news by author and tags
	 *
	 * @param sc
	 *            search criteria
	 * @return list of news objects
	 * @throws ServiceException
	 */
	List<News> getAllNews(SearchCriteria sc, int startIndex, int endIndex) throws ServiceException;

	/**
	 * Gets next and previous news identifiers
	 * 
	 * @param sc
	 *            search criteria
	 * @param currentNewsId
	 *            current news identifier
	 * 
	 * @return list of next and previous news identifiers
	 * @throws ServiceException
	 */
	List<Long> getPrevNextNewsId(SearchCriteria sc, Long currentNewsId) throws ServiceException;

	/**
	 * Counts number of all news
	 * 
	 * @param sc
	 *            search criteria
	 * @return number of news
	 * @throws ServiceException
	 */
	int countNews(SearchCriteria sc) throws ServiceException;
}
