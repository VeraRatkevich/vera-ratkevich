package com.epam.newsmanagement.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "AUTHORS")
public class Author implements Serializable {

	private static final long serialVersionUID = 1702851194012015874L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "AUTHORS_SEQUENCE")
	@SequenceGenerator(name = "AUTHORS_SEQUENCE", sequenceName = "AUTHORS_SEQUENCE",allocationSize=1)
	@Column(name = "AUTHOR_ID")
	private Long authorId;

	@Column(name = "AUTHOR_NAME")
	@Size(min = 2, max = 30, message = "{error.author.name.size}")
	private String name;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRED")
	private Date expired;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_AUTHORS",joinColumns={@JoinColumn(name = "AUTHOR_ID")}, 
	inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
	private Set<News> newsSet  = new HashSet<News>();
	
	public Author() {
	}

//	public void addNews(News news){
//		newsSet.add(news);
//	}
		
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long id) {
		this.authorId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	public Set<News> getNewsSet() {
		return newsSet;
	}

	public void setNewsSet(Set<News> newsSet) {
		this.newsSet = newsSet;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;

		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId)) {
			return false;
		}

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return (int) (((authorId == null) ? 0 : authorId.hashCode() * 31) + ((name == null) ? 0 : name.hashCode()) + ((expired == null) ? 0 : expired.hashCode()));

	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Author [").append(" id: ").append(authorId).append(" name ").append(name).append(" expired ").append(expired).append("]");

		return builder.toString();

	}
}
