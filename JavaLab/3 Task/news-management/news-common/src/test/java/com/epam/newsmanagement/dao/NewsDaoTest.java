package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.EntityCreator;

@DataSet
@SpringApplicationContext("eclipseLink.cfg.test.xml")
//@SpringApplicationContext("hibernate.cfg.test.xml")
public class NewsDaoTest extends UnitilsJUnit4 {

	@SpringBean("newsDao")
	private INewsDao newsDao;

	@Test
	public void testGetAll() throws DaoException {
		int expectedCountNews = 5;
		SearchCriteria sc = null;
		List<News> news = newsDao.getAllNews(sc, 1, 5);
		int actualCountNews = news.size();
		assertEquals(expectedCountNews, actualCountNews);
	}

	@Test
	public void testShowNewsBySearchCriteria() throws DaoException {
		List<Long> tagList = new ArrayList<Long>();
		tagList.add(11L);
		tagList.add(12L);

		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(11L);
		sc.setTagIdList(tagList);

		List<News> news = newsDao.getAllNews(sc, 1, 5);

		assertEquals(2, news.size());
	}

	@Test
	public void testCountAllNews() throws DaoException {
		int expectedCountNews = 5;
		int actualCountNews = newsDao.countNews(null);
		assertEquals(expectedCountNews, actualCountNews);
	}

	@Test
	public void testDelete() throws DaoException {
		newsDao.delete(15L);
		News news = newsDao.get(15L);
		assertEquals(null, news);
	}

	@Test
	public void testRead() throws DaoException {
		News actualNews = newsDao.get(13L);
		String title = actualNews.getTitle();
		assertEquals("title13", title);
	}

	@Test
	public void testCreate() throws DaoException {
		Date date = new Date();
		News expectedNews = EntityCreator.createNews(0L, "title", "short text", "full text", date, date, 0L);
		Long newsId = newsDao.add(expectedNews);
		expectedNews.setNewsId(newsId);
		News actualNews = newsDao.get(newsId);
		compareNewsData(expectedNews, actualNews);
	}

	@Test
	public void testUpdate() throws DaoException {
		Long newsId = 14L;
		Date date = new Date();
		News expectedNews = EntityCreator.createNews(newsId, "title 14141414", "short text 14", "full text 14", date, date, 0L);
		newsDao.edit(expectedNews);
		News actualNews = newsDao.get(newsId);
		compareNewsData(expectedNews, actualNews);
	}

	private void compareNewsData(News expectedNews, News actualNews) {
		assertEquals(expectedNews.getNewsId(), actualNews.getNewsId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}

}