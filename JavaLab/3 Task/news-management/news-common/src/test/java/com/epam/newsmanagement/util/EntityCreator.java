package com.epam.newsmanagement.util;

import java.util.Date;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;

public class EntityCreator {
	
	public static Author createAuthor(Long authorId, String name) {
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setName(name);
		return author;
	}
	public static Tag createTag(Long tagId, String name) {
		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setName(name);
		return tag;
	}
	public static Comment createComment(Long commentId, String commentText,
			Date creationDate, Long newsId) {
		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);
		return comment;
	}
	public static News createNews(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate, Long version) {
        News news = new News();
        news.setNewsId(id);
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);
        news.setVersion(version);
        return news;
    }
}
