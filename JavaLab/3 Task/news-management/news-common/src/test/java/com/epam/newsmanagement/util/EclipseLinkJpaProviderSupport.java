package com.epam.newsmanagement.util;

import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.unitils.orm.jpa.util.JpaProviderSupport;

import javax.persistence.EntityManager;
import javax.persistence.spi.PersistenceProvider;

/**
 * Implementation of {@link JpaProviderSupport} for Oracle EclipseLink JPA
 *
 */
public class EclipseLinkJpaProviderSupport implements JpaProviderSupport {


    public void assertMappingWithDatabaseConsistent(EntityManager entityManager, Object configurationObject) {
        throw new UnsupportedOperationException("The method assertMappingWithDatabaseConsistent is not implemented for toplink");
    }


    public Object getProviderSpecificConfigurationObject(
            PersistenceProvider persistenceProvider) {
        return null;
    }


    public JpaVendorAdapter getSpringJpaVendorAdaptor() {
        return new EclipseLinkJpaVendorAdapter();
    }


    public LoadTimeWeaver getLoadTimeWeaver() {
        return new InstrumentationLoadTimeWeaver();
    }

}