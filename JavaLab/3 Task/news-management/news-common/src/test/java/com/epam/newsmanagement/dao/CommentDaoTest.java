package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.util.EntityCreator;

@DataSet
@SpringApplicationContext("eclipseLink.cfg.test.xml")
//@SpringApplicationContext("hibernate.cfg.test.xml")
public class CommentDaoTest extends UnitilsJUnit4 {

	@SpringBean("commentDao")
	private ICommentDao commentDao;

	@Test
	public void testAddComment() throws DaoException {
		Comment expectedComment = EntityCreator.createComment(0L, "wow", new Date(), 21L);
		Long idComment = commentDao.add(expectedComment);
		expectedComment.setCommentId(idComment);
		Comment actualComment = commentDao.get(idComment);
		compareCommentData(expectedComment, actualComment);
	}

	@Test
	public void testGetComment() throws DaoException {
		String expectedCommentText = "comment text 1";
		Comment actualComment = commentDao.get(1L);
		String actualCommentText = actualComment.getText();
		assertEquals(expectedCommentText, actualCommentText);
	}

	@Test
	public void testDeleteComment() throws DaoException {
		Long commentId = 4L;
		commentDao.delete(commentId);
		Comment actualComment = commentDao.get(commentId);
		assertEquals(null, actualComment);
	}

	@Test
	public void testEditComment() throws DaoException {
		Long commentId = 4L;
		Comment commentExpected = EntityCreator.createComment(commentId, "wow", new Date(), 21L);
		commentDao.edit(commentExpected);
		Comment commentActual = commentDao.get(commentId);
		compareCommentData(commentExpected, commentActual);
	}

	private void compareCommentData(Comment expectedComment, Comment actualComment) {
		assertEquals(expectedComment.getCommentId(), actualComment.getCommentId());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		assertEquals(expectedComment.getText(), actualComment.getText());
	}
}
