package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import com.epam.newsmanagement.util.EntityCreator;

public class NewsServiceTest {

	@Mock
	private INewsDao newsDao;
	private NewsServiceImpl newsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		newsService = new NewsServiceImpl();
		newsService.setDao(newsDao);
	}

	@Test
	public void addNewsSuccess() throws DaoException, ServiceException {
		News news = createNews();
		when(newsDao.add(news)).thenReturn(1L);
		assertEquals(news.getNewsId(), newsService.addNews(news));
	}

	@Test
	public void editNewsSuccess() throws DaoException, ServiceException {
		News news = createNews();
		newsService.editNews(news);
		verify(newsDao, times(1)).edit(news);
	}

	@Test
	public void deleteSuccess() throws ServiceException, DaoException {
		long idNews = 1;
		newsService.deleteNews(idNews);
		verify(newsDao, times(1)).delete(idNews);
	}

	@Test(expected = ServiceException.class)
	public void deleteFail() throws DaoException, ServiceException {
		doThrow(DaoException.class).when(newsDao).delete(-1L);
		newsService.deleteNews(-1L);
	}

	@Test
	public void getNewsSuccess() throws DaoException, ServiceException {
		News news = createNews();
		when(newsDao.get(1L)).thenReturn(news);
		News returnedNews = newsService.getNews(news.getNewsId());
		verify(newsDao, times(1)).get(1L);
		assertEquals(news, returnedNews);
	}

	@Test(expected = ServiceException.class)
	public void getNewsFail() throws DaoException, ServiceException {
		doThrow(DaoException.class).when(newsDao).get(-1L);
		newsService.getNews(-1L);
	}

	@Test
	public void searchNewsSuccess() throws DaoException, ServiceException {
		SearchCriteria sc = new SearchCriteria();
		newsService.getAllNews(sc, 1, 5);
		verify(newsDao, times(1)).getAllNews(sc, 1, 5);
	}

	private News createNews() {
		Date date = new Date();
		return EntityCreator.createNews(1L, "title", "shortText", "fullText", date, date, 0L);

	}

}
