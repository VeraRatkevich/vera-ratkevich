package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagServiceImpl;
import com.epam.newsmanagement.util.EntityCreator;

public class TagServiceTest {
	@Mock
	private ITagDao tagDao;
	private TagServiceImpl tagService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		tagService = new TagServiceImpl();
		tagService.setDao(tagDao);
	}

	@Test
	public void addTagSuccess() throws DaoException, ServiceException {
		Tag tag = EntityCreator.createTag(1L, "tag 1");
		when(tagDao.add(tag)).thenReturn(1L);
		assertEquals(tag.getTagId(), tagService.addTag(tag));
	}

	@Test
	public void getAllTagsSuccess() throws DaoException, ServiceException {
		tagService.getAllTags();
		verify(tagDao, times(1)).getAllTags();
	}

}
