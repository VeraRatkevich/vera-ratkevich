package com.epam.newsmanagement.service;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.ManagerServiceImpl;

public class MainNewsServiceTest {

	@Mock
	private INewsService newsService;
	@Mock
	private ITagService tagService;
	@Mock
	private IAuthorService authorService;
	@Mock
	private ICommentService commentService;

	private ManagerServiceImpl mainNewsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mainNewsService = new ManagerServiceImpl();
		mainNewsService.setNewsService(newsService);
		mainNewsService.setTagService(tagService);
		mainNewsService.setAuthorService(authorService);
		mainNewsService.setCommentService(commentService);
	}

	@Test
	public void addNewsSuccess() throws ServiceException {
		News news = new News();
		Long newsId = 1L;
		doReturn(newsId).when(newsService).addNews(news);
		mainNewsService.addNews(news);
		verify(newsService, times(1)).addNews(news);
	}
}
