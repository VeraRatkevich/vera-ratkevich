package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IManagerService;

@Controller
@RequestMapping("viewnews")
public class ViewNewsController {

	@Autowired
	private IManagerService managerService;

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	public String back(HttpSession session, Model model) throws ServiceException {
		return "redirect:/newslist";
	}

	@RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
	public String goToViewNews(@PathVariable("newsId") long newsId, Model model, HttpSession session) throws ServiceException {
		SearchCriteria sc = getSearchCriteria(session);
		setCurrentNextPrevNews(newsId, sc, model);
		model.addAttribute("comment", new Comment());
		return "view";
	}

	@RequestMapping(value = "/{newsId}", method = RequestMethod.POST)
	public String addComents(@Valid @ModelAttribute("comment") Comment comment, BindingResult bindingResult, @PathVariable("newsId") long newsId, RedirectAttributes attr, HttpSession session,
			Model model) throws ServiceException {
		if (bindingResult.hasErrors()) {
			SearchCriteria sc = getSearchCriteria(session);
			setCurrentNextPrevNews(newsId, sc, model);
			return "view";
		} else {
			comment.setCreationDate(new Date());
			comment.setNewsId(newsId);
			managerService.addComment(comment);
			return "redirect:/viewnews/" + newsId;
		}

	}

	private void setCurrentNextPrevNews(Long currentNewsId, SearchCriteria sc, Model model) throws ServiceException {
		List<Long> prevNextNewsIds = managerService.getPrevNextNewsId(sc, currentNewsId);
		model.addAttribute("prevNewsId", prevNextNewsIds.get(0));
		model.addAttribute("nextNewsId", prevNextNewsIds.get(1));

		News news = managerService.getNews(currentNewsId);
		model.addAttribute("news", news);
	}

	private SearchCriteria getSearchCriteria(HttpSession session) {
		Long authorId = (Long) session.getAttribute("filterAuthor");
		@SuppressWarnings("unchecked")
		List<Long> tagIdList = (List<Long>) session.getAttribute("filterTag");

		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(authorId);
		sc.setTagIdList(tagIdList);

		return sc;
	}
}
