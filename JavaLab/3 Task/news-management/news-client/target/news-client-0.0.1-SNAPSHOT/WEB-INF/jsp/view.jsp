<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<tiles:insertDefinition name="SitedefaultTemplate">
<tiles:putAttribute name="title">${fullNews.news.title}"</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="back-botton">
			<a href="back"><spring:message code="label.back" /></a>
		</div>
		<div class="view-news">
			<div class="tags-comments-edit">
				<div class="left">
					<strong><c:out value="${fullNews.news.title}" /> &nbsp;&nbsp; (<spring:message code="label.by" /> ${fullNews.author.name})</strong>
				</div>
				<div>
					<fmt:parseDate value="${fullNews.news.modificationDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
					<fmt:formatDate value="${parsedDate}" var="formatedDate" type="date" pattern="dd/MM/yyyy" />
					<c:out value="${formatedDate}" />
				</div>
			</div>
			<div class="fulltext">

				<c:out value="${fullNews.news.fullText}" />
			</div>
			<br /> <br />
			<div class="comments-heap">
				<c:forEach items="${fullNews.commentList}" var="comment">
					<fmt:parseDate value="${comment.creationDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
					<fmt:formatDate value="${parsedDate}" var="formatedDate" type="date" pattern="dd/MM/yyyy" />
					<c:out value="${formatedDate}" />
					<br />
					<div class="comments">
						<c:out value="${comment.text}" />
					</div>
					<br />
				</c:forEach>
				<br /> <br />
				<sf:form method="POST" modelAttribute="comment" action="${fullNews.news.newsId}">
					<sf:textarea path="text" class="comment-text" />
					<br />
					<div class="error-message">
						<sf:errors path="text" />
					</div>
					
					<br />
					<sf:button>
						<spring:message code="label.postcomment" />
					</sf:button>
				</sf:form>
			</div>
		</div>

		<div class="back-previous">
			<c:if test="${prevNewsId != 0}">
				<a href="${prevNewsId}"><spring:message code="label.previous" /></a>
			</c:if>
		</div>
		<div class="back-next">
			<c:if test="${nextNewsId != 0}">
				<a href="${nextNewsId}"><spring:message code="label.next" /></a>
			</c:if>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>