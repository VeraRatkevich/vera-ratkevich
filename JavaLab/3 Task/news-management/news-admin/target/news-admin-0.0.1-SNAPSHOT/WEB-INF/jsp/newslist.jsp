<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<script src="<c:url value="/resources/js/checkbox.js" />"></script>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="top-filter">
				<table>
					<tr>
						<td>
							<form name="filter" method="GET" action="newslist/filter">
								<table>
									<tr>
										<td><select name="author">
												<option value=""><spring:message code="label.select.author" /></option>

												<c:forEach items="${authors}" var="author">
													<option ${filterAuthor == author.authorId ? 'selected' : ''} value="${author.authorId}">${author.name}</option>
												</c:forEach>

										</select></td>
										<td>
											<div class="multiselect">
												<div class="selectBox" onclick="showCheckboxes()">
													<select>
														<option><spring:message code="label.select.tag" /></option>
													</select>
													<div class="overSelect"></div>
												</div>
												<div id="checkboxes">
													<c:forEach var="tag" items="${tags}">
														<label for="one"><input type="checkbox" name="tag" ${filterTag.contains(tag.tagId) ? 'checked' : ''} value="${tag.tagId}" />${tag.name}</label>
													</c:forEach>
												</div>
											</div>

										</td>
										<td>
											<button type="submit" value="">
												<spring:message code="label.button.filter" />
											</button>
										<td>
									</tr>
								</table>
							</form>
						</td>
						<td>
							<form name="reset" method="GET" action="newslist/reset">
								<button type="submit" value="">
									<spring:message code="label.button.reset" />
								</button>
							</form>
						</td>
					</tr>
				</table>
			</div>

			<form name="newslist" method="GET" action="newslist/removenews">
				<c:forEach items="${newslist}" var="fullnews">
					<div class="list-news">
						<div class="tags-comments-edit">
							<div class="left">
								<a href="viewnews/${fullnews.news.newsId}"><strong class="title"><c:out value="${fullnews.news.title}" /> (<spring:message
											code="label.by" /> ${fullnews.author.name})</strong></a>
							</div>
							<fmt:message key="date.format" var="dateFormat" />
							<fmt:parseDate value="${fullnews.news.modificationDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
							<fmt:formatDate value="${parsedDate}" var="formatedDate" type="date" pattern="${dateFormat}" />
							<c:out value="${formatedDate}" />
						</div>
						<div class="shorttext">
							<c:out value="${fullnews.news.shortText}" />
						</div>
						<div class="tags-comments-edit">

							<c:forEach items="${fullnews.tagList}" var="tags">
								<a href="newslist/filter/${tags.tagId}">${tags.name}</a>
								<c:if test="${tags.name != null}">
									,
								</c:if>
							</c:forEach>
							<spring:message code="label.comments" />
							(${fullnews.commentList.size()}) <a href="newseditor/${fullnews.news.newsId}"><spring:message code="label.edit" /></a> <input type="checkbox"
								name="removal" value="${fullnews.news.newsId}">
						</div>
					</div>
					<br />
					<br />
				</c:forEach>
				<br />
				<c:if test="${!newslist.isEmpty()}">
					<div class="tags-comments-edit">
						<button type="submit" name="buttonDelete" onclick="return confirm('<spring:message code="label.delete.news.comfirm"/>')">
							<spring:message code="label.delete" />
						</button>
					</div>
				</c:if>

				<br /> <br />
			</form>
			<form name="newslist" method="GET" action="newslist">
				<c:if test="${pageManager.getNumberOfPages() > 1}">
					<div class="page-counter">
						<c:forEach items="${pageManager.pages}" var="page">
							<button type="submit" ${page == pageManager.currentPage ? 'class="button-current"' : ''} name="page" value="${page}">${page}</button>
						</c:forEach>
					</div>
				</c:if>
			</form>

		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>