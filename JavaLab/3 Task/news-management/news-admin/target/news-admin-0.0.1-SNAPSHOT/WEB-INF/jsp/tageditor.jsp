<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<c:forEach var="tag" items="${tags}">
			<div>
				<div class="author-field">
					<spring:message code="label.tag" />
					:
				</div>
				<c:choose>
					<c:when test="${tag.tagId == tagForUpdate.tagId}">
						<div class="tag-field-name">
							<sf:form method="POST" action="${tag.tagId}" modelAttribute="tagForUpdate">
								<sf:hidden path="tagId"/>
								<sf:input path="name" placeholder="${tag.name}" type="text" />
								<input class="btn-update" type="submit" value="<spring:message code="label.tag.update"/>">
								<br />
								<sf:errors path="name" class="error-message" />
							</sf:form>
						</div>
						<div class="author-field-delete">
							<a href="delete/${tag.tagId}"><spring:message code="label.tag.delete" /></a>
						</div>
						<div class="author-field-delete">
							<a href="/news-admin/tageditor"><spring:message code="label.tag.cancel" /></a>
						</div>
					</c:when>
					<c:otherwise>
						<div class="tag-field-name">
							<input name="id" class="author" type="text" disabled="disabled" required maxlength="30" value="${tag.name}"> <a
								href="/news-admin/tageditor/${tag.tagId}"><spring:message code="label.tag.edit" /></a>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</c:forEach>
		<br>
		<spring:message code="label.tag.add" />:
			<div class="author-field-name">
			<sf:form method="POST" modelAttribute="tagForAdd">
				<sf:input path="name" />
				<input class="btn-update" type="submit" value="<spring:message code="label.tag.save"/>" onclick="return confirm('<spring:message code="label.save.tag.comfirm"/>')">
				<br />
				<sf:errors path="name" class="error-message" />
			</sf:form>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>