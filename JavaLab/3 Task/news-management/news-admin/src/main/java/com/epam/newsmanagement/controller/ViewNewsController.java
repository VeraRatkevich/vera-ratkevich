package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IManagerService;

@Controller
@RequestMapping("/viewnews")
public class ViewNewsController {

	private IManagerService managerService;

	@Inject
	public void setManagerService(IManagerService managerService) {
		this.managerService = managerService;
	}

	@RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
	public String goToViewNews(@PathVariable("newsId") Long newsId, Model model, HttpSession session) throws ServiceException {
		SearchCriteria sc = getSearchCriteria(session);
		setCurrentNextPreviousNews(newsId, sc, model);
		model.addAttribute("comment", new Comment());
		return "viewnews";
	}

	@RequestMapping(value = "/{newsId}", method = RequestMethod.POST)
	public String addComent(@Valid @ModelAttribute("comment") Comment comment, BindingResult bindingResult, @PathVariable("newsId") Long newsId, HttpSession session, Model model)
			throws ServiceException {
		if (bindingResult.hasErrors()) {
			SearchCriteria sc = getSearchCriteria(session);
			setCurrentNextPreviousNews(newsId, sc, model);
			return "viewnews";
		} else {
			comment.setCreationDate(new Date());
			comment.setNewsId(newsId);
			managerService.addComment(comment);
			return "redirect:/viewnews/" + newsId;
		}
	}

	@RequestMapping(value = "/deletecomment/{commentId}", method = RequestMethod.GET)
	public String deleteComent(@PathVariable("commentId") Long commentId, @RequestParam(value = "newsId", required = true) Long newsId) throws ServiceException {
		managerService.deleteComment(commentId);
		return "redirect:/viewnews/" + newsId;
	}

	private void setCurrentNextPreviousNews(Long newsId, SearchCriteria sc, Model model) throws ServiceException {
		List<Long> prevNextNewsIds = managerService.getPrevNextNewsId(sc, newsId);
		model.addAttribute("prevNewsId", prevNextNewsIds.get(0));
		model.addAttribute("nextNewsId", prevNextNewsIds.get(1));

		News news = managerService.getNews(newsId);
		model.addAttribute("news", news);
	}

	private SearchCriteria getSearchCriteria(HttpSession session) {

		Long authorId = (Long) session.getAttribute("filterAuthor");
		@SuppressWarnings("unchecked")
		List<Long> tagIdList = (List<Long>) session.getAttribute("filterTag");

		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(authorId);
		sc.setTagIdList(tagIdList);

		return sc;
	}
}
