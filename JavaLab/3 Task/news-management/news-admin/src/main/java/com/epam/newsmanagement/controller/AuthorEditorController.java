package com.epam.newsmanagement.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IManagerService;

@Controller
@RequestMapping("/authoreditor")
public class AuthorEditorController {

	private IManagerService managerService;

	@Inject
	public void setManagerService(IManagerService managerService) {
		this.managerService = managerService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String authorEditor(Model model, HttpSession session) throws ServiceException {
		List<Author> authors = managerService.getAllAuthors();
		session.setAttribute("authors", authors);
		model.addAttribute("authorForAdd", new Author());
		return "authoreditor";
	}

	@RequestMapping(value = "/{authorId}", method = RequestMethod.GET)
	public String editAuthor(@PathVariable("authorId") Long authorId, Model model) throws ServiceException {
		Author authorForUpdate = managerService.getAuthor(authorId);
		model.addAttribute("authorForUpdate", authorForUpdate);
		model.addAttribute("authorForAdd", new Author());
		return "authoreditor";
	}

	@RequestMapping(value = "/{authorId}", method = RequestMethod.POST)
	public String updateAuthor(@Valid @ModelAttribute("authorForUpdate") Author authorForUpdate, BindingResult bindingResult, Model model) throws ServiceException {

		if (bindingResult.hasErrors()) {
			model.addAttribute("authorForAdd", new Author());
			return "authoreditor";
		} else {
			managerService.editAuthor(authorForUpdate);
			return "redirect:/authoreditor";
		}
	}

	@RequestMapping(value = "delete/{authorId}", method = RequestMethod.GET)
	public String deleteAuthor(@PathVariable("authorId") Long authorId, Model model) throws ServiceException {
		managerService.deleteAuthor(authorId);
		return "redirect:/authoreditor";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String saveAuthor(@Valid @ModelAttribute("authorForAdd") Author authorForAdd, BindingResult bindingResult, Model model) throws ServiceException {

		if (bindingResult.hasErrors()) {
			return "authoreditor";
		} else {
			managerService.addAuthor(authorForAdd);
			return "redirect:/authoreditor";
		}
	}

	@RequestMapping(value = "expire/{authorId}", method = RequestMethod.GET)
	public String expiredAuthor(@PathVariable("authorId") long authorId) throws ServiceException {
		managerService.deleteAuthor(authorId);
		return "redirect:/authoreditor";
	}

}
