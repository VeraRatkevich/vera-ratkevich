<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%-- <script src="//code.jquery.com/jquery-1.10.2.js"></script> --%>
<%-- <script src="<c:url value="/resources/js/checkbox.js" />"></script> --%>
<%-- <script src="<c:url value="/resources/js/tagToggle.js" />"></script> --%>
<%-- <c:import url="datepicker.jsp"></c:import> --%>
<div class="header">
	<div class="header-content">
		<div class="portal-name">
			<h2>
				<s:message code="label.portalname" />
			</h2>
		</div>
		<div class="vertical">
			<div class="logout" align="right">
				<sec:authorize access="isAuthenticated()">
					<div>
						<strong><s:message code="label.greeting" />, <sec:authentication property="principal.username" />! </strong>
					</div>
					<div>
						<form action="j_spring_security_logout">
							<button type="submit" value="">
								<s:message code="label.logout" />
							</button>
						</form>
					</div>
				</sec:authorize>
			</div>
			<div align="right">
				<a href="?lang=en"><s:message code="label.en" /></a> <a class="language" href="?lang=ru"><s:message code="label.ru" /></a>
			</div>
		</div>
	</div>
</div>