<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="SitedefaultTemplate">
	<tiles:putAttribute name="title">
		<spring:message code="label.portalname" />
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<br>
		<c:set var="stringTagId" value="tag" />
		<c:set var="stringEditId" value="edit" />
		<c:set var="stringInputId" value="input" />
		<c:forEach var="tag" items="${tags}">
			<c:set var="elementId" value="${stringTagId.concat(tag.tagId)}" />
			<c:set var="editElementId" value="${stringEditId.concat(tag.tagId)}" />
			<c:set var="inputElementId" value="${stringInputId.concat(tag.tagId)}" />
			<c:set var="editId" value="${elementId.concat(Edit)}"></c:set>
			<fmt:message key="error.tag.name.size" var="errTagMess" />
			<div>
				<div class="author-field">
					<spring:message code="label.tag" />
					:
				</div>
				<div class="author-field-name">
					<form method="POST" action="/news-admin/tageditor/update/${tag.tagId}">
						<input id="${inputElementId}" disabled="disabled" name="name" placeholder="${tag.name}" type="text" value="${tag.name}" required="required"
							minlength="1" maxlength="30" title="${errTagMess}" />
						<input id="${elementId}" class="btn-update" style="display: none" type="submit" value="<spring:message code="label.tag.update"/>">
						<br />
					</form>
				</div>
				<div id="${elementId}" class="author-field-delete" style="display: none">
					<a href="/news-admin/tageditor/delete/${tag.tagId}"><spring:message code="label.tag.delete" /></a>
				</div>
				<div id="${elementId}" class="author-field-delete" style="display: none">
					<a href="#" onclick="hideStuff(${editElementId}, ${inputElementId})"><spring:message code="label.tag.cancel" /></a>
				</div>
				<div class="author-field-name" id="${editElementId}">
					<a href="#" onclick="showStuff(${elementId},${editElementId},${inputElementId})"><spring:message code="label.tag.edit" /></a>
				</div>
			</div>
		</c:forEach>
		<br>
		<spring:message code="label.tag.add" />:
			<div class="author-field-name">
			<sf:form method="POST" modelAttribute="tagForAdd">
				<sf:input path="name" required="required" minlength="1" maxlength="30" title="${errTagMess}" />
				<input class="btn-update" type="submit" value="<spring:message code="label.tag.save"/>">
				<br />
				<sf:errors path="name" class="error-message" />
			</sf:form>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>