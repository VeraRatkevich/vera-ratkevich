<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="menu">
    <ul>
        <li>
            <s:url value="/newslist" var="listNewsUrl" htmlEscape="true"/>
            <a href="${listNewsUrl}"><s:message code="label.listnews"/></a>
        </li>
        <li>
            <s:url value="/newseditor" var="addNewsUrl" htmlEscape="true"/>
            <a href="${addNewsUrl}"><s:message code="label.addnews"/></a>
        </li>
        <li>
            <s:url value="/authoreditor" var="authorUrl" htmlEscape="true"/>
            <a href="${authorUrl}"><s:message code="label.authorpage"/></a>
        </li>
        <li>
            <s:url value="/tageditor" var="tagUrl" htmlEscape="true"/>
            <a href="${tagUrl}"><s:message code="label.tagpage"/></a>
        </li>
    </ul>
</div>
